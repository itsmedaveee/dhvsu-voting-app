<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElectionsStatus extends Model
{
    protected $fillable = [
        'name',
        'course_id',
        'department_id',
        'status'
    ];

 
}
