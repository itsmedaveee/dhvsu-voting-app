<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'code',
        'title'
    ];
    public function courses()
    {
        return $this->hasMany(Course::class);
    }   
    public function programs()
    {
        return $this->hasMany(Program::class);
    }   

    public function elections()
    {
        return $this->hasMany(Election::class);
    }

    public function electionDepartment()
    {
        return $this->hasMany(ElectionDepartment::class);
    }
    
    public function resultNominee()
    {
        return $this->hasManyThrough(ResultDepartment::class , Student::class)->latest();
    }
    public function result()
    {
        return $this->belongsTo(Result::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function nominees()
    {
        return $this->hasMany(Nominee::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function studentsWithCourse()
    {
        return $this->hasManyThrough(Student::class, Course::class);
    }



    public function users()
    {
        return $this->hasMany(User::class);
    }

    // public function campus()
    // {
    //     return $this->belongsTo(Campus::class);
    // }
}
