<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = [
    	'position_name',
    	'user_id',
    	// 'department_id',
    	// 'campus_id'
    ];


    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    // public function campus()
    // {
    // 	return $this->belongsTo(Campus::class);
    // }

    // public function department()
    // {
    // 	return $this->belongsTo(Department::class);
    // }

    public function nominees()
    {
    	return $this->hasMany(Nominee::class);
    }

    // public function election()
    // {
    //     return $this->belongsTo(Election::class);
    // }
}
