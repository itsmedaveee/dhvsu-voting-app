<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()   

    {
        view()->composer('layouts._header', function ($view) {
            $view->with('student', \App\Announcement::students());
            $view->with('notify', \App\ElectionsStatus::where('course_id', auth()->user()->course_id)
                                                        ->orWhere('department_id', auth()->user()->department_id)
                                                        ->get()); 
            $view->with('campuses', \App\CampusesNotification::all());
        });
    }
}
