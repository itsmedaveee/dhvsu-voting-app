<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Report;
use App\Course;
use App\Nominee;
use App\Pageant;
use App\Department;
use App\Election;
use App\ElectionDepartment;
use App\ElectionCourse;

class ReportsController extends Controller
{
    public function index(Request $request)
    {
    	// $dept = Department::query()->with('nominees');

        $nominees = Nominee::with('course', 'department', 'election', 'pageant', 'results')->has('department');

        if (request()->has('pageant')) {
            $nominees->whereHas('pageant', function ($q) {
                $q->where('event_name', request('pageant'));
            });
        } 

        if (request()->has('department')) {
            $nominees->whereHas('department', function ($q)  {
                 $q->where('title', request('department'));
            });

        } 

        if (request()->has('course')) {
            $nominees->whereHas('course', function ($q) {
                $q->where('title',  request('course'));
            });
        } 

        if (request()->has('election')) {
             $nominees->whereHas('election', function ($q) {
                $q->where('election_name', request('election'));
            });
        }

        if (request()->has('election_department')) {
             $nominees->whereHas('electionDepartment', function ($q) {
                $q->where('election_name', request('election_department'));
            });
        }

        if (request()->has('election_course')) {
             $nominees->whereHas('electionCourse', function ($q) {
                $q->where('election_name', request('election_course'));
            });
        }

        


        $departments = Department::pluck('title');
        $courses = Course::all();
        $elections = Election::all();
        $electionsDepartment = ElectionDepartment::all();
        $electionsCourse = ElectionCourse::all();
        $pageants = Pageant::all();

        $nominees = $nominees->get(); 
 

    	return view('reports.index', compact('courses', 'nominees', 'pageants', 'elections', 'departments', 'electionsDepartment', 'electionsCourse'));
    }

    public function nominees()
    {
     
        $departments = Department::pluck('title');
        $courses = Course::all();
        $elections = Election::all();
        $electionsDepartment = ElectionDepartment::all();
        $electionsCourse = ElectionCourse::all();
        $pageants = Pageant::all();
         $nominee = Nominee::first();


          $nominees = Nominee::with('course', 'department', 'election', 'pageant', 'results')->has('department');

      
        if (request()->has('pageant')) {
            $nominees->whereHas('pageant', function ($q) {
                $q->where('event_name', request('pageant'));
            });
        } 

        if (request()->has('department')) {
            $nominees->whereHas('department', function ($q)  {
                 $q->where('title', request('department'));
            });

        } 

        if (request()->has('course')) {
            $nominees->whereHas('course', function ($q) {
                $q->where('title',  request('course'));
            });
        } 

        if (request()->has('election')) {
             $nominees->whereHas('election', function ($q) {
                $q->where('election_name', request('election'));
            });
        }

        if (request()->has('election_department')) {
             $nominees->whereHas('electionDepartment', function ($q) {
                $q->where('election_name', request('election_department'));
            });
        }

        if (request()->has('election_course')) {
             $nominees->whereHas('electionCourse', function ($q) {
                $q->where('election_name', request('election_course'));
            });
        }

        


        $nominees = $nominees->get();

        $pdf        = \PDF::loadView('reports.pdf-list', [
            'nominees'    => $nominees,        
            'departments'    => $departments,        
            'elections'    => $elections,        
            'pageants'    => $pageants,        
            'courses'    => $courses,        
            'electionsDepartment'    => $electionsDepartment,        
            'electionsCourse'    => $electionsCourse,        
        ]);
        
        $pdf->setPaper('legal','landscape'); 
        return $pdf->stream(); 
    }

}
