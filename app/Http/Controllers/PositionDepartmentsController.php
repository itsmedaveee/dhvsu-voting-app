<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PositionDepartment;
class PositionDepartmentsController extends Controller
{
    public function index()
    {
    	$positions = PositionDepartment::all();
    	return view('events.position-department.index', compact('positions'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
    		'position_name'	=> 'required'
    	]);
    	$position = PositionDepartment::create([
    		'position_name'	=> request('position_name'),
    		'user_id'	=> auth()->user()->id
    	]);

    	return back()->with('success', 'Position has been added!');
    }

    public function edit(PositionDepartment $position)
    {
    	return view('events.position-department.edit', compact('position'));
    }

    public function update(PositionDepartment $position)
    {
    	$this->validate(request(), [
    		'position_name'	=> 'required',
    	]);
    	$position->update([
    		'position_name'	=> request('position_name'),
    		'user_id'	=> auth()->user()->id
    	]);

    	return redirect('/position-department')->with('info', 'Position has been updated!');
    }

    public function destroy(PositionDepartment $position)
    {
    	$position->delete();
    	return back()->with('error', 'Position has been removed!');
    }
}
