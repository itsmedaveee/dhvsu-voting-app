<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pageant;
use App\ResultPageant;
use App\Department;
class PageantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (auth()->user()->isCouncil()) {
            $pageants = Pageant::where('department_id', auth()->user()->department_id)->with(['nominees'])->get();
        }
        
        if (auth()->user()->isSuperAdmin()) {
            $pageants = Pageant::with(['nominees'])->get();
        }
       $departments = Department::all();
        return view('pageants.index', compact('pageants', 'departments'));
    }

    /**
     * Show the form for showing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Pageant $pageant)
    {
        $pageant->load(['nominees.resultsPageant', 'nominees' => function ($query) {
            $query->where('status', 'Approved');
        }]);

        $totalVoteUsers = ResultPageant::count();
        return view('pageants.show', compact('pageant', 'totalVoteUsers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
                'event_name'    => 'required',
                'department'    => 'required',
        ]);

       $department = Department::find(request('department'));
        Pageant::create([
            'user_id'        => auth()->user()->id,
            'department_id' => $department->id,
            'event_name' => request('event_name'),
            'status'    => 'On Going'
        ]);

        return back()->with('success', 'Event has been added!');
    }

    /**
     * Stop  the pageant event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function stopPageant(Pageant $pageantIds)
    {
          $pageantIds->update([

            'status'    => 'End Pageant',
        ]);

         return response()->json($pageantIds);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pageant $pageant)
    {
        return view('pageants.edit', compact('pageant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pageant $pageant)
    {
         $this->validate(request(), [
                'event_name'    => 'required'
        ]);
        $pageant->update([
            'event_name' => request('event_name'),
         
        ]);

        return redirect('/pageants')->with('info', 'Event has been updated!');
    }

    /**
     * Get the Pageant.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxPageant(Pageant $pageant)
    {
        return response()->json($pageant);
    }    

    /**
     * Store the Pageant Event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function startPageant(Pageant $pageantIds)
    {
        
         $pageantIds->update([

            'status'    => 'Starting now',
            'ended_time'   => \Carbon\Carbon::parse(request('ended_time')),
        ]);

        // $eventName          = Pageant::first();
        // $name               = $eventName->event_name; 
        \App\ElectionsStatus::create([
           'name'      =>  'The event election has started !',
            'department_id' => auth()->user()->department_id
        ]);  


         return response()->json($pageantIds);
         
    }    

    /**
     * Get the Pageant Event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPageant(Pageant $pageant)
    { 
         return response()->json($pageant);
    }    

    /**
     * Stop the Pageant Event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function stopPageantEvent(Pageant $pageantIds)
    {  
         $pageantIds->update([

            'status'    => 'End Events',
        ]);
        $eventName       = Pageant::first();
        $name               = $eventName->event_name; 
        \App\ElectionsStatus::create([
            'name'      =>  'The event election has ended !',
            'department_id' => auth()->user()->department_id
        ]);  


         return response()->json($pageantIds);
    }    

    /**
     * Print the Pageant Results.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function printResults(Pageant $pageant)
    {  
        $pageant->load('nominees.resultsPageant');
        return $pageant;
        $pdf = \PDF::loadView('pdf.print-results-pageant', [
            'pageant' => $pageant,
        ]);
       $pdf->setPaper('legal','landscape');
        return $pdf->stream();
    }

    public function showResults(Pageant $pageant)
    {
         $pageant->load('nominees.resultsPageant');
        $pdf = \PDF::loadView('pdf.print-results-pageant', [
            'pageant' => $pageant,
        ]);
         $pdf->setPaper('legal','portrait');
        return $pdf->stream();
    }
}
