<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Position;
use App\Nominee;
use App\Election;
use App\ElectionDepartment;
use App\ElectionCourse;
use App\ResultDepartment;
use App\Pageant;
use App\ResultCourse;

use DB;
class CandidatesController extends Controller
{
    public function index()
    { 

        $electionId = Election::latest()->first()->id;
        $timeElection = Election::latest()->first();
        // if (is_array($electionId) || is_object($electionId))
        // {
        //     foreach ($electionId as $election)
        //     {
        //         if ($election->status == 'On Going') {
        //             $election = Election::where('status', 'On Going')->latest()->first();
        //             return "On Going";
        //         } else if ($election->status == 'End Election') {
        //             $election = Election::where('status', 'End Election')->latest()->first();
        //              return "has Ended";
        //         } else if ($election->status == 'Starting now') {
        //             $election = Election::where('status', 'Starting now')->latest()->first();
        //             return "Starting now";
        //         }
        //     }
        // } 
   
      
        $nominees = Nominee::with(['position', 'results'])->where('status', 'Approved')->where('election_id', $electionId)->latest()->get();
        $grouped = $nominees->groupBy('position.position_name');
        $voted     = \App\Result::where('user_id', auth()->id())->pluck('nominee_id');
        return view('student-dashboard.candidates.index', [
                // 'elections' => $elections,
                'electionId' => $electionId,
                'voted'     => $voted,
                'grouped'     => $grouped,
                'timeElection'     => $timeElection,
        ]);
    }

    public function show(Nominee $nominee)
    {

        $nominee->load('position');
        $results = Nominee::with('results')->where('status', 'Approved')->withCount('results')->latest()->get();
        $datas = [];
        $resultNominee = [];
        $nomineeFullName = [];
        foreach ($results as $result) {
             $datas[] = $result->load('results');
        
             $resultNominee[] = $result->results_count;
             $nomineeFullName[]  = $result->firstname . '-' . $result->middlename . '-' . $result->lastname;
        }

        return view('student-dashboard.candidates.show', compact('nominee', 'results'))->with('resultNominee',json_encode($resultNominee,JSON_NUMERIC_CHECK))->with('nomineeFullName',json_encode($nomineeFullName,JSON_NUMERIC_CHECK));
    }

    public function getByCandidatesDepartment()
    {
        // $elections = ElectionDepartment::with(['nominees.positionDepartment', 'nominees' => function ($query) {
        //     $query->where('department_id', auth()->user()->department_id);
        // }])->where('status', 'Starting now')->get();


        //  $election = ElectionDepartment::latest()->first();

        //   if ($election->status == 'Starting now') {
        //     $election = ElectionDepartment::where('status', 'Starting now')->latest()->first();
        //     $election->status; 
        // }  else if ($election->status == 'End Election') {
        //     $election = ElectionDepartment::where('status', 'End Election')->latest()->first();
        //     $election->status; 
        // }
           $timeElection = ElectionDepartment::first();
            $electionId = ElectionDepartment::where([
                ['department_id', auth()->user()->department_id]
            ])->latest()->first()->id;

            if (is_array($electionId) || is_object($electionId))
            {
                foreach ($electionId as $election)
                {
                    if ($election->status == 'On Going') {
                        $election = ElectionDepartment::where('status', 'On Going')->latest()->first();
                        return "On Going";
                    } else if ($election->status == 'End Election') {
                        $election = ElectionDepartment::where('status', 'End Election')->latest()->first();
                         return "has Ended";
                    } else if ($election->status == 'Starting now') {
                        $election = ElectionDepartment::where('status', 'Starting now')->latest()->first();
                        return "Starting now";
                    }
                }
            } 

         $nominees = Nominee::with(['positionDepartment', 'election' => function ($query) {
            $query->where('status', 'Starting now');
        }])->where('status', 'Approved')->where('election_department_id', $electionId)->get();
        $grouped = $nominees->groupBy('positionDepartment.position_name');

         // $elections = ElectionDepartment::with('nominees.positionDepartment')->where('status', 'Starting now')->get();

        $voted     = ResultDepartment::where('user_id', auth()->id())->pluck('nominee_id');
        return view('student-dashboard.candidates-by-department.index', [
            'nominees'  => $nominees,  
            'electionId' => $electionId,
            'voted'     => $voted,
            'grouped'     => $grouped,
            'timeElection'     => $timeElection,

        ]);
    }

    public function candidatesCourses()
    {
        // $elections = ElectionCourse::with(['nominees.positionCourse', 'nominees'    => function ($query) {
        //     $query->where('course_id', auth()->user()->course_id);
        // }])->where('status', 'Starting now')->get();
        // $election = ElectionCourse::where('status', 'On Going')->latest()->first();
        // $election = ElectionCourse::latest()->first();
        // if ($election->status == 'Starting now') {
        //     $election = ElectionCourse::where('status', 'Starting now')->latest()->first();
        //     $election->status; 
        // }  else if ($election->status == 'End Election') {
        //     $election = ElectionCourse::where('status', 'End Election')->latest()->first();
        //     $election->status; 
        // }

     //   $electionId = ElectionCourse::latest()->first()->id;
         $timeElection = ElectionCourse::first();
         $electionId = ElectionCourse::where([
                ['course_id', auth()->user()->course_id]
            ])->latest()->first()->id;

        // if (is_array($electionId) || is_object($electionId))
        // {
        //     foreach ($electionId as $election)
        //     {
        //         if ($election->status == 'On Going') {
        //             $election = ElectionCourse::where('status', 'On Going')->latest()->first();
        //             return "On Going";
        //         } else if ($election->status == 'End Election') {
        //             $election = ElectionCourse::where('status', 'End Election')->latest()->first();
        //              return "has Ended";
        //         } else if ($election->status == 'Starting now') {
        //             $election = ElectionCourse::where('status', 'Starting now')->latest()->first();
        //             return "Starting now";
        //         }
        //     }
        // } 
          $nominees = Nominee::with(['positionCourse', 'election' => function ($query) {
            $query->where('status', 'Starting now');
        }])->where('status', 'Approved')->where('election_course_id', $electionId)->get();

        $grouped = $nominees->groupBy('positionCourse.position_name');

        $voted     = ResultCourse::where('user_id', auth()->id())->pluck('nominee_id');
        return view('student-dashboard.candidates-courses.index', compact( 'nominees', 'grouped' ,'voted' ,'electionId', 'timeElection'));
    }

    public function candidatesCourseShow(Nominee $nominee)
    {
        //$nominees = $nominee->load('resultsCourse')->loadCount('resultsCourse');
        $nominee->load('positionCourse');
        $results = Nominee::with('resultsCourse')->withCount('resultsCourse')->get();
        $datas = [];
        $resultNominee = [];
        $nomineeFullName = [];
        foreach ($results as $result) {
             $datas[] = $result->load('resultsCourse');
             $resultNominee[] = $result->results_course_count;
             $nomineeFullName[]  = $result->firstname . '-' . $result->middlename . '-' . $result->lastname;
        }
        return view('student-dashboard.candidates-courses.show', compact('nominee', 'results'))->with('resultNominee',json_encode($resultNominee,JSON_NUMERIC_CHECK))->with('nomineeFullName',json_encode($nomineeFullName,JSON_NUMERIC_CHECK));
    }

    public function candidatesDepartmentShow(Nominee $nominee)
    {
        $nominee->load('positionDepartment');
 
        $results = Nominee::with('resultsCourse')->withCount('resultsCourse')->latest()->get();
        $datas = [];
        $resultNominee = [];
        $nomineeFullName = [];
        foreach ($results as $result) {
             $datas[] = $result->load('resultsCourse');
             $resultNominee[] = $result->results_course_count;
             $nomineeFullName[]  = $result->firstname . '-' . $result->middlename . '-' . $result->lastname;
        }
        return view('student-dashboard.candidates-by-department.show', compact('nominee', 'results'))->with('resultNominee',json_encode($resultNominee,JSON_NUMERIC_CHECK))->with('nomineeFullName',json_encode($nomineeFullName,JSON_NUMERIC_CHECK));
    }

    public function pageant()
    {

        $timeElection = Pageant::first();

        $events = Pageant::where('department_id', auth()->user()->department_id)->with(['nominees.department', 'nominees.campus', 'nominees' => function ($query) {
            $query->where('status', 'Approved');
        }])->where('status', 'Starting now')->get(); 
         $voted     = \App\ResultPageant::where('user_id', auth()->id())->pluck('nominee_id');
        return view('student-dashboard.candidates-pageant.index', compact('events', 'voted', 'timeElection'));
    }

    public function showPageant(Nominee $nominee)
    {
        $results = Nominee::with(['resultsPageant'])->where('status', 'Approved')->withCount('resultsPageant')->get();
        $datas = [];
        $resultNominee = [];
        $nomineeFullName = [];
        $result = [];
        foreach ($results as $result) {
             $datas[] = $result->load('resultsPageant');
             $resultNominee[] = $result->results_pageant_count ;
             $nomineeFullName[]  = $result->firstname . '-' . $result->middlename . '-' . $result->lastname;
        }

        return view('student-dashboard.candidates-pageant.show', compact('nominee', 'result'))->with('resultNominee',json_encode($resultNominee,JSON_NUMERIC_CHECK))->with('nomineeFullName',json_encode($nomineeFullName,JSON_NUMERIC_CHECK));
    }
}
