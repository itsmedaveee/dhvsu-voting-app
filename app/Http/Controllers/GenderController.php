<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gender;
class GenderController extends Controller
{
    public function index()
    {
        $genders = Gender::with('students')->get();
    	return view('genders.index', [
            'genders'   => $genders
        ]);
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'gender_name'   => 'required'
        ]); 
        Gender::create([
            'gender_name'   => request('gender_name')
        ]);

        return back()->with('success', 'Gender has been added!');
    }
    public function edit(Gender $gender)
    {
        return view('genders.edit' , compact('gender'));
    }
    public function update(Request $request, Gender $gender)
    {
        $this->validate(request(), [
            'gender_name'   => 'required'
        ]);
        $gender->update([
            'gender_name'   => request('gender_name'),
        ]);

        return redirect('/genders')->with('info', 'Gender has been updated!');
    }

    public function show(Gender $gender)
    {
        $gender->load(['students.gender'])->loadCount('students');

        return view('genders.show', [
            'gender'    => $gender
        ]);
    }
}
