<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendingMail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\IdGenerator;
use App\Election;
use Yajra\DataTables\DataTables;

class PendingStudentsController extends Controller
{
    public function pending()
    {
        $user = auth()->user()->userable;
          // if (auth()->user()->isCouncil()) {
          //       $user = auth()->user()->load(['campus.students.course.department']);
          //       return view('students.students-pending', compact('user'));
          // }
 
           if (request()->ajax()) {
            $students = Student::with(['course', 'department', 'campus'])->where('approved', 'Pending')->get();
            return Datatables::of($students)
                ->addColumn('action', function ($student) {
                    return view('students.partials.action', compact('student'));
                })
                ->make(true);
        }

       
        
        return view('students.students-pending');
    }

    public function manage(Student $student)
    {
        return view('students.manage', compact('student'));
    }

    public function approved(Request $request, Student $student)
    {

        
       // $qrcode = QrCode::size(400)->generate(
       //      $student->student_no
       //  );


       //$qrcode = \QrCode::format('png')->merge('img/qrcode.png')->generate();
 

        $generateId = IdGenerator::generate([

            'table' => 'students',
            'field' => 'generate_id', //Order of Payment
            'length' => 13,
            'prefix' => 'DHVSU-',  
            'reset_on_prefix_change' => true
            
        ]);


        $election = Election::first();
        $qrcode = QrCode::size(400)->generate(route('qr-students', [$student, $election]));
        $image = \QrCode::format('png')
        ->merge('img/qrcode.png', 0.1, true)
        ->size(200)->errorCorrection('H')
        ->generate(route('qr-students', [$student, $election]));
        $output_file = 'public/qr-code/' . time() . '.png';

        \Storage::disk('local')->put($output_file, $image);
 

            $student->update([
                'generate_qr_code'  => $output_file,
                'email'       => $student->email,
                'generate_id'  => $generateId,
                'approved'    => 'confirmed'
            ]);


            $student->user()->update([
                'approved'  => 'confirmed'
            ]);

           Mail::to($student)->send(new SendingMail($student));
           return redirect('/pending-students')->with('success', 'Student has been approved!');
    }

    public function remove(Student $student)
    {
        
        $student->delete();

        $student->user()->delete();

        return redirect('/pending-students')->with('error', 'Student has been removed!');
    }

    //QR CODE SCANNER VIEW

    public function view(Student $student, Election $election)
    {

        $student->load(['results.nominee.results', 'campus']);
        $election->load('nominees.results');

        $departments = \App\Department::with('nominees.results')->withCount('resultNominee')->get();
        $elections  = \App\Election::with('nominees.results')->get();
        $result = [];
        $dept = [];
        foreach ($departments as $department) {
            //$dept[] = $department->load('nominees.results')->pluck('code','id');
            //$result[] = $department->nominees->count();
            $test[] = $department->load('nominees')->loadCount('resultNominee');
            $result[] = $department->result_nominee_count;
            $dept[] = $department->code;
           // $result[] = $data->count();
        }

        

       // $elections = Election::with('nominees.results')->get();

        
        $totalVoteUsers = \App\Result::count();
        return view('students.qr-students-view', compact('student','totalVoteUsers', 'election'))->with('dept',json_encode($dept,JSON_NUMERIC_CHECK))->with('result',json_encode($result,JSON_NUMERIC_CHECK));
    }

}
