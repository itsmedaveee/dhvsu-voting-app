<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campus;
use App\Department;
use App\Course;
use App\Student;
use App\Role;
use App\User;
use App\Gender;
use App\Registration;
use App\Program;
use Carbon\Carbon;


class RegistrationsController extends Controller
{
    public function index()
    {
        $campuses = Campus::pluck('name', 'id');
        $departments = Department::pluck('title', 'id');
        $courses = Course::all();
        $genders = Gender::pluck('gender_name', 'id');
        $programs = Program::all();

        
        return view('registration-form.index', compact('campuses','courses', 'departments', 'genders', 'programs'));
    }

    public function store(Request $request)
    {   

        $this->validate(request(), [
            'student_no'    => 'required|numeric|unique:students',
            'firstname'     => 'required',
            'middlename'    => 'required',
            'lastname'      => 'required',
            'contact_no'    => 'required|digits:11',
            'birth_date'    => 'required',
            'email'         => 'required|email|unique:users',
            'year'          => 'required',
            'section'       => 'required',
            'address'       => 'required',
            'campus'        => 'required',
            'section'       => 'required',
            'gender'        => 'required',
            'department'    => 'required',
            'course'        => 'required',
            'photo'         => 'required',
        ]);

        $register = \App\Registration::where('status', 'Published')->first();

        if (!$register) {
 
            return back()->with('error', 'Registration is already closed!');
        }

        if (now() > $register->ended_at) {
           
            return back()->with('error', 'Register has been expired!');
        }

        // return 'Started!';

        $campus     = Campus::find(request('campus'));
        $department = Department::find(request('department'));
        $course     = Course::find(request('course'));
        $gender     = Gender::find(request('gender'));
        $program     = Program::find(request('program'));

        $role       = Role::where('label', 'student')->first();


        $uploadId = $request->photo;

        if (request()->hasFile('photo')) {
            $file      = $request->file('photo');
            $extension = $file->extension();
            $filename  = time() . '-' . str_slug($uploadId) . '.' . $extension;
            $uploadScanId   = $file->storeAs('public/avatar',$filename);
        }

        $student    = Student::create([
            'student_no'    => request('student_no'),
            'firstname'    => request('firstname'),
            'middlename'   => request('middlename'),
            'lastname'     => request('lastname'),
            'contact_no'     => request('contact_no'),
            'birth_date'     => Carbon::parse(request('birth_date')),
            'email'     => request('email'),
            'year'     => request('year'),
            'section'     => request('section'),
            'address'     => request('address'),
            'nationality'     => request('nationality'),
            'religion'     => request('religion'),
            'place_of_birth'     => request('place_of_birth'),
            'photo'              =>   $uploadScanId
             
        ]);

        $password  = str_replace('-', '', $request->birth_date);

        $user = User::create([
            'username'  => request('student_no'),
            'email'     => request('email'),
            'password'  => bcrypt($password),
            'approved'  => 'Pending'
        ]);

        $user->userable()->associate($student)->save();
     
        $student->department()->associate($department)->save();
        $student->program()->associate($program)->save();
        $student->campus()->associate($campus)->save();
        $student->user()->associate($user)->save();
        $student->course()->associate($course)->save();
        $student->gender()->associate($gender)->save();
        $user->campus()->associate($campus)->save();
        $user->role()->associate($role)->save();
        $user->department()->associate($department)->save();
        $user->course()->associate($course)->save(); 

        return back()->with('success', 'Register has been success!');

    }

    public function select()
    {
        if (request()->ajax()) {

            $courses = Course::where('department_id', request('department'))->get();
            //$programs = Program::where('department_id', request('department_id'))->get();

            $data = view('registration-form.partials.select-ajax-department-course', [
                 'courses' => $courses,
               // 'programs' => $programs,
            ])->render();

            return response()->json(['options'=> $data]);
        }

    }

    

    public function selectProgram()
    {
            if (request()->ajax()) {

                $programs = Program::where('course_id', request('course'))->get();

                $data = view('registration-form.partials.select-ajax-program', [
                    'programs' => $programs,
                ])->render();

                return response()->json(['options'=> $data]);
        }
    }
}
