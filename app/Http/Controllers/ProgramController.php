<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Course;
class ProgramController extends Controller
{
    public function index()
    {
    	$programs = Program::with('course')->get();
    	$courses  = Course::pluck('title', 'id');
    	return view('programs.index', compact('programs', 'courses'));
    }

    public function store(Request $request) 
    {
    	$course  = Course::find(request('course'));

    	$program = Program::create([
    		'program_name'	=> request('program_name'),
    	]);

    	$program->course()->associate($course)->save();

    	return back()->with('success', 'Program has been added!');
    }
    public function destroy(Program $program)
    {
    	$program->delete();
    	return back()->with('error', 'Program has been removed!');
    }
}
