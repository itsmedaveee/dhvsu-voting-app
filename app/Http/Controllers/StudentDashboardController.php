<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Election;
use App\Result;
class StudentDashboardController extends Controller
{
    public function index()
    {

        // $elections = Election::with(['nominees.position'])->where('status', 'End Election')->get();
 
      

        // $departments = \App\Department::with('nominees.results')->withCount('resultNominee')->get();

        // $result = [];
        // $dept = [];

        // foreach ($departments as $department) {

        //     //$dept[] = $department->load('nominees.results')->pluck('code','id');

        //     //$result[] = $department->nominees->count();
        //     $test[] = $department->load('nominees')->loadCount('resultNominee');

        //     $result[] = $department->result_nominee_count;

        //     $dept[] = $department->code;
        //    // $result[] = $data->count();

        // }

        // $courses = \App\Course::with(['resultsDepartment'])->get();
        // $resultDepartment = [];
        // $deptCourse = [];
        // foreach ($courses as $course) {
        //     $test1[] = $course->load('nominees')->loadCount('resultsDepartment');
        //     $resultDepartment[] = $course->results_department_count;
        //     $deptCourse[] = $course->code;
        // }


        //  $events = \App\Pageant::with(['nominees.resultsPageant'])->withCount('pageantEventResults')->get();
        // $resultEvent = [];
        // $eventsPageant = [];
        // foreach ($events as $event) {
        //     $test2[] = $event->load('nominees')->loadCount('pageantEventResults');
        //     $resultEvent[] = $event->pageant_event_results_count;
        //     $eventsPageant[] = $event->event_name;
        // }

        // $results = \App\Nominee::with(['resultsPageant'])->where('status', 'Approved')->withCount('resultsPageant')->get();
        // $datas = [];
        // $resultNominee = [];
        // $nomineeFullName = [];
        // $result = [];
        // foreach ($results as $result) {
        //      $datas[] = $result->load('resultsPageant');
        //      $resultNominee[] = $result->results_pageant_count ;
        //      $nomineeFullName[]  = $result->firstname . '-' . $result->middlename . '-' . $result->lastname;
        // }

        // $studentResults = \App\Nominee::where('categories', 'Filling Course')->where('course_id', auth()->user()->course_id)->with(['resultsCourse'])->where('status', 'Approved')->withCount('resultsCourse')->get();
        // $datas = [];
        // $resultStudent = [];
        // $studentFullName = [];
        // $resultStud = [];
        // foreach ($studentResults as $resultStud) {
        //      $datas[] = $resultStud->load('resultsCourse');
        //      $resultStudent[] = $resultStud->results_course_count ;
        //      $studentFullName[]  = $resultStud->firstname . '-' . $resultStud->middlename . '-' . $resultStud->lastname;
        // }

       $student = auth()->user()->load('announcements');
        
      
        return view('student-dashboard.index', ['student' => $student]);
        // ->with('resultStudent',json_encode($resultStudent,JSON_NUMERIC_CHECK))
    //                                                             ->with('studentFullName',json_encode($studentFullName,JSON_NUMERIC_CHECK))
    //                                                             ->with('deptCourse',json_encode($deptCourse,JSON_NUMERIC_CHECK))
    //                                                             ->with('resultNominee',json_encode($resultNominee,JSON_NUMERIC_CHECK))
    //                                                             ->with('nomineeFullName',json_encode($nomineeFullName,JSON_NUMERIC_CHECK))
    //                                                             ->with('resultEvent',json_encode($resultEvent,JSON_NUMERIC_CHECK))
    //                                                             ->with('eventsPageant',json_encode($eventsPageant,JSON_NUMERIC_CHECK))
    //                                                             ->with('resultDepartment',json_encode($resultDepartment,JSON_NUMERIC_CHECK))
    //                                                             ->with('dept',json_encode($dept,JSON_NUMERIC_CHECK))
    //                                                             ->with('result',json_encode($result,JSON_NUMERIC_CHECK));
    // 

    }


}
