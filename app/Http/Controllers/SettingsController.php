<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class SettingsController extends Controller
{
    public function index()
    {
    	return view('settings.index');
    }


    public function update(Request $request)
    {
    	$this->validate(request(),[

    		'username'                  => 'required|unique:users,username,'.auth()->id(),
    		'password'                  => 'required_if:password,value|confirmed',
    		'password_confirmation'     => 'required_if:password,value'

    	]);

        if ($request->password != null) {
            auth()->user()->update([
                'password'     => bcrypt(request('password')),
            ]);
        }

        $origFilename = $request->image;
        if (request()->hasFile('image')) {

            $file      = $request->file('image');
            $extension = $file->extension();
            $filename  = time() . '-' . str_slug($origFilename) . '.' . $extension;
            $path      = $file->storeAs('public/images',$filename);

            auth()->user()->userable->update([
                'student_no'  => request('student_no'),
                'firstname'   => request('firstname'),
                'middlename'  => request('middlename'),
                'lastname'    => request('lastname'),
                'address'     => request('address'),
                'year'        => request('year'),
                'section'     => request('section'),
                'contact_no'  => request('contact_no'),
                'birth_date'  => Carbon::parse(request('birth_date')),
                'image'      => $path

            ]);

        }

        auth()->user()->update([
            'username'     => request('username'),
            'email'		   => request('email')
        ]);

    	return back()->with('success', 'User has been updated!');
    }
}
