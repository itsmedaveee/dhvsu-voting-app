<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partylist;
use Yajra\DataTables\DataTables;
class PartylistController extends Controller
{
    public function index()
    {
          if (request()->ajax()) {
             $partylists = Partylist::all();
            return Datatables::of($partylists)
                ->addColumn('action', function ($partylist) {
                    return view('partylists.partials.action', compact('partylist'));
                })
                ->make(true);
        }
    	return view('partylists.index');
    }


    public function store(Request $request)
    {
        $this->validate(request(), [
            'name'  => 'required'
        ]);

    	Partylist::create([
    		'name'	=> request('name'),
    		'user_id'	=> auth()->user()->id
    	]);


    	return back()->with('success', 'Partylist has been added!');
    }

    public function edit(Partylist $partylist)
    {
    	return view('partylists.edit', compact('partylist'));
    }

    public function update(Request $request, Partylist $partylist)
    {
    	$this->validate(request(), [
    		'name'	=> 'required'
    	]);

    	$partylist->update([
    		'name'	=> request('name'),
    		'user_id'	=> auth()->user()->id
    	]);

    	return redirect('/partylists')->with('info', 'Partylist has been updated!');
    }

    public function destroy(Partylist $partylist)
    {
    	$partylist->delete();

    	return back()->with('error', 'Partylist has been removed!');
    }
}
