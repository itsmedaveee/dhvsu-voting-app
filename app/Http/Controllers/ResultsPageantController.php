<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nominee;
use App\Pageant;
use App\ResultPageant;
class ResultsPageantController extends Controller
{
    public function index()
    {
        $pageants = Pageant::where('status', 'End Events')->with('nominees')->get();
        $totalVoteUsers = ResultPageant::count();
        return view('results.results-pageant', compact('pageants', 'totalVoteUsers'));
    }
}
