<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use Yajra\DataTables\DataTables;
class DepartmentsController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $departments = Department::with('courses')->get();
            return Datatables::of($departments)
                ->addColumn('action', function ($department) {
                    return view('departments.partials.action', compact('department'));
                })
                ->make(true);
        }
   
        return view('departments.index');
    }

    public function store(Request $request, Department $department)
    {
        $this->validate(request(), [
            'code'  => 'required',
            'title'  => 'required',
        ]);

        Department::create([
            'code'  => request('code'),
            'title' => request('title'),
        ]);

        return back()->with('success', 'Department has been added!');
    }

    public function edit(Department $department)
    {
        return view('departments.edit', compact('department'));
    }

    public function update(Request $request, Department $department)
    {
         $this->validate(request(), [
            'code'  => 'required',
            'title'  => 'required',
        ]);


        $department->update([
            'code'  => request('code'),
            'title' => request('title'),
        ]);

        return redirect('/departments')->with('info', 'Department has been updated!');
    }

    public function show(Department $department)
    {
        $department->load('courses');

        return view('departments.show', compact('department'));    
    }

    public function destroy(Department $department)
    {   
        $department->delete();

        return back()->with('error', 'Department has been removed!');
    }
}
