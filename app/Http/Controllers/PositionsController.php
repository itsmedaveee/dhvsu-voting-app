<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Position;
use App\Campus;
use App\Department;
use App\Election;
use Yajra\DataTables\DataTables;
class PositionsController extends Controller
{
    public function index()
    {

    	// $campuses = Campus::pluck('name', 'id');
    	// $departments = Department::pluck('title', 'id');
     //    $elections  = Election::pluck('election_name', 'id');

         if (request()->ajax()) {
          
            $positions = Position::where('user_id', auth()->user()->id)->get();
            return Datatables::of($positions)
                ->addColumn('action', function ($position) {
                    return view('positions.partials.action', compact('position'));
                })
                ->make(true);
        }

    	return view('positions.index');
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
    		'position_name'	=> 'required',
            'department'    => 'required',
            'campuses'      => 'required',
    	]);

    	$campus = Campus::find(request('campuses'));
    	$department = Department::find(request('department'));
        $election = Election::find(request('election'));

    	$position = Position::create([
    		'position_name'	=> request('position_name'),
    		'user_id'	=> auth()->user()->id
    	]);


    	$position->campus()->associate($campus)->save();
    	$position->department()->associate($department)->save();
        $position->election()->associate($election)->save();


    	return back()->with('success', 'Position has been added!');
    }

    public function edit(Position $position)
    {
    	$campuses = Campus::pluck('name', 'id');
    	$departments = Department::pluck('title', 'id');

    	return view('positions.edit', compact('position', 'campuses', 'departments'));
    }

    public function update(Request $request, Position $position)
    {
    	$this->validate(request(), [
    		'position_name'	=> 'required',
            'department'    => 'required',
            'campuses'      => 'required'
    	]);
    	  
    	$campus = Campus::find(request('campuses'));
    	$department = Campus::find(request('department'));

    	$position->update([
    		'position_name'	=> request('position_name'),
    		'user_id'	=> auth()->user()->id
    	]);


    	$position->campus()->associate($campus)->save();
    	$position->department()->associate($department)->save();
    	
    	return redirect('/positions')->with('info', 'Position has been updated!');
    }

    public function destroy(Position $position)
    {
    	$position->delete();

    	return back()->with('error', 'Position has been removed!');
    }
}
