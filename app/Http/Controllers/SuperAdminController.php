<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Campus;
use App\Department;

class SuperAdminController extends Controller
{
    public function index()
    {
        $superAdmin = User::where('role_id', 1)->get();
        $departments  = Department::pluck('code', 'id');
         $campuses   = Campus::pluck('name', 'id');

        return view('super-admin.index', compact('superAdmin', 'departments', 'campuses'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'email' => 'required|email|unique:users',
            'username'  => 'required',
            'password'  => 'required',
        ]);


        $role   = Role::where('label', 'super admin')->first();
        $campus      = Campus::find(request('campuses'));


        $department  = Department::find(request('department'));

        $user = User::create([
            'email' => request('email'),
            'username'  => request('username'),
            'password'  => bcrypt(request('password')),
            'approved'  => 'confirmed'
        ]);

        $user->role()->associate($role)->save();
        $user->campus()->associate($campus)->save();
        $user->userable()->associate($user)->save();
        $user->department()->associate($department)->save();
        //$superAdmin->account()->associate($user)->save();

        return back()->with('success', 'Super Admin has been added!');
    }

    public function edit(User $admin)
    {
        $departments  = Department::pluck('code', 'id');
        $campuses     = Campus::pluck('name', 'id');
        return view('super-admin.edit', compact('admin', 'departments', 'campuses'));
    }

    public function update(Request $request, User $admin)
    {
        $this->validate(request(), [
          
            'username'  => 'required',
            'password'                  => 'required_if:password,value|confirmed',
            'password_confirmation'     => 'required_if:password,value'
        ]);


        if ($request->password != null) {
            
            auth()->user()->update([

                'password'     => bcrypt(request('password')),

            ]);
        }

        $role  = Role::where('label', 'super admin')->first();
        $campus      = Campus::find(request('campuses'));
 
        $department  = Department::find(request('department'));
 

        $admin->update([
            'email' => request('email'),
            'username'  => request('username'),
            'approved'  => 'confirmed'

        ]);

        $admin->role()->associate($role)->save();
        $admin->campus()->associate($campus)->save();
        $admin->userable()->associate($admin)->save();
         $user->department()->associate($department)->save();

        return redirect('/super-admin')->with('info', 'Super Admin has been update!');
    }
}
