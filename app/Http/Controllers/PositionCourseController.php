<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\PositionCourse;
class PositionCourseController extends Controller
{
    public function index()
    {
    	$positionCourses = PositionCourse::all();
    	return view('events.position-course.index', compact('positionCourses'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
    		'position_name'	=> 'required'
    	]);
		$position = PositionCourse::create([
    		'position_name'	=> request('position_name'),
    		'user_id'		=> auth()->user()->id
    	]);

    	return back()->with('success', 'Position has been added!');
    }

    public function edit(PositionCourse $position)
    {
    	return view('events.position-course.edit', compact('position'));
    }

    public function update(PositionCourse $position)
    {
		$this->validate(request(), [
    		'position_name'	=> 'required'
    	]);
    	$position->update([
    		'position_name'	=> request('position_name'),
    		'user_id'		=> auth()->user()->id
    	]);

    	return redirect('/position-course')->with('info', 'Position has been updated!');
    }

    public function destroy(PositionCourse $position)
    {
    	$position->delete();
    	return back()->with('error', 'Position has been removed!');
    }
}
