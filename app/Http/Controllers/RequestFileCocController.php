<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nominee;
use App\Position;
use App\Campus;
use App\Department;
use App\Course;
// use App\Partylist;
use App\Student;
use App\Election;
use App\ElectionDepartment;
use App\PositionDepartment;
use App\PositionCourse;
use App\ElectionCourse;
use App\Program;

use App\Pageant;
use Carbon\Carbon;

class RequestFileCocController extends Controller
{
    public function index()
    {
    	$user = auth()->user()->load('student');

        $positions              = Position::all();
        $positionsDepartment    = PositionDepartment::all();
        $positionsCourse        = PositionCourse::all();
        $departments            = Department::pluck('title', 'id');
        $programs               = Program::pluck('program_name', 'id');
        $elections              = Election::where('status', 'On Going')->pluck('election_name', 'id');
        $electionDepartments    = ElectionDepartment::where('department_id', auth()->user()->department_id)->where('status', 'On Going')->pluck('election_name', 'id');
        $electionCourses        = ElectionCourse::where('course_id', auth()->user()->course_id)->where('status', 'On Going')->pluck('election_name', 'id');
        $courses                = Course::pluck('title', 'id'); 
        $pageants               = Pageant::where('department_id', auth()->user()->department_id)->where('status', 'On Going')->pluck('event_name', 'id');
        $campuses               = Campus::pluck('name', 'id');
    	return view('student-dashboard.request-coc-file.index', compact( 
    				'positions', 
                    'campuses', 
                    'departments',  
                    'courses', 
                    'elections',
                    'programs',
                    'user',
                    'electionDepartments',
                    'positionsDepartment',
                    'electionCourses',
                    'positionsCourse',
                    'pageants'  
                ));
    }


    public function store(Request $request)
    {
    	    $this->validate(request(), [
                'religion'                          => 'required',
                // 'gender'                            => 'required',
                'avatar'                            => 'required',
                'place_of_birth'                    => 'required',
                'categories'                        => 'required',
                'religion'                          => 'required',
                'nationality'                       => 'required', 
                'certificate_of_registration'                       => 'required', 
                'validate_school_id'                       => 'required', 
                'recent_photo'                       => 'required', 
                'partyname'                         => 'required',
                'recomend_letter_from_dean'         => 'required',
                'certificate_of_good_moral'         => 'required',
                'class_of_grade'                    => 'required',
                'certified'                         => 'required',
                'training_seminars'                 => 'required',
            ]);
            //$user  = auth()->user()->load('userable');

            if (request('categories') == 'Filing Campus') {
                $onGoingElection = Election::where('status', 'On Going')->count();

                if (! $onGoingElection) {
                    return back()->with('error', 'You cannot file candidacy form');
                }
            }

            if (request('categories') == 'Filing Department') {
                $onGoingElectionDepartment = ElectionDepartment::where('status', 'On Going')->count();

                if (! $onGoingElectionDepartment) {
                    return back()->with('error', 'You cannot file candidacy form');
                }
            }    

            if (request('categories') == 'Filing Course') {
                $onGoingElectionCourse = ElectionCourse::where('status', 'On Going')->count();

                if (! $onGoingElectionCourse) {
                    return back()->with('error', 'You cannot file candidacy form');
                }
            }      

            if (request('categories') == 'Filing Event') {
                $onGoingPageant = Pageant::where('status', 'On Going')->count();

                if (! $onGoingPageant) {
                    return back()->with('error', 'You cannot file candidacy form');
                }
            }

            // if (request('categories') == 'Filling Course') {
            //     $election = ElectionCourse::where('status', 'Starting now')->latest()->get();
            //     return back()->with('error', 'You cannot file candidacy form while the Election must Starting now.');
            // }  if (request('categories') == "Filling Course") {
            //     $election = ElectionCourse::where('status', 'End Election')->latest()->get();
            //     return back()->with('error', 'You cannot file candidacy form while the Election has Ended.');
            // }
            // if election return message started

            $register = \App\Registration::where('status', 'Published')->first();
            if (!$register) {
                return back()->with('error', 'Candidacy Form is closed!');
            }
            if (now() > $register->ended_at) {  
                return back()->with('error', 'Register has been expired!');
            }

            $nominee = Nominee::where([
                ['status', 'Pending'], 
                ['user_id', auth()->user()->id]
            ])->count();
            if ($nominee) {
                return back()->with('error', 'You still have pending Candidacy Form');
            }
            $origFilename = $request->avatar;
            if (request()->hasFile('avatar')) {

                $file      = $request->file('avatar');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($origFilename) . '.' . $extension;
                $path      = $file->storeAs('public/avatar',$filename);
            }


            $cor = $request->certificate_of_registration;

            if (request()->hasFile('certificate_of_registration')) {

                $file      = $request->file('certificate_of_registration');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($cor) . '.' . $extension;
                $certificateRegistration      = $file->storeAs('public/avatar',$filename);
            }


            $validID = $request->validate_school_id;

            if (request()->hasFile('validate_school_id')) {

                $file      = $request->file('validate_school_id');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($validID) . '.' . $extension;
                $schoolId      = $file->storeAs('public/avatar',$filename);
            }

            $certAttendance = $request->certificate_of_attendance;

            if (request()->hasFile('certificate_of_attendance')) {

                $file      = $request->file('certificate_of_attendance');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($certAttendance) . '.' . $extension;
                $certificateAttendance      = $file->storeAs('public/avatar',$filename);
            }


            $recentPhoto = $request->recent_photo;

            if (request()->hasFile('recent_photo')) {

                $file      = $request->file('recent_photo');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($recentPhoto) . '.' . $extension;
                $recPhoto      = $file->storeAs('public/avatar',$filename);
            }

            $letterForDean = $request->recomend_letter_from_dean;

            if (request()->hasFile('recomend_letter_from_dean')) {

                $file      = $request->file('recomend_letter_from_dean');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($letterForDean) . '.' . $extension;
                $recommendLetter      = $file->storeAs('public/avatar',$filename);
            }


            $certGoodMoral = $request->certificate_of_good_moral;
            if (request()->hasFile('certificate_of_good_moral')) {
                $file      = $request->file('certificate_of_good_moral');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($certGoodMoral) . '.' . $extension;
                $goodMoral      = $file->storeAs('public/avatar',$filename);
            }

            $cog = $request->class_of_grade;
            if (request()->hasFile('class_of_grade')) {
                $file      = $request->file('class_of_grade');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($cog) . '.' . $extension;
                $certificateGrade      = $file->storeAs('public/avatar',$filename);
            }

            $trainingSeminars = $request->training_seminars;
            if (request()->hasFile('training_seminars')) {
                $file      = $request->file('training_seminars');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($cog) . '.' . $extension;
                $trainingSem      = $file->storeAs('public/avatar',$filename);
            }

            $position                   = Position::find(request('position'));
            $positionDepartment         = PositionDepartment::find(request('positionDepartment'));
            $positionCourse             = PositionCourse::find(request('positionCourse'));
            $campus                     = Campus::find(request('campuses'));
            $department                 = Department::find(request('department'));
            // $partylist                  = Partylist::find(request('partylist'));
            $election                   = Election::find(request('election'));
            $electionDepartment         = ElectionDepartment::find(request('electionDepartment'));
            $electionCourse             = ElectionCourse::find(request('electionCourse'));
            $course                     = Course::find(request('course'));
            $pageant                    = Pageant::find(request('pageant'));
            // $gender                     = Gender::find(request('gender'));
            $user                       = auth()->user()->userable;
            $student                    = \App\Student::where('user_id', auth()->user()->id)->first();


            $nominees                   = Nominee::create([
                
                'firstname'                     => $request->firstname,
                'middlename'                    => $request->middlename,
                'lastname'                      => $request->lastname,
                'contact_no'                    => $request->contact_no,
                'gender'                        => request('gender'),
                'about'                         => request('about'),
                'birthdate'                     => Carbon::parse($request->birth_date),
                'about'                         => request('about'),
                'user_id'                       => auth()->user()->id,
                'achievements'                  => request('achievements'),
                'avatar'                        => $path,
                'certificate_of_registration'   => $certificateRegistration ?? null,
                'validate_school_id'            => $schoolId ?? null,
                'certificate_of_attendance'     => $certificateAttendance ?? null,
                'recent_photo'                  => $recPhoto ?? null,
                'recomend_letter_from_dean'     => $recommendLetter ?? null,
                'certificate_of_good_moral'     => $goodMoral ?? null,
                'class_of_grade'                => $certificateGrade ?? null,
                'reason_for_candidacy'          => request('reason_for_candidacy'),
                'average_last_semister'         => request('average_last_semister'),
                'previous_failed_subject'       => request('previous_failed_subject'),
                'award_honors_received'         => request('award_honors_received'),
                'extra_curricular_activities'   => request('extra_curricular_activities'),
                'date_filled_out'               => request('date_filled_out'),
                'nationality'                   => request('nationality'),
                'place_of_birth'                => request('place_of_birth'),
                'religion'                      => request('religion'),
                'categories'                    => request('categories'),
                'title'                         => request('title'),
                'partyname'                     => request('partyname'),
                'status'                        => 'Pending',
                'propaganda'                    => request('propaganda'),
                'certified'                     => 'I hereby certify that all information state herein is true end accurate to the best of my knowledge.',
                'usc'                           => request('usc'),
                'training_seminars'             => $trainingSem
            ]);

            $nominees->position()->associate($position)->save();
            $nominees->campus()->associate($campus)->save();
            $nominees->department()->associate($department)->save();
            // $nominees->partylist()->associate($partylist)->save();
            $nominees->course()->associate($course)->save();
            $nominees->election()->associate($election)->save();
            $nominees->electionDepartment()->associate($electionDepartment)->save();
            $nominees->positionDepartment()->associate($positionDepartment)->save();
            $nominees->electionCourse()->associate($electionCourse)->save();
            $nominees->positionCourse()->associate($positionCourse)->save();
            // $nominees->gender()->associate($gender)->save();
            $nominees->pageant()->associate($pageant)->save();
            return back()->with('statuses', 'Test');
    }



    public function fileCoc()
    {
        $user = auth()->user()->load('department', 'campus');
        $nominees = $user->load('nominees');
        return view('student-dashboard.request-coc-file.show-request-coc', compact('nominees' , 'user'));
    }

    public function show(Nominee $nominee)
    {
        return view('student-dashboard.request-coc-file.show', compact('nominee'));
    }
}
