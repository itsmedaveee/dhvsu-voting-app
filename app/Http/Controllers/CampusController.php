<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campus;
use App\Student;
use Yajra\DataTables\DataTables;
class CampusController extends Controller
{
    public function index()
    {
         if (request()->ajax()) {
            $campuses = Campus::with('students')->get();
            return Datatables::of($campuses)
                ->addColumn('action', function ($campus) {
                    return view('campuses.partials.action', compact('campus'));
                })
                ->make(true);
        }
        return view('campuses.index');
    }


    public function store(Request $request)
    {
        $this->validate(request(),[

            'name'      => 'required',
            'location'  => 'required',

        ]);

    
            $campuses = Campus::create([

            'name'      => $request->name,
            'location'  => $request->location,
            
        ]);
    
       return redirect('/campuses')->with('success', " $campuses->name has been Added!");
    }

    public function edit(Campus $campus)
    {
        return view('campuses.edit', compact('campus'));
    }


     public function update(Request $request, Campus $campus)
    {
        $this->validate(request(),[

            'name'      => 'required',
            'location'  => 'required',
        ]);

            $campuses   = $campus->update([

            'name'      => $request->name,
            'location'  => $request->location,
            
        ]);
    
       return redirect('/campuses')->with('info', " Campus has been Updated!");
    }

    public function show(Campus $campus)
    {
        $campus->load('students');

        return view('campuses.show', compact('campus'));
    }

    public function destroy(Request $request, Campus $campus)
    {
        $campus->delete();

        return back()->with('error', 'Campus has been removed!');
    }
}
