<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Department;
use Yajra\DataTables\DataTables;
class CoursesController extends Controller
{
    public function index()
    {
        $departments = Department::pluck('title', 'id');
        if (request()->ajax()) {
            $courses = Course::all();
            return Datatables::of($courses)
                ->addColumn('action', function ($course) {
                    return view('courses.partials.action', compact('course'));
                })
                ->make(true);
        }
        return view('courses.index', compact('departments'));
    }

    public function store(Request $request)
    {
        $this->validate(request(),[

            'code'       =>  'required',
            'title'      =>  'required',
            'department' =>  'required'

        ]);

        $department = Department::find(request('department'));

        $course = Course::create([

            'code'    =>  $request->code,
            'title'   =>  $request->title

        ]);

        $course->department()->associate($department)->save();

        session()->flash('success', "Course: {$course->code} Added!");

        return back();
    }

    public function edit(Course $course)
    {
        $departments = Department::pluck('title', 'id');
        return view('courses.edit', compact('course','departments'));
    }

    public function destroy(Course $course)
    {
        $course->delete();

        return back()->with('error', 'Course has been removed!');
    }

    public function update(Request $request, Course $course)
    {
        $this->validate(request(),[

            'code'       =>  'required',
            'title'      =>  'required',
            'department' =>  'required'

        ]);

        $department = Department::find(request('department'));

        $course->update([

            'code'    =>  $request->code,
            'title'   =>  $request->title

        ]);

         $course->department()->associate($department)->save();

         session()->flash('info',  "Course: {$course->code} Updated!");

        return redirect('/courses');
    }


}
