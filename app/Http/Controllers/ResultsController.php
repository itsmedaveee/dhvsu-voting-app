<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

use App\Result;
use App\Nominee;
use App\Election;
class ResultsController extends Controller
{
    public function index()
    {

    	 //$user = auth()->user()->load(['results.nominee.election']);
   		 $elections = Election::with(['nominees.position', 'nominees' => function ($q) {
            $q->where('status', 'Approved');
         }])->get();

           $electionId = Election::latest()->first()->id;
        // if (is_array($electionId) || is_object($electionId))
        // {
        //     foreach ($electionId as $election)
        //     {
        //         if ($election->status == 'On Going') {
        //             $election = Election::where('status', 'On Going')->latest()->first();
        //             return "On Going";
        //         } else if ($election->status == 'End Election') {
        //             $election = Election::where('status', 'End Election')->latest()->first();
        //              return "has Ended";
        //         } else if ($election->status == 'Starting now') {
        //             $election = Election::where('status', 'Starting now')->latest()->first();
        //             return "Starting now";
        //         }
        //     }
        // } 
   
        $nominees = Nominee::with(['position', 'results'])->where('status', 'Approved')->where('election_id', $electionId)->get();
        $grouped = $nominees->groupBy('position.position_name');

    	 $totalVoteUsers = Result::count();
    	return view('results.index', compact('totalVoteUsers', 'elections', 'nominees', 'grouped'));
    }
}
