<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nominee;
use App\Position;
use App\Campus;
use App\Department;
use App\Course;
use App\Partylist;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
class NomineesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $user = auth()->user()->userable;

        $positions       = Position::all();
        $departments     = Department::pluck('title', 'id');
        $courses         = Course::all();
        $partylists      = Partylist::all();
        $courses         = Course::all();
        $campuses        = Campus::all();


        //$nominees = Nominee::with(['user','department','campus'])->get();
        // if (auth()->user()->isCouncil()) {
        //     # code...
        //     $user = auth()->user()->load(['campus.nominees.department']);

        //     return view('nominees.index', compact('positions', 'campuses', 'departments', 'courses', 'user', 'partylists'));
        // } 

    if (auth()->user()->isCouncil()) {
                $nominees = Nominee::where('department_id', auth()->user()->department_id)->with('position', 'department', 'campus')->orderBy('lastname')->where('status', 'Approved')->get();
    }



  if (auth()->user()->isSuperAdmin()) {
                $nominees = Nominee::orderBy('lastname')->with('position', 'department', 'campus')->where('status', 'Approved')->get();
             
    }
 

        return view('nominees.index', compact(
                    'positions', 
                    'campuses', 
                    'departments', 
                    'nominees', 
                    'courses', 
                    'user', 
                    'partylists', 
                    'courses'
                ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
            $this->validate(request(), [
                'firstname'     => 'required',
                'middlename'    => 'required',
                'lastname'      => 'required',
                'contact_no'    => 'required',
                'religion'        => 'required',
                'gender'        => 'required',
                'birthdate'     => 'required',
                'avatar'        => 'required',
                'certificate_of_registration'        => 'required',
                'validate_school_id'        => 'required',
                'certificate_of_attendance'        => 'required',
                'recent_photo'        => 'required',
                'recomend_letter_from_dean'        => 'required',
                'certificate_of_good_moral'        => 'required',
                'class_of_grade'        => 'required',
            ]);
            //$user  = auth()->user()->load('userable');

            $origFilename = $request->avatar;

            if (request()->hasFile('avatar')) {

                $file      = $request->file('avatar');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($origFilename) . '.' . $extension;
                $path      = $file->storeAs('public/avatar',$filename);
            }


            $cor = $request->certificate_of_registration;

            if (request()->hasFile('certificate_of_registration')) {

                $file      = $request->file('certificate_of_registration');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($cor) . '.' . $extension;
                $certificateRegistration      = $file->storeAs('public/avatar',$filename);
            }


            $validID = $request->validate_school_id;

            if (request()->hasFile('validate_school_id')) {

                $file      = $request->file('validate_school_id');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($validID) . '.' . $extension;
                $schoolId      = $file->storeAs('public/avatar',$filename);
            }

            $certAttendance = $request->certificate_of_attendance;

            if (request()->hasFile('certificate_of_attendance')) {

                $file      = $request->file('certificate_of_attendance');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($certAttendance) . '.' . $extension;
                $certificateAttendance      = $file->storeAs('public/avatar',$filename);
            }


            $recentPhoto = $request->recent_photo;

            if (request()->hasFile('recent_photo')) {

                $file      = $request->file('recent_photo');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($recentPhoto) . '.' . $extension;
                $recPhoto      = $file->storeAs('public/avatar',$filename);
            }

            $letterForDean = $request->recomend_letter_from_dean;

            if (request()->hasFile('recomend_letter_from_dean')) {

                $file      = $request->file('recomend_letter_from_dean');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($letterForDean) . '.' . $extension;
                $recommendLetter      = $file->storeAs('public/avatar',$filename);
            }


            $certGoodMoral = $request->certificate_of_good_moral;

            if (request()->hasFile('certificate_of_good_moral')) {

                $file      = $request->file('certificate_of_good_moral');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($certGoodMoral) . '.' . $extension;
                $goodMoral      = $file->storeAs('public/avatar',$filename);
            }


            $cog = $request->class_of_grade;

            if (request()->hasFile('class_of_grade')) {

                $file      = $request->file('class_of_grade');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($cog) . '.' . $extension;
                $certificateGrade      = $file->storeAs('public/avatar',$filename);
            }


            $position     = Position::find(request('position'));
         
            $campus       = Campus::find(request('campuses'));
            $department   = Department::find(request('department'));
            $partylist    = Partylist::find(request('partylist'));
            $course       = Course::find(request('course'));


            $nominees = Nominee::create([
                'student_no'         => request('student_no'),
                'firstname'          => request('firstname'),
                'middlename'         => request('middlename'),
                'lastname'           => request('lastname'),
                'contact_no'         => request('contact_no'),
                'gender'             => request('gender'),
                'about'              => request('about'),
                'birthdate'          => Carbon::parse(request('birthdate')),
                'about'              => request('about'),
                'user_id'            => auth()->user()->id,
                'achievements'       => request('achievements'),
                'avatar'             => $path,
                'certificate_of_registration'   => $certificateRegistration,
                'validate_school_id'            => $schoolId,
                'certificate_of_attendance'     => $certificateAttendance,
                'recent_photo'                  => $recPhoto,
                'recomend_letter_from_dean'     => $recommendLetter,
                'certificate_of_good_moral'     => $goodMoral,
                'class_of_grade'                => $certificateGrade,
                'reason_for_candidacy'          => request('reason_for_candidacy'),
                'average_last_semister'         => request('average_last_semister'),
                'previous_failed_subject'       => request('previous_failed_subject'),
                'award_honors_received'         => request('award_honors_received'),
                'extra_curricular_activities'   => request('extra_curricular_activities'),
                'date_filled_out'               => request('date_filled_out'),
                'nationality'                   => request('nationality'),
                'place_of_birth'                => request('place_of_birth'),
                'religion'                      => request('religion'),
                'title'                         => request('title'),
                'status'                        => 'Pending'

            ]);


            $nominees->position()->associate($position)->save();
            $nominees->campus()->associate($campus)->save();
            $nominees->department()->associate($department)->save();
            $nominees->partylist()->associate($partylist)->save();
            $nominees->course()->associate($course)->save();


            return back()->with('success', 'Nominee has been added!');

        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Nominee $nominee)
    {
        $nominee->load('results');
        return view('nominees.show', compact('nominee'));
    }

    public function showDetails(Nominee $nominee)
    {
        return view('nominees.show', compact('nominee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Nominee $nominee)
    {
        $positions       = Position::all();
        $campuses        = Campus::all();
        $departments     = Department::pluck('title', 'id');
        $courses         = Course::all();
        $partylists      = Partylist::all();
        return view('nominees.edit', compact('nominee', 'campuses', 'departments', 'courses', 'partylists', 'positions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nominee $nominee)
    {

  $this->validate(request(), [
                'firstname'     => 'required',
                'middlename'    => 'required',
                'lastname'      => 'required',
                'contact_no'    => 'required',
                'gender'        => 'required',
                'birthdate'     => 'required',
                'avatar'        => 'required'
            ]);
            //$user  = auth()->user()->load('userable');

            $origFilename = $request->avatar;

            if (request()->hasFile('avatar')) {

                $file      = $request->file('avatar');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($origFilename) . '.' . $extension;
                $path      = $file->storeAs('public/avatar',$filename);
            }


            $cor = $request->certificate_of_registration;

            if (request()->hasFile('certificate_of_registration')) {

                $file      = $request->file('certificate_of_registration');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($cor) . '.' . $extension;
                $certificateRegistration      = $file->storeAs('public/avatar',$filename);
            }


            $validID = $request->validate_school_id;

            if (request()->hasFile('validate_school_id')) {

                $file      = $request->file('validate_school_id');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($validID) . '.' . $extension;
                $schoolId      = $file->storeAs('public/avatar',$filename);
            }

            $certAttendance = $request->certificate_of_attendance;

            if (request()->hasFile('certificate_of_attendance')) {

                $file      = $request->file('certificate_of_attendance');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($certAttendance) . '.' . $extension;
                $certificateAttendance      = $file->storeAs('public/avatar',$filename);
            }


            $recentPhoto = $request->recent_photo;

            if (request()->hasFile('recent_photo')) {

                $file      = $request->file('recent_photo');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($recentPhoto) . '.' . $extension;
                $recPhoto      = $file->storeAs('public/avatar',$filename);
            }

            $letterForDean = $request->recomend_letter_from_dean;

            if (request()->hasFile('recomend_letter_from_dean')) {

                $file      = $request->file('recomend_letter_from_dean');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($letterForDean) . '.' . $extension;
                $recommendLetter      = $file->storeAs('public/avatar',$filename);
            }


            $certGoodMoral = $request->certificate_of_good_moral;

            if (request()->hasFile('certificate_of_good_moral')) {

                $file      = $request->file('certificate_of_good_moral');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($certGoodMoral) . '.' . $extension;
                $goodMoral      = $file->storeAs('public/avatar',$filename);
            }


            $cog = $request->class_of_grade;

            if (request()->hasFile('class_of_grade')) {

                $file      = $request->file('class_of_grade');
                $extension = $file->extension();
                $filename  = time() . '-' . str_slug($cog) . '.' . $extension;
                $certificateGrade      = $file->storeAs('public/avatar',$filename);
            }


            $position     = Position::find(request('position'));
         
            $campus       =  Campus::find(request('campuses'));
            $department   = Department::find(request('department'));
            $partylist    = Partylist::find(request('partylist'));
            $course       = Course::find(request('course'));



            $nominee->update([
              'student_no'         => request('student_no'),
                'firstname'          => request('firstname'),
                'middlename'         => request('middlename'),
                'lastname'           => request('lastname'),
                'contact_no'         => request('contact_no'),
                'gender'             => request('gender'),
                'about'              => request('about'),
                'birthdate'          => Carbon::parse(request('birthdate')),
                'about'              => request('about'),
                'user_id'            => auth()->user()->id,
                'achievements'       => request('achievements'),
                'avatar'             => $path,
                'certificate_of_registration'   => $certificateRegistration,
                'validate_school_id'   => $schoolId,
                'certificate_of_attendance'   => $certificateAttendance,
                'recent_photo'   => $recPhoto,
                'recomend_letter_from_dean'   => $recommendLetter,
                'certificate_of_good_moral'   => $goodMoral,
                'class_of_grade'   => $certificateGrade,
                'reason_for_candidacy'  => request('reason_for_candidacy'),
                'average_last_semister' => request('average_last_semister'),
                'previous_failed_subject'   => request('previous_failed_subject'),
                'award_honors_received' => request('award_honors_received'),
                'extra_curricular_activities'   => request('extra_curricular_activities'),
                'date_filled_out'   => request('date_filled_out'),
                'nationality'   => request('nationality'),
                'place_of_birth'   => request('place_of_birth'),
                'religion'   => request('religion'),
                'status'            => 'Pending'

            ]);


            $nominees->position()->associate($position)->save();
            $nominees->campus()->associate($campus)->save();
            $nominees->department()->associate($department)->save();
            $nominees->partylist()->associate($partylist)->save();
            $nominees->course()->associate($course)->save();

            return redirect('/nominees')->with('info', 'Nominee has been updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nominee $nominee)
    {
        $nominee->delete();

        return back()->with('error', 'Nominee has been removed!');
    }

    // public function select()
    // {
    //   if (request()->ajax()) {
        
    //         $campusId = request('campus');
    //         $departmentId = request('department');

    //         $department = Department::with('courses')->find($departmentId);

          
    //         $course = Course::where([
    //             ['campus_id', $campusId],
    //             ['department_id', $department->course->id]
    //         ])->get();

    //         $data = view('nominees.partials.select-ajax-course', ['department' => $department])->render();

    //         return response()->json(['options' => $data]);
    //     }
    // }
}
