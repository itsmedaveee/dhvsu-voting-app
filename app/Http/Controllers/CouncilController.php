<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Council;
use App\Department;
use App\Campus;
use App\Role;
use App\User;
class CouncilController extends Controller
{
     public function index()
    {

        $councils = Council::all();

        $departments  = Department::all();
        $campuses     = Campus::pluck('name', 'id');

        return view('councils.index', compact('councils' , 'departments' , 'campuses'));
    }

       public function store(Request $request)
    {
        $this->validate(request(),[

            'firstname'     => 'required',
            'lastname'      => 'required',
            'middlename'    => 'required',
            'username'      => 'required',
            'password'      => 'required',
            'campuses'      => 'required',
            'email'         => 'required|unique:users,email',

        ]);

        $department  = Department::find(request('department'));
        $campus      = Campus::find(request('campuses'));
        $course      = \App\Course::find(request('course'));

        $role = Role::where('name', 'Council')->first();

        $user = User::create([
            'username'           => request('username'),
            'password'           => bcrypt(request('password')),
            'email'              => request('email'),
            'approved'           => 'confirmed'

        ]);

        $user->role()->associate($role)->save();


        $council = Council::create([
            'firstname' => request('firstname'),
            'middlename'    => request('middlename'),
            'lastname'  => request('lastname'),
            'email' => request('email'),
        ]);

        $user->userable()->associate($council)->save();
        $council->user()->associate($user)->save();
        $user->department()->associate($department)->save();

        $user->campus()->associate($campus)->save();
        $user->course()->associate($course)->save();
        $council->campus()->associate($campus)->save();
        $council->course()->associate($course)->save();
        $council->department()->associate($department)->save();

        return back()->with('success', 'Council: Success Added!');
    }



    public function edit(Council $council)
    {

        $departments  = Department::pluck('code', 'id');
        $campuses     = Campus::pluck('name', 'id');

        return view('councils.edit', compact('council', 'departments', 'campuses'));
    }

    public function update(Request $request, Council $council)
    {
        $this->validate(request(),[

            'firstname'     => 'required',
            'lastname'      => 'required',
            'middlename'    => 'required',
            'username'      => 'required',
            'password'      => 'required',
            'campuses'      => 'required',
            'department'    => 'required',
            'email'         => 'required_if:unique:users,email',
        ]);

        $department  = Department::find(request('department'));
        $campus      = Campus::find(request('campuses'));

        $role = Role::where('name', 'Council')->first();
        //  if ($request->password != null) {
        //     $council->update([
        //         'password'     => bcrypt(request('password')),
        //     ]);
        // }

        $user = $council->user;
        $user->update([
            'username'           => request('username'),
            'email'              => request('email'),
            'password'           => bcrypt(request('password')),
            'approved'           => 'confirmed',
        ]);
 
        $council->update([
            'firstname' => request('firstname'),
            'middlename'    => request('middlename'),
            'lastname'  => request('lastname'),
            'email' => request('email'),
        ]);

         $user->userable()->associate($council)->save();
         $council->user()->associate($user)->save();

        $user->campus()->associate($campus)->save();
        $user->department()->associate($department)->save();
        $user->campus()->associate($campus)->save();

        return redirect('/councils')->with('info', 'Council: Success Updated!');
    }
}
