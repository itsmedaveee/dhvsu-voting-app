<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nominee;
use Yajra\DataTables\DataTables;
class PendingRequestCocController extends Controller
{
    public function pending ()
    {
        //return $nominees = Nominee::with('department')->where('status', 'Pending')->get();
      
         // $nominees = Nominee::with(['user.councils' => function  ($query) {
         //    $query->where('department_id', auth()->user()->department_id);
         // }])->where('status', 'Pending')->get();

        if (auth()->user()->isCouncil()) {
                $nominees = Nominee::where('department_id', auth()->user()->department_id)->with('department')->where('status', 'Pending')->get();
            
        }

       if (auth()->user()->isSuperAdmin()) {
            $nominees = Nominee::with('department', 'position')->where('status', 'Pending')->get();
        }
    	
    	return view('nominees.request-file.index', compact('nominees'));
    }

    public function approved(Nominee $nominee)
    {
    	$nominee->update([

    		'status'	=> 'Approved'

    	]);


    	return redirect('/pending-request-file-nominee')->with('info', 'Request COC has been approved!');
    }

    public function removed(Nominee $nominee) 
    {
        $nominee->delete();
        return back()->with('error', 'Request has been removed!');
    }

    public function disqualified(Nominee $nominee)
    {
        $nominee->update([

            'status'    => 'Disqualified',
            'comment'   => request('comment')

        ]);


        return redirect('/pending-request-file-nominee')->with('info', 'Disqualified!');

    }

    public function manage(Nominee $nominee)
    {
        return view('nominees.request-file.show', compact('nominee'));
    }


    public function pdfTest(Nominee $nominee)
    {
        
        $pdf = \PDF::loadView('nominees.pdf.index', [
            'nominee' => $nominee,
        ]);

        $pdf->setPaper('legal','portrait');
        return $pdf->stream();
    }

    public function downloadCOR(Nominee $nominee)
    {
        $path = storage_path('app/'.$nominee->certificate_of_registration);

        return response()->download($path);
    }    

    public function downloadValidSchoolId(Nominee $nominee)
    {
        $path = storage_path('app/'.$nominee->validate_school_id);

        return response()->download($path);
    }    

    public function downloadCertificateAttendance(Nominee $nominee)
    {
        $path = storage_path('app/'.$nominee->certificate_of_attendance);

        return response()->download($path);
    }   

    public function downloadRecentPhoto(Nominee $nominee)
    {
        $path = storage_path('app/'.$nominee->recent_photo);

        return response()->download($path);
    }

    public function downloadRecomendLetter(Nominee $nominee)
    {
        $path = storage_path('app/'.$nominee->recomend_letter_from_dean);

        return response()->download($path);
    }    

    public function downloadCertificateOfGoodMoral(Nominee $nominee)
    {
        $path = storage_path('app/'.$nominee->certificate_of_good_moral);

        return response()->download($path);
    }   


    public function downloadClassOfGrade(Nominee $nominee)
    {
        $path = storage_path('app/'.$nominee->class_of_grade);

        return response()->download($path);
    }
    public function downloadTrainingSeminars(Nominee $nominee)
    {
        $path = storage_path('app/'.$nominee->training_seminars);
        return response()->download($path);
    }


}
