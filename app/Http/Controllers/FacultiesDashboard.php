<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FacultiesDashboard extends Controller
{
    public function index()
    {
    	return view('faculty-dashboard.index');
    }
}
