<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ElectionsStatus;
use App\CampusesNotification;
class ElectionNotificationsController extends Controller
{
    public function index()
    {

        $notify = ElectionsStatus::where('course_id', auth()->user()->course_id)
                                  ->orWhere('department_id', auth()->user()->department_id)
                                  ->get();

        $campusesNotification = CampusesNotification::all();
        return view('election-statuses.index', compact('notify', 'campusesNotification'));
    }

    public function show(ElectionsStatus $notif, CampusesNotification $campus)
    {   
        return view('election-statuses.show', compact('notif', 'campus'));
    }
}
