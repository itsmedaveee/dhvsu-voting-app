<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResultCourse;
use App\Course;
use App\ElectionCourse;
class ElectionCoursesController extends Controller
{
    public function index() 
    {
        if (auth()->user()->isCouncil()) {
            $elections = ElectionCourse::where('course_id', auth()->user()->course_id)->get();
        }

        if (auth()->user()->isSuperAdmin()) {
             $elections = ElectionCourse::all();
        }

        $courses   = Course::all();
        return view('events.election-course.index', compact('elections', 'courses'));
    }

    public function store(Request $request)
    {
            $this->validate(request(), [
                'election_name' => 'required',
                'course'        => 'required'
            ]);
            $course = Course::find(request('course'));
            ElectionCourse::create([
                'election_name' => request('election_name'),
                'status'        => 'On Going',
                'user_id'       => auth()->user()->id,
                'course_id'     => $course->id
            ]);

            return back()->with('success', 'Election has been added!');
    }

    public function ajaxElection(ElectionCourse $election)
    {
        return response()->json($election);
    }    

    public function getElection(ElectionCourse $election)
    {
        return response()->json($election);
    }

    public function start(ElectionCourse $electionIds)
    {
         $electionIds->update([
            'status'    => 'Starting now',
            'ended_time'   => \Carbon\Carbon::parse(request('ended_time')),
        ]);

      $electionName = ElectionCourse::first();
       $name         = $electionName->election_name;
       // $course       = $electionName->course_id;
        \App\ElectionsStatus::create([
            'name'      =>  'The department election has started !',
            'course_id' => auth()->user()->course_id
        ]);


         return response()->json($electionIds);
         //return back()->with('info', 'The election was started!');
    }    

    public function stop(ElectionCourse $electionIds)
    {

        $electionIds->update([

            'status'    => 'End Election',
        ]);

       $electionName = ElectionCourse::first();
       $name         = $electionName->election_name;
       // $course       = $electionName->course_id;
        \App\ElectionsStatus::create([
            'name'      =>  'The department election has ended !',
            'course_id' => auth()->user()->course_id
        ]);


        return response()->json($electionIds);
    }

    public function show(ElectionCourse $election)
    {
        $election->load('nominees.resultsCourse');
        $electionId = ElectionCourse::latest()->first()->id;

         $nominees = \App\Nominee::with(['positionCourse', 'election' => function ($query) {
            $query->where('status', 'Starting now');
        }])->where('status', 'Approved')->where('election_course_id', $electionId)->get();

        $grouped = $nominees->groupBy('positionCourse.position_name');

        $totalVoteUsers = ResultCourse::count();
        return view('events.election-course.show', compact('election', 'totalVoteUsers', 'nominees', 'grouped'));
    }

    public function printResults(ElectionCourse $election)
    {
        $election->load('nominees.resultsCourse');

        $electionId = ElectionCourse::latest()->first()->id;
        $nominees = \App\Nominee::with(['positionCourse', 'election' => function ($query) {
            $query->where('status', 'Starting now');
         }])->where('status', 'Approved')->where('election_course_id', $electionId)->get();

        $grouped = $nominees->groupBy('positionCourse.position_name');

        $pdf = \PDF::loadView('pdf.print-results-course', [
            'election' => $election,
            'electionId' => $electionId,
            'nominees' => $nominees,
            'grouped' => $grouped,
        ]);
         $pdf->setPaper('legal','portrait');
        return $pdf->stream();
    }

    public function edit(ElectionCourse $election)
    {
        return view('events.election-course.edit', compact('election'));
    }


    public function update(Request $request, ElectionCourse $election)
    {
            $this->validate(request(), [
                'election_name' => 'required'
            ]);

           $election->update([
                'election_name' => request('election_name'),
                'user_id'       => auth()->user()->id
            ]);

            return redirect('/election-course')->with('info', 'Election has been added!');
    }
}
