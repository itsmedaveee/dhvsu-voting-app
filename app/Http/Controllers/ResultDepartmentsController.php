<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nominee;
use App\ElectionDepartment;
use App\ResultDepartment;
class ResultDepartmentsController extends Controller
{
    public function index()
    {
        //$user = auth()->user()->load(['results.nominee.election']);
        $departmentId = auth()->user()->department_id;


        $elections = ElectionDepartment::where('department_id', $departmentId)->with(['nominees.resultsDepartment' ,'nominees' => function ($query) use ($departmentId) {
                $query->where('department_id', $departmentId);
        }])->get();
 
            $electionId = ElectionDepartment::where([
                ['department_id', auth()->user()->department_id]
            ])->latest()->first()->id;

            if (is_array($electionId) || is_object($electionId))
            {
                foreach ($electionId as $election)
                {
                    if ($election->status == 'On Going') {
                        $election = ElectionDepartment::where('status', 'On Going')->latest()->first();
                        return "On Going";
                    } else if ($election->status == 'End Election') {
                        $election = ElectionDepartment::where('status', 'End Election')->latest()->first();
                         return "has Ended";
                    } else if ($election->status == 'Starting now') {
                        $election = ElectionDepartment::where('status', 'Starting now')->latest()->first();
                        return "Starting now";
                    }
                }
            } 

         $nominees = Nominee::with(['positionDepartment', 'election' => function ($query) {
            $query->where('status', 'Starting now');
        }])->where('status', 'Approved')->where('election_department_id', $electionId)->get();
        $grouped = $nominees->groupBy('positionDepartment.position_name');

        $totalVoteUsers = ResultDepartment::where('election_department_id', $departmentId)->count();

        return view('results.results-department', [
            'elections' => $elections,
            'totalVoteUsers' => $totalVoteUsers,
            'grouped' => $grouped,
            'nominees' => $nominees,
        ]);
    }
}
