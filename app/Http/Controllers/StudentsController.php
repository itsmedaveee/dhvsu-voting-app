<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campus;
use App\Department;
use App\Course;
use App\Student;
use App\Role;
use App\User;
use App\Election;
use Carbon\Carbon;

use Yajra\DataTables\DataTables;
class StudentsController extends Controller
{
    
    public function index()
    {

         if (request()->ajax()) {
            $students = Student::with(['course', 'campus', 'department'])->where('approved', 'confirmed')->get();
            return Datatables::of($students)
                ->addColumn('action', function ($student) {
                    return view('students.partials.students-action', compact('student'));
                })
                ->make(true);
        }

        return view('students.index');
    }

    public function show(Student $student)
    {
    	$election = Election::first();
        return view('students.show', compact('student', 'election'));
    }

    public function download(Student $student) 
    {
        $path = storage_path('app/'.$student->photo);
        return response()->download($path);
    }
   
}
