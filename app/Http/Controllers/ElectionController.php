<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Result;
use App\Election;
use App\Department;

class ElectionController extends Controller
{
    public function index()
    {
        $elections = Election::with('nominees.results')->get();
    	return view('elections.index', compact('elections'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
    		'election_name'	=> 'required'
    	]);

        $started = Election::where('status', 'On Going')->count();
        if ($started >= 1) {
            return back()->with('error', "There's still ongoing election!");
        }


    	Election::create([
    		'election_name' => request('election_name'),
    		'status'		=> 'On Going',
    		'user_id'		=> auth()->user()->id
    	]);

    	return back()->with('success', 'Election has been added!');
    }

    public function edit(Election $election)
    {
        return view('elections.edit', compact('election'));
    }

    public function show(Election $election)
    {

         $election->load(['nominees.results' , 'nominees' => function ($q) {
            $q->where('status', 'Approved');
        }]);

        $election = Election::latest()->first();

        $totalVoteUsers = Result::where('election_id', $election->id)->latest()->count();

        $electionId = Election::latest()->first()->id;
        $nominees = \App\Nominee::with(['position', 'results'])->where('status', 'Approved')->where('election_id', $electionId)->get();
        $grouped = $nominees->groupBy('position.position_name');


       // $totalVoteUsers = Result::count();
        //$elections = Election::with(['positions.nominees', 'positions'])->where('status', 1)->get();
        return view('elections.show', compact('election', 'totalVoteUsers', 'nominees', 'grouped', 'electionId'));
    }

    public function update(Election $election)
    {
        $this->validate(request(), [
            'election_name' => 'required',
        ]);

        $election->update([
            'election_name' => request('election_name'),
            'user_id'       => auth()->user()->id
        ]);

        return redirect('/elections')->with('info', 'Election has been updated!');
    }


    public function ajaxElection(Election $election)
    {
        return response()->json($election);
    }    

    public function getElection(Election $election)
    {
        return response()->json($election);
    }

    public function startElection(Election $electionIds)
    {
        $started = Election::where('status', 'Starting now')->count();
        if ($started >= 1) {
            return response()->json([
                'message' => "There's still ongoing election!"
            ]);
        }
 
        $electionIds->update([
            'status'    => 'Starting now',
            'ended_time'   => \Carbon\Carbon::parse(request('ended_time')),
        ]);

        $electionName = Election::first();
        $name         = $electionName->election_name;
        \App\CampusesNotification::create([
            'name'      =>  'The  election of campuses has started !',
        ]);

        return response()->json([
            'message' => 'Starting Election!'
        ]);
         
         //eturn back()->with('info', 'The election was started!');
    }    

    public function stopElection(Election $electionIds)
    {
  
         $electionIds->update([
            'status'    => 'End Election',
        ]);

      $electionName = Election::first();
        $name         = $electionName->election_name;
        \App\CampusesNotification::create([
             'name'      =>  'The  election of campuses has ended !',
        ]);

         return response()->json($electionIds);
         
         //return back()->with('info', 'The election was started!');
    }


    public function printResults(Election $election)
    {

        $election->load(['nominees.results' , 'nominees' => function ($q) {
            $q->where('status', 'Approved');
        }]);

        $electionId = Election::latest()->first()->id;

        $nominees = \App\Nominee::with(['position', 'results'])->where('status', 'Approved')->where('election_id', $electionId)->latest()->get();

        $totalVoteUsers = Result::with('department')->get();
    
        $totalParticipant = Result::where('election_id', $electionId)->select('student_id')->groupBy('student_id')->get()->count();
       
        // $totalParticipant = \App\Student::whereHas('results', function($query){
        //  $query->select('student_id', \DB::raw('count(*) as total'))->groupBy('student_id');
        // })->count();
        // return $totalParticipant;
        // $total =  Result::withCount('students')->count();
     

        // $election = Election::first();
        // $totalParticipant = Result::where('election_id', $election->id)->count();
        //return $totalVoteUsers;
        // foreach ($totalVoteUsers as $key => $vote) {
        //     $ccs = $vote->department->where('code', 'CCS')->count();

        // }

        $grouped = $nominees->groupBy('position.position_name');
        $pdf = \PDF::loadView('pdf.print-results', [
            'election' => $election,
            'nominees' => $nominees,
            'grouped' => $grouped,
            'totalParticipant' => $totalParticipant,
            'totalVoteUsers' => $totalVoteUsers,
        ]);
         $pdf->setPaper('legal','landscape');
        return $pdf->stream();
    }

    public function removed(Election $election)
    {
        $election->delete();
        return back()->with('error', 'Election has been removed!');
    }
}
