<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use Carbon\Carbon;
class RegistersController extends Controller
{
    public function index()
    {
    	$registrations = Registration::all();
    	return view('lock-in-period.index', compact('registrations'));
    }

    public function store(Request $request) 
    {
    	$registration = Registration::create([
    		'started_at'	=> Carbon::parse(request('started_at')),
    		'ended_at'	=> Carbon::parse(request('ended_at')),
    		'status'		=> 'Published'
    	]);
    	
 		return back()->with('success', 'Publish success!');
    }

    public function ajaxClose(Registration $registrationId) 
    {
    	return response()->json($registrationId);
    }

    public function close(Registration $registrationId) 
    {
    	$registrationId->update([
    		'status'	=> 'Closed'
    	]);

    	return response()->json($registrationId);
    }
}
