<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;

class AnnouncementsController extends Controller
{   
    public function index()
    {
         $user = auth()->user()->load('myAnnouncements');
          $announcements = $user->myAnnouncements;

        return view('announcements.index', compact('announcements'));
    }

    public function store(Request $request)
    {
            $this->validate(request(), [
                'body'  => 'required'
            ]);

           $announcement = auth()->user()->myAnnouncements()->create([
                'body'  => $request->body
           ]);
           if (auth()->user()->isSuperAdmin()) {
                if (request('selected') == 'myStudentDepartment') {
                    $department = \App\Department::with(['students.user' => function ($query) {
                             $query->where('campus_id', auth()->user()->userable->campus_id);
                        }])->where('id', auth()->user()->userable->department_id)->first(); 
                      $announcement->users()->sync($department->students->pluck('user_id'));
               } 
           } 

           if (auth()->user()->isSuperAdmin()) {
               if (request('selected') == 'allCampuses') {
                    $campus = \App\Campus::with(['students.user' => function ($query) {
                                          $query->where('campus_id', auth()->user()->userable->campus_id);
                                          }])->where('id', auth()->user()->userable->campus_id)->first();
                     $announcement->users()->sync($campus->students->pluck('user_id'));
                }
           }

           // if (auth()->user()->isCouncil()) {
           //        if (request('selected') == 'myStudentDepartment') {
           //              //$councilor = auth()->user()->userable->load('students.user');
           //              $department = \App\Department::with(['students.user' => function ($query) {
           //                   $query->where('campus_id', auth()->user()->userable->campus_id);
           //              }])->where('id', auth()->user()->userable->department_id)->first(); 
           //             //$announcement->users()->sync($department->students->pluck('user_id'));
           //             $announcement->users()->sync($department->students->pluck('user_id'));
           //     }
           //  }          
      
            return back()->with('success', 'Announcement has been added!');
    }

    public function show(Announcement $announcement)
    {
     
          $user = auth()->user()->load('announcements');
 

        return view('announcements.show', compact('announcement' ,'user'));
    }
}
