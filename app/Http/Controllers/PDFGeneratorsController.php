<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nominee;


class PDFGeneratorsController extends Controller
{
    public function index(Nominee $nominee)
    {

        $pdf        = \PDF::loadView('pdf.index', [
            'nominee'    => $nominee,        
        ]);
        
        $pdf->setPaper('legal','portrait'); 
        return $pdf->stream(); 
    }
}
