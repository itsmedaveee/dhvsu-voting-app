<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ElectionDepartment;
use App\ResultDepartment;
use App\Department;
class ElectionDepartmentController extends Controller
{
    public function index()
    {

        if (auth()->user()->isCouncil()) {
            $elections = ElectionDepartment::where('department_id', auth()->user()->department_id)->with(['department'])->get();
        }


        if (auth()->user()->isSuperAdmin()) {
           $elections = ElectionDepartment::with(['department'])->get();
        }
    	

        $departments = Department::all();
    	return view('events.election-department.index', compact('elections', 'departments'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
    		'election_name'	=> 'required',
            'department'    => 'required'
    	]);

        $department = Department::find(request('department'));

    	$electionDepartment = ElectionDepartment::create([
            'department_id' => $department->id,
    		'election_name' => request('election_name'),
    		'status'		=> 'On Going',
    		'user_id'		=> auth()->user()->id
    	]);

        // $electionDepartment->department()->associate($department)->save();

    	return back()->with('success', 'Election has been added!');
    }

    public function edit(ElectionDepartment $election)
    {
    	return view('events.election-department.edit', compact('election'));
    }
    public function update(ElectionDepartment $election)
    {
        $this->validate(request(), [
            'election_name' => 'required'
        ]);

        $election->update([
            'election_name' => request('election_name'),
            'user_id'       => auth()->user()->id
        ]);

        return redirect('/election-department')->with('info', 'Election has been updated!');
    }


	public function ajaxElection(ElectionDepartment $election)
    {
        return response()->json($election);
    }    

    public function getElection(ElectionDepartment $election)
    {
        return response()->json($election);
    }


    public function start(ElectionDepartment $electionIds)
    {
  
         $electionIds->update([

            'status'    => 'Starting now',
            'ended_time'   => \Carbon\Carbon::parse(request('ended_time')),
        ]);

       $electionName     = ElectionDepartment::first();
 
        $name               = $electionName->election_name; 
     
        \App\ElectionsStatus::create([
            'name'      =>  'The college election  has started !',
            'department_id' => auth()->user()->department_id
        ]);  

         return response()->json($electionIds);
         
        // return back()->with('info', 'The election was started!');
    }    

    public function stop(ElectionDepartment $electionIds)
    {

        $electionIds->update([

            'status'    => 'End Election',
        ]);

       // $electionName     = ElectionDepartment::first();
       //  $name               = $electionName->election_name; 
        \App\ElectionsStatus::create([
           'name'      =>  'The college election  has started !',
            'department_id' => auth()->user()->department_id
        ]);  
        return response()->json($electionIds);
    }

    public function show(ElectionDepartment $election)
    {

        $election->load('nominees.results')->where('status', 'End Election');

        $electionId = ElectionDepartment::where([
                ['department_id', auth()->user()->department_id]
            ])->latest()->first()->id;

            // if (is_array($electionId) || is_object($electionId))
            // {
            //     foreach ($electionId as $election)
            //     {
            //         if ($election->status == 'On Going') {
            //             $election = ElectionDepartment::where('status', 'On Going')->latest()->first();
            //             return "On Going";
            //         } else if ($election->status == 'End Election') {
            //             $election = ElectionDepartment::where('status', 'End Election')->latest()->first();
            //              return "has Ended";
            //         } else if ($election->status == 'Starting now') {
            //             $election = ElectionDepartment::where('status', 'Starting now')->latest()->first();
            //             return "Starting now";
            //         }
            //     }
            // } 

         $nominees = \App\Nominee::with(['positionDepartment', 'election' => function ($query) {
            $query->where('status', 'Starting now');
        }])->where('status', 'Approved')->where('election_department_id', $electionId)->get();
        $grouped = $nominees->groupBy('positionDepartment.position_name');
        
        $totalVoteUsers = ResultDepartment::count();
    	return view('events.election-department.show', compact('election', 'totalVoteUsers', 'electionId', 'nominees', 'grouped'));
    }


	public function printResults(ElectionDepartment $election)
    {
        $election->load('nominees.resultsDepartment');
        $electionId = ElectionDepartment::where([
                ['department_id', auth()->user()->department_id]
            ])->latest()->first()->id;

            if (is_array($electionId) || is_object($electionId))
            {
                foreach ($electionId as $election)
                {
                    if ($election->status == 'On Going') {
                        $election = ElectionDepartment::where('status', 'On Going')->latest()->first();
                        return "On Going";
                    } else if ($election->status == 'End Election') {
                        $election = ElectionDepartment::where('status', 'End Election')->latest()->first();
                         return "has Ended";
                    } else if ($election->status == 'Starting now') {
                        $election = ElectionDepartment::where('status', 'Starting now')->latest()->first();
                        return "Starting now";
                    }
                }
            } 

         $nominees = \App\Nominee::with(['positionDepartment', 'election' => function ($query) {
            $query->where('status', 'Starting now');
        }])->where('status', 'Approved')->where('election_department_id', $electionId)->get();
        $grouped = $nominees->groupBy('positionDepartment.position_name');
        
        $pdf = \PDF::loadView('pdf.print-results-department', [
            'election' => $election,
            'nominees' => $nominees,
            'grouped' => $grouped,
        ]);
         $pdf->setPaper('legal','portrait');
        return $pdf->stream();
    }

    public function destroy(ElectionDepartment $election)
    {
        $election->delete();
        return back()->with('error', 'Election has been remove!');
    }

    public function removed(ElectionDepartment $election)
    {
        $election->delete();
        return back()->with('error', 'Election Department has been removed!');
    }
}
