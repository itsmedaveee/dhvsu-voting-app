<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nominee;
use App\Result;
use App\ResultDepartment;
use App\ResultCourse;
use App\ResultPageant;
use App\Comment;

class VotesController extends Controller
{
    public function store(Request $request)
    {
        $resultExist = Result::where([
            ['user_id', auth()->id()],
            ['election_id', $request->election_id]
        ])->count();

        if ($resultExist) {
            return back()->with([
                'error' => 'You have already voted!',
            ]);
        }

       $electionTime = \App\Election::first();
        $mytime = \Carbon\Carbon::now();
        $mytime->timezone('Asia/Manila');
        $mytime->format('h:i A');
        $endedTime = $electionTime->ended_time;
        if ($mytime > $endedTime) {
           return back()->with('error', 'The Election has been Ended!');
        }

    	$student = auth()->user()->userable;
    	$nominees = Nominee::find($request->nominee);
        $votedNominees = [];
        $comment = auth()->user()->comments()->create([
         'comment' => request('comment')
        ]);
        foreach ($nominees as $nominee) {
            $votedNominee = Result::with('nominee')->where([
                ['user_id', auth()->id()],
                ['nominee_id', $nominee->id],
            ])->first();
            if (!$votedNominee) {
                $nominee->results()->create([
                    'election_id' => $request->election_id,
                    'user_id'       => auth()->user()->id,
                    'comment_id'    => $comment->id,
                    'student_id'    => $student->id,
                    'department_id'   => auth()->user()->department_id
                ]);
            }
            if ($votedNominee) {
                $votedNominees[] = $votedNominee;
            }
        }
        if ($votedNominees) {
            return back()->with([
                // 'error' => 'You already voted!',
                'votedNominees' => $votedNominees,
            ]);
        }
        
        return redirect('/student/home')->with('status', 'Test');
    }

    public function votesDepartment(Request $request)
    {
            $resultExist = ResultDepartment::where([
                ['user_id', auth()->id()],
                ['election_department_id', $request->election_department_id]
            ])->count();

            if ($resultExist) {
                return back()->with([
                    'error' => 'You have already voted!',
                ]);
            }

            $electionTime = \App\ElectionDepartment::first();
                $mytime = \Carbon\Carbon::now();
                $mytime->timezone('Asia/Manila');
                $mytime->format('h:i A');
                $endedTime = $electionTime->ended_time;
                if ($mytime > $endedTime) {
                   return back()->with('error', 'The Election has been Ended!');
            }

            $student = auth()->user()->userable;
            $nominees = Nominee::find($request->nominee);

            $votedNominees = [];
            $comment = auth()->user()->comments()->create([
             'comment' => request('comment')
            ]);
            foreach ($nominees as $nominee) {
                $votedNominee = ResultDepartment::with('nominee')->where([
                    ['user_id', auth()->id()],
                    ['nominee_id', $nominee->id],
                    ['election_department_id', $request->election_department_id],
                ])->first();

                if (!$votedNominee) {
                    $nominee->resultsDepartment()->create([
                        'user_id'       => auth()->user()->id,
                        'comment_id'    => $comment->id,
                        'student_id'    => $student->id,
                        'election_department_id' => $request->election_department_id
                    ]);
                }
                if ($votedNominee) {
                    $votedNominees[] = $votedNominee;
                }
            }
            if ($votedNominees) {
                return back()->with([
                    // 'error' => 'You already voted!',
                    'votedNominees' => $votedNominees,
                ]);
            }
            
            return redirect('/student/home')->with('status', 'Test');
     }

     public function votesCourse(Request $request)
     {
         $resultExist = ResultCourse::where([
                ['user_id', auth()->id()],
                ['election_course_id', $request->election_course_id]
            ])->count();

            if ($resultExist) {
                return back()->with([
                    'error' => 'You have already voted!',
                ]);
            }

            $electionTime = \App\ElectionCourse::first();
                $mytime = \Carbon\Carbon::now();
                $mytime->timezone('Asia/Manila');
                $mytime->format('h:i A');
                $endedTime = $electionTime->ended_time;
                if ($mytime > $endedTime) {
                   return back()->with('error', 'The Election has been Ended!');
            }


            $student = auth()->user()->userable;
            $nominees = Nominee::find($request->nominee);
            $votedNominees = [];
            $comment = auth()->user()->comments()->create([
             'comment' => request('comment')
            ]);
            foreach ($nominees as $nominee) {
                $votedNominee = ResultCourse::with('nominee')->where([
                    ['user_id', auth()->id()],
                    ['nominee_id', $nominee->id],
                    ['election_course_id', $request->election_course_id],
                ])->first();
                if (!$votedNominee) {
                    $nominee->resultsCourse()->create([
                        'user_id'       => auth()->user()->id,
                        'comment_id'    => $comment->id,
                        'student_id'    => $student->id,
                        'election_course_id'    => $request->election_course_id
                    ]);
                }
                if ($votedNominee) {
                    $votedNominees[] = $votedNominee;
                }
            }
            if ($votedNominees) {
                return back()->with([
                    // 'error' => 'You already voted!',
                    'votedNominees' => $votedNominees,
                ]);
            }
            
          return redirect('/student/home')->with('status', 'Test');     
     }

     public function votesPageant(Request $request)
     {

             // $resultExist = Pageant::where([
             //        ['user_id', auth()->id()],
             //        ['pageant_id', $request->pageant_id]
             //    ])->count();

             //    if ($resultExist) {
             //        return back()->with([
             //            'error' => 'You have already voted!',
             //        ]);
             //    }


            $electionTime = \App\Pageant::first();
                $mytime = \Carbon\Carbon::now();
                $mytime->timezone('Asia/Manila');
                $mytime->format('h:i A');
                $endedTime = $electionTime->ended_time;
                if ($mytime > $endedTime) {
                   return back()->with('error', 'The Event has been Ended!');
            }



            $student = auth()->user()->userable;
            $nominees = Nominee::find($request->nominee);
            $votedNominees = [];
            $comment = auth()->user()->comments()->create([
             'comment' => request('comment')
            ]);
            foreach ($nominees as $nominee) {
                $votedNominee = ResultPageant::with('nominee')->where([
                    ['user_id', auth()->id()],
                    ['nominee_id', $nominee->id],
                ])->first();
                if (!$votedNominee) {
                    $nominee->resultsPageant()->create([
                        'user_id'       => auth()->user()->id,
                        'comment_id'    => $comment->id,
                        'student_id'    => $student->id
                    ]);
                }
                if ($votedNominee) {
                    $votedNominees[] = $votedNominee;
                }
            }
            if ($votedNominees) {
                return back()->with([
                    // 'error' => 'You already voted!',
                    'votedNominees' => $votedNominees,
                ]);
            }
           return redirect('/student/home')->with('status', 'Test');
     }
}

