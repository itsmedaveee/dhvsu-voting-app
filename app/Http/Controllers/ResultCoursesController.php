<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ElectionCourse;
use App\ResultCourse;
class ResultCoursesController extends Controller
{
    public function index()
    {
    	$elections = ElectionCourse::with(['nominees.resultsCourse'])->where('status', 'End Election')->get();
        $electionId = ElectionCourse::latest()->first()->id;

        $nominees = \App\Nominee::with(['positionCourse', 'election' => function ($query) {
         $query->where('status', 'Starting now');
        }])->where('status', 'Approved')->where('election_course_id', $electionId)->latest()->get();

        $grouped = $nominees->groupBy('positionCourse.position_name');

    	 $totalVoteUsers = ResultCourse::count();

    	 return view('results.results-course', compact('totalVoteUsers', 'elections', 'electionId', 'nominees' ,'grouped'));
    }
}
