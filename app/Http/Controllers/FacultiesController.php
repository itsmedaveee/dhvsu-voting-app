<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faculty;
use App\Department;
use App\Campus;
use App\Role;
use App\User;

class FacultiesController extends Controller
{
    public function index()
    {

    	$faculties = Faculty::all();

		$departments  = Department::pluck('code', 'id');
		$campuses     = Campus::pluck('name', 'id');

    	return view('faculties.index', compact('faculties' , 'departments' , 'campuses'));
    }

    public function store(Request $request)
    {
		$this->validate(request(),[

            'firstname'     => 'required',
            'lastname'      => 'required',
            'middlename'    => 'required',
            'username'      => 'required',
            'password'      => 'required',
            'campuses'      => 'required',
            'department'    => 'required',
            'email'         => 'required|unique:users,email',

        ]);

		$department  = Department::find(request('department'));
		$campus      = Campus::find(request('campuses'));

		$role = Role::where('name', 'Faculty')->first();

		$user = User::create([
			'username'           => request('username'),
			'password'           => bcrypt(request('password')),
			'email'              => request('email'),
			'approved'           => 1

		]);

		$user->role()->associate($role)->save();


    	$faculty = Faculty::create([
    		'firstname'	=> request('firstname'),
    		'middlename'	=> request('middlename'),
    		'lastname'	=> request('lastname'),
    		'email'	=> request('email'),
    	]);

    	$user->userable()->associate($faculty)->save();
    	$faculty->user()->associate($user)->save();

    	$user->campus()->associate($campus)->save();
    	$faculty->department()->associate($department)->save();
    	$faculty->campus()->associate($campus)->save();

    	return back()->with('success', 'Faculty: Success Added!');
    }

    public function edit(Faculty $faculty)
    {

		$departments  = Department::pluck('code', 'id');
		$campuses     = Campus::pluck('name', 'id');

    	return view('faculties.edit', compact('faculty', 'departments', 'campuses'));
    }

    public function update(Request $request, Faculty $faculty)
    {
    	$this->validate(request(),[

            'firstname'     => 'required',
            'lastname'      => 'required',
            'middlename'    => 'required',
            'username'      => 'required',
            'password'      => 'required',
            'campuses'      => 'required',
            'department'    => 'required',
            'email'         => 'required|unique:users,email',
			'password'      => 'required_if:password,value|confirmed',


        ]);

		$department  = Department::find(request('department'));
		$campus      = Campus::find(request('campuses'));

		$role = Role::where('name', 'Faculty')->first();


		 if ($request->password != null) {
            
            auth()->user()->update([

                'password'     => bcrypt(request('password')),

            ]);
        }

		$user = $faculty->user;

		$user->update([
			'username'           => request('username'),
			'email'              => request('email'),
			'approved'           => 1

		]);

		$user->role()->associate($role)->save();


    	$faculty->update([
    		'firstname'	=> request('firstname'),
    		'middlename'	=> request('middlename'),
    		'lastname'	=> request('lastname'),
    		'email'	=> request('email'),
    	]);

    	$user->userable()->associate($faculty)->save();
    	$faculty->user()->associate($user)->save();

    	$user->campus()->associate($campus)->save();
    	$faculty->department()->associate($department)->save();
    	$faculty->campus()->associate($campus)->save();

    	return redirect('/faculties')->with('info', 'Faculty: Success Updated!');
    }
}
