<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CouncilDashboardController extends Controller
{
    public function index()
    {
    	// $departments = \App\Department::with('nominees.results')->withCount('resultNominee')->get();
     //    $elections  = \App\Election::with('nominees.results')->get();
     //    $result = [];
     //    $dept = [];
     //    foreach ($departments as $department) {
     //        //$dept[] = $department->load('nominees.results')->pluck('code','id');
     //        //$result[] = $department->nominees->count();
     //        $test[] = $department->load('nominees')->loadCount('resultNominee');
     //        $result[] = $department->result_nominee_count;
     //        $dept[] = $department->code;
     //       // $result[] = $data->count();
     //    }

     //     $courses = \App\Course::with(['resultsDepartment'])->get();

     //    $resultDepartment = [];
     //    $deptCourse = [];
     //    foreach ($courses as $course) {
     //        $test1[] = $course->load('nominees')->loadCount('resultsDepartment');
     //        $resultDepartment[] = $course->results_department_count;
     //        $deptCourse[] = $course->code;
     //    }

     //    $events = \App\Pageant::with(['nominees.resultsPageant'])->withCount('pageantEventResults')->get();
     //    $resultEvent = [];
     //    $eventsPageant = [];
     //    foreach ($events as $event) {
     //        $test2[] = $event->load('nominees')->loadCount('pageantEventResults');
     //        $resultEvent[] = $event->pageant_event_results_count;
     //        $eventsPageant[] = $event->event_name;
     //    }


             $departments = \App\Election::with('electionCampuses')->withCount('electionCampuses')->get();


            $elections  = \App\Election::with('electionCampuses')->get();
            $result = [];
            $dept = [];
            foreach ($departments as $department) {
                //$dept[] = $department->load('nominees.resultsDepartment')->pluck('code','id');
                //$result[] = $department->nominees->count();
                $test[] = $department->load('nominees')->loadCount('electionCampuses');
                $result[] = $department->election_campuses_count;
                $dept[] = $department->election_name;
               // $result[] = $data->count();
            }

            $courses = \App\ElectionDepartment::with(['electionDepartments'])->withCount('electionDepartments')->get();

            $resultDepartment = [];
            $deptCourse = [];
            foreach ($courses as $course) {
                $test1[] = $course->load('nominees')->loadCount('electionDepartments');
                $resultDepartment[] = $course->election_departments_count;
                $deptCourse[] = $course->election_name;
            }



            $electionCourses = \App\ElectionCourse::with('electionCourses')->withCount('electionCourses')->get();

            $resultCourse = [];
            $electionCourseName = [];

            foreach ($electionCourses as $electionCourse) {

                $query[] = $electionCourse->load('nominees')->loadCount('electionCourses');
                $resultCourse[] = $electionCourse->election_courses_count;
                $electionCourseName[] = $electionCourse->election_name; 
            }


            $events = \App\Pageant::with(['nominees.resultsPageant'])->withCount('pageantEventResults')->get();
            $resultEvent = [];
            $eventsPageant = [];
            foreach ($events as $event) {
                $test2[] = $event->load('nominees')->loadCount('pageantEventResults');
                $resultEvent[] = $event->pageant_event_results_count;
                $eventsPageant[] = $event->event_name;
            }

        return view('council-dashboard.index', compact('elections'))->with('resultEvent',json_encode($resultEvent,JSON_NUMERIC_CHECK))
                                                 ->with('resultCourse',json_encode($resultCourse,JSON_NUMERIC_CHECK))
                                                 ->with('electionCourseName',json_encode($electionCourseName,JSON_NUMERIC_CHECK))
                                                 ->with('eventsPageant',json_encode($eventsPageant,JSON_NUMERIC_CHECK))
                                                 ->with('deptCourse',json_encode($deptCourse,JSON_NUMERIC_CHECK))
                                                 ->with('resultDepartment',json_encode($resultDepartment,JSON_NUMERIC_CHECK))
                                                 ->with('dept',json_encode($dept,JSON_NUMERIC_CHECK))->with('result',json_encode($result,JSON_NUMERIC_CHECK));
    }
    
}
