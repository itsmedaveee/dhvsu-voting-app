<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campus extends Model
{
    protected $fillable = [

        'name',
        'location'
    ];


   public function students()
   {
      return $this->hasMany(Student::class);
   }

   public function users()
   {
        return $this->hasMany(User::class);
   }

   public function nominees()
   {
      return $this->hasMany(Nominee::class);
   }

   // public function departments()
   // {
   //    return $this->hasMany(Department::class);
   // }

}
