<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pageant extends Model
{
    protected $fillable  = [
        'event_name',
        'user_id',
        'department_id',
        'status',
        'ended_time'
    ];

    public function nominees()
    {
        return $this->hasMany(Nominee::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function pageantEventResults()
    {
        return $this->hasManyThrough(ResultPageant::class, Nominee::class);
    }
}
