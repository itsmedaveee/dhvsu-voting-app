<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nominee extends Model
{
    protected $fillable = [

    	'user_id',
    	'position_id',
        'position_department_id',
        'course_id',
    	'campus_id',
        'election_id',
        'gender',
        'usc',
        'election_department_id',
        'pageant_id',
    	'department_id',
        'firstname',
        'middlename',
        'lastname',
        'nationality',
        'contact_no',
        'certified',
        'title',
        'place_of_birth',
        'birthdate', 
        'comment',
        'religion',
        'certificate_of_registration',
        'validate_school_id',
        'certificate_of_attendance',
        'recent_photo',
        'recomend_letter_from_dean',
        'certificate_of_good_moral',
        'class_of_grade',
        'avatar',
        'reason_for_candidacy',
        'average_last_semister',
        'previous_failed_subject',
        'award_honors_received',
        'extra_curricular_activities',
        'training_seminars',
        'date_filled_out',
        'status',
        'propaganda',
        'categories',
        'remark',
        'partyname'
    ];


    public function position()
    {
    	return $this->belongsTo(Position::class);
    }         
    public function program()
    {
        return $this->belongsTo(Program::class);
    }         

    public function student()
    {
        return $this->belongsTo(Student::class);
    }       
    public function positionDepartment()
    {
        return $this->belongsTo(PositionDepartment::class);
    }    
    
    public function positionCourse()
    {
        return $this->belongsTo(PositionCourse::class);
    }    
     
    public function electionDepartment()
    {
        return $this->belongsTo(ElectionDepartment::class);
    }    

    public function electionCourse()
    {
        return $this->belongsTo(ElectionCourse::class);
    }
    public function department()
    {
    	return $this->belongsTo(Department::class);
    }    

    public function pageant()
    {
        return $this->belongsTo(Pageant::class);
    }

    public function campus()
    {
    	return $this->belongsTo(Campus::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function partylist()
    {
        return $this->belongsTo(Partylist::class);
    }

    public function election()
    {
        return $this->belongsTo(Election::class);
    }

    public function results()
    {
        return $this->hasMany(Result::class);
    }    
    public function resultsDepartment()
    {
        return $this->hasMany(ResultDepartment::class);
    }    
    public function resultsCourse()
    {
        return $this->hasMany(ResultCourse::class);
    }    



    public function course()
    {
        return $this->belongsTo(Course::class);
    }


    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    // public function gender()
    // {
    //     return $this->belongsTo(Gender::class);
    // } 

    public function resultsPageant()
    {
        return $this->hasMany(ResultPageant::class);
    }
 
    public function positions()
    {
        return $this->hasManyThrough(Position::class, Nominee::class);
    }
}
