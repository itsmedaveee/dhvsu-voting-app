<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperAdmin extends Model
{
    protected $fillable = [
		'image',
		'email',
		'firstname',
		'middlename',
		'lastname',
		'user_id',

    ];

    public function users()
    {
    	return $this->morphMany(User::class,'userable');
    }

    public function campus()
    {
    	return $this->belongsTo(Campus::class);
    }

	public function account()
    {
    	return $this->belongsTo(User::class,'user_id');
    }

}
