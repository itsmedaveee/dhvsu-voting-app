<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElectionCourse extends Model
{
    
        protected $fillable = [
            'election_name',
            'user_id',
            'course_id',
            'status',
            'ended_time',
        ];

        public function user()
        {
            return $this->belongsTo(User::class);
        }
        public function course()
        {
            return $this->belongsTo(Course::class);
        }

        public function positions()
        {
            return $this->hasMany(Position::class);
        }

        public function nominees()
        {
            return $this->hasMany(Nominee::class);
        }

        // public function students()
        // {
        //     return $this->hasMany(Student::class);
        // }


        public function electionCourses()
        {
            return $this->hasManyThrough(ResultCourse::class, Nominee::class);
        }
}
