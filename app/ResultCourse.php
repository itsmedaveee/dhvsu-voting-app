<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultCourse extends Model
{
     protected $fillable = [
        'user_id',
        'nominee_id',
        'comment_id',
        'election_course_id',
        'student_id'
    ];

    public function nominee()
    {
        return $this->belongsTo(Nominee::class);
    }   

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }    
    
    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }
}
