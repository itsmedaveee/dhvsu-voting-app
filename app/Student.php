<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'student_no',
        'generate_id',
        'program_id',
        'generate_qr_code',
        'user_id',
        'course_id',
        'department_id',
        'campus_id',
        'firstname',
        'middlename',
        'lastname',
        'address',
        'birth_date',
        'email',
        'year',
        'section',
        'photo',
        'contact_no',
        'image',
        'approved',
        'nationality',
        'place_of_birth',
        'religion',
        'gender_id'
    ];  

    protected $dates = ['birth_date'];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }   
    public function department()
    {
        return $this->belongsTo(Department::class);
    }       
    public function program()
    {
        return $this->belongsTo(Program::class);
    }   

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function campus()
    {
        return $this->belongsTo(Campus::class);
    }    
    public function course()
    {
        return $this->belongsTo(Course::class);
    }    
    public function user()
    {
        return $this->belongsTo(User::class);
    }    
    public function student()
    {
        return $this->belongsTo(Student::class);
    }    

   public function results()
   {
        return $this->hasMany(Result::class);
   }

   public function registrations()
   {
        return $this->belongsToMany(Registration::model);
   }

   public function resultsEvent()
   {
        return $this->hasMany(ResultPageant::class);
   }

   public function electionStatus()
   {
        $elections = Election::where('status', 'Starting now')->latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = Election::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = Election::where('status', 'End Election')->latest()->first();
                     return "End Election";
                } else if ($elections->status == 'Starting now') {
                    $elections = Election::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }   


   public function electionEndStatus()
   {
        $elections = Election::where('status', 'End Election')->latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = Election::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = Election::where('status', 'End Election')->latest()->first();
                     return "End Election";
                } else if ($elections->status == 'Starting now') {
                    $elections = Election::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }

   public function statusesElection()
   {
         $elections = Election::latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = Election::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = Election::where('status', 'End Election')->latest()->first();
                     return "has Ended";
                } else if ($elections->status == 'Starting now') {
                    $elections = Election::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }  

 //FOR STARTING TIME
   public function startingTimeElection()
   {
        $elections = Election::where('status', 'Starting now')->latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = Election::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = Election::where('status', 'End Election')->latest()->first();
                     return "has Ended";
                } else if ($elections->status == 'Starting now') {
                    $elections = Election::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }

   public function electionDepartmentStatus()
   {
        $elections = ElectionDepartment::where('status', 'Starting now')->latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = ElectionDepartment::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = ElectionDepartment::where('status', 'End Election')->latest()->first();
                     return "End Election";
                } else if ($elections->status == 'Starting now') {
                    $elections = ElectionDepartment::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }    


  public function departmentStatus()
   {
        $elections = ElectionDepartment::latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = ElectionDepartment::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = ElectionDepartment::where('status', 'End Election')->latest()->first();
                     return " has Ended";
                } else if ($elections->status == 'Starting now') {
                    $elections = ElectionDepartment::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }   


   public function electionCoursetStatus()
   {
        $elections = ElectionCourse::where('status', 'Starting now')->latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = ElectionCourse::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = ElectionCourse::where('status', 'End Election')->latest()->first();
                     return "End Election";
                } else if ($elections->status == 'Starting now') {
                    $elections = ElectionCourse::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }   


   public function courseStatus()
   {
        $elections = ElectionCourse::latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = ElectionCourse::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = ElectionCourse::where('status', 'End Election')->latest()->first();
                     return " has Ended";
                } else if ($elections->status == 'Starting now') {
                    $elections = ElectionCourse::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }   

   public function courseEndStatus()
   {
        $elections = ElectionCourse::where('status', 'End Election')->latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = ElectionCourse::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Election') {
                    $elections = ElectionCourse::where('status', 'End Election')->latest()->first();
                     return " has Ended";
                } else if ($elections->status == 'Starting now') {
                    $elections = ElectionCourse::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }   


   public function pageantEventStatus()
   {
        $elections = Pageant::where('status', 'Starting now')->latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = Pageant::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Events') {
                    $elections = Pageant::where('status', 'End Events')->latest()->first();
                     return "End Event";
                } else if ($elections->status == 'Starting now') {
                    $elections = Pageant::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }

     public function eventStatus()
   {
        $elections = Pageant::latest()->first();
        if (is_array($elections) || is_object($elections))
        {
            foreach ($elections as $election)
            {
                if ($elections->status == 'On Going') {
                    $elections = Pageant::where('status', 'On Going')->latest()->first();
                    return "On Going";
                } else if ($elections->status == 'End Events') {
                    $elections = Pageant::where('status', 'End Events')->latest()->first();
                     return "End Event";
                } else if ($elections->status == 'Starting now') {
                    $elections = Pageant::where('status', 'Starting now')->latest()->first();
                    return "Starting now";
                }
            }
        } 
   }
}
