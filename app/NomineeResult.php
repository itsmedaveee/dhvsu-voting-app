<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NomineeResult extends Model
{
    protected $fillable = [
        'user_id',
        'result_id',
        'comment'
    ];
}
