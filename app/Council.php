<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Council extends Model
{
    protected $fillable = [
        'user_id',
        'department_id',
        'campus_id',
        'email',
        'image',
        'firstname',
        'middlename',
        'lastname',

    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }  

    public function user()
    {
        return $this->belongsTo(User::class);
    }    

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function campus()
    {
        return $this->belongsTo(Campus::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
