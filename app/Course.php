<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    protected $fillable = [

        'department_id',
        'code',
        'title'
    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function nominees()
    {
        return $this->hasMany(Nominee::class);
    }    

    public function resultsCourse()
    {
        return $this->hasManyThrough(ResultCourse::class, Student::class);
    }

    public function programs()
    {
        return $this->hasMany(Program::class);
    }
}
