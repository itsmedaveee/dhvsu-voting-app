<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampusesNotification extends Model
{
    protected $fillable = [
        'name'
    ];

}
