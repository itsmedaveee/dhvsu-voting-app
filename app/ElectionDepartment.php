<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElectionDepartment extends Model
{
    protected $fillable = [
    	'election_name',
        'department_id',
    	'user_id',
        'ended_time',
    	'status'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function nominees()
    {
        return $this->hasMany(Nominee::class);
    }

    public function electionDepartments() 
    {
        return $this->hasManyThrough(ResultDepartment::class, Nominee::class);
    }

  
}
