<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $guarded = [];

    public function course()
    {
    	return $this->belongsTo(Course::class);
    }      

    public function department()
    {
    	return $this->belongsTo(Department::class);
    }    

    public function students()
    {
    	return $this->hasMany(Student::class);
    }
}
