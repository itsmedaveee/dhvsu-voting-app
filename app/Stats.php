<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
class Stats extends Model
{
	public function totalUsers()
	{
		return User::count(); 
	}
	public function totalDepartments()
	{
		return Department::count();
	}	

	public function totalCampuses()
	{
		return Campus::count();
	}	
	public function totalCourses()
	{
		return Course::count();
	}	
	public function totalPartyName()
	{
		return Partylist::count();
	}	

	public function totalNomineeForDepartment()
	{
		return Nominee::where('department_id', auth()->user()->id)->where('status', 'Approved')->count();
	}	

	public function totalPositionInDepartment()
	{
		return PositionDepartment::count();
	}

	public function electionStatuses()
	{
		return ElectionsStatus::where('course_id', auth()->user()->course_id)
								->orWhere('department_id', auth()->user()->department_id)
								->count();
	}

	public function campusStatus() 
	{
		return CampusesNotification::count();
	}

	public function totalElectionsDepartment() 
	{
		return ElectionDepartment::where('department_id', auth()->user()->department_id)->count();
	}


	public function totalNominees()
	{
			return Nominee::where('status', 'Approved')->count();
	}	
	public function totalNomineePending()
	{
		return Nominee::where('status', 'Pending')->count();
	}	
	public function totalStudents()
	{
		return Student::count();
	}

	public function totalElections()
	{
		return Election::count();
	}
	public function totalPositions()
	{
		return Position::count();
	}

	public function counts()
	{
		if (auth()->user()->isStudent()) {
			$announcement = auth()->user()->load('announcements')->loadCount('announcements');
			return $announcement->announcements_count;
		}
	}

	public function genderMale()
	{
		return Student::with(['gender'	=> function ($q) {
			$q->where('gender_name', 'Male');
		}])->count();
	}	

	// public function gender()
	// {
	// 	$male = Student::with(['gender'	=> function ($q) {
	// 				$q->where('gender_name', 'Male');
	// 			}])->count();

	// 	$female Student::with([

	// 		'gender'	=> funtion ($q) {
	// 			$q->where('gender_name', 'Female');
	// 		}
	// 	])->count();

	// 	// return Student::with(['gender'	=> function ($q) {
	// 	// 	$q->where('gender_name', 'Female');
	// 	// }])->count();


	// }

	public function totalPendingStudent()
	{
		return Student::where('approved', 'Pending')->count();
	}	
	public function totalPendingNominee()
	{
		return Student::where('status', 'Pending')->count();
	}
}