<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YearLevel extends Model
{
    protected $fillable = [
    	'course_id','year_level'
    ];

    public function course()
    {
    	return $this->belongsTo(Course::class);
    }
}
