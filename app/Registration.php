<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $fillable = ['status', 'started_at', 'ended_at'];

    protected $dates = [
		'started_at',
    	'ended_at',
	];
    public function students()
    {
    	return $this->belongsToMany(Student::class);
    }
}
