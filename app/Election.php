<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    protected $fillable = [
    	'election_name',
    	'status',
        'ended_time',
    	'user_id'
    ];


// public static function createFromFormat($format, $time, $tz = null)
// {
//     if ($tz !== null) {
//         $dt = parent::createFromFormat($format, $time, static::safeCreateDateTimeZone($tz));
//     } else {
//         $dt = parent::createFromFormat($format, $time); // Where the error happens.
//     }

//     if ($dt instanceof DateTime) {
//         return static::instance($dt);
//     }

//     $errors = static::getLastErrors();
//     throw new InvalidArgumentException(implode(PHP_EOL, $errors['errors'])); // Where the exception was thrown.
// }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function electionCampuses() 
    {
        return $this->hasManyThrough(Result::class, Nominee::class);
    }

    public function nominees()
    {
        return $this->hasMany(Nominee::class);
    }

    public function results()
    {
        return $this->hasMany(Result::class);
    }

    // public function nomineePositions()
    // {
    //     return $this->hasManyThrough(
    //         Nominee::class, 
    //         Position::class,
    //         'id',
    //         'position_id',
    //         'id',
    //         'id',
    //     );
    // }
  

}
