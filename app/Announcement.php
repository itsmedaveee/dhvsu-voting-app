<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable  = [
        'body'
    ];

    public function council()
    {
        return $this->belongsTo(Council::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public static function students()
    {
        if (auth()->user()->isStudent())
        {
           return auth()->user()->load('announcements');
        }

        if (auth()->user()->isSuperAdmin())
        {
            return auth()->user()->load('myAnnouncements');
        }
        if (auth()->user()->isCouncil())
        {
            return auth()->user()->load('myAnnouncements');
        }
    }
}
