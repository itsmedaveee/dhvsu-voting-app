<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
		'user_id',
        'nominee_id',
		'comment_id',
        'election_id',
        'student_id',
        'department_id'
    ];


    public function nominee()
    {
    	return $this->belongsTo(Nominee::class);
    }   

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
    public function students()
    {
        return $this->hasManyThrough(Result::class, Student::class);
    }

    // public function department()
    // {
    //     return $this->belongsTo(Department::class);
    // }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }    
    public function department()
    {
        return $this->belongsTo(Department::class);
    }    



    
    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }
}
