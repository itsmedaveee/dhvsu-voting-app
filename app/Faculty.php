<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $fillable = [
		'user_id',
		'department_id',
		'campus_id',
		'email',
		'image',
		'firstname',
		'middlename',
		'lastname',

    ];

    public function department()
    {
    	return $this->belongsTo(Department::class);
    }  

    public function user()
    {
    	return $this->belongsTo(User::class);
    }    

    public function campus()
    {
    	return $this->belongsTo(Campus::class);
    }

}
