<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'role_id', 'email', 'password', 'approved', 'campus_id', 'department_id', 'course_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function election()
    {
        return $this->belongsTo(Election::class);
    }

    public function student()
    {
        return $this->hasOne(Student::class);
    }


    public function results()
    {
        return $this->hasMany(Result::class);
    }    

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getStatusPerDepartment() 
    {
        $elections =  ElectionDepartment::where('department_id', auth()->user()->department_id)->get();
    }



    // public function student()
    // {
    //     return $this->hasOne(Student::class);
    // }
    public function myVotes()
    {   
        return $this->hasManyThrough(Nominee::class, Result::class);
    }

    public function events()
    {
        return $this->hasMany(Pageant::class);
    }

    public function announcement()
    {
        return $this->belongsTo(Announcement::class);
    }

    public function announcements()
    {
        return $this->belongsToMany(Announcement::class);
    }

    public function myAnnouncements()
    {
        return $this->hasMany(Announcement::class);
    }



    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }    

    public function nominees()
    {
        return $this->hasMany(Nominee::class);
    }

    public function campus()
    {
        return $this->belongsTo(Campus::class);
    }
 

    public function course()
    {
        return $this->belongsTo(Course::class);
    }    

    public function councils()
    {
        return $this->hasMany(Council::class);
    }

    public function isSuperAdmin()
    {
        return auth()->check() && auth()->user()->role->name == 'Super Admin';
    }

    // public function isFaculty()
    // {
    //     return auth()->check() && auth()->user()->role->name == 'Faculty';
    // }

    public function isCouncil()
    {
        return auth()->check() && auth()->user()->role->name == 'Council';
    }

    public function isStudent()
    {
        return auth()->check() && auth()->user()->role->name == 'Student';
    }
 
    public function userable()
    {
        return $this->morphTo();
    }


    // public function getAttributeStatus()
    // {
    //     return $this->hasManyThrough(Election::class, Nominee::class);
    // }


    public function hasRole($role)
    {
        return in_array($this->role->name, $role);
    }
   
}
