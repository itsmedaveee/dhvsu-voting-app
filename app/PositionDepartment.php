<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositionDepartment extends Model
{
   	protected $fillable = [
   		'user_id',
   		'position_name'
   	];

   	public function user()
   	{
   		return $this->belongsTo(User::class);
   	}

	public function nominees()
    {
    	return $this->hasMany(Nominee::class);
    }
}
