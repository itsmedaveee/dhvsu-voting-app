-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for dhvsu
CREATE DATABASE IF NOT EXISTS `halal` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `halal`;

-- Dumping structure for table dhvsu.announcements
CREATE TABLE IF NOT EXISTS `announcements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.announcements: ~0 rows (approximately)
DELETE FROM `announcements`;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;

-- Dumping structure for table dhvsu.announcement_user
CREATE TABLE IF NOT EXISTS `announcement_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `announcement_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.announcement_user: ~0 rows (approximately)
DELETE FROM `announcement_user`;
/*!40000 ALTER TABLE `announcement_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement_user` ENABLE KEYS */;

-- Dumping structure for table dhvsu.campuses
CREATE TABLE IF NOT EXISTS `campuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.campuses: ~5 rows (approximately)
DELETE FROM `campuses`;
/*!40000 ALTER TABLE `campuses` DISABLE KEYS */;
INSERT INTO `campuses` (`id`, `name`, `location`, `created_at`, `updated_at`) VALUES
	(1, 'Dhvsu Bacolor', 'Bacolor Pampanga', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(2, 'Dhvsu Porac', 'Porac Pampanga', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(3, 'Dhvsu Mexico', 'Mexico Pampanga', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(4, 'Dhvsu Sto. Tomas', 'Sto. Tomas Pampanga', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(5, 'Dhvsu Lubao', 'Lubao Pampanga', '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `campuses` ENABLE KEYS */;

-- Dumping structure for table dhvsu.campuses_notifications
CREATE TABLE IF NOT EXISTS `campuses_notifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.campuses_notifications: ~0 rows (approximately)
DELETE FROM `campuses_notifications`;
/*!40000 ALTER TABLE `campuses_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `campuses_notifications` ENABLE KEYS */;

-- Dumping structure for table dhvsu.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.comments: ~0 rows (approximately)
DELETE FROM `comments`;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table dhvsu.councils
CREATE TABLE IF NOT EXISTS `councils` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `campus_id` int(11) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `councils_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.councils: ~0 rows (approximately)
DELETE FROM `councils`;
/*!40000 ALTER TABLE `councils` DISABLE KEYS */;
/*!40000 ALTER TABLE `councils` ENABLE KEYS */;

-- Dumping structure for table dhvsu.courses
CREATE TABLE IF NOT EXISTS `courses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.courses: ~14 rows (approximately)
DELETE FROM `courses`;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` (`id`, `department_id`, `code`, `title`, `created_at`, `updated_at`) VALUES
	(1, 1, 'BSIT', 'Bachelor of Science in Information Technology', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(2, 1, 'BSCS', 'Bachelor of Science in Computer Science', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(3, 1, 'BSCpE', 'Bachelor of Science in Computer Engineering', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(4, 1, 'BSIS', 'Bachelor of Science in Information System', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(5, 2, 'BSCE', 'Bachelor of in Civil Engineering', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(6, 2, 'BSECE', 'Bachelor of in Electronics and Communication Engineering', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(7, 2, 'BSECE', 'Bachelor of in Electrical Engineering', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(8, 2, 'BSIT', 'Bachelor of in Industrial Technology', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(9, 3, 'BSEE', 'Bachelor of Science in Elementary Education', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(10, 3, 'BSE', 'Bachelor of Secondary Education', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(11, 3, 'BTTE', 'Bachelor of Technical Teacher Education (LADDERIZED)', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(12, 4, 'BSBA', 'Bachelor of Science in Business Administration', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(13, 4, 'BSE', 'Bachelor of Science in Entrepreneurship', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(14, 5, 'BSHRM', 'Bachelor of Science in Hotel and Restaurant Management', '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;

-- Dumping structure for table dhvsu.departments
CREATE TABLE IF NOT EXISTS `departments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.departments: ~5 rows (approximately)
DELETE FROM `departments`;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`id`, `code`, `title`, `created_at`, `updated_at`) VALUES
	(1, 'CCS', 'College Computing Studies', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(2, 'CEA', 'College of Engineering and Architecture ', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(3, 'COE', 'College of Education', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(4, 'CBS', 'College of Business Studies', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(5, 'CHM', 'College of Hospitality Management', '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table dhvsu.elections
CREATE TABLE IF NOT EXISTS `elections` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `election_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ended_time` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `elections_user_id_foreign` (`user_id`),
  CONSTRAINT `elections_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.elections: ~1 rows (approximately)
DELETE FROM `elections`;
/*!40000 ALTER TABLE `elections` DISABLE KEYS */;
INSERT INTO `elections` (`id`, `user_id`, `election_name`, `status`, `ended_time`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Eleksyon 2021', 'On Going', NULL, '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `elections` ENABLE KEYS */;

-- Dumping structure for table dhvsu.elections_statuses
CREATE TABLE IF NOT EXISTS `elections_statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.elections_statuses: ~0 rows (approximately)
DELETE FROM `elections_statuses`;
/*!40000 ALTER TABLE `elections_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `elections_statuses` ENABLE KEYS */;

-- Dumping structure for table dhvsu.election_courses
CREATE TABLE IF NOT EXISTS `election_courses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `course_id` int(11) NOT NULL DEFAULT '0',
  `election_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ended_time` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `election_courses_user_id_foreign` (`user_id`),
  CONSTRAINT `election_courses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.election_courses: ~1 rows (approximately)
DELETE FROM `election_courses`;
/*!40000 ALTER TABLE `election_courses` DISABLE KEYS */;
INSERT INTO `election_courses` (`id`, `user_id`, `course_id`, `election_name`, `status`, `ended_time`, `created_at`, `updated_at`) VALUES
	(1, 1, 0, 'Eleksyon 2021', 'On Going', NULL, '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `election_courses` ENABLE KEYS */;

-- Dumping structure for table dhvsu.election_departments
CREATE TABLE IF NOT EXISTS `election_departments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `department_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `election_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ended_time` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `election_departments_user_id_foreign` (`user_id`),
  CONSTRAINT `election_departments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.election_departments: ~1 rows (approximately)
DELETE FROM `election_departments`;
/*!40000 ALTER TABLE `election_departments` DISABLE KEYS */;
INSERT INTO `election_departments` (`id`, `user_id`, `department_id`, `election_name`, `status`, `ended_time`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 'Eleksyon 2021', 'On Going', NULL, '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `election_departments` ENABLE KEYS */;

-- Dumping structure for table dhvsu.faculties
CREATE TABLE IF NOT EXISTS `faculties` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `campus_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `faculties_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.faculties: ~0 rows (approximately)
DELETE FROM `faculties`;
/*!40000 ALTER TABLE `faculties` DISABLE KEYS */;
/*!40000 ALTER TABLE `faculties` ENABLE KEYS */;

-- Dumping structure for table dhvsu.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table dhvsu.genders
CREATE TABLE IF NOT EXISTS `genders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gender_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.genders: ~2 rows (approximately)
DELETE FROM `genders`;
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
INSERT INTO `genders` (`id`, `gender_name`, `created_at`, `updated_at`) VALUES
	(1, 'Male', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(2, 'Female', '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;

-- Dumping structure for table dhvsu.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.migrations: ~35 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2021_10_05_113439_create_roles_table', 1),
	(5, '2021_10_05_113514_create_departments_table', 1),
	(6, '2021_10_05_113616_create_courses_table', 1),
	(7, '2021_10_05_114053_create_campuses_table', 1),
	(8, '2021_10_21_052629_create_students_table', 1),
	(9, '2021_10_22_145056_create_faculties_table', 1),
	(10, '2021_10_22_175312_create_partylists_table', 1),
	(11, '2021_10_22_190844_create_nominees_table', 1),
	(12, '2021_10_22_193114_create_positions_table', 1),
	(13, '2021_10_27_044735_create_notifications_table', 1),
	(14, '2021_10_29_042808_create_results_table', 1),
	(15, '2021_11_05_055456_create_elections_table', 1),
	(16, '2021_11_09_100203_create_councils_table', 1),
	(17, '2021_11_23_133636_create_comments_table', 1),
	(18, '2021_12_11_172314_create_position_departments_table', 1),
	(19, '2021_12_11_180940_create_election_departments_table', 1),
	(20, '2021_12_14_071815_create_result_departments_table', 1),
	(21, '2021_12_16_012516_create_election_courses_table', 1),
	(22, '2021_12_16_142246_create_position_courses_table', 1),
	(23, '2021_12_16_160949_create_result_courses_table', 1),
	(24, '2021_12_22_090656_create_announcements_table', 1),
	(25, '2021_12_22_133804_create_announcement_user_table', 1),
	(26, '2021_12_26_051201_create_genders_table', 1),
	(27, '2022_01_03_013927_create_pageants_table', 1),
	(28, '2022_01_04_080844_create_result_pageants_table', 1),
	(29, '2022_01_15_085844_create_registrations_table', 1),
	(30, '2022_01_15_085941_create_registration_student_table', 1),
	(31, '2022_01_18_072647_create_elections_statuses_table', 1),
	(32, '2022_01_19_081717_create_campuses_notifications_table', 1),
	(33, '2022_01_27_144718_create_year_levels_table', 1),
	(34, '2022_01_27_204913_create_programs_table', 1),
	(35, '2022_01_27_222936_create_reports_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table dhvsu.nominees
CREATE TABLE IF NOT EXISTS `nominees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `election_id` int(11) DEFAULT NULL,
  `election_department_id` int(11) DEFAULT NULL,
  `election_course_id` int(11) DEFAULT NULL,
  `pageant_id` int(11) DEFAULT NULL,
  `campus_id` int(11) NOT NULL DEFAULT '0',
  `position_id` int(11) DEFAULT NULL,
  `position_department_id` int(11) DEFAULT NULL,
  `position_course_id` int(11) DEFAULT NULL,
  `course_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthdate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partyname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_of_registration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validate_school_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_of_attendance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recent_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certified` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recomend_letter_from_dean` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_of_good_moral` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_of_grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason_for_candidacy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `average_last_semister` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_failed_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_honors_received` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_curricular_activities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_seminars` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_filled_out` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_for_win` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `propaganda` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.nominees: ~0 rows (approximately)
DELETE FROM `nominees`;
/*!40000 ALTER TABLE `nominees` DISABLE KEYS */;
/*!40000 ALTER TABLE `nominees` ENABLE KEYS */;

-- Dumping structure for table dhvsu.notifications
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.notifications: ~0 rows (approximately)
DELETE FROM `notifications`;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Dumping structure for table dhvsu.pageants
CREATE TABLE IF NOT EXISTS `pageants` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `event_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ended_time` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.pageants: ~1 rows (approximately)
DELETE FROM `pageants`;
/*!40000 ALTER TABLE `pageants` DISABLE KEYS */;
INSERT INTO `pageants` (`id`, `user_id`, `department_id`, `event_name`, `status`, `ended_time`, `created_at`, `updated_at`) VALUES
	(1, 0, 0, 'MR and MS DHVSU 2022', 'On Going', NULL, '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `pageants` ENABLE KEYS */;

-- Dumping structure for table dhvsu.partylists
CREATE TABLE IF NOT EXISTS `partylists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.partylists: ~1 rows (approximately)
DELETE FROM `partylists`;
/*!40000 ALTER TABLE `partylists` DISABLE KEYS */;
INSERT INTO `partylists` (`id`, `user_id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'No Partylist', '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `partylists` ENABLE KEYS */;

-- Dumping structure for table dhvsu.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table dhvsu.positions
CREATE TABLE IF NOT EXISTS `positions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `position_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.positions: ~9 rows (approximately)
DELETE FROM `positions`;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` (`id`, `user_id`, `position_name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'President', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(2, 1, 'Vice President', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(3, 1, 'Senator on Records', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(4, 1, 'Senator on Audit', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(5, 1, 'Senator on Finance', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(6, 1, 'Senator on Business Affairs ', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(7, 1, 'Senator on Student Affairs ', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(8, 1, 'Senator on Student Services ', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(9, 1, 'Senator on Public Information Officer', '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;

-- Dumping structure for table dhvsu.position_courses
CREATE TABLE IF NOT EXISTS `position_courses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `position_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.position_courses: ~8 rows (approximately)
DELETE FROM `position_courses`;
/*!40000 ALTER TABLE `position_courses` DISABLE KEYS */;
INSERT INTO `position_courses` (`id`, `user_id`, `position_name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'President', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(2, 1, 'Vice President', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(3, 1, 'Secretary', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(4, 1, 'Treasurer', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(5, 1, 'Auditor', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(6, 1, 'Business Manager', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(7, 1, 'Public Information Officer', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(8, 1, 'GFPS Officer', '2022-01-29 09:45:06', '2022-01-29 09:45:06');
/*!40000 ALTER TABLE `position_courses` ENABLE KEYS */;

-- Dumping structure for table dhvsu.position_departments
CREATE TABLE IF NOT EXISTS `position_departments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `position_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.position_departments: ~16 rows (approximately)
DELETE FROM `position_departments`;
/*!40000 ALTER TABLE `position_departments` DISABLE KEYS */;
INSERT INTO `position_departments` (`id`, `user_id`, `position_name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Governor', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(2, 1, 'Vice Governor', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(3, 1, 'Board Member on Records', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(4, 1, 'Board Member on Finance', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(5, 1, 'Board Member on Audit', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(6, 1, 'Board Member on Student', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(7, 1, 'Board Member on Student Affairs', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(8, 1, 'Board Member on Student Services', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(9, 1, 'Board Member on Business Manager', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(10, 1, 'Board Member on Public Information', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(11, 1, 'Bs Information Technology Representative', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(12, 1, 'Bs Computer Engineering Representative', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(13, 1, 'Bs Computer Science Representative', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(14, 1, 'Bs Information System Representative', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(15, 1, 'Associate in Computer Technology Representative', '2022-01-29 09:45:06', '2022-01-29 09:45:06'),
	(16, 1, 'Gender and Development Focal Point System', '2022-01-29 09:45:06', '2022-01-29 09:45:06');
/*!40000 ALTER TABLE `position_departments` ENABLE KEYS */;

-- Dumping structure for table dhvsu.programs
CREATE TABLE IF NOT EXISTS `programs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL DEFAULT '0',
  `program_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.programs: ~0 rows (approximately)
DELETE FROM `programs`;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;
/*!40000 ALTER TABLE `programs` ENABLE KEYS */;

-- Dumping structure for table dhvsu.registrations
CREATE TABLE IF NOT EXISTS `registrations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `started_at` timestamp NOT NULL,
  `ended_at` timestamp NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.registrations: ~0 rows (approximately)
DELETE FROM `registrations`;
/*!40000 ALTER TABLE `registrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `registrations` ENABLE KEYS */;

-- Dumping structure for table dhvsu.registration_student
CREATE TABLE IF NOT EXISTS `registration_student` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `registration_id` bigint(20) unsigned NOT NULL,
  `student_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registration_student_registration_id_foreign` (`registration_id`),
  KEY `registration_student_student_id_foreign` (`student_id`),
  CONSTRAINT `registration_student_registration_id_foreign` FOREIGN KEY (`registration_id`) REFERENCES `registrations` (`id`),
  CONSTRAINT `registration_student_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.registration_student: ~0 rows (approximately)
DELETE FROM `registration_student`;
/*!40000 ALTER TABLE `registration_student` DISABLE KEYS */;
/*!40000 ALTER TABLE `registration_student` ENABLE KEYS */;

-- Dumping structure for table dhvsu.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.reports: ~0 rows (approximately)
DELETE FROM `reports`;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;

-- Dumping structure for table dhvsu.results
CREATE TABLE IF NOT EXISTS `results` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `nominee_id` int(11) NOT NULL DEFAULT '0',
  `election_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `results_user_id_foreign` (`user_id`),
  CONSTRAINT `results_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.results: ~0 rows (approximately)
DELETE FROM `results`;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
/*!40000 ALTER TABLE `results` ENABLE KEYS */;

-- Dumping structure for table dhvsu.result_courses
CREATE TABLE IF NOT EXISTS `result_courses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `nominee_id` int(11) NOT NULL DEFAULT '0',
  `election_course_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `result_courses_user_id_foreign` (`user_id`),
  CONSTRAINT `result_courses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.result_courses: ~0 rows (approximately)
DELETE FROM `result_courses`;
/*!40000 ALTER TABLE `result_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `result_courses` ENABLE KEYS */;

-- Dumping structure for table dhvsu.result_departments
CREATE TABLE IF NOT EXISTS `result_departments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `nominee_id` int(11) NOT NULL DEFAULT '0',
  `election_department_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `result_departments_user_id_foreign` (`user_id`),
  CONSTRAINT `result_departments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.result_departments: ~0 rows (approximately)
DELETE FROM `result_departments`;
/*!40000 ALTER TABLE `result_departments` DISABLE KEYS */;
/*!40000 ALTER TABLE `result_departments` ENABLE KEYS */;

-- Dumping structure for table dhvsu.result_pageants
CREATE TABLE IF NOT EXISTS `result_pageants` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `pageant_id` int(11) NOT NULL DEFAULT '0',
  `nominee_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `election_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `result_pageants_user_id_foreign` (`user_id`),
  CONSTRAINT `result_pageants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.result_pageants: ~0 rows (approximately)
DELETE FROM `result_pageants`;
/*!40000 ALTER TABLE `result_pageants` DISABLE KEYS */;
/*!40000 ALTER TABLE `result_pageants` ENABLE KEYS */;

-- Dumping structure for table dhvsu.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.roles: ~3 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 'super admin', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(2, 'Student', 'student', '2022-01-29 09:45:05', '2022-01-29 09:45:05'),
	(3, 'Council', 'council', '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table dhvsu.students
CREATE TABLE IF NOT EXISTS `students` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `course_id` int(10) unsigned NOT NULL DEFAULT '0',
  `department_id` int(10) unsigned NOT NULL DEFAULT '0',
  `campus_id` int(10) unsigned NOT NULL DEFAULT '0',
  `gender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `program_id` int(10) unsigned NOT NULL DEFAULT '0',
  `student_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `generate_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_qr_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_filled_out` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `students_student_no_unique` (`student_no`),
  UNIQUE KEY `students_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.students: ~0 rows (approximately)
DELETE FROM `students`;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- Dumping structure for table dhvsu.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `campus_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `userable_id` int(11) NOT NULL DEFAULT '0',
  `userable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `campus_id`, `department_id`, `course_id`, `username`, `email`, `email_verified_at`, `password`, `approved`, `userable_id`, `userable_type`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 0, 'admin', 'superadmin@yahoo.com', NULL, '$2y$10$mqbCmapCk7f79F06FNTGjeMCgeHZTj7klcs1a9s9Z6z6jgqM0acWq', 'confirmed', 1, 'App\\User', NULL, '2022-01-29 09:45:05', '2022-01-29 09:45:05');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table dhvsu.year_levels
CREATE TABLE IF NOT EXISTS `year_levels` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `year_level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dhvsu.year_levels: ~0 rows (approximately)
DELETE FROM `year_levels`;
/*!40000 ALTER TABLE `year_levels` DISABLE KEYS */;
/*!40000 ALTER TABLE `year_levels` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
