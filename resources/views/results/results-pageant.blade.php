@extends('layouts.master')
@section('content')


<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
        @if (auth()->user()->userable->image)
            <div class="profile-header-img">
               <img src="{{ Storage::url(auth()->user()->userable->image) }}" alt="" style="height: 100%" style="width: 100%"> 
              {{--  <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%"> --}}
            </div>
            @else
            <div class="profile-header-img">
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
            @endif
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>
<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right"> 
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Results Event </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Results</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-condensed">

							<thead>
								<tr>
									<th>Candidates</th>
									<th>Title</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($pageants as $pageant)
							<div class="form-group">
								@if ($loop->last)
							@endif
						</div>
								@foreach($pageant->nominees as $nominee)
								<tr>
									<td>Candidate {{ $nominee->id }}</td>
									<td>{{ $nominee->title }}</td>
									<td>
										<h4> {{ count($nominee->resultsPageant) / $totalVoteUsers * 100 . '%'  }} </h4>
										<div class="progress h-10px"> 
											<div class="progress-bar fs-10px fw-bold" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"  style="width:{{ count($nominee->resultsPageant) / $totalVoteUsers * 100 . '%'  }}   "> 
												{{--  {{ count($result->nominee->results) / $totalVoteUsers * 100 . '%'  }}  --}}
												   
											</div>
										</div>
									</td>

								</tr>
								@endforeach
								@endforeach
							</tbody>
							
						</table> 
						
					</div>
				</div>

 
@endsection


{{--  @push('scripts')


<script type="text/javascript">
	const data = {
	  labels: [
	    'Red',
	    'Blue',
	    'Yellow'
	  ],
	  datasets: [{
	    label: 'My First Dataset',
	    data: [300, 50, 100],
	    backgroundColor: [
	      'rgb(255, 99, 132)',
	      'rgb(54, 162, 235)',
	      'rgb(255, 205, 86)'
	    ],
	    hoverOffset: 4
	  }]
	};
</script>
 @endpush --}}