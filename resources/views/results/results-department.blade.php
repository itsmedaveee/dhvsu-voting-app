@extends('layouts.master')
@section('content')


<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
        @if (auth()->user()->userable->image)
            <div class="profile-header-img">
               <img src="{{ Storage::url(auth()->user()->userable->image) }}" alt="" style="height: 100%" style="width: 100%"> 
              {{--  <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%"> --}}
            </div>
            @else
            <div class="profile-header-img">
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
            @endif
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>
<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/results-department">Results Department</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Results Department </h1>
				<!-- end page-header -->
				<!-- begin panel -->
{{-- 				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Results</h4>
						<div class="panel-heading-btn">
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-condensed">
							<thead>
								<tr>
									<th>Candidates</th>
									<th>Position</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($elections as $election)
							<div class="form-group">
						</div>
								@foreach($election->nominees as $nominee)
								<tr>
									<td>Candidate {{ $nominee->id }}</td>
									<td>{{ $nominee->positionDepartment->position_name }}</td>
									
									<td>
										<div class="progress h-10px"> 
											<div class="progress-bar progress-bar-striped progress-bar-animated bg-success fs-10px fw-bold" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"  style="width:{{ count($nominee->resultsDepartment) / $totalVoteUsers * 100 . '%'  }}   "> 
												  {{ count($nominee->resultsDepartment) / $totalVoteUsers * 100 . '%'   }}  
											</div>
										</div>
									</td>
								</tr>
								@endforeach
								@endforeach
							</tbody>
						</table> 
					</div>
				</div> --}}

					<div class="row">
							<div class="col-md-12">
								@if (count($grouped)) 
								 	@foreach($grouped as $position => $nominees)
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">{{ $position }}</h4>
											</div>
											<div class="panel-body">
												<div class="table-responsive">
													<table class="table table-bordered table-hover">
														<thead>
															<tr> 
																<th>Image</th>
																<th>Name</th>
																<th>PartyName</th>
																<th>Colleges</th>
																<th>Campus</th>
																<th>Actions</th>
															</tr>
														</thead>
														<tbody>
															@foreach ($nominees as $nominee)
																<tr class="odd gradeX">
																	 <td width="1%">
																	{{-- <img src="{{ asset('img/admin.png') }}" class="rounded h-30px my-n1 mx-n1" /> --}}
																	 	 <img src="{{ Storage::url($nominee->avatar) ? Storage::url($nominee->avatar) :  asset('img/admin.png') }}" class="rounded h-30px my-n3 mx-n1 center"/ >
																	 </td>
																	<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
																	<td>{{ $nominee->partyname }}</td>
																	<td>{{ $nominee->department->title }}</td>
																	<td>{{ $nominee->campus->name }}</td>
																	<td>
																	<h4>	{{ count($nominee->resultsDepartment) / $totalVoteUsers * 100 . '%'  }} </h4>
																		<div class="progress h-10px"> 
															<div class="progress-bar fs-10px fw-bold" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"  style="width:{{ count($nominee->resultsDepartment) / $totalVoteUsers * 100 . '%'  }}   "> 
																   
															</div></td>
																</tr>
															@endforeach
														</tbody>
													</table>
												</div>
											</div>
										</div>
									@endforeach

								@endif
							</div>
						</div>

				 
@endsection


{{--  @push('scripts')


<script type="text/javascript">
	const data = {
	  labels: [
	    'Red',
	    'Blue',
	    'Yellow'
	  ],
	  datasets: [{
	    label: 'My First Dataset',
	    data: [300, 50, 100],
	    backgroundColor: [
	      'rgb(255, 99, 132)',
	      'rgb(54, 162, 235)',
	      'rgb(255, 205, 86)'
	    ],
	    hoverOffset: 4
	  }]
	};
</script>
 @endpush --}}