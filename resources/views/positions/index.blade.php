@extends('layouts.master')
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/position">Position</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Positions </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Position</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/positions">
							@csrf
						 
							<div class="form-group{{ $errors->has('position_name') ? ' has-error' : '' }}">
								<label>Position name</label>
								<input type="text" class="form-control" name="position_name">
								@if ($errors->has('position_name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('position_name') }}</strong>
								    </span>
								@endif
							</div>


					{{-- 		<div class="form-group{{ $errors->has('campuses') ? ' has-error' : '' }}">
		                      <label for="name">Campus:</label>
		                      <select class="form-control" id="campuses" name="campuses">
		                        <option value="" disabled="" selected="">Select Campuses</option>
		                        @foreach ($campuses as $campusId => $campus )
		                        <option value="{{ $campusId }}">{{ $campus }}</option>
		                        <span class="help-block	">	                          
		                          @if ($errors->has('campus'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('campuses') }}</strong>
		                                    </span>
		                                @endif
		                        @endforeach
		                      </select>
		                    </div>

		                    <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
	                      <label for="name">Department:</label>
	                      <select class="form-control" id="department" name="department">
	                        <option value="" disabled="" selected="">Select Department</option>
	                        @foreach ($departments as $departmentId => $department )
	                        <option value="{{ $departmentId }}">{{ $department }}</option>
	                        <span class="help-block">
	                            @if ($errors->has('department'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('department') }}</strong>
	                                    </span>
	                                @endif
	                        @endforeach
	                      </select>
	                    </div>

          		   <div class="form-group{{ $errors->has('election') ? ' has-error' : '' }}">
	                      <label for="name">Election:</label>
	                      <select class="form-control" id="election" name="election">
	                        <option value="" disabled="" selected="">Select Election</option>
	                        @foreach ($elections as $electionId => $election )
	                        <option value="{{ $electionId }}">{{ $election }}</option>
	                        <span class="help-block">
	                            @if ($errors->has('election'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('election') }}</strong>
	                                    </span>
	                                @endif
	                        @endforeach
	                      </select>
	                    </div> --}}


							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->

				<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Position</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="positions-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
							{{-- 	@foreach ($positions as $position)
								<tr>
									<td>{{ $position->id }}</td>
									<td>{{ $position->position_name }}</td>
									<td>
										<a href="/positions/{{ $position->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
										<form method="POST" action="/positions/{{ $position->id }}" style="display: inline-block;">
											@csrf
											{{ method_field('DELETE') }}
											<button type="submit" class="btn btn-danger btn-xs">Delete</button>
										</form>
									</td>
								</tr>
								@endforeach --}}
						 
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>



@endsection

@push('scripts')
 <script type="text/javascript">
    $(document).ready(function () {
	 $('#positions-table').DataTable({
	    "ordering":'true',
        "order": [0, 'asc'],
        processing: true,
        serverSide: true,
        ajax: '{!! route('positions.index') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'position_name',},
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ]
	  });
});
</script>
@endpush