<td>
<a href="/positions/{{ $position->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
<form method="POST" action="/positions/{{ $position->id }}" style="display: inline-block;">
	@csrf
	{{ method_field('DELETE') }}
	<button type="submit" class="btn btn-danger btn-xs">Delete</button>
</form>
</td>