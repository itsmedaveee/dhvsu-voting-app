@extends('layouts.master')
@section('content')
 
<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/positions">Position</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Positions </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Position</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/positions/{{ $position->id }}">
							@csrf
						 	{{ method_field('PATCH') }}
							<div class="form-group{{ $errors->has('position_name') ? ' has-error' : '' }}">
								<label>Position name</label>
								<input type="text" class="form-control" name="position_name" value="{{ $position->position_name }}">
								@if ($errors->has('position_name'))
								    <span class="help-block">
								        <strong>{{ $errors->first('position_name') }}</strong>
								    </span>
								@endif
							</div>

{{-- 
							<div class="form-group{{ $errors->has('campuses') ? ' has-error' : '' }}">
		                      <label for="name">Campus:</label>
		                      <select class="form-control" id="campuses" name="campuses">
		                        <option value="" disabled="" selected="">Select Campuses</option>
		                        @foreach ($campuses as $campusId => $campus )
		                        <option value="{{ $campusId }}">{{ $campus }}</option>
		                        <span class="help-block	">	                          
		                          @if ($errors->has('campus'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('campuses') }}</strong>
		                                    </span>
		                                @endif
		                        @endforeach
		                      </select>
		                    </div>

		                    <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
	                      <label for="name">Department:</label>
	                      <select class="form-control" id="department" name="department">
	                        <option value="" disabled="" selected="">Select Department</option>
	                        @foreach ($departments as $departmentId => $department )
	                        <option value="{{ $departmentId }}">{{ $department }}</option>
	                        <span class="help-block">
	                            @if ($errors->has('department'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('department') }}</strong>
	                                    </span>
	                                @endif
	                        @endforeach
	                      </select>
	                    </div>
 --}}


							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->









@endsection