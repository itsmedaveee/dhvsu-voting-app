
@extends('layouts.frontend')
@section('content')

<div id="app" class="app">

<div class="register register-with-news-feed">

<div class="news-feed">
<div class="news-image" style="background-image: url('img/Login.png');"></div>
<div class="news-caption">
<h4 class="caption-title">SECURE ONLINE VOTING SYSTEM</h4>
<p>

</p>
</div>
</div>


<div class="register-container" style="width: 700px;">

<div class="register-header mb-25px h1">
<div class="mb-1">Login</div>
</div>
                    <div class="panel-body" style="background: ">

 
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                   <div class="mb-3">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" 
                                value="{{ old('username') }}" 
                                required autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                   <div class="mb-3">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                   <div class="mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                   <div class="mb-3">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
 
                           {{--          <a class="btn btn-link" href="/registration-from">
                                        {{ __('Register here') }}   

                                                                         </a>
--}}        

                                    @include ('auth.modal.privacy-act')
                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                    Register here
                                    </a>
                                
                            </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push ('scripts')

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
@endpush