<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 100%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">PRIVACY POLICY</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <p class="mt-4">
            You are encouraged to register in this website and provide certain information about yourself. It is assured that all information that will be collected about you in connection with the website is subjected to Data Privacy Act. The Republic Act No. 10173, otherwise known as the Data Privacy Act, a law that seeks to protect all forms of information, be it private, personal, or sensitive. It is meant to cover both natural and juridical persons involved in the processing of personal information.

            In using the website, you give your permission to all actions that will be taken by the researchers with respect to your information, including being contacted via email. You are also responsible for all activities that occur under your user account. You shall: 
            (i) Have the complete responsibility for the accuracy, integrity, and appropriateness of all data you provide; 
            (ii) Maintain the confidentiality of your password and user account information; 
            (iii) Provide efforts to prevent unauthorized access to your user account; and 
            (iv) Comply with all applicable local, state, and federal laws in using the website.
</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a class="btn btn-primary" href="/registration-from">
                                      {{ __('I Agree') }}   </a>

      </div>
    </div>
  </div>
</div>