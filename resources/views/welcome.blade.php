@extends ('layouts.master')
@section('content')
<style type="text/css">
   /*#formButton {
   display: block;
   margin-right: auto;
   margin-left: auto;
   }*/
</style>
<div id="content" class="content">
  <div id="app">
       <div class="col-sm-6">
          <div class="form-group">
             <label>Categories:</label>
             <select class="form-control" id="mySelect"  onchange="myFunction()">
             <option value="" disabled="" selected="" >Select Categories</option>
             <option value="Campus">Filling Campus</option>
             <option value="Department">Filling Department</option>
             <option value="Course">Filling Course</option>
           </select>
          </div>
          <div class="col-sm-12  ">
             <div class="form-group">
                <div id="election"></div>
             </div>
          </div>
       </div>
  </div>
</div>


@endsection
@push ('scripts')
<script type="text/javascript">
   function myFunction() {
      var selected = document.getElementById("mySelect").value;
      if (selected === 'Campus') {
          document.getElementById("election").innerHTML = "<label>Election Campus</label><input type='text' name='' class='form-control' placeholder='' value=''>";
      } else if (selected === 'Department') {
         document.getElementById("election").innerHTML = "<label>Election Department</label><input type='text' name='' class='form-control' placeholder='' value=''>";
      } else if (selected === 'Course') {
        document.getElementById("election").innerHTML = "<label>Election Course</label><input type='text' name='' class='form-control' placeholder='' value=''>";
      }
   }
</script> 
@endpush