  
<div class="modal fade" id="ajax-close-modal" aria-hidden="true">
                 <div class="modal-dialog modal-dialog-centered" role="document">
                   <div class="modal-content">
                     <div class=""></div>
                     <div class="modal-body">{{-- 
                        <p class="text-center"><img src="{{ asset('img/mark.png') }}" style="width: 20%;" ></p> --}}
                        <div class="swal-icon swal-icon--warning">
                        <span class="swal-icon--warning__body">
                        <span class="swal-icon--warning__dot"></span>
                        </span>
                        </div>

                     <form id="closeForm" class="form-horizontal">
                     <input type="hidden" name="registrationId" id="registrationId">

                       <h5 style="font-size: 1.875rem;font-weight:600; " class="text-center">DO YOU WANT TO CLOSE THE REGISTRATION FORM?</h5>

                       <div class="form-group" align="center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel </button>
                       <button type="submit" class="btn btn-primary">CLOSE</button>
                        </form>
                    </div>
                     </div>
                   </div>
                 </div>
               </div>