@extends('layouts.master')
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content"> 
				<h1 class="page-header">Date for Registration </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Date for Registration</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form method="POST" action="/add-date-regisrations">
							@csrf
							<div class="form-group">
								<label>Start Date </label>
								<input type="date" name="started_at" class="form-control">
							</div>			
							<div class="form-group">
								<label>End Date </label>
								<input type="date" name="ended_at" class="form-control">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> Date for Registrations</h4>
						<div class="panel-heading-btn">
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>ID</th>
									<th>Started At</th>
									<th>Ended At</th>
									<th>Status</th>
									<th>Manage</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($registrations as $registration)
								<tr>
									<td>{{ $registration->id }}</td>
									<td>{{ $registration->started_at->toFormattedDateString() }}</td>
									<td>{{ $registration->ended_at->toFormattedDateString() }}</td>
									<td>{{ $registration->status }}</td>
									<td>
									{{-- 	<form method="POST" action="/lock-in-period/{{ $registration->id }}">
											@csrf
											{{ method_field('PATCH') }}
											<button type="submit" class="btn btn-danger btn-xs">Close</button>
										</form> --}}

										@include('lock-in-period.modal.close')
										<a href="javascript:void(0)" data-id="{{ $registration->id }}" id="close" class="btn btn-danger btn-xs">Close Form</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>


@push ('scripts')
<script>
   $('body').on('click', '#close', function () {
       var registration = $(this).data('id');
       $.get('/lock-in-period/' + registration, function (registration) {
           $('#electionsModal').html("Modal");
           $('#btn-update').val("edit-fee");
           $('#registrationId').val(registration.id);
           $('#ajax-close-modal').modal('show');
       })
   });
    $("#closeForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#btn-update').html('<i class="fa fa fa-spinner fa-spin"></i> Close Form..');
        var form = $(this);
        var registrationId = $('#registrationId').val();
        $.ajax({
            type: "PUT",
            url: '/ajax-close-registration/' + registrationId,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                toastr.error("Close Form!");
                $('#test').trigger("reset");
                $('#ajax-close-modal').modal('hide');
                location.reload();
            }
         });
    });
</script>

@endpush












@endsection