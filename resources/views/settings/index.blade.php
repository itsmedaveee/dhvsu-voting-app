@extends('layouts.master')
@section('content')
<div id="content" class="content">
   <!-- begin breadcrumb -->
   <ol class="breadcrumb float-xl-right">
      <li class="breadcrumb-item"><a href="/home">Settings</a></li>
      <li class="breadcrumb-item"><a href="/pending-students">Settings</a></li>
   </ol>
   <!-- end breadcrumb -->
   <!-- begin page-header -->
   <h1 class="page-header">Settings </h1>
   <!-- end page-header -->
   <!-- begin panel -->
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading">
            <h4 class="panel-title">Settings</h4>
            <div class="panel-heading-btn">
            </div>
         </div>
         <div class="panel-body">
            <form method="POST" action="/settings"  enctype="multipart/form-data">
               {{ csrf_field() }}
               {{ method_field('PATCH') }}

               @if (auth()->user()->isStudent())
                <div class="form-group{{ $errors->has('student_no') ? ' has-error' : '' }}">
                  <label for="student_no">Student_no</label>
                  <input type="text" class="form-control" id="student_no" name="student_no" placeholder="Enter student_no" value="{{ auth()->user()->userable->student_no }}">
                  @if ($errors->has('student_no'))
                  <span class="help-block">
                  <strong>{{ $errors->first('student_no') }}</strong>
                  </span>
                  @endif
               </div>

                <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                  <label for="firstname">Firstname</label>
                  <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Enter Firstname" value="{{ auth()->user()->userable->firstname }}">
                  @if ($errors->has('firstname'))
                  <span class="help-block">
                  <strong>{{ $errors->first('firstname') }}</strong>
                  </span>
                  @endif
               </div>                
               <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
                  <label for="middlename">Middlename</label>
                  <input type="text" class="form-control" id="middlename" name="middlename" placeholder="Enter Middlename" value="{{ auth()->user()->userable->middlename }}">
                  @if ($errors->has('middlename'))
                  <span class="help-block">
                  <strong>{{ $errors->first('middlename') }}</strong>
                  </span>
                  @endif
               </div>               

               <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                  <label for="lastname">Lastname</label>
                  <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter Lastname" value="{{ auth()->user()->userable->lastname }}">
                  @if ($errors->has('lastname'))
                  <span class="help-block">
                  <strong>{{ $errors->first('lastname') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                  <label for="address">Address</label>
                  <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="{{ auth()->user()->userable->address }}">
                  @if ($errors->has('address'))
                  <span class="help-block">
                  <strong>{{ $errors->first('address') }}</strong>
                  </span>
                  @endif
               </div>               

               <div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
                  <label for="contact_no">Contact No</label>
                  <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Enter Contact No" value="{{ auth()->user()->userable->contact_no }}">
                  @if ($errors->has('contact_no'))
                  <span class="help-block">
                  <strong>{{ $errors->first('contact_no') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                  <label for="Section">Section</label>
                  <input type="text" class="form-control" id="section" name="section" placeholder="Enter section" value="{{ auth()->user()->userable->section }}">
                  @if ($errors->has('section'))
                  <span class="help-block">
                  <strong>{{ $errors->first('section') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                  <label for="year">Year</label>
                  <input type="text" class="form-control" id="year" name="year" placeholder="Enter Year" value="{{ auth()->user()->userable->year }}">
                  @if ($errors->has('year'))
                  <span class="help-block">
                  <strong>{{ $errors->first('year') }}</strong>
                  </span>
                  @endif
               </div>               

               <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                  <label for="birth_date">BirthDate</label>
                  <input type="date" class="form-control" id="birth_date" name="birth_date" placeholder="Enter birth_date" value="{{ auth()->user()->userable->birth_date->format('Y-m-d') }}">
                  @if ($errors->has('birth_date'))
                  <span class="help-block">
                  <strong>{{ $errors->first('birth_date') }}</strong>
                  </span>
                  @endif
               </div>
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                  <label for="image">Avatar</label>
                  <input type="file" class="form-control" id="image" name="image" placeholder="Enter image" value="{{ auth()->user()->userable->image }}">
                  @if ($errors->has('image'))
                  <span class="help-block">
                  <strong>{{ $errors->first('image') }}</strong>
                  </span>
                  @endif
                </div>       

                @endif 

               <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                  <label for="username">Username</label>
                  <input type="text" class="form-control" id="username" name="username" placeholder="Enter Name" value="{{ auth()->user()->username }}">
                  @if ($errors->has('username'))
                  <span class="help-block">
                  <strong>{{ $errors->first('username') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{ auth()->user()->email }}">
                  @if ($errors->has('email'))
                  <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
                  @if ($errors->has('password'))
                  <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label for="password_confirmation">Password Confirmation</label>
                  <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Enter Password">
                  @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-primary">Update</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection