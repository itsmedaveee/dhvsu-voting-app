@extends('layouts.master')
@section('content')


<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/administrators">Administrators</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Super Admin </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Super Admin</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/super-admin">
							@csrf
						 
							<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
								<label>Username</label>
								<input type="text" class="form-control" name="username">
								<span class="help-block	">	                          
								@if ($errors->has('username'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('username') }}</strong>
								    </span>
								@endif
							</div>


		                 <div class="form-group{{ $errors->has('campuses') ? 'has-error' : '' }}">
		                      <label for="campuses">Satellite Campus:</label>
		                      <select class="form-control" id="campuses" name="campuses">
		                       <option value="" disabled="" selected="">Select Campus</option>
		                      @foreach ($campuses as $campusesId => $campus)
		                        <option value="{{ $campusesId }}">{{ $campus }}</option>
		                      @endforeach       
		                      </select>
		                       @if ($errors->has('campuses'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('campuses') }}</strong>
		                        </span>
		                    @endif
		                    </div>
		                    
							<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
		                      <label for="department">Department:</label>
		                      <select class="form-control" id="department" name="department">
		                       <option value="" disabled="" selected="">Select Department</option>
		                      @foreach ($departments as $departmentId => $department)
		                        <option value="{{ $departmentId }}">{{ $department }}</option>
		                      @endforeach       
		                  
		                      </select>

		                       @if ($errors->has('department'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('department') }}</strong>
		                        </span>
		                    @endif

		                    </div>

							<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
								<label>Email</label>
								<input type="text" class="form-control" name="email">
								@if ($errors->has('email'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('email') }}</strong>
								    </span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label>Password</label>
								<input type="password" class="form-control" name="password">
								@if ($errors->has('password'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('password') }}</strong>
								    </span>
								@endif
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->


				<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Administrator list</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="admin-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Email</th>
									<th>Username</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($superAdmin as $admin)
								<tr>
									<td>{{ $admin->id }}</td>
									<td>{{ $admin->email }}</td>
									<td>{{ $admin->username }}</td>
									<td><a href="/super-admin/{{ $admin->id }}/edit" class="btn btn-primary btn-xs">Edit</a></td>
								</tr>
								@endforeach
						 
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>






@endsection