@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Edit Super Admin </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Super Admin</h4>
						<div class="panel-heading-btn">
						
						</div>
					</div>
					<div class="panel-body">

						<form method="POST" action="/super-admin/{{ $admin->id }}">

							@csrf

							{{ method_field('PATCH') }}

							<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
								<label>Name</label>
								<input type="text" class="form-control" name="username" value="{{ $admin->username }}">
								@if ($errors->has('username'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('username') }}</strong>
								    </span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label>Email</label>
								<input type="text" class="form-control" name="email" value="{{ $admin->email }}">
								@if ($errors->has('email'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('email') }}</strong>
								    </span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}" >
		                      <label for="department">Department:</label>
		                      <select class="form-control" id="department" name="department">
		                       <option value="" disabled="" selected="">Select Department</option>
		                      @foreach ($departments as $departmentId => $department)
		                        <option value="{{ $departmentId }}" {{ $admin->department_id == $departmentId ? 'selected' : '' }}>{{ $department }}</option>
		                      @endforeach       
		                  
		                      </select>

		                       @if ($errors->has('department'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('department') }}</strong>
		                        </span>
		                    @endif

		                    </div>

		                 <div class="form-group{{ $errors->has('campuses') ? 'has-error' : '' }}">
		                      <label for="campuses">Satellite Campus:</label>
		                      <select class="form-control" id="campuses" name="campuses">
		                       <option value="" disabled="" selected="">Select Campus</option>
		                      @foreach ($campuses as $campusesId => $campus)
		                        <option value="{{ $campusesId }}" {{ $admin->campus_id == $campusesId ? 'selected' : '' }}>{{ $campus }}</option>
		                      @endforeach       
		                      </select>
		                       @if ($errors->has('campuses'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('campuses') }}</strong>
		                        </span>
		                    @endif
		                    </div>

									    
		                 <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		                    <label for="password">Password</label>
		                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
		                    @if ($errors->has('password'))
		                            <span class="help-block">
		                                <strong style="color: red;">{{ $errors->first('password') }}</strong>
		                            </span>
		                        @endif
		                    </div>

							 <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
			                    <label for="password_confirmation">Password Confirmation</label>
			                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Enter Confirm Password">
			                    @if ($errors->has('password_confirmation'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('password_confirmation') }}</strong>
			                            </span>
			                        @endif
			                    </div>


							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
							
						</form>

					</div>
				</div>
			</div>
		</div>
</div>


@endsection