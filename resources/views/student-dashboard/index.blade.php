@extends('layouts.master')
@section('content')
<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
        @if (auth()->user()->userable->image)
            <div class="profile-header-img">
               <img src="{{ Storage::url(auth()->user()->userable->image) }}" alt="" style="height: 100%" style="width: 100%"> 
              {{--  <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%"> --}}
            </div>
            @else
            <div class="profile-header-img">
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
            @endif
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content">
   <!-- begin breadcrumb -->
   <ol class="breadcrumb float-xl-right">
   </ol>
   <h1>Student Info</h1>
   <!-- end breadcrumb --> 
    <div class="row">
        <div class="col-md-4">
      <div class="panel panel-default">
          <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">
            <h4 class="panel-title">Student Info</h4>
            <div class="panel-heading-btn">
              
            </div>
          </div>
          <div class="panel-body">
            <div class="d-flex flex-column align-items-center text-center">
             {{--  <img src="{{ asset('img/admin.png') }}" alt="Admin" class="rounded-circle" width="150"> --}}
                <img src="{{ Storage::url(auth()->user()->userable->generate_qr_code) }}" / class="rounded-square" width="150">

              <div class="mt-3">
                <h4>QR CODE</h4>
                <p class="text-secondary mb-1">Student No. : {{ auth()->user()->userable->student_no }}</p>
                <p class="text-secondary mb-1">ID No. : {{ auth()->user()->userable->generate_id }}</p>
            </div>
          </div>
          </div> 
          </div> 
          </div>  
 
   

        <div class="col-md-8">
            <div class="panel panel-default" style="height: 93%;" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Profile</div>
                <div class="panel-body">
                    <table class="table table-condensed table-hover">
                        <thead>
                          {{--   <tr>
                                <th>Student No : {{ auth()->user()->userable->student_no }}</th>
                                <th>Fullname : {{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</th>          
                            </tr>
                            <tr>
                                <th>Email : {{ auth()->user()->userable->email }}</th> 
                                <th>Address: {{ auth()->user()->userable->address }}</th>
                            </tr>
                            <tr>
                                <th>Contact No: {{ auth()->user()->userable->contact_no }}</th>
                                <th>Birthdate: {{ auth()->user()->userable->birth_date->toFormattedDateString() }}</th>
                            </tr> --}}

                        </thead>
                     <tbody>
                          <tr>
                            <td>Email Address :  {{ auth()->user()->userable->email }}</td>
                            <td>Address : {{ auth()->user()->userable->address }}</td>
                          </tr>
                          <tr>
                            <td> Contact No : {{ auth()->user()->userable->contact_no }}</td>
                            <td> Year :  {{ auth()->user()->userable->year }}</td>
                          </tr>
                          <tr>
                            <td> Campus : {{ auth()->user()->userable->campus->name }}</td>
                            <td> Department : {{ auth()->user()->userable->department->title }}</td>
                          </tr>
                          <tr>
                            <td> Course : {{ auth()->user()->userable->course->title }}</td>
                            <td> Birthdate : {{ auth()->user()->userable->birth_date }}</td>
                          </tr>
                    </table>
                </div>
            </div>
        </div>
  
<!--       <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics - (Courses)</div>
                <div class="panel-body">
          <div id="app">
            <canvas id="myCharts" width="400" height="200"></canvas>
              
          </div>
        </div>
      </div>
    </div>

   </div>
   </div>

       <div class="col-md-6">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics - (Departments)</div>
                <div class="panel-body">
          <div id="app">
            <canvas id="myChart" width="400" height="200"></canvas>
          </div>
        </div>
      </div>
    </div>

       <div class="col-md-6">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Events)</div>
                <div class="panel-body">
               <div id="app">
                  <canvas id="myChartes" width="400" height="200"></canvas>
               </div>
            </div>
         </div>
   </div>
   
</div>

 -->

  <div class="col-md-12">
      <div class="panel panel-default">
          <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">
            <h4 class="panel-title">Announcement</h4>
            <div class="panel-heading-btn">
              
            </div>
          </div>
          <div class="panel-body">
            <table class="table table-bordered table-hover">
                <thead>
                    <th>Body</th>
                </thead>
                <tbody>
                    @foreach ($student->announcements as $announcement)
                    <tr>
                        <td>{!! $announcement->body !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
      </div>
  </div>
</div>
</div>


@endsection

@push ('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>






@endpush