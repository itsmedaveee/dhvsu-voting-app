@extends('layouts.master')
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
              <img src="{{ Storage::url($nominee->avatar) ? Storage::url($nominee->avatar) :  asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%"> 
            {{--    <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%"> --}}
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</h4>
               <p class="mb-2"></p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content">
   <!-- begin breadcrumb -->
   <ol class="breadcrumb float-xl-right">
       
   </ol>
   <!-- end breadcrumb -->
   <!-- begin page-header -->
   <h1 class="page-header"> Event Details </h1>
            <!-- end page-header -->
            <!-- begin panel -->

      <div class="row">
            <div class="col-md-4">
               
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">Candidate Propaganda</h4>
                  <div class="panel-heading-btn">
                  </div>
               </div>
               <div class="panel-body">
                  <div class="form-group">
                     <div class="col-md-3">
                                 <p>{!! $nominee->propaganda !!}</p>
                        </tbody>
                     </table>
                     </div>
                  </div>
               </div>
            </div>
            </div>
      

<div class="col-md-8">
 <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Nominee Pageant Profile</h4>
         <div class="panel-heading-btn">
         </div>
      </div>
      <div class="panel-body">
         <table class="table table-bordered">
            <tbody>
               <tr>
                  <td>Title</td>
                  <td colspan="2">{{ $nominee->title }}</td>
               </tr>             

               <tr>
                  <td>Contact No</td>
                  <td colspan="2">{{ $nominee->contact_no }}</td>
               </tr>             
 
               <tr>
                  <td>Campus</td>
                  <td colspan="2">{{ $nominee->campus->name }}</td>
               </tr>
               <tr>
                  <td>College</td>
                  <td colspan="2"> {{ $nominee->department->title }}</td>
               </tr>
               <tr>
                  <td>Course</td>
                  <td colspan="2">{{ $nominee->course->title }}</td>
               </tr>
               <tr>
                  <td>Place of Birth</td>
                  <td colspan="2">{{ $nominee->place_of_birth }}</td>
               </tr>
               <tr>
                  <td>Religion</td>
                  <td colspan="2"> {{ $nominee->religion }}</td>
               </tr>

            </tbody>
         </table>
         
           
</div>
</div>

</div>



 

@endsection

@push ('scripts')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-datalabels/2.0.0/chartjs-plugin-datalabels.min.js" integrity="sha512-R/QOHLpV1Ggq22vfDAWYOaMd5RopHrJNMxi8/lJu8Oihwi4Ho4BRFeiMiCefn9rasajKjnx9/fTQ/xkWnkDACg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
{{-- <script>
var resultsWithNominees = {!! $resultNominee !!};
var nomineeFullName = {!! $nomineeFullName !!};
console.log(resultsWithNominees);
const charting = document.getElementById('myCharts').getContext('2d');
const myCharts = new Chart(charting, {
    type: 'pie',
    data: {
        labels: nomineeFullName,
        datasets: [{
            label:  '# of Votes',
            data: resultsWithNominees,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },


    options: {
        scales: {
            y: {
                beginAtZero: true
            },
            plugins: [ChartDataLabels]
        }
    }
});

</script> --}}
   {{--  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-datalabels/2.0.0/chartjs-plugin-datalabels.min.js" integrity="sha512-R/QOHLpV1Ggq22vfDAWYOaMd5RopHrJNMxi8/lJu8Oihwi4Ho4BRFeiMiCefn9rasajKjnx9/fTQ/xkWnkDACg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script> 
   var resultsWithNominees = {!! $resultNominee !!};
   var nomineeFullName = {!! $nomineeFullName !!};
   console.log(resultsWithNominees);
    const data = {
      labels: nomineeFullName,
      datasets: [{
        label: 'Events',
        data: resultsWithNominees,
        backgroundColor: [
             'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
        ],
        borderColor: [
          'rgba(255, 26, 104, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(0, 0, 0, 1)'
        ],
        borderWidth: 1
      }]
    };
 

    const config = {
      type: 'doughnut',
      data, 
      options: {
         scales: {
         },
         plugins: {
            tooltip: {
               enabled: false
         },
         datalabels: {
            formatter: (value, context) => {
               const datapoints = context.chart.data.datasets[0].data;
               function totalSum(total, datapoint) {
                  return total + datapoint;
               }
               const totalPercentage = datapoints.reduce(totalSum, 0);
               const percentageValue = (value / totalPercentage * 100).toFixed(1);
               return `${percentageValue}%`;
            }
         }
      }
   },
         plugins: [ChartDataLabels]
    };
    // render init block
    const myChart = new Chart(
      document.getElementById('myChart'),
      config
    );
    </script>
 --}}

@endpush