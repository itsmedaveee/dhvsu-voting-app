@extends('layouts.master')
@inject('election','App\Student') 
@section('content')

<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		 
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Candidates Event </h1>
	<!-- end page-header -->
	<!-- begin panel --> 
		<div class="form-group">
		    <div class="alert alert-primary rounded-0 d-flex align-items-center mb-0 text-blue-900">
                  <div class="fs-24px w-80px text-center">
                     <i class="fa fa-lightbulb fa-4x"></i>
                  </div>
                  <div class="flex-1 ms-3">
                     <h4> NOTES</h4>
                     <ul class="ps-3">
                        <li><strong> Select your preferred candidate by clicking/tapping the sliding button to the left of the candidate's name. </strong></li>
                        <li><strong> If it turns green, it means you already voted for that candidate; otherwise you did not vote for that candidate.</strong></li>
                        <li><strong>Tap or click the done button to submit your votes.</strong>
                        </li>
                         
                     </ul>
                  </div>
               </div>
		</div>
			<div class="form-group">
		    <div class="alert alert-success rounded-0 d-flex align-items-center mb-0 text-blue-900">
                  <div class="fs-24px w-80px text-center">
                  </div>
                  <div class="flex-1 ms-3">
	                    	<h4>  {{ $election->eventStatus() }}</h4>
	                    	@if ($election->pageantEventStatus())
	                    	<h4>The event will end at the time of {{ \Carbon\Carbon::parse($timeElection->ended_time)->format('h:i A') }}
           	</h4>
           	@endif
                  </div>
               </div>
	</div>
	@if ($election->pageantEventStatus())
		<div class="row">
			<div class="col-md-12">
				
			<form method="POST" action="/votes-for-pageant">
			 <input type="hidden" name="id" value="{{ Auth::user()->id }}">
		
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Events</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="table-responsive">
					<div class="panel-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th></th>
									<th>Image</th>
									<th>Name</th>
									<th>Title</th>
									<th>Colleges</th>
									<th>Campus</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>	

							@foreach ($events as $event)
								@foreach ($event->nominees as $nominee)
									@csrf
									<tr class="odd gradeX">
										<td> 
										      <input type="checkbox"  
										      
										      	name="nominee[]" 
										      	value="{{ $nominee->id }}" {{ $voted->contains($nominee->id) ? 'checked' : '' }} 
										      	/>

										 {{-- 	</div> --}}
										 </td>
										 <td width="1%">
									{{-- 	 	<img src="{{ asset('img/admin.png') }}" class="rounded h-30px my-n1 mx-n1" /> --}}
										 	 <img src="{{ Storage::url($nominee->avatar) ? Storage::url($nominee->avatar) :  asset('img/admin.png') }}" class="rounded h-30px my-n3 mx-n1 center"/ >

										 </td>
										<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
										<td>{{ $nominee->title }}</td>
										<td>{{ $nominee->department->title }}</td>
										<td>{{ $nominee->campus->name }}</td>
										<td>
											<a href="/candidates-pageant/{{ $nominee->id }}/view" class="btn btn-primary btn-xs">View Details</a>
										</td>
									</tr>
		 		
									@endforeach
								@endforeach 
							</tbody>
						</table>
					</div>
				</div>
				</div>


									<!-- Modal -->
					<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
					  <div class="modal-dialog modal-dialog-centered" role="document">
					    <div class="modal-content">
					      <div class="">
					 
					      </div>
					      <div class="modal-body">{{-- 
					      	<p class="text-center"><img src="{{ asset('img/mark.png') }}" style="width: 20%;" ></p> --}}
								<div class="swal-icon swal-icon--warning">
								<span class="swal-icon--warning__body">
								<span class="swal-icon--warning__dot"></span>
								</span>
								</div>
					        <h5 style="font-size: 1.875rem;font-weight:600; " class="text-center">DO YOU WISH TO SUBMIT YOUR VOTE?</h5>
					        <p style="font-size:.875rem;display:block;max-width:100%;text-align:center;padding:0 0 .9375rem;font-weight:600;">Please note that once submitted you cannot edit/resubmit another vote. You can only vote once, so vote wisely</p>
					        <div class="form-group" align="center">
					         <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel Vote</button>
					        <button type="submit" class="btn btn-primary">Submit Vote</button>
					     </div>
					      </div>
					       
					   
					    </div>
					  </div>
					</div>


				<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Comment/Suggestion</h4>
								<div class="panel-heading-btn">
								</div>
							</div>
							<div class="table-responsive">
							<div class="panel-body">
								<textarea class="form-control" name="comment"></textarea>
							</div>
						</div>
					</div>
					 <button type="button" class="btn btn-sm btn-primary swal-overlay swal-overlay--show-modal"  data-toggle="modal" data-target="#exampleModalCenter" onclick="handleSubmit()">Submit</button>
			
				</form>
			</div>
		</div>
	@endif
	</div>

</div>
 
@endsection

@push ('scripts')

	<script>

		let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switchery'));
		elems.forEach(function(html) {
			let switchery = new Switchery(html, { 
				color: '#0A9913', 
				secondaryColor: '#feba29 ', 
				Color: '#fff', 
				SecondaryColor: '#fff' 
			});
		});

	</script>
	<script>
	function handleSubmit () {
	    document.getElementById('form').submit();
	}
	</script>

	<script>
   function handleSubmit () {
       document.getElementById('form').submit();
   }
   $('input[type="checkbox"]').on('change', function() {
       $('input[id="' + this.id + '"]').not(this).prop('checked', false);
   });
</script>

@endpush