@extends('layouts.master')
@section('content')
<div id="content" class="content"> 
   <ol class="breadcrumb float-xl-right"></ol>
   <h1 class="page-header">Candidacy Form</h1>
 
   <div class="row">
         <div class="col-md-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     Candidacy Form
                     </b>
                  </h4>
                  <div class="panel-heading-btn">
                  </div>
               </div>
               <div class="alert alert-warning rounded-0 d-flex align-items-center mb-0 text-blue-900">
                  <div class="fs-24px w-80px text-center">
                     <i class="fa fa-lightbulb fa-4x"></i>
                  </div>
                  <div class="flex-1 ms-3">
                     <h4> REQUIRED DOCUMENTS</h4>
                     <ul class="ps-3">
                        <li><strong>PHOTOCOPY OF CERTIFICATE OF REGISTRATION </strong></li>
                        <li><strong>PHOTOCOPY OF VALIDATED SCHOOL I.D</strong></li>
                        <li><strong> PHOTOCOPY OF CERTIFICATE OF ATTENDANCE/
                           PARTICIPATION OF LEADERSHIP SEMINAR/ TRAINING ATTENDED</strong>
                        </li>
                        <li><strong>RECENT 2X2 PHOTO (COLORED)</strong></li>
                        <li><strong>COPY OF GRADES (COG)</strong></li>
                        <li><strong>RECOMMENDATION LETTER FROM THE DEAN ON HIS/HER COLLEGE</strong></li>
                        <li><strong>CERTIFICATE OF GOOD MORAL </strong></li>
                        <strong>NOTE: <i> Regular and Irregular student/s must have at least 15 units load.</i></strong><br>
                       <strong>PLEASE FILL OUT THE FORM BELOW.</strong>
                     </ul>
                  </div>
               </div>
               <div class="panel-body">
                  <form id="form1" method="POST" action="/request-file-coc"  enctype="multipart/form-data" >
                     @csrf
                     @include('student-dashboard.request-coc-file.modal.index')
                     <div class="row">
                       <div class="col-md-12">  
                           <div class="form-group">
                              <label>Select Category</label>
                              <select class="form-control" id="mySelect"  name="categories" onchange="myFunction()">
                                 <option value="" disabled="" selected="" >Categories</option>
                                 <option value="Filing Campus">Filing Campus (University Level)</option>
                                 <option value="Filing Department">Filing College (College Level)</option>
                                 <option value="Filing Course">Filing Department (Department Level)</option>
                                 <option value="Filing Event">Filing Events (Events Level)</option>
                              </select>
                           </div>

                          {{--  <input type="text" class="form-control" id="jquery-autocomplete" /> --}}

                          <div class="form-group">
                              @if ($errors->any())
                                 <div class="alert alert-danger">
                                    <ul>
                                       @foreach ($errors->all() as $error)
                                       <li>{{ $error }}</li>
                                       @endforeach
                                    </ul>
                                 </div>
                              @endif
                          </div>

                           <div class="form-group">
                              <div id="usc"></div>
                           </div>
                           <div class="form-group">
                              <div id="personal"></div>
                           </div>
                        </div>  
                
                        <div class="col-md-4">
                           <div id="firstname"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="middlename"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="lastname"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="contact_no"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="campus"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="department"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="courses"></div>
                        </div>

                        <div class="col-md-4">
                           <div id="religion"></div>
                        </div>

                        <div class="col-md-4">
                           <div id="place_of_birth"></div>
                        </div>

                       <div class="col-md-12">
                              <div id="nationality"></div>
                        </div>

                        <div class="col-md-6">
                           <div id="birthdate"></div>
                        </div>

                        <div class="col-md-6{{ $errors->has('gender') ? ' has-error' : '' }}">
                           <div id="gender"></div>
                        </div>
 
                        <div class="col-md-6">
                           <div id="election"></div>
                        </div>
                        <div class="col-md-6">
                           <div id="position"></div>
                        </div>
 
                          <div class="col-md-12">
                            <div id="acad"></div>
                        </div>



                        <div class="col-md-4">
                           <div id="avatar"></div>
                        </div>
             
                        <div class="col-md-4">
                           <div id="upload_cor"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="validate_school_id"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="certificate_of_attendance"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="recent_photo"></div>
                        </div>
                        <div class="col-md-4">
                              <div id="recomend_letter_from_dean"></div>
                        </div>
                        <div class="col-md-4">
                              <div id="certificate_of_good_moral"></div>
                        </div>
                        <div class="col-md-4">
                              <div id="class_of_grade"></div>
                        </div>
                         <div class="col-md-4">
                              <div id="extra_curricular_activities"></div>
                        </div>

                           <div class="col-md-4">
                              <div id="training_seminars"></div>
                        </div>
                        <div class="col-md-4">
                              <div id="partyname"></div>
                        </div>
                        <div class="col-md-4">
                              <div id="average_last_semister"></div>
                        </div>
                       
                      {{--   <div class="col-md-4">
                              <div id="award_honors_received"></div>
                        </div> --}}
                       
                     

                         <div class="col-md-6">
                              <div id="previous_failed_subject"></div>
                        </div>
                        <div class="col-md-6">
                              <div id="date_filled_out"></div>
                        </div>
               
                        <div class="col-md-12">
                              <div id="reason_for_candidacy"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="event"></div>
                        </div>                   
                        <div class="col-md-4">
                           <div id="title"></div>
                        </div>
                      
                        <div class="col-md-4">
                           <div id="upload_avatar"></div>
                        </div>
                        
                    {{--     <div class="col-md-12">
                           <div class="form-group">
                              <label>Propaganda</label>
                              <textarea id="myeditorinstance" name="propaganda"></textarea>
                              @if ($errors->has('propaganda'))
                              <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('propaganda') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div> --}}
                        <div class="col-md-12">
                           <div id="propaganda"></div>
                        </div>
                        <div class="col-md-12">
                           <div id="agree"></div>
                        </div>
                        <div class="col-md-4">
                           <div id="submit"></div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
</div>
<!-- end #content -->
@endsection
@push ('scripts')
<script>
   tinymce.init({
     selector: 'textarea#myeditorinstance', // Replace this CSS selector to match the placeholder element for TinyMCE
     plugins: 'code table lists',
     toolbar: 'undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table'
   });
</script>
<script type = "text/javascript" >
    function myFunction() {
        var selected = document.getElementById("mySelect").value;
        if (selected === 'Filing Campus') {
          document.getElementById("title").innerHTML = '';
          document.getElementById("event").innerHTML =
          document.getElementById("upload_avatar").innerHTML = '';
            document.getElementById("personal").innerHTML = ` <h6>PERSONAL INFORMATION:</h6>`;
                      document.getElementById("election").innerHTML = `<div class="form-group"><label for="name">Election: (Campus) <font style="color:red;">*</font></label> 
                  <select class="form-control" id="election" name="election">
                         <option value=""  selected="" disabled>Select Election:</option>
                         @foreach ($elections as $electionId => $election )
                         <option value="{{ $electionId }}">{{ $election }}</option>
                           <span class="help-block">
                         @if ($errors->has('election'))
                          <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('election') }}</strong>
                          </span>
                         @endif
                        @endforeach
                     </select></div>`;

            document.getElementById("position").innerHTML = `<div class="form-group"><label for="name">Position: (Campus) <font style="color:red;">*</font></label> 
                   <select class="form-control" id="position" name="position">
                        <option value=""  selected="">Select Position</option>
                        @foreach ($positions as $position )
                           <option value="{{ $position->id }}">{{ $position->position_name }}</option>
                           <span class="help-block">
                              @if ($errors->has('position'))
                                 <span class="help-block">
                                 <strong style="color:red;">{{ $errors->first('position') }}</strong>
                                 </span>
                              @endif
                        @endforeach
                     </select></div>`;

            document.getElementById("avatar").innerHTML = `<div class="form-group"> <label>Upload Photo <font style="color:red;">*</font></label>
                   <input type="file" class="form-control" name="avatar">
                     @if ($errors->has('avatar'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('avatar') }}</strong>
                      </span>
                     @endif</div>`;
            document.getElementById("upload_cor").innerHTML = `<div class="form-group">  <label>Upload COR <font style="color:red;">*</font></label>
                      <input type="file" class="form-control" name="certificate_of_registration">
                     @if ($errors->has('certificate_of_registration'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('certificate_of_registration') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("validate_school_id").innerHTML = ` <div class="form-group"><label>Upload Validate ID <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="validate_school_id">
                     @if ($errors->has('validate_school_id'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('validate_school_id') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("certificate_of_attendance").innerHTML = `   <div class="form-group"><label> Certificate of Awards / Honors received <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="certificate_of_attendance">
                     @if ($errors->has('certificate_of_attendance'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('certificate_of_attendance') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("recent_photo").innerHTML = `<div class="form-group"><label>Upload (2x2) Recent Photo (Colored) <font style="color:red;">*</font></label>
                      <input type="file" class="form-control" name="recent_photo">
                     @if ($errors->has('recent_photo'))
                      <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('recent_photo') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("recomend_letter_from_dean").innerHTML = ` <div class="form-group"><label> Recommendation Letter From Dean <font style="color:red;">*</font> </label>
                        <input type="file" class="form-control" name="recomend_letter_from_dean">
                     @if ($errors->has('recomend_letter_from_dean'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('recomend_letter_from_dean') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("certificate_of_good_moral").innerHTML = `<div class="form-group"><label> Certificate of Good Moral <font style="color:red;">*</font> </label>
                        <input type="file" class="form-control" name="certificate_of_good_moral">
                     @if ($errors->has('certificate_of_good_moral'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('certificate_of_good_moral') }}</strong>
                      </span>
                     @endif</div>`;
            document.getElementById("class_of_grade").innerHTML = `<div class="form-group"> <label> Copy of Grades <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="class_of_grade">
                     @if ($errors->has('class_of_grade'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('class_of_grade') }}</strong>
                      </span>
                     @endif</div>`;

                  document.getElementById("partyname").innerHTML = `<div class="form-group"><label for="name">Party Name: <font style="color:red;">*</font></label>
                  <input type="text" name="partyname" class="form-control">
                  </div>`;

            document.getElementById("average_last_semister").innerHTML = `  <div class="form-group"> <label>Average last Semester <font style="color:red;">*</font></label>
                      <input type="text" class="form-control" name="average_last_semister">
                     @if ($errors->has('average_last_semister'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('average_last_semister') }}</strong>
                      </span>
                     @endif</div>`;
            document.getElementById("previous_failed_subject").innerHTML = `<div class="form-group"> <label>Any previous failed subject (specify) </label>
                      <input type="text" class="form-control" name="previous_failed_subject" placeholder="Optional">
                     @if ($errors->has('previous_failed_subject'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('previous_failed_subject') }}</strong>
                      </span>
                     @endif</div>`;

       
            document.getElementById("extra_curricular_activities").innerHTML = `<div class="form-group"><label>Certificate of Extra-Curricular Activities <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="extra_curricular_activities">
                     @if ($errors->has('extra_curricular_activities'))
                      <span class="help-block">
                          <strong style="color:red;">{{ $errors->first('extra_curricular_activities') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("training_seminars").innerHTML = ` <div class="form-group"> <label>Certificate of Training/Seminars Attended<font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="training_seminars">
                     @if ($errors->has('training_seminars'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('training_seminars') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("date_filled_out").innerHTML = `<div class="form-group"> <label>Date Filled Out <font style="color:red;">*</font></label>
                        <input type="date" class="form-control" name="date_filled_out" value="{{ $user->student->created_at->format('Y-m-d')  }}">
                     @if ($errors->has('date_filled_out'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('date_filled_out') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("nationality").innerHTML = `<div class="form-group">  <label>Nationality <font style="color:red;">*</font></label>
                        <input type="text" class="form-control" name="nationality"  value="{{ $user->student->nationality }}" >
                     @if ($errors->has('nationality'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('nationality') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("reason_for_candidacy").innerHTML = `<div class="form-group"> <label>Reason for Candidacy <font style="color:red;">*</font></label>
                        <textarea type="text" class="form-control" name="reason_for_candidacy"></textarea>
                     @if ($errors->has('reason_for_candidacy'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('reason_for_candidacy') }}</strong>
                      </span>
                     @endif</div>`;

             document.getElementById("gender").innerHTML = `<label>Gender <font style="color:red;">*</font></label>
               <input type="text" class="form-control" name="gender" value="{{ $user->student->gender->gender_name }}" readonly>
               @if ($errors->has('gender'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('gender') }}</strong>
                  </span>
               @endif`;

            document.getElementById("birthdate").innerHTML = `<div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
               <label>Birthdate <font style="color:red;">*</font></label> 
               <input type="date" class="form-control" name="birthdate" value="{{ $user->student->birth_date->format('Y-m-d') }}">
                  @if ($errors->has('birthdate'))
                     <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('birthdate') }}</strong>
                     </span>
                  @endif
            </div>`;

           document.getElementById("place_of_birth").innerHTML = `<div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : '' }}">
                     <label>Place of Birth <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="place_of_birth" value="{{ $user->student->place_of_birth }}" >
                     @if ($errors->has('place_of_birth'))
                     <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('place_of_birth') }}</strong>
                     </span>
                     @endif
                  </div>`;

         document.getElementById("religion").innerHTML = ` <div class="form-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                     <label>Religion <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="religion" value="{{ $user->student->religion }}" >
                     @if ($errors->has('religion'))
                     <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('religion') }}</strong>
                     </span>
                     @endif
                  </div>`;

        document.getElementById("courses").innerHTML = `<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                              <label for="name">Select Course <font style="color:red;">*</font></label>
                              <select class="form-control" id="course" name="course" readonly="">
                                 <option value="" disabled="" selected="">Select Course</option>
                                    @foreach ($courses as  $courseId => $course )
                                    <option value="{{ $courseId }}"  {{ auth()->user()->course_id == $courseId ? 'selected' : '' }}>{{ $course }}</option>
                                    <span class="help-block">
                                       @if ($errors->has('course'))
                                          <span class="help-block">
                                             <strong style="color:red;">{{ $errors->first('course') }}</strong>
                                          </span>
                                       @endif
                                    @endforeach
                              </select>
                           </div>`;
 

                     document.getElementById("campus").innerHTML = `<div class="form-group{{ $errors->has('campus') ? ' has-error' : '' }}">
                        <label for="name">Select Campus <font style="color:red;">*</font></label>
                           <select class="form-control" id="campus" name="campuses" readonly="">
                              <option value=""  selected="">Select Campus</option>
                              @foreach ($campuses as $campusId =>  $campus )
                              <option value="{{ $campusId }}" {{ auth()->user()->campus_id == $campusId ? 'selected' : '' }}>{{ $campus }}</option>
                              <span class="help-block">
                                 @if ($errors->has('campus'))
                                    <span class="help-block">
                                       <strong style="color:red;">{{ $errors->first('campus') }}</strong>
                                    </span>
                                 @endif
                              </span>
                           @endforeach
                        </select>
                     </div>`;
  
               document.getElementById("department").innerHTML = ` <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                  <label for="name">Select Department <font style="color:red;">*</font></label>
                  <select class="form-control" id="department" name="department" readonly="">
                     <option value="" disabled="" selected="">Select Department</option>
                     @foreach ($departments as $departmentId => $department )
                     <option value="{{ $departmentId }}" {{ auth()->user()->department_id == $departmentId ? 'selected' : '' }}>{{ $department }}</option>
                     <span class="help-block">
                        @if ($errors->has('department'))
                           <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('department') }}</strong>
                           </span>
                        @endif
                     </span>
                     @endforeach
                  </select>
               </div>`;     

         document.getElementById("contact_no").innerHTML = `<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
                  <label>Contact No <font style="color:red;">*</font></label>
                  <input type="text" class="form-control" name="contact_no" value="{{ $user->student->contact_no }}" readonly="">
                  @if ($errors->has('contact_no'))
                  <span class="help-block">
                  <strong style="color:red;">{{ $errors->first('contact_no') }}</strong>
                  </span>
                  @endif
               </div>`;      

         document.getElementById("lastname").innerHTML = ` <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                        <label>Last Name <font style="color:red;">*</font></label>
                        <input type="text" class="form-control" name="lastname"  value="{{ $user->student->lastname }}" readonly="">
                        @if ($errors->has('lastname'))
                        <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('lastname') }}</strong>
                        </span>
                        @endif   
                     </div>`;
         document.getElementById("middlename").innerHTML = ` <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
                              <label>Middle Name <font style="color:red;">*</font></label>
                              <input type="text" class="form-control" name="middlename" value="{{ $user->student->middlename }}" readonly="">
                              @if ($errors->has('middlename'))
                              <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
                              </span>
                              @endif
                           </div>`;         


         document.getElementById("firstname").innerHTML = ` <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                              <label>First Name <font style="color:red;">*</font></label>
                              <input type="text" class="form-control" name="firstname" value="{{ $user->student->firstname }}" readonly="">
                              @if ($errors->has('firstname'))
                              <span class="help-block">
                                 <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
                              </span>
                              @endif
                           </div>`;
          document.getElementById("propaganda").innerHTML = ` <div class="form-group">
                  <label>Propaganda (Optional)</label>
                  <textarea  name="propaganda" class="form-control"></textarea>
                  @if ($errors->has('propaganda'))
                  <span class="help-block">
                  <strong style="color:red;">{{ $errors->first('propaganda') }}</strong>
                  </span>
                  @endif
            </div>`;         

         document.getElementById("agree").innerHTML = `  <input type="checkbox" name="certified"> <i>I hereby certify that all information state herein is true end accurate to the best of my knowledge.</i>`;             
         document.getElementById("usc").innerHTML = `  <input type="checkbox" name="usc" value="University Student Council">  University Student Council (Optional)`;   

         // document.getElementById("submit").innerHTML = ` <div class="form-group">
         //    <button type="submit" class="btn btn-primary">Submit</button>
         // </div>`;        

        document.getElementById("acad").innerHTML = `<h6>ACADEMIC RECORD:</h6> <br> 
        <p><i>Notes: (Separate multiple entries with commas)</i></p>
        `;      

         document.getElementById("submit").innerHTML = `    <button type="button" class="btn btn-sm btn-primary swal-overlay swal-overlay--show-modal"  data-toggle="modal" data-target="#exampleModalCenter" onclick="handleSubmit()">Submit</button>`;



        } else if (selected === 'Filing Department') {
          document.getElementById("title").innerHTML = '';
          document.getElementById("upload_avatar").innerHTML = '';
          document.getElementById("event").innerHTML = '';
          document.getElementById("usc").innerHTML = '';
          document.getElementById("acad").innerHTML = `<h6>ACADEMIC RECORD:</h6>  <br> 
        <p><i>Notes: (Separate multiple entries with commas)</i></p>`;  
           document.getElementById("personal").innerHTML = ` <h6>PERSONAL INFORMATION:</h6>`;    
            document.getElementById("election").innerHTML = document.getElementById("election").innerHTML = `<div class="form-group"><label for="name">Election: (College) <font style="color:red;">*</font></label> 
                  <select class="form-control" id="election" name="electionDepartment">
                         <option value=""  selected="">Select Election</option>
                         @foreach ($electionDepartments as $electionId => $election )
                         <option value="{{ $electionId }}">{{ $election }}</option>
                           <span class="help-block">
                         @if ($errors->has('election'))
                          <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('election') }}</strong>
                          </span>
                         @endif
                        @endforeach
                     </select></div>`;

               document.getElementById("birthdate").innerHTML = `<div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
               <label>Birthdate <font style="color:red;">*</font></label> 
               <input type="date" class="form-control" name="birthdate" value="{{ $user->student->birth_date->format('Y-m-d') }}">
                  @if ($errors->has('birthdate'))
                     <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('birthdate') }}</strong>
                     </span>
                  @endif
            </div>`;
            document.getElementById("position").innerHTML = `<div class="form-group"><label for="name">Position: (College)</label> 
                   <select class="form-control" id="position" name="positionDepartment">
                        <option value=""  selected="">Select Position <font style="color:red;">*</font></option>
                        @foreach ($positionsDepartment as $position )
                           <option value="{{ $position->id }}">{{ $position->position_name }}</option>
                           <span class="help-block">
                              @if ($errors->has('position'))
                                 <span class="help-block">
                                 <strong style="color:red;">{{ $errors->first('position') }}</strong>
                                 </span>
                              @endif
                        @endforeach
                     </select></div>`;
            document.getElementById("avatar").innerHTML = `<div class="form-group"> <label>Upload Photo <font style="color:red;">*</font></label>
                   <input type="file" class="form-control" name="avatar">
                     @if ($errors->has('avatar'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('avatar') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("upload_cor").innerHTML = `<div class="form-group">   <label>Upload COR <font style="color:red;">*</font></label>
                      <input type="file" class="form-control" name="certificate_of_registration">
                     @if ($errors->has('certificate_of_registration'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('certificate_of_registration') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("validate_school_id").innerHTML = ` <div class="form-group"><label>Upload Validate ID <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="validate_school_id">
                     @if ($errors->has('validate_school_id'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('validate_school_id') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("certificate_of_attendance").innerHTML = ` <div class="form-group">  <label>Certificate of Awards / Honors received <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="certificate_of_attendance">
                     @if ($errors->has('certificate_of_attendance'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('certificate_of_attendance') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("recent_photo").innerHTML = `  <div class="form-group"><label>Upload (2x2) Recent Photo (Colored) <font style="color:red;">*</font></label>
                      <input type="file" class="form-control" name="recent_photo">
                     @if ($errors->has('recent_photo'))
                      <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('recent_photo') }}</strong>
                      </span>
                     @endif</div>`;
            document.getElementById("recomend_letter_from_dean").innerHTML = `  <div class="form-group"><label> Recommendation Letter from the Dean <font style="color:red;">*</font> </label>
                        <input type="file" class="form-control" name="recomend_letter_from_dean">
                     @if ($errors->has('recomend_letter_from_dean'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('recomend_letter_from_dean') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("certificate_of_good_moral").innerHTML = `  <div class="form-group"> <label> Certificate of Good Moral <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="certificate_of_good_moral">
                     @if ($errors->has('certificate_of_good_moral'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('certificate_of_good_moral') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("class_of_grade").innerHTML = ` <div class="form-group"> <label> Copy of Grades <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="class_of_grade">
                     @if ($errors->has('class_of_grade'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('class_of_grade') }}</strong>
                      </span>
                     @endif</div>`;
              
                  document.getElementById("partyname").innerHTML = `<div class="form-group"><label for="name">Party Name: <font style="color:red;">*</font></label>
                  <input type="text" name="partyname" class="form-control">
                  </div>`;

              document.getElementById("gender").innerHTML = `<label>Gender <font style="color:red;">*</font></label>
               <input type="text" class="form-control" name="gender" value="{{ $user->student->gender->gender_name }}" readonly>
               @if ($errors->has('gender'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('gender') }}</strong>
                  </span>
               @endif`;

            document.getElementById("average_last_semister").innerHTML = `  <div class="form-group">  <label>Average last Semester <font style="color:red;">*</font></label>
                      <input type="text" class="form-control" name="average_last_semister">
                     @if ($errors->has('average_last_semister'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('average_last_semister') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("previous_failed_subject").innerHTML = ` <div class="form-group"> <label>Any previous failed subject (specify)</label>
                      <input type="text" class="form-control" name="previous_failed_subject" placeholder="Optional">
                     @if ($errors->has('previous_failed_subject'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('previous_failed_subject') }}</strong>
                      </span>
                     @endif</div>`;
         

            document.getElementById("extra_curricular_activities").innerHTML = ` <div class="form-group"><label>Extra Curricular_Activities <font style="color:red;">*</font></label>
                        <input type="text" class="form-control" name="extra_curricular_activities">
                     @if ($errors->has('extra_curricular_activities'))
                      <span class="help-block">
                          <strong style="color:red;">{{ $errors->first('extra_curricular_activities') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("training_seminars").innerHTML = ` <div class="form-group{{ $errors->has('training_seminars') ? ' has-error' : '' }}"><label>Certificate of Training/Seminars Attended* <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="training_seminars" >
                     @if ($errors->has('training_seminars'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('training_seminars') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("date_filled_out").innerHTML = ` <div class="form-group"> <label>Date Filled Out <font style="color:red;">*</font></label>
                        <input type="date" class="form-control" name="date_filled_out" value="{{ $user->student->created_at->format('Y-m-d')  }}">
                     @if ($errors->has('date_filled_out'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('date_filled_out') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("nationality").innerHTML = ` <div class="form-group">  <label>Nationality <font style="color:red;">*</font></label>
                        <input type="text" class="form-control" name="nationality" value="{{ $user->student->nationality }}" >
                     @if ($errors->has('nationality'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('nationality') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("reason_for_candidacy").innerHTML = `  <div class="form-group"> <label>Reason for Candidacy <font style="color:red;">*</font></label>
                        <textarea type="text" class="form-control" name="reason_for_candidacy"></textarea>
                     @if ($errors->has('reason_for_candidacy'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('reason_for_candidacy') }}</strong>
                      </span>
                     @endif</div>`;
                  document.getElementById("gender").innerHTML = `<label>Gender <font style="color:red;">*</font></label>
               <input type="text" class="form-control" name="gender" value="{{ $user->student->gender->gender_name }}" readonly>
               @if ($errors->has('gender'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('gender') }}</strong>
                  </span>
               @endif`;

               document.getElementById("birthdate").innerHTML = `<div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
               <label>Birthdate <font style="color:red;">*</font></label> 
               <input type="date" class="form-control" name="birthdate" value="{{ $user->student->birth_date->format('Y-m-d') }}">
                  @if ($errors->has('birthdate'))
                     <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('birthdate') }}</strong>
                     </span>
                  @endif
            </div>`;


            document.getElementById("place_of_birth").innerHTML = `<div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : '' }}">
                              <label>Place of Birth <font style="color:red;">*</font></label>
                              <input type="text" class="form-control" name="place_of_birth" value="{{ $user->student->place_of_birth }}" >
                              @if ($errors->has('place_of_birth'))
                              <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('place_of_birth') }}</strong>
                              </span>
                              @endif
                           </div>`;

            document.getElementById("religion").innerHTML = ` <div class="form-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                     <label>Religion <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="religion" value="{{ $user->student->religion }}" >
                     @if ($errors->has('religion'))
                     <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('religion') }}</strong>
                     </span>
                     @endif
                  </div>`;


               document.getElementById("courses").innerHTML = `<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                  <label for="name">Select Course <font style="color:red;">*</font></label>
                  <select class="form-control" id="course" name="course" readonly="">
                     <option value="" disabled="" selected="">Select Course</option>
                        @foreach ($courses as  $courseId => $course )
                        <option value="{{ $courseId }}"  {{ auth()->user()->course_id == $courseId ? 'selected' : '' }}>{{ $course }}</option>
                        <span class="help-block">
                           @if ($errors->has('course'))
                              <span class="help-block">
                                 <strong style="color:red;">{{ $errors->first('course') }}</strong>
                              </span>
                           @endif
                        @endforeach
                  </select>
               </div>`;
               document.getElementById("campus").innerHTML = `<div class="form-group{{ $errors->has('campus') ? ' has-error' : '' }}">
                        <label for="name">Select Campus <font style="color:red;">*</font></label>
                           <select class="form-control" id="campus" name="campuses" readonly="">
                              <option value=""  selected="">Select Campus</option>
                              @foreach ($campuses as $campusId =>  $campus )
                              <option value="{{ $campusId }}" {{ auth()->user()->campus_id == $campusId ? 'selected' : '' }}>{{ $campus }}</option>
                              <span class="help-block">
                                 @if ($errors->has('campus'))
                                    <span class="help-block">
                                       <strong style="color:red;">{{ $errors->first('campus') }}</strong>
                                    </span>
                                 @endif
                              </span>
                           @endforeach
                        </select>
                     </div>`;

                document.getElementById("department").innerHTML = ` <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                  <label for="name">Select Department <font style="color:red;">*</font></label>
                  <select class="form-control" id="department" name="department" readonly="">
                     <option value="" disabled="" selected="">Select Department</option>
                     @foreach ($departments as $departmentId => $department )
                     <option value="{{ $departmentId }}" {{ auth()->user()->department_id == $departmentId ? 'selected' : '' }}>{{ $department }}</option>
                     <span class="help-block">
                        @if ($errors->has('department'))
                           <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('department') }}</strong>
                           </span>
                        @endif
                     </span>
                     @endforeach
                  </select>
               </div>`;


         document.getElementById("contact_no").innerHTML = `<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
                  <label>Contact No <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="contact_no" value="{{ $user->student->contact_no }}" readonly="">
                     @if ($errors->has('contact_no'))
                        <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('contact_no') }}</strong>
                        </span>
                     @endif
               </div>`; 

         document.getElementById("lastname").innerHTML = ` <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                        <label>Last Name <font style="color:red;">*</font></label>
                        <input type="text" class="form-control" name="lastname"  value="{{ $user->student->lastname }}" readonly="">
                        @if ($errors->has('lastname'))
                        <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('lastname') }}</strong>
                        </span>
                        @endif   
             </div>`;         

             document.getElementById("middlename").innerHTML = ` <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
                              <label>Middle Name <font style="color:red;">*</font></label>
                              <input type="text" class="form-control" name="middlename" value="{{ $user->student->middlename }}" readonly="">
                              @if ($errors->has('middlename'))
                              <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
                              </span>
                              @endif
                           </div>`;


         document.getElementById("firstname").innerHTML = ` <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                  <label>First Name <font style="color:red;">*</font></label>
                  <input type="text" class="form-control" name="firstname" value="{{ $user->student->firstname }}" readonly="">
                  @if ($errors->has('firstname'))
                  <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
                  </span>
                  @endif
               </div>`;
       document.getElementById("propaganda").innerHTML = ` <div class="form-group">
                  <label>Propaganda (Optional)</label>
                  <textarea  name="propaganda" class="form-control"></textarea>
                  @if ($errors->has('propaganda'))
                  <span class="help-block">
                  <strong style="color:red;">{{ $errors->first('propaganda') }}</strong>
                  </span>
                  @endif
            </div>`;
         
           document.getElementById("agree").innerHTML = `  <input type="checkbox" name="certified"> <i>I hereby certify that all information state herein is true end accurate to the best of my knowledge.</i>`;     


            document.getElementById("submit").innerHTML = `    <button type="button" class="btn btn-sm btn-primary swal-overlay swal-overlay--show-modal"  data-toggle="modal" data-target="#exampleModalCenter" onclick="handleSubmit()">Submit</button>`;


        } else if (selected === 'Filing Course') {
            document.getElementById("title").innerHTML = '';
            document.getElementById("event").innerHTML = '';
              document.getElementById("usc").innerHTML = '';

              document.getElementById("acad").innerHTML = `<h6>ACADEMIC RECORD:</h6>  <br> 
        <p><i>Notes: (Separate multiple entries with commas)</i></p>`;  
           document.getElementById("personal").innerHTML = ` <h6>PERSONAL INFORMATION:</h6>`;   
            
            document.getElementById("election").innerHTML = document.getElementById("election").innerHTML = `  <div class="form-group"><label for="name">Election: (Department) <font style="color:red;">*</font></label> 
                  <select class="form-control" id="election" name="electionCourse">
                         <option value=""  selected="">Select Election</option>
                         @foreach ($electionCourses as $electionId => $election )
                         <option value="{{ $electionId }}">{{ $election }}</option>
                           <span class="help-block">
                         @if ($errors->has('election'))
                          <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('election') }}</strong>
                          </span>
                         @endif
                        @endforeach
                     </select></div>`;

            document.getElementById("position").innerHTML = ` <div class="form-group"><label for="name">Position: (Department) <font style="color:red;">*</font></label> 
                   <select class="form-control" id="position" name="positionCourse">
                        <option value=""  selected="">Select Position</option>
                        @foreach ($positionsCourse as $position )
                           <option value="{{ $position->id }}">{{ $position->position_name }}</option>
                           <span class="help-block">
                              @if ($errors->has('position'))
                                 <span class="help-block">
                                 <strong style="color:red;">{{ $errors->first('position') }}</strong>
                                 </span>
                              @endif
                        @endforeach
                     </select></div>`;

            document.getElementById("avatar").innerHTML = `  <div class="form-group"> <label>Upload Photo <font style="color:red;">*</font></label>
                   <input type="file" class="form-control" name="avatar">
                     @if ($errors->has('avatar'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('avatar') }}</strong>
                      </span>                                   
                     @endif</div>`;

            document.getElementById("upload_cor").innerHTML = ` <div class="form-group">  <label>Upload COR <font style="color:red;">*</font></label>
                   <input type="file" class="form-control" name="certificate_of_registration">
                  @if ($errors->has('certificate_of_registration'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('certificate_of_registration') }}</strong>
                      </span>
                  @endif</div>`;

            document.getElementById("validate_school_id").innerHTML = ` <div class="form-group"><label>Upload Validate ID <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="validate_school_id">
                     @if ($errors->has('validate_school_id'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('validate_school_id') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("certificate_of_attendance").innerHTML = ` <div class="form-group">  <label>Certificate of Awards / Honors received <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="certificate_of_attendance">
                     @if ($errors->has('certificate_of_attendance'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('certificate_of_attendance') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("recent_photo").innerHTML = ` <div class="form-group"><label>Upload (2x2) Recent Photo (Colored) <font style="color:red;">*</font></label>
                      <input type="file" class="form-control" name="recent_photo">
                     @if ($errors->has('recent_photo'))
                      <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('recent_photo') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("recomend_letter_from_dean").innerHTML = ` <div class="form-group"><label> Recommendation Letter from the Dean <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="recomend_letter_from_dean">
                     @if ($errors->has('recomend_letter_from_dean'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('recomend_letter_from_dean') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("certificate_of_good_moral").innerHTML = ` <div class="form-group"> <label> Certificate of Good Moral <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="certificate_of_good_moral">
                     @if ($errors->has('certificate_of_good_moral'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('certificate_of_good_moral') }}</strong>
                      </span>
                     @endif</div>`;
            document.getElementById("class_of_grade").innerHTML = `  <div class="form-group"><label> Copy of Grades <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="class_of_grade">
                     @if ($errors->has('class_of_grade'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('class_of_grade') }}</strong>
                      </span>
                     @endif</div>`;
           
                  document.getElementById("partyname").innerHTML = `<div class="form-group"><label for="name">Party Name: <font style="color:red;">*</font></label>
                  <input type="text" name="partyname" class="form-control">
                  </div>`;

            document.getElementById("average_last_semister").innerHTML = ` <div class="form-group"> <label>Average last Semester <font style="color:red;">*</font></label>
                      <input type="text" class="form-control" name="average_last_semister">
                     @if ($errors->has('average_last_semister'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('average_last_semister') }}</strong>
                      </span>
                     @endif</div>`;
            document.getElementById("previous_failed_subject").innerHTML = ` <div class="form-group"> <label>Any previous failed subject (specify) Notes: (Seperate multiple entries commas)</label>
                      <input type="text" class="form-control" name="previous_failed_subject" placeholder="Optional">
                     @if ($errors->has('previous_failed_subject'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('previous_failed_subject') }}</strong>
                      </span>
                     @endif</div>`;

            


            document.getElementById("extra_curricular_activities").innerHTML =  `<div class="form-group"><label>Extra Curricular_Activities <font style="color:red;">*</font></label>
                        <input type="text" class="form-control" name="extra_curricular_activities">
                     @if ($errors->has('extra_curricular_activities'))
                      <span class="help-block">
                          <strong style="color:red;">{{ $errors->first('extra_curricular_activities') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("training_seminars").innerHTML = ` <div class="form-group"> <label>Certificate of Training/Seminars Attended <font style="color:red;">*</font></label>
                        <input type="file" class="form-control" name="training_seminars">
                     @if ($errors->has('training_seminars'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('training_seminars') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("date_filled_out").innerHTML = ` <div class="form-group"> <label>Date Filled Out <font style="color:red;">*</font></label>
                        <input type="date" class="form-control" name="date_filled_out" value="{{ $user->student->created_at->format('Y-m-d')  }}">
                     @if ($errors->has('date_filled_out'))
                      <span class="help-block">
                         <strong style="color:red;">{{ $errors->first('date_filled_out') }}</strong>
                      </span>
                     @endif</div>`;
            document.getElementById("nationality").innerHTML = ` <div class="form-group"> <label>Nationality <font style="color:red;">*</font></label>
                        <input type="text" class="form-control" name="nationality"  value="{{ $user->student->nationality }}" >
                     @if ($errors->has('nationality'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('nationality') }}</strong>
                      </span>
                     @endif</div>`;

            document.getElementById("reason_for_candidacy").innerHTML = ` <div class="form-group"> <label>Reason for Candidacy <font style="color:red;">*</font></label>
                        <textarea type="text" class="form-control" name="reason_for_candidacy"></textarea>
                     @if ($errors->has('reason_for_candidacy'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('reason_for_candidacy') }}</strong>
                      </span>
                     @endif</div>`;

              document.getElementById("gender").innerHTML = `<label>Gender <font style="color:red;">*</font></label>
               <input type="text" class="form-control" name="gender" value="{{ $user->student->gender->gender_name }}" readonly>
               @if ($errors->has('gender'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('gender') }}</strong>
                  </span>
               @endif`;
                document.getElementById("birthdate").innerHTML = `<div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
               <label>Birthdate</label> 
               <input type="date" class="form-control" name="birthdate" value="{{ $user->student->birth_date->format('Y-m-d') }}">
                  @if ($errors->has('birthdate'))
                     <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('birthdate') }}</strong>
                     </span>
                  @endif
               </div>`;

           document.getElementById("place_of_birth").innerHTML = `<div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : '' }}">
                     <label>Place of Birth <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="place_of_birth" value="{{ $user->student->place_of_birth }}">
                     @if ($errors->has('place_of_birth'))
                     <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('place_of_birth') }}</strong>
                     </span>
                     @endif
                  </div>`;

            document.getElementById("religion").innerHTML = ` <div class="form-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                     <label>Religion <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="religion" value="{{ $user->student->religion }}" >
                     @if ($errors->has('religion'))
                     <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('religion') }}</strong>
                     </span>
                     @endif
                  </div>`;


                  document.getElementById("courses").innerHTML = `<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                     <label for="name">Select Course <font style="color:red;">*</font> </label>
                     <select class="form-control" id="course" name="course" readonly="">
                        <option value="" disabled="" selected="">Select Course</option>
                           @foreach ($courses as  $courseId => $course )
                           <option value="{{ $courseId }}"  {{ auth()->user()->course_id == $courseId ? 'selected' : '' }}>{{ $course }}</option>
                           <span class="help-block">
                              @if ($errors->has('course'))
                                 <span class="help-block">
                                    <strong style="color:red;">{{ $errors->first('course') }}</strong>
                                 </span>
                              @endif
                           @endforeach
                     </select>
                  </div>`;
                  document.getElementById("campus").innerHTML = `<div class="form-group{{ $errors->has('campus') ? ' has-error' : '' }}">
                        <label for="name">Select Campus <font style="color:red;">*</font></label>
                           <select class="form-control" id="campus" name="campuses" readonly="">
                              <option value=""  selected="">Select Campus</option>
                              @foreach ($campuses as $campusId =>  $campus )
                              <option value="{{ $campusId }}" {{ auth()->user()->campus_id == $campusId ? 'selected' : '' }}>{{ $campus }}</option>
                              <span class="help-block">
                                 @if ($errors->has('campus'))
                                    <span class="help-block">
                                       <strong style="color:red;">{{ $errors->first('campus') }}</strong>
                                    </span>
                                 @endif
                              </span>
                           @endforeach
                        </select>
                     </div>`;

            document.getElementById("department").innerHTML = ` <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                  <label for="name">Select Department <font style="color:red;">*</font></label>
                  <select class="form-control" id="department" name="department" readonly="">
                     <option value="" disabled="" selected="">Select Department</option>
                     @foreach ($departments as $departmentId => $department )
                     <option value="{{ $departmentId }}" {{ auth()->user()->department_id == $departmentId ? 'selected' : '' }}>{{ $department }}</option>
                     <span class="help-block">
                        @if ($errors->has('department'))
                           <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('department') }}</strong>
                           </span>
                        @endif
                     </span>
                     @endforeach
                  </select>
               </div>`;
 
               document.getElementById("contact_no").innerHTML = `<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
                  <label>Contact No <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="contact_no" value="{{ $user->student->contact_no }}" readonly="">
                     @if ($errors->has('contact_no'))
                        <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('contact_no') }}</strong>
                        </span>
                     @endif
               </div>`;


         document.getElementById("lastname").innerHTML = ` <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                  <label>Last Name <font style="color:red;">*</font></label>
                  <input type="text" class="form-control" name="lastname"  value="{{ $user->student->lastname }}" readonly="">
                  @if ($errors->has('lastname'))
                  <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('lastname') }}</strong>
                  </span>
                  @endif   
               </div>`;

          document.getElementById("middlename").innerHTML = ` <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
                     <label>Middle Name <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="middlename" value="{{ $user->student->middlename }}" readonly="">
                     @if ($errors->has('middlename'))
                     <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
                     </span>
                     @endif
                  </div>`;


            document.getElementById("firstname").innerHTML = ` <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                  <label>First Name <font style="color:red;">*</font></label>
                  <input type="text" class="form-control" name="firstname" value="{{ $user->student->firstname }}" readonly="">
                  @if ($errors->has('firstname'))
                  <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
                  </span>
                  @endif
               </div>`;

               document.getElementById("propaganda").innerHTML = ` <div class="form-group">
                  <label>Propaganda (Optional)</label>
                  <textarea  name="propaganda" class="form-control"></textarea>
                  @if ($errors->has('propaganda'))
                  <span class="help-block">
                  <strong style="color:red;">{{ $errors->first('propaganda') }}</strong>
                  </span>
                  @endif
            </div>`;
 

          document.getElementById("submit").innerHTML = `    <button type="button" class="btn btn-sm btn-primary swal-overlay swal-overlay--show-modal"  data-toggle="modal" data-target="#exampleModalCenter" onclick="handleSubmit()">Submit</button>`;

 

        } else if (selected === 'Filing Event') {
             document.getElementById("election").innerHTML = '';
             document.getElementById("usc").innerHTML = '';
             document.getElementById("position").innerHTML = '';
             document.getElementById("upload_cor").innerHTML = '';
             document.getElementById("validate_school_id").innerHTML = '';
             document.getElementById("certificate_of_attendance").innerHTML = '';
             document.getElementById("recent_photo").innerHTML = '';
             document.getElementById("recent_photo").innerHTML = '';
             document.getElementById("recomend_letter_from_dean").innerHTML = '';
             document.getElementById("certificate_of_good_moral").innerHTML = '';
             document.getElementById("class_of_grade").innerHTML = '';
             document.getElementById("partyname").innerHTML = '';
             document.getElementById("average_last_semister").innerHTML = '';
             document.getElementById("previous_failed_subject").innerHTML = '';
             // document.getElementById("award_honors_received").innerHTML = '';
             document.getElementById("extra_curricular_activities").innerHTML = '';
             document.getElementById("training_seminars").innerHTML = '';
             document.getElementById("date_filled_out").innerHTML = '';
             document.getElementById("nationality").innerHTML = '';
             document.getElementById("reason_for_candidacy").innerHTML = '';
             document.getElementById("avatar").innerHTML = '';  
               document.getElementById("acad").innerHTML = `<h6>ACADEMIC RECORD:</h6>  <br> 
        <p><i>Notes: (Separate multiple entries with commas)</i></p>`;  
           document.getElementById("personal").innerHTML = ` <h6>PERSONAL INFORMATION:</h6>`;   

                     document.getElementById("upload_avatar").innerHTML = `<div class="form-group"> <label>Upload Photo <font style="color:red;">*</font></label>
                   <input type="file" class="form-control" name="avatar">
                     @if ($errors->has('avatar'))
                      <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('avatar') }}</strong>
                      </span>
                     @endif</div>`;
             document.getElementById("title").innerHTML = ` <div class="form-group"> <label>Title <font style="color:red;">*</font></label>
                        <input type="text" class="form-control" name="title">
                    </div>`;

            document.getElementById("gender").innerHTML = `<label>Gender <font style="color:red;">*</font></label>
               <input type="text" class="form-control" name="gender" value="{{ $user->student->gender->gender_name }}" readonly>
               @if ($errors->has('gender'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('gender') }}</strong>
                  </span>
               @endif`;

               document.getElementById("birthdate").innerHTML = `<div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
               <label>Birthdate <font style="color:red;">*</font></label> 
               <input type="date" class="form-control" name="birthdate" value="{{ $user->student->birth_date->format('Y-m-d') }}">
                  @if ($errors->has('birthdate'))
                     <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('birthdate') }}</strong>
                     </span>
                  @endif
            </div>`;

            document.getElementById("place_of_birth").innerHTML = `<div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : '' }}">
                  <label>Place of Birth <font style="color:red;">*</font></label>
                   <input type="text" class="form-control" name="place_of_birth" value="{{ $user->student->place_of_birth }}" >
                     @if ($errors->has('place_of_birth'))
                        <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('place_of_birth') }}</strong>
                        </span>
                     @endif
               </div>`;

            document.getElementById("religion").innerHTML = ` <div class="form-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                     <label>Religion <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="religion" value="{{ $user->student->religion }}" >
                     @if ($errors->has('religion'))
                     <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('religion') }}</strong>
                     </span>
                     @endif
                  </div>`;

               document.getElementById("courses").innerHTML = `<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                  <label for="name">Select Course <font style="color:red;">*</font></label>
                  <select class="form-control" id="course" name="course" readonly="">
                     <option value="" disabled="" selected="">Select Course</option>
                        @foreach ($courses as  $courseId => $course )
                        <option value="{{ $courseId }}"  {{ auth()->user()->course_id == $courseId ? 'selected' : '' }}>{{ $course }}</option>
                        <span class="help-block">
                           @if ($errors->has('course'))
                              <span class="help-block">
                                 <strong style="color:red;">{{ $errors->first('course') }}</strong>
                              </span>
                           @endif
                        @endforeach
                  </select>
               </div>`;
               document.getElementById("campus").innerHTML = `<div class="form-group{{ $errors->has('campus') ? ' has-error' : '' }}">
                        <label for="name">Select Campus <font style="color:red;">*</font></label>
                           <select class="form-control" id="campus" name="campuses" readonly="">
                              <option value=""  selected="">Select Campus</option>
                              @foreach ($campuses as $campusId =>  $campus )
                              <option value="{{ $campusId }}" {{ auth()->user()->campus_id == $campusId ? 'selected' : '' }}>{{ $campus }}</option>
                              <span class="help-block">
                                 @if ($errors->has('campus'))
                                    <span class="help-block">
                                       <strong style="color:red;">{{ $errors->first('campus') }}</strong>
                                    </span>
                                 @endif
                              </span>
                           @endforeach
                        </select>
                     </div>`;

           document.getElementById("department").innerHTML = ` <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                  <label for="name">Select Department <font style="color:red;">*</font></label>
                  <select class="form-control" id="department" name="department" readonly="">
                     <option value="" disabled="" selected="">Select Department</option>
                     @foreach ($departments as $departmentId => $department )
                     <option value="{{ $departmentId }}" {{ auth()->user()->department_id == $departmentId ? 'selected' : '' }}>{{ $department }}</option>
                     <span class="help-block">
                        @if ($errors->has('department'))
                           <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('department') }}</strong>
                           </span>
                        @endif
                     </span>
                     @endforeach
                  </select>
               </div>`;

          document.getElementById("contact_no").innerHTML = `<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
                  <label>Contact No <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="contact_no" value="{{ $user->student->contact_no }}" readonly="">
                     @if ($errors->has('contact_no'))
                        <span class="help-block">
                           <strong style="color:red;">{{ $errors->first('contact_no') }}</strong>
                        </span>
                     @endif
               </div>`;


         document.getElementById("lastname").innerHTML = ` <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                  <label>Last Name <font style="color:red;">*</font></label>
                  <input type="text" class="form-control" name="lastname"  value="{{ $user->student->lastname }}" readonly="">
                  @if ($errors->has('lastname'))
                  <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('lastname') }}</strong>
                  </span>
                  @endif   
               </div>`;
          document.getElementById("middlename").innerHTML = ` <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
                     <label>Middle Name <font style="color:red;">*</font></label>
                     <input type="text" class="form-control" name="middlename" value="{{ $user->student->middlename }}" readonly="">
                     @if ($errors->has('middlename'))
                     <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
                     </span>
                     @endif
                  </div>`;

        document.getElementById("firstname").innerHTML = ` <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                  <label>First Name <font style="color:red;">*</font></label>
                  <input type="text" class="form-control" name="firstname" value="{{ $user->student->firstname }}" readonly="">
                  @if ($errors->has('firstname'))
                  <span class="help-block">
                     <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
                  </span>
                  @endif
               </div>`;

                 document.getElementById("event").innerHTML = `<div class="form-group{{ $errors->has('pageant') ? ' has-error' : '' }}">
                     <label for="name">Select Event <font style="color:red;">*</font></label>
                     <select class="form-control" id="pageant" name="pageant">
                        <option value="" disabled="" selected="">Select Event</option>
                           @foreach ($pageants as  $pageantId => $pageant )
                           <option value="{{ $pageantId }}"  {{ auth()->user()->pageant_id == $pageantId ? 'selected' : '' }}>{{ $pageant }}</option>
                           <span class="help-block">
                              @if ($errors->has('pageant'))
                                 <span class="help-block">
                                    <strong style="color:red;">{{ $errors->first('pageant') }}</strong>
                                 </span>
                              @endif
                           @endforeach
                     </select>
                  </div>`;

                   document.getElementById("propaganda").innerHTML = ` <div class="form-group">
                  <label>Propaganda (Optional)</label>
                  <textarea  name="propaganda" class="form-control"></textarea>
                  @if ($errors->has('propaganda'))
                  <span class="help-block">
                  <strong style="color:red;">{{ $errors->first('propaganda') }}</strong>
                  </span>
                  @endif
            </div>`;

              document.getElementById("agree").innerHTML = `  <input type="checkbox" name="certified"> <i>I hereby certify that all information state herein is true end accurate to the best of my knowledge.</i>`;     


            document.getElementById("submit").innerHTML = `    <button type="button" class="btn btn-sm btn-primary swal-overlay swal-overlay--show-modal"  data-toggle="modal" data-target="#exampleModalCenter" onclick="handleSubmit()">Submit</button>`;


 
        }   
    } 

$("#jquery-autocomplete").autocomplete({
    source: ["None"]
  });
</script> 

<script>
function handleSubmit () {
    document.getElementById('form').submit();
}
 
</script>

{{-- <script>
  $("#jquery-autocomplete").autocomplete({
    source: ["None"]
  });
</script> --}}


@endpush