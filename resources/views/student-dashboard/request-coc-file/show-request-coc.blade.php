@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">File COC </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> File Coc</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>FullName</th>
									<th>Address</th>
									<th>Sex</th>
									<th>Status</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($user->nominees as $nominee)
								<tr>
									<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
									<td>{{ $nominee->place_of_birth }}</td>
									<td>{{ $nominee->gender }}</td>
									<td>
									@if ($nominee->status == 'Approved')
										<span class="label label-success">{{ $nominee->status }}</span>
									@endif	

									@if ($nominee->status == 'Disqualified')
										<span class="label label-danger">{{ $nominee->status }}</span>
									@endif

									@if ($nominee->status == 'Pending')
										<span class="label label-warning">{{ $nominee->status }}</span>
									@endif

									</td>
									<td><a href="/file-coc/{{ $nominee->id }}/show" class="btn btn-primary btn-xs">View</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>






@endsection