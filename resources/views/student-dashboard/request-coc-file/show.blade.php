@extends('layouts.master')
@section('content')



<div id="content" class="content content-full-width">

<div class="profile">
<div class="profile-header">

<div class="profile-header-cover"></div>

<div class="profile-header-content" id="particles-js">

   <div class="profile-header-img" >
      <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
   </div>

<div class="profile-header-info" >
<h4 class="mt-0 mb-1">{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</h4>
@if(! $nominee->categories == 'Pageant')
<p class="mb-2"><a href="/generate-pdf/{{ $nominee->id }}" class="btn btn-xs btn-yellow"  target="_blank">View PDF File</a>
@endif
</p>
<br>
<br>
</div>
</div>


<ul class="profile-header-tab nav nav-tabs" style="background: #f0f8ff21">
<li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;"></a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
</ul>
</div>
</div>



<div class="profile-content">
   
   <div class="form-group">
   	 <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Message</h4>
         <div class="panel-heading-btn">
   {{--          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> --}}
         </div>
      </div>
	<div class="panel-body">
   	<table class="table table-bordered">
   		<tr>
   			<thead>
   				<th>Message</th>
   			</thead>
   		</tr>
   		<tbody>
   			<tr>
   				<td>{{ $nominee->comment }}</td>
   			</tr>
   		</tbody>
   	</table>
   </div>

</div>

 
<div class="tab-content p-0">
<div class="tab-pane fade show active" id="profile-post">
 <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Nominee Profile </h4>
         <div class="panel-heading-btn">
   {{--          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> --}}
         </div>
      </div>
      <div class="panel-body">

      	<table class="table table-bordered">
      		<tbody>
      			<tr>
      				<td> Position </td>
                  <td colspan="2">
                              @if ($nominee->position)
                                    {{ $nominee->position->position_name  }}
                              @elseif ($nominee->positionDepartment)
                                    {{ $nominee->positionDepartment->position_name }}
                              @elseif ($nominee->positionCourse)
                                    {{ $nominee->positionCourse->position_name }}
                              @elseif ($nominee->title)
                                 {{ $nominee->title }}
                              @endif
                              </td>
      			</tr>
      			 <tr>
                        
                  <td>Partyname</td>
                        <td colspan="2">
                              {{ $nominee->partyname }}
                        </td>
                       </tr>
                        
               <tr>
                     <td>Sex</td>
                     <td colspan="2">{{ $nominee->gender }}</td>
             
               </tr>             

               <tr>
                              @if ($nominee->contact_no)
                        <td>Contact No</td>
                        <td colspan="2">{{ $nominee->contact_no }}</td>
                              @endif
               </tr>             

               <tr>
                        @if ($nominee->nationality)
                  <td>Nationality</td>
                  <td colspan="2">{{ $nominee->nationality }}</td>
                        @endif
               </tr>
               <tr>
                  <td>Campus</td>
                  <td colspan="2">{{ $nominee->campus->name }}</td>
               </tr>
               <tr>
                  <td>College</td>
                  <td colspan="2"> {{ $nominee->department->code }}</td>
               </tr>
               <tr>
                  <td>Course</td>
                  <td colspan="2">{{ $nominee->course->title }}</td>
               </tr>
               <tr>
                  <td>Place of Birth</td>
                  <td colspan="2">{{ $nominee->place_of_birth }}</td>
               </tr>
               <tr>
                  <td>Religion</td>
                  <td colspan="2"> {{ $nominee->religion }}</td>
               </tr>
               <tr>
                              @if ($nominee->certificate_of_registration )
                        <td>Cert. of Registration</td>
                        <td >{{ $nominee->certificate_of_registration }}</td>
                        <td>
                                                <a href="/nominee/{{ $nominee->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i> Download</a>
                                    </td>
                              @endif
               </tr>
               <tr>
                             @if ($nominee->validate_school_id)
                  <td>Validate School ID</td>
                  <td>{{ $nominee->validate_school_id }}</td>
                  <td>
                                          <a href="/nominee/{{ $nominee->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i>Download</a>
                              </td>
                              @endif
               </tr>
               <tr>
                              @if ($nominee->certificate_of_attendance)
                  <td>Cert. of Attendance</td>
                  <td>{{ $nominee->certificate_of_attendance }}</td>
                  <td>
                                          <a href="/nominee/{{ $nominee->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i>Download</a>
                              </td>
                              @endif
               </tr>
               <tr>
                              @if ($nominee->recent_photo)
                  <td>Recent Photo</td>
                  <td>{{ $nominee->recent_photo }}</td>
                  <td>
                                          <a href="/nominee/{{ $nominee->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i>Download</a>
                              </td>
                               @endif
               </tr>
               <tr>
                              @if ($nominee->recomend_letter_from_dean)
                  <td>Recommend Letter from Dean</td>
                  <td >{{ $nominee->recomend_letter_from_dean }}</td>
                  <td>
                                          <a href="/nominee/{{ $nominee->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i>Download</a>
                              </td>
                              @endif
               </tr>
               <tr>
                             @if ($nominee->certificate_of_good_moral)
                  <td>Cert. of Good Moral</td>
                  <td >{{ $nominee->certificate_of_good_moral }}</td>
                  <td>
                                          <a href="/nominee/{{ $nominee->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i>Download</a>
                              </td>
                              @endif
               </tr>
               <tr>
                             @if ($nominee->class_of_grade)
                  <td>Class of Grade</td>
                  <td >{{ $nominee->class_of_grade }}</td>
                  <td>
                                          <a href="/nominee/{{ $nominee->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i>Download</a>
                              </td>
                              @endif
               </tr>
               <tr>
                              @if ($nominee->reason_for_candidacy)
                        <td>Reason for Candidacy</td>
                        <td colspan="2">{{ $nominee->reason_for_candidacy }}</td>
                              @endif
               </tr>
               <tr>
                              @if ($nominee->average_last_semister)
                        <td>Average Last Semister</td>
                        <td colspan="2">{{ $nominee->average_last_semister }}</td>
                              @endif
               </tr>
               <tr>
                              @if ($nominee->previous_failed_subject)
                        <td>Previous Failed Subject</td>
                        <td colspan="2">{{ $nominee->previous_failed_subject }}</td>
                              @endif
               </tr>
               <tr>
                              @if ($nominee->award_honors_received)
                        <td>Award Honors Received</td>
                        <td colspan="2">{{ $nominee->award_honors_received }}</td>
                              @endif
               </tr>             
               <tr>
                              @if ($nominee->extra_curricular_activities)
                  <td>Extra Curricular Activities</td>
                  <td colspan="2">{{ $nominee->extra_curricular_activities }}</td>
                              @endif
               </tr>
               <tr>
                              @if ($nominee->date_filled_out)
                  <td>Date Filled Out</td>
                  <td colspan="2">{{ $nominee->date_filled_out }}</td>
                              @endif
               </tr>
            </tbody>
         </table>
         
						            





@endsection