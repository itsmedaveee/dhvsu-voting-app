@inject('election','App\Student') 

<div id="sidebar" class="sidebar">
   <!-- begin sidebar scrollbar -->
   <div data-scrollbar="true" data-height="100%">
      <!-- begin sidebar user -->
      <ul class="nav">
         <li class="nav-profile">
            <a href="javascript:;" data-toggle="nav-profile">
               <div class="cover with-shadow"></div>

              @if (auth()->user()->userable->image)
	              <div class="image">
                  <img src="{{ Storage::url(auth()->user()->userable->image) }}"> 
	              </div>
               @else
               <div class="image">
                    <img src="{{ asset('img/admin.png') }}" alt="" /> 
                 </div>
               @endif
	         
               <div class="info">
                  {{ auth()->user()->username }}
                  <small>{{ auth()->user()->role->name }}</small>
               </div>
            </a>
         </li>
      </ul>
      <!-- end sidebar user -->
      <!-- begin sidebar nav -->
      <ul class="nav">
         <li class="nav-header">Navigation</li>
         <li class="has-sub {{ Request::is('student/home') ? 'active' : '' }}">
            <a href="/student/home">
               <div class="icon-img">
                  <i class="fa fa-home"></i>
               </div>
               <span>Dashboard</span>
            </a>
         </li>
         


         <li class="has-sub {{ Request::is('request-file-coc') ? 'active' : '' }} {{ Request::is('file-coc') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               <i class="fa fa-file"></i>
               </div>
               <span>Candidacy Form </span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('request-file-coc') ? 'active' : '' }}"><a href="/request-file-coc">Candidacy Form</a></li>     
               <li class="{{ Request::is('file-coc') ? 'active' : '' }}"><a href="/file-coc">Candidacy Form list</a></li>
            </ul>
         </li>
         
         
         <li class="has-sub {{ Request::is('file-coc') ? 'active' : '' }}    {{ Request::is('request-file-coc') ? 'active' : '' }}  {{ Request::is('student/home') ? 'active' : '' }} {{ Request::is('results-pageant') ? 'active' : '' }}{{ Request::is('results-courses') ? 'active' : '' }} {{ Request::is('candidates') ? 'active' : '' }} {{ Request::is('candidates-pageant') ? 'active' : '' }} {{ Request::is('candidates-pageant/*') ? 'active' : '' }} {{ Request::is('candidates/*') ? 'active' : '' }}  {{ Request::is('candidates-by-dept') ? 'active' : ''}}  {{ Request::is('candidates-by-dept/*') ? 'active' : ''}}  {{ Request::is('candidates-by-courses') ? 'active' : '' }} {{ Request::is('results') ? 'active' : '' }} {{ Request::is('results-department') ? 'active' : '' }} {{ Request::is('candidates-by-courses/*') ? 'active' : '' }}"  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-file"></i>
               </div>
               <span>Campuses
                   <span class="label label-warning" style="background: linear-gradient(to right, #5c0017  0%,  #feba29 100%)"> {{ $election->statusesElection() }}</span>
               </span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('candidates') ? 'active' : '' }} {{ Request::is('candidates/*') ? 'active' : '' }}"><a href="/candidates">Candidates all Campus</a></li>
               <li class="{{ Request::is('results') ? 'active' : '' }}"><a href="/results">Results Campus</a></li>
            </ul>
         </li>    

         <li class="has-sub {{ Request::is('file-coc') ? 'active' : '' }}    {{ Request::is('request-file-coc') ? 'active' : '' }}  {{ Request::is('student/home') ? 'active' : '' }} {{ Request::is('results-pageant') ? 'active' : '' }} {{ Request::is('results-courses') ? 'active' : '' }}  {{ Request::is('results') ? 'active' : '' }} {{ Request::is('candidates') ? 'active' : '' }} {{ Request::is('candidates-pageant') ? 'active' : '' }} {{ Request::is('candidates-pageant/*') ? 'active' : '' }} {{ Request::is('candidates/*') ? 'active' : '' }}  {{ Request::is('candidates-by-dept') ? 'active' : ''}}  {{ Request::is('candidates-by-dept/*') ? 'active' : ''}}  {{ Request::is('candidates-by-courses') ? 'active' : '' }} {{ Request::is('results-department') ? 'active' : '' }} {{ Request::is('candidates-by-courses/*') ? 'active' : '' }}"  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-building"></i>
               </div>
               <span>Colleges
                   <span class="label label-warning" style="background: linear-gradient(to right, #5c0017  0%,  #feba29 100%)" > {{ $election->departmentStatus() }}</span>
               </span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('candidates-by-dept') ? 'active' : '' }}"><a href="/candidates-by-dept">Candidates by Colleges.</a></li>
                <li class="{{ Request::is('results-department') ? 'active' : '' }}"><a href="/results-department">Results Colleges</a></li>
            </ul>
         </li>    

       <li class="has-sub {{ Request::is('file-coc') ? 'active' : '' }}  {{ Request::is('request-file-coc') ? 'active' : '' }}  {{ Request::is('student/home') ? 'active' : '' }} {{ Request::is('results-pageant') ? 'active' : '' }} {{ Request::is('results') ? 'active' : '' }} {{ Request::is('candidates') ? 'active' : '' }} {{ Request::is('candidates-pageant') ? 'active' : '' }} {{ Request::is('candidates-pageant/*') ? 'active' : '' }} {{ Request::is('candidates/*') ? 'active' : '' }}{{ Request::is('results-courses') ? 'active' : '' }}  {{ Request::is('candidates-by-dept') ? 'active' : ''}}  {{ Request::is('candidates-by-dept/*') ? 'active' : ''}} {{ Request::is('results-department') ? 'active' : '' }}  {{ Request::is('candidates-by-courses') ? 'active' : '' }} {{ Request::is('candidates-by-courses/*') ? 'active' : '' }}"  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-book"></i>
               </div>
               <span>Departments 
                   <span class="label label-warning" style="background: linear-gradient(to right, #5c0017  0%,  #feba29 100%)" > {{ $election->courseStatus() }}</span>
               </span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('candidates-by-courses') ? 'active' : '' }}"><a href="/candidates-by-courses">Candidates by Dept</a></li>
                <li class="{{ Request::is('results-courses') ? 'active' : '' }}"><a href="/results-courses">Results Departments</a></li>
            </ul>
         </li>    

       <li class="has-sub {{ Request::is('file-coc') ? 'active' : '' }}   {{ Request::is('request-file-coc') ? 'active' : '' }}  {{ Request::is('student/home') ? 'active' : '' }} {{ Request::is('results-pageant') ? 'active' : '' }} {{ Request::is('results-courses') ? 'active' : '' }} {{ Request::is('results') ? 'active' : '' }} {{ Request::is('results-department') ? 'active' : '' }} {{ Request::is('candidates') ? 'active' : '' }} {{ Request::is('candidates-pageant') ? 'active' : '' }} {{ Request::is('candidates-pageant/*') ? 'active' : '' }} {{ Request::is('candidates/*') ? 'active' : '' }}  {{ Request::is('candidates-by-dept') ? 'active' : ''}}  {{ Request::is('candidates-by-dept/*') ? 'active' : ''}}  {{ Request::is('candidates-by-courses') ? 'active' : '' }} {{ Request::is('candidates-by-courses/*') ? 'active' : '' }}"  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-server"></i>
               </div>
               <span>  Events &nbsp; &nbsp; &nbsp;<span class="label label-warning" style="background: linear-gradient(to right, #5c0017  0%,  #feba29 100%)" > {{ $election->eventStatus() }}</span>
               </span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('candidates-pageant') ? 'active' : '' }}"><a href="/candidates-pageant">Events</a></li>
               <li class="{{ Request::is('results-pageant') ? 'active' : '' }}"><a href="/results-pageant">Results Event</a></li>
            </ul>
         </li>    
 
    {{--      <li class="has-sub {{ Request::is('results') ? 'active' : '' }} {{ Request::is('results-department') ? 'active' : '' }} {{ Request::is('results-courses') ? 'active' : '' }} {{ Request::is('results-pageant') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
 
               <i class="fa fa-bar-chart"></i>
               </div>
               <span>Results</span>
            </a>
            <ul class="sub-menu">
              
              
            
   
            </ul>
         </li> --}}

       
         <!-- begin sidebar minify button -->
         <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
         <!-- end sidebar minify button -->
      </ul>
      <!-- end sidebar nav -->
   </div>
   <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>