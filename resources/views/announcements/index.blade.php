@extends('layouts.master')
@section('content')


<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>


	<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Announcements </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Announcement</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<form method="POST" action="/announcements">
							@csrf
							<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
	                           <input id="x" value="" type="hidden" name="body">
	                           <trix-editor input="x"  style="height: 100%"></trix-editor>
	                        @if ($errors->has('body'))
	                            <span class="help-block">
	                                <strong style="color:red;">{{ $errors->first('body') }}</strong>
	                            </span>
	                        @endif
	                    </div>


	                      @if(auth()->user()->isCouncil() || auth()->user()->isSuperAdmin())
		                      <div class="form-group">
		                        <select class="form-control" id="students" name="selected" required="">
		                        <option selected="" disabled="">Select</option>
		                        <option value="myStudentDepartment">My Student Department</option>
		                        <option value="allCampuses">All Campuses</option>
		                        {{-- <option value="department" >My Department</option> --}}
		                      
		                        </select>
		                      </div>
	                    @endif

	                    <div class="form-group">
	                    	<button type="submit" class="btn btn-primary">Send</button>
	                    </div>
						</form>

					</div>
				</div>
			</div>

				<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> Announcements list</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>ID</th>
									<th>Body</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($announcements as $announcement)
								<tr>
									<td>{{ $announcement->id }}</td>
									<td>{!! $announcement->body !!}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection