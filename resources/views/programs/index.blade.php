@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Program </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Program</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form method="POST" action="/programs">
							@csrf 
							<div class="form-group">
					    	<label for="course">{{ __('Department') }}<font style="color: red;">*</font></label>
					             <select class="form-control  @error('course') is-invalid @enderror"  name="course" id="course">
					             <option   disabled=""  selected="">Select Department</option>
					             	@foreach ($courses as $courseId => $course)
					             		<option value="{{ $courseId }}">{{ $course }}</option>
					             	@endforeach
					             </select>
					            @error('course')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        
					        </div>
							<div class="form-group">
								<label>Program</label>
								<input type="text" name="program_name" class="form-control">
							</div>
							<div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> Program</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Department</th>
									<th>Program</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($programs as $program)
								<tr>
									<td>{{ $program->id }}</td>
									<td>{{ $program->course->title }}</td>
									<td>{{ $program->program_name }}</td>
								  
									<td>
										<form method="POST" action="/program/{{ $program->id }}">
												@csrf
												{{ method_field('DELETE') }}
											<button type="submit" class="btn btn-danger btn-xs">Remove</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>









@endsection