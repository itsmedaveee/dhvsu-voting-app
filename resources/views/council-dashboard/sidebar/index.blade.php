<div id="sidebar" class="sidebar">
   <!-- begin sidebar scrollbar -->
   <div data-scrollbar="true" data-height="100%">
      <!-- begin sidebar user -->
      <ul class="nav">
         <li class="nav-profile">
            <a href="javascript:;" data-toggle="nav-profile">
               <div class="cover with-shadow"></div>

              
	              <div class="image">
	                 <img src="{{ asset('img/admin.png') }}" alt="" /> 
	              </div>
	         

               <div class="info">
                  {{ auth()->user()->username }}
                  <small>{{ auth()->user()->department->code ?? null }}-{{ auth()->user()->role->name }}</small>
               </div>
            </a>
         </li>
      </ul>
      <!-- end sidebar user -->
      <!-- begin sidebar nav -->
      <ul class="nav">
         <li class="nav-header">Navigation</li>
      <li class="has-sub {{ Request::is('council/home') ? 'active' : '' }}">
            <a href="/council/home">
               <div class="icon-img">
             		<i class="fa fa-home"></i>
               </div>
               <span>Dashboard</span>
            </a>
         </li>

  {{--        <li class="has-sub {{ Request::is('faculties') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
                  <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" />
               <i class="fa fa-user-md"></i>
               </div>
               <span>Faculty User</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('faculties') ? 'active' : '' }}"><a href="/faculties">Faculty Users</a></li>
            </ul>
         </li>
 --}}

 
          <li class="has-sub {{ Request::is('nominees') ? 'active' : '' }}  {{ Request::is('pending-request-file-nominee') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               <i class="fa fa-user"></i>
               </div>
               <span>Pending File COC</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('pending-request-file-nominee') ? 'active' : '' }}"><a href="/pending-request-file-nominee">Pending Request COC </a></li>
               <li class="{{ Request::is('nominees') ? 'active' : '' }}"><a href="/nominees">Nominees</a></li>
            </ul>
         </li>

         

           <li class="has-sub {{ Request::is('election-course') ? 'active' : '' }} {{ Request::is('positions') ? 'active' : '' }} {{ Request::is('elections') ? 'active' : '' }}{{ Request::is('pending-request-file-nominee') ? 'active' : '' }} {{ Request::is('partylists') ? 'active' : '' }} {{ Request::is('announcements') ? 'active' : '' }} {{ Request::is('council/home') ? 'active' : '' }} {{ Request::is('position-department') ? 'active' : '' }}   {{ Request::is('election-department') ? 'active' : '' }} {{ Request::is('nominees') ? 'active' : '' }} {{ Request::is('position-course') ? 'active' : '' }}"  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-toggle-on"></i>
               </div>
               <span> Election Colleges</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('election-department') ? 'active' : '' }}"><a href="/election-department">Election </a></li>
               <li class="{{ Request::is('position-department') ? 'active' : '' }}"><a href="/position-department">Position </a></li>
            </ul>
         </li>         
               <li class="has-sub {{ Request::is('position-election') ? 'active' : '' }} {{ Request::is('announcements') ? 'active' : '' }} {{ Request::is('election-course') ? 'active' : '' }} {{ Request::is('position-course') ? 'active' : '' }} {{ Request::is('position-department') ? 'active' : '' }} {{ Request::is('elections') ? 'active' : '' }} {{ Request::is('positions') ? 'active' : '' }} {{ Request::is('courses') ? 'active' : '' }} {{ Request::is('departments') ? 'active' : '' }}{{ Request::is('campuses') ? 'active' : '' }} {{ Request::is('super-admin') ? 'active' : '' }} {{ Request::is('councils') ? 'active' : '' }}  {{ Request::is('election-department') ? 'active' : '' }} {{ Request::is('nominees') ? 'active' : '' }}  {{ Request::is('pending-request-file-nominee') ? 'active' : '' }}  {{ Request::is('partylists') ? 'active' : '' }}  {{ Request::is('pending-students') ? 'active' : '' }} {{ Request::is('students') ? 'active' : '' }}   {{ Request::is('home') ? 'active' : '' }} "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-server"></i>
               </div>
               <span> Election Department</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('election-course') ? 'active' : '' }}"><a href="/election-course">Election </a></li>
               <li class="{{ Request::is('position-course') ? 'active' : '' }}"><a href="/position-course">Position </a></li>
            </ul>
         </li>
 
 


       <li class="has-sub {{ Request::is('pageants') ? 'active' : '' }} {{ Request::is('pageants*') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
              <i class="fa fa-file-text-o" aria-hidden="true"></i>
               </div>
               <span>Events</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('pageants') ? 'active' : '' }} {{ Request::is('pageants*') ? 'active' : '' }} "><a href="/pageants">Events</a></li>
            </ul>
         </li>
    
  
         <!-- begin sidebar minify button -->
         <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
         <!-- end sidebar minify button -->
      </ul>
      <!-- end sidebar nav -->
   </div>
   <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>