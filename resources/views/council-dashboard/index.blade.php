@extends('layouts.master')
@inject('stats','App\Stats') 
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>
<div id="content" class="content">

	<h1>Dashboard</h1>

<div class="row">

<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-list fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">TOTAL ELECTIONS</div>
<div class="stats-number">{{ $stats->totalElections() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>


<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #00acac 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-tasks fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">TOTAL POSITIONS</div>
<div class="stats-number">{{ $stats->totalPositionInDepartment() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>
<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #fc6075  0%, #ff9b44 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-users fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">TOTAL NOMINEES</div>
<div class="stats-number">{{ $stats->totalNomineeForDepartment() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>

      

        <div class="col-md-6">
          <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Campuses)</div>
                <div class="panel-body">
                    <div id="app">
                        <canvas id="myChart" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
   </div>
   </div>

       <div class="col-md-6">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Department)</div>
                <div class="panel-body">
               <div id="app">
                  <canvas id="myCharts" width="400" height="200"></canvas>
               </div>
            </div>
         </div>
   </div>


        <div class="col-md-6">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Courses)</div>
                <div class="panel-body">
               <div id="app">
                  <canvas id="myChartCourse" width="400" height="200"></canvas>
               </div>
            </div>
         </div>
        </div>


       <div class="col-md-6">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Events)</div>
                <div class="panel-body">
               <div id="app">
                  <canvas id="myChartes" width="400" height="200"></canvas>
               </div>
            </div>
         </div>
   </div>

	


@endsection
 

 @push ('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

<script>

var results = {!! $result !!};
var depts = {!! $dept !!};
console.log(depts);
const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: depts,
        datasets: [{
            label: '# of Votes',
            data: results,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>
 
<script>

var resultsWithCourses = {!! $resultDepartment !!};
var courses = {!! $deptCourse !!};
console.log(resultsWithCourses);
const charting = document.getElementById('myCharts').getContext('2d');
const myCharts = new Chart(charting, {
    type: 'bar',
    data: {
        labels: courses,
        datasets: [{
            label: '# of Votes',
            data: resultsWithCourses,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>






<script>
var resultEvent = {!! $resultEvent !!};
var eventsPageant = {!! $eventsPageant !!};
console.log(resultEvent);
const test = document.getElementById('myChartes').getContext('2d');
const myChartes = new Chart(test, {
    type: 'bar',
    data: {
        labels: eventsPageant,
        datasets: [{
            label: '# of Votes',
            data: resultEvent,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>

  {{--   <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-datalabels/2.0.0/chartjs-plugin-datalabels.min.js" integrity="sha512-R/QOHLpV1Ggq22vfDAWYOaMd5RopHrJNMxi8/lJu8Oihwi4Ho4BRFeiMiCefn9rasajKjnx9/fTQ/xkWnkDACg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
    // setup 

var resultEvent = {!! $resultEvent !!};
var eventsPageant = {!! $eventsPageant !!};
    const data = {
      labels: eventsPageant,
      datasets: [{
        label: 'Events',
        data: resultEvent,
        backgroundColor: [
             'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
        ],
        borderColor: [
          'rgba(255, 26, 104, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(0, 0, 0, 1)'
        ],
        borderWidth: 1
      }]
    };
 

    const config = {
      type: 'bar',
      data, 
      options: {
         scales: {
         },
         plugins: {
            tooltip: {
               enabled: false
         },
         datalabels: {
            formatter: (value, context) => {
               const datapoints = context.chart.data.datasets[0].data;
               function totalSum(total, datapoint) {
                  return total + datapoint;
               }
               const totalPercentage = datapoints.reduce(totalSum, 0);
               const percentageValue = (value / totalPercentage * 100).toFixed(1);
               return `${percentageValue}%`;
            }
         }
      }
   },
         plugins: [ChartDataLabels]
    };
    // render init block
    const myChartes = new Chart(
      document.getElementById('myChartes'),
      config
    );
    </script> --}}




<!-- 
Course Election
 -->
<script>

var resultCourse = {!! $resultCourse !!};
var electionCourseName = {!! $electionCourseName !!};
console.log(resultsWithCourses);
const chartCourse = document.getElementById('myChartCourse').getContext('2d');
const myChartCourse = new Chart(chartCourse, {
    type: 'bar',
    data: {
        labels: electionCourseName,
        datasets: [{
            label: '# of Votes',
            data: resultCourse,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>


 

 @endpush