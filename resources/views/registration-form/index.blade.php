@extends('layouts.frontend')
@section('content')

<div id="app" class="app">

<div class="register register-with-news-feed">

<div class="news-feed">
<div class="news-image" style="background-image: url('img/Login.png');"></div>
<div class="news-caption">
<h4 class="caption-title">SECURE ONLINE VOTING SYSTEM</h4>
<p>

</p>
</div>
</div>


<div class="register-container" style="width: 700px;">

<div class="register-header mb-25px h1">
<div class="mb-1">Sign Up</div>
</div>

	<div class="panel-body" style="background: ">
                    <form method="POST" action="/registration-from" enctype="multipart/form-data" >
                        @csrf
                        <div class="row"> 

					<div class="col-md-4">
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="firstname">{{ __('Firstname') }}<font style="color: red;">*</font></label>
                                <input id="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}"  autocomplete="firstname">

                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>			
                        </div>	
                        <div class="col-md-4">
                        <div class="mb-3">		
                        <div class="form-group">
                            <label for="middlename">{{ __('Middlename') }}<font style="color: red;">*</font></label>
                                <input id="middlename" type="text" class="form-control @error('middlename') is-invalid @enderror" name="middlename" value="{{ old('middlename') }}"  autocomplete="middlename">

                                @error('middlename')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        </div>

					<div class="col-md-4">
						<div class="mb-3">
					    <div class="form-group">
					        <label for="lastname">{{ __('Lastname') }}<font style="color: red;">*</font></label>
					            <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}"  autocomplete="lastname">

					            @error('lastname')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    </div>
					    </div>
					    	<div class="col-md-4">
					    		<div class="mb-3">
						<div class="form-group">
                            <label for="student_no">{{ __('Student No') }}<font style="color: red;">*</font></label>
                                <input id="student_no" type="text" class="form-control @error('student_no') is-invalid @enderror" name="student_no" value="{{ old('student_no') }}"  autocomplete="student_no" autofocus>
                                @error('student_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        </div>

					    <div class="col-md-4">
					    	<div class="mb-3">
					    <div class="form-group">
					        <label for="email">{{ __('Email') }}<font style="color: red;">*</font></label>
					            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">

					            @error('email')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    </div>					   
					    </div>					   

					     <div class="col-md-4">
					     	<div class="mb-3">
					    <div class="form-group">
					        <label for="address">{{ __('Address') }}<font style="color: red;">*</font></label>
					            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}"  autocomplete="address">
					            @error('address')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    </div>			     
					    </div>			     

					    <div class="col-md-4">
					    	<div class="mb-3">
					    <div class="form-group">
					        <label for="birth_date">{{ __('Birthdate') }}<font style="color: red;">*</font></label>
					            <input id="birth_date" type="date" class="form-control @error('birth_date') is-invalid @enderror" name="birth_date" value="{{ old('birth_date') }}"  autocomplete="birth_date">
					            @error('birth_date')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    </div>			   
					    </div>   

					        <div class="col-md-4">
					    	<div class="mb-3">
					    <div class="form-group">
					        <label for="contact_no">{{ __('Contact No') }}<font style="color: red;">*</font></label>
					        	<input type="text" name="contact_no" class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}">
					            @error('contact_no')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    </div>
					    </div>

					      <div class="col-md-4">
					      		<div class="mb-3">
					    <div class="form-group">
					      	<label for="photo">{{ __('Upload ID/COR') }}<font style="color: red;">*</font></label>
					            <input type="file" name="photo" value="{{ old('photo') }}" class="form-control @error('photo') is-invalid @enderror">
					             @error('photo')

					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        	
					        </div>
					    </div>	
					    </div>	
 

					    <div class="col-md-6">
							<div class="mb-3">
								<div class="form-group">
					        	<label for="sex">{{ __('Sex') }}<font style="color: red;">*</font></label>
					             <select class="form-control  @error('gender') is-invalid @enderror" name="gender">
					             <option  selected="" disabled="">Select Sex</option>
					             	@foreach ($genders as $genderId => $gender)
					             		<option {{ old('gender') == $genderId ? "selected" : "" }} value="{{ $genderId }}">{{ $gender }}</option>
					             	@endforeach
					             </select>
					            @error('gender')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    
					    </div>	
					    </div>	

						<div class="col-md-6">
							<div class="form-group">
					        <label for="year">{{ __('Campus') }}<font style="color: red;">*</font></label>

					             <select class="form-control  @error('campus') is-invalid @enderror" name="campus">
					             <option value=""  selected="" value="{{ old('campus') }}">Select Campus</option>
					             	@foreach ($campuses as $campusId => $campus)
					             		<option {{ old('campus') == $campusId ? "selected" : "" }}  value="{{ $campusId }}">{{ $campus }}</option>
					             	@endforeach
					             </select>
					            @error('year')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    
					    </div>	
					     

						<div class="col-md-4">
							<div class="mb-3">
					    <div class="form-group">
					    	<div class="form-group">
					        <label for="department">{{ __('College') }}<font style="color: red;">*</font></label>
					             <select class="form-control  @error('department') is-invalid @enderror" name="department" id="college" data-dependent="college">
					             <option  selected="" disabled="">Select College</option>
					             	@foreach ($departments as $departmentId => $department)
					             		<option {{ old('department') == $departmentId ? "selected" : "" }} value="{{ $departmentId }}">{{ $department }}</option>
					             	@endforeach
					             </select>
					            @error('department')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>

					    </div>
					    </div>
					    </div>
					      


							<div class="col-md-4">
						<div class="form-group">
					    	<label for="course">{{ __('Department/Major') }}<font style="color: red;">*</font></label>
					             <select class="form-control  @error('course') is-invalid @enderror"  name="course" id="course" >
					             <option   disabled=""  selected="">Select Department/Major</option>
					             	{{-- @foreach ($courses as $course)
					             		<option value="{{ $course->id }}">{{ $course->title }}</option>
					             	@endforeach --}}
					             </select>
					            @error('course')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        
					        </div>
								
					    
					    </div>	

			 

					    <div class="col-md-4">
						 <div class="mb-3">
						 	<div class="form-group">
					        <label for="program">{{ __('Program/Strand') }}<font style="color: red;">*</font></label>
					             <select class="form-control  @error('program') is-invalid @enderror" name="program" id="program">
					             <option  selected="" disabled="">Select Program/Strand</option>
							{{-- 	@foreach ($programs as $program)
								<option  value="{{ $program->id }}">{{ $program->program_name }}</option>
								@endforeach --}}
             </select>
					            @error('program')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>

					    
					    </div>

					    </div>

					     <div class="col-md-6">
					     	<div class="mb-3">
					    <div class="form-group">
					        <label for="year">{{ __('Year/Grade Level') }}<font style="color: red;">*</font></label>

					             <select class="form-control  @error('year') is-invalid @enderror" name="year">
					             <option value=""  selected="" >Select Year/Grade Level</option>
					             	<option value="Grade 7">Grade 7</option>
					             	<option value="Grade 8">Grade 8</option>
					             	<option value="Grade 9">Grade 9</option>
					             	<option value="Grade 10">Grade 10</option>
					             	<option value="Grade 11">Grade 11</option>
					             	<option value="Grade 12">Grade 12</option>
					             	<option value="1st Year">1st Year</option>
					             	<option value="2nd Year">2nd Year</option>
					             	<option value="3rd Year">3rd Year</option>
					             	<option value="4th Year">4th Year</option>
					             	<option value="5th Year">5th Year</option>
					             </select>
					            @error('year')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    </div>					     
					    </div>		

					      <div class="col-md-6">
					    	<div class="mb-3">
					    <div class="form-group">
					        <label for="section">{{ __('Section') }}<font style="color: red;">*</font></label>
					        	<input type="text" name="section"class="form-control @error('section') is-invalid @enderror" value="{{ old('section') }}">
					            @error('section')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
					    </div>				    
					    </div>				    
					
 
					   

                        <div class="form-group">
                        	<div class="mb-3">
                            <div class="col-md-6 ">
                                <button type="submit" class="btn btn-primary register-confirm">
                                    {{ __('Register') }}
                                </button>

                                <a href="/login"  class="mb-3">Login here</a>
                            </div>
                          
                        </div>
                    </form>
                </div>
                </div>

            </div>
        </div>
    </div>
</div>

  
@endsection

@push('scripts')
    <script type="text/javascript">
        $("select[name='department']").change(function () {
            let department = $(this).val();

            $.ajax({
                url: "{!! url('/select-course') !!}",
                method: 'GET',
                data: { department: department },
                success: function(data) {
                    $("select[name='course']").html('');
                    // $("select[name='program']").html('');
                     $("select[name='course']").html(data.options);
                    // $("select[name='program']").html(data.options);
                    console.log(data)
                }
            }); 
        });



    </script>

        <script type="text/javascript">
        $("select[name='course']").change(function () {
            let course = $(this).val();

            $.ajax({
                url: "{!! url('/select-program') !!}",
                method: 'GET',
                data: { course: course },
                success: function(data) {
                    $("select[name='program']").html('');
                    console.log(data)
                    $("select[name='program']").html(data.options);
                }
            }); 
        });



    </script>

 
@endpush