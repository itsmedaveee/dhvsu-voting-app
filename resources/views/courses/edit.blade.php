@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/courses">Department</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Department </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Department</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/courses/{{ $course->id }}">
							@csrf
							{{ method_field ('PATCH') }}
							<div class="form-group">
							<label for="name">College:</label>
								<select class="form-control" id="department" name="department">
									 <option value="" disabled="" selected="">Select College</option>
									@foreach ($departments as $departmentId => $department )
									<option value="{{ $departmentId }}" {{ $course->department_id == $departmentId ? 'selected' : '' }}>{{ $department }}</option>
									@endforeach
								</select>
							</div>
						 
							<div class="form-group">
								<label>Name</label>
								<input type="text" class="form-control" name="code" value="{{ $course->code }}">
							</div>
							<div class="form-group">
								<label>Location</label>
								<input type="text" class="form-control" name="title" value="{{ $course->title }}">
							</div>
					
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div> 
 
			<!-- end #content -->












@endsection