@extends('layouts.master')
@section('content')

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right"> 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Results </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Results</h4>
						<div class="panel-heading-btn">
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<a href="/print-results-pageant/{{ $pageant->id }}" class="btn btn-primary"  target="_blank">Print Results</a>
						</div>
						<table class="table table-bordered table-condensed table-hover">
							<thead>
								<tr>
									<th>FullName</th>
									<th>Title</th>
									<th></th>
								</tr>
							</thead>
						  	<tbody>
						  		@foreach ($pageant->nominees as $nominee)
						  		<tr>
						  			<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
						  			<td>{{ $nominee->title }}</td>
						  			<td>
						  				{{ count($nominee->resultsPageant) / $totalVoteUsers * 100 . '%'  }} 
						  				<div class="progress h-10px"> 
											<div class="progress-bar progress-bar-striped progress-bar-animated bg-success fs-10px fw-bold" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"  style="width:{{ count($nominee->resultsPageant) / $totalVoteUsers * 100 . '%'  }}   "> 
												{{--  {{ count($result->nominee->results) / $totalVoteUsers * 100 . '%'  }}  --}}
												   
											</div>
										</div>
									</td>
						  		</tr>
						  		@endforeach
						  	</tbody>
						</table> 
					</div>
				</div>

 

@endsection