{{-- <div class="modal fade" id="ajax-stop-pageant-modal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="pageantModal">Modal</h4>
         </div>

         <div class="modal-body">
            <form id="pageantStopForm" class="form-horizontal">
               <input type="hidden" name="pageantId" id="pageantId">
                <h4 align="center">Do you want to Stop Pageant?</h4>
               <div class="form-group">
                  <button type="submit" class="btn btn-block btn-primary" id="btn-update">Stop Pageant</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div> --}}

<!-- Modal -->
           <div class="modal fade" id="ajax-stop-pageant-modal" aria-hidden="true">
                 <div class="modal-dialog modal-dialog-centered" role="document">
                   <div class="modal-content">
                     <div class=""></div>
                     <div class="modal-body">{{-- 
                        <p class="text-center"><img src="{{ asset('img/mark.png') }}" style="width: 20%;" ></p> --}}
                        <div class="swal-icon swal-icon--warning">
                        <span class="swal-icon--warning__body">
                        <span class="swal-icon--warning__dot"></span>
                        </span>
                        </div>
                     <form id="pageantStopForm" class="form-horizontal">
                     <input type="hidden" name="pageantId" id="pageantId">

                       <h5 style="font-size: 1.875rem;font-weight:600; " class="text-center">DO YOU WANT TO STOP THE EVENT?</h5>
         
                       <div class="form-group" align="center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel </button>
                       <button type="submit" class="btn btn-primary">Stop Event</button>
                        </form>
                    </div>
                     </div>
                   </div>
                 </div>
               </div>