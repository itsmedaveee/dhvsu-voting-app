@extends('layouts.master')
@section('content')
<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>



<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Event </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Event</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/pageants">
							@csrf 
							<div class="form-group {{ $errors->has('event_name') ? ' has-error' : '' }}">
								<label> Name</label>
								<input type="text" class="form-control" name="event_name" >
								@if ($errors->has('event_name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('event_name') }}</strong>
								    </span>
								@endif
							</div>

							   <div class="form-group">
					        <label for="department">{{ __('College') }}</label>
					             <select class="form-control{{ $errors->has('department') ? ' has-error' : '' }}" name="department" >
					             <option value=""  selected="" disabled="">Select College</option>
					             	@foreach ($departments as $department)
					             		<option value="{{ $department->id }}">{{ $department->title }}</option>
					             	@endforeach
					             </select>
					            @if ($errors->has('department'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('department') }}</strong>
								    </span>
								@endif
					        </div>


						 


							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->

			<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Events</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						@if (auth()->user()->isSuperAdmin())
							<table class="table table-bordered table-hover" id="partyname-table">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>College</th>
										<th>Time Ended</th>
										<th>Status</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($pageants as $pageant)
									@include('pageants.modal.start-pageant')
									@include('pageants.modal.stop-pageant')
									<tr>
										<td>{{ $pageant->id }}</td>
										<td>{{ $pageant->event_name }}</td>
										<td>{{ $pageant->department->code ?? null }}</td>
										<td>
											@if ($pageant->ended_time)
												{{ \Carbon\Carbon::parse($pageant->ended_time)->format('h:s A')  }}</td>
											@endif
										<td>
											@if ($pageant->status == 'On Going')

													<span class="label label-warning"> {{ $pageant->status }}</span>
												@elseif ($pageant->status == 'Starting now')

													<span class="label label-green"> {{ $pageant->status }}</span>

												@elseif ($pageant->status == 'End Events')

													<span class="label label-danger"> {{ $pageant->status }}</span>

												@endif
											</td>
										<td>
											<a href="/pageants/{{ $pageant->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
										
										</td>
										<td>
												<a href="/pageants/{{ $pageant->id }}/show-results" class="btn btn-info btn-xs"> View Results </a>
											{{-- 	@if ($pageant->status == 'On Going' || $pageant->status == 'Starting now')
												<a href="javascript:void(0)" data-id="{{ $pageant->id }}" id="update-pageant" class="btn btn-green btn-xs">Start Event</a>	
												@endif
	 
												<a href="javascript:void(0)" data-id="{{ $pageant->id }}" id="stop-pageant" class="btn btn-danger btn-xs">Stop Event</a>
 --}}
											</td>
									</tr>
									@endforeach
							 
								</tbody>
								
							</table>
						@endif
						@if (auth()->user()->isCouncil())
							<table class="table table-bordered table-hover" id="partyname-table">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>College</th>
										<th>Time Ended</th>
										<th>Status</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($pageants as $pageant)
									@include('pageants.modal.start-pageant')
									@include('pageants.modal.stop-pageant')
									<tr>
										<td>{{ $pageant->id }}</td>
										<td>{{ $pageant->event_name }}</td>
										<td>{{ $pageant->department->title ?? null }}</td>
										<td>
											@if ($pageant->ended_time)
												{{ \Carbon\Carbon::parse($pageant->ended_time)->format('h:s A')  }}</td>
											@endif
										<td>
											@if ($pageant->status == 'On Going')

													<span class="label label-warning"> {{ $pageant->status }}</span>
												@elseif ($pageant->status == 'Starting now')

													<span class="label label-green"> {{ $pageant->status }}</span>

												@elseif ($pageant->status == 'End Events')

													<span class="label label-danger"> {{ $pageant->status }}</span>

												@endif
											</td>
										<td>
											<a href="/pageants/{{ $pageant->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
										
										</td>
										<td>
												<a href="/pageants/{{ $pageant->id }}/show-results" class="btn btn-info btn-xs"> View Results </a>
												@if ($pageant->status == 'On Going' || $pageant->status == 'Starting now')
												<a href="javascript:void(0)" data-id="{{ $pageant->id }}" id="update-pageant" class="btn btn-green btn-xs">Start Event</a>	
												@endif
	 
												<a href="javascript:void(0)" data-id="{{ $pageant->id }}" id="stop-pageant" class="btn btn-danger btn-xs">Stop Event</a>

											</td>
									</tr>
									@endforeach
							 
								</tbody>
								
							</table>

						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

 
@endsection

@push('scripts')
<script>
   $('body').on('click', '#update-pageant', function () {
       var pageant = $(this).data('id');
       $.get('/ajax-pageant/' + pageant, function (pageant) {
           $('#pageantModal').html("Modal");
           // $('#btn-update').val("edit-fee");
           $('#pageantId').val(pageant.id);
           $('#ajax-pageant-modal').modal('show');
       })
   });
    $("#pageantForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#btn-update').html('<i class="fa fa fa-spinner fa-spin"></i> Starting..');
        var form = $(this);
        var pageantIds = $('#pageantId').val();
        $.ajax({
            type: "PUT",
            url: '/ajax-start-pageant/' + pageantIds,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                toastr.info("Starting Pageant!");
                $('#test').trigger("reset");
                $('#ajax-election-modal').modal('hide');
                location.reload();
            }
         });
    });
</script>

<script>
   $('body').on('click', '#stop-pageant', function () {
       var pageant = $(this).data('id');
       $.get('/ajax-stop-pageant/' + pageant, function (pageant) {
           $('#pageantModal').html("Modal");
           $('#btn-update').val("edit-fee");
           $('#pageantId').val(pageant.id);
           $('#ajax-stop-pageant-modal').modal('show');
       })
   });
    $("#pageantStopForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#btn-update').html('<i class="fa fa fa-spinner fa-spin"></i> End Pageant..');
        var form = $(this);
        var pageantIds = $('#pageantId').val();
        $.ajax({
            type: "PUT",
            url: '/ajax-stop-pageant/' + pageantIds,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                toastr.error("Stop Event!");
                $('#test').trigger("reset");
                $('#ajax-stop-election-modal').modal('hide');
                location.reload();
            }
         });
    });
</script>
@endpush
