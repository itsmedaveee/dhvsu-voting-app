@extends('layouts.master')
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>



<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Edit Event </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Event</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/pageants/{{ $pageant->id }}">
							@csrf 
							{{ method_field('PATCH') }}
							<div class="form-group {{ $errors->has('event_name') ? ' has-error' : '' }}">
								<label> Name</label>
								<input type="text" class="form-control" name="event_name" value="{{ $pageant->event_name }}">
								@if ($errors->has('event_name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('event_name') }}</strong>
								    </span>
								@endif
							</div>


							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->





@endsection