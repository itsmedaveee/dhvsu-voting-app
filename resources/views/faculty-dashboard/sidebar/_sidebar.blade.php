<div id="sidebar" class="sidebar">
   <!-- begin sidebar scrollbar -->
   <div data-scrollbar="true" data-height="100%">
      <!-- begin sidebar user -->
      <ul class="nav">
         <li class="nav-profile">
            <a href="javascript:;" data-toggle="nav-profile">
               <div class="cover with-shadow"></div>

              
	              <div class="image">
	                 <img src="{{ asset('img/admin.png') }}" alt="" /> 
	              </div>
	         

               <div class="info">
                  {{ auth()->user()->username }}
                  <small>{{ auth()->user()->role->name }}</small>
               </div>
            </a>
         </li>
      </ul>
      <!-- end sidebar user -->
      <!-- begin sidebar nav -->
      <ul class="nav">
         <li class="nav-header">Navigation</li>
      <li class="has-sub {{ Request::is('faculty/home') ? 'active' : '' }}">
            <a href="/faculty/home">
               <div class="icon-img">
             		<i class="fa fa-home"></i>
               </div>
               <span>Dashboard</span>
            </a>
         </li>

  {{--        <li class="has-sub {{ Request::is('faculties') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
                  <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" />
               <i class="fa fa-user-md"></i>
               </div>
               <span>Faculty User</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('faculties') ? 'active' : '' }}"><a href="/faculties">Faculty Users</a></li>
            </ul>
         </li>
 --}}
     {{--     <li class="has-sub {{ Request::is('positions') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
       
               <i class="fa fa-tasks"></i>
               </div>
               <span>Positions</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('positions') ? 'active' : '' }}"><a href="/positions">Positions</a></li>
            </ul>
         </li>

         <li class="has-sub {{ Request::is('partylists') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
         
               <i class="fa fa-flag"></i>
               </div>
               <span>Partylists</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('partylists') ? 'active' : '' }}"><a href="/partylists">Partylists</a></li>
            </ul>
         </li>
 --}}

         <li class="has-sub {{ Request::is('nominees') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-user"></i>
               </div>
               <span>Nominees</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('nominees') ? 'active' : '' }}"><a href="/nominees">Nominees</a></li>
            </ul>
         </li>

          <li class="has-sub {{ Request::is('students') ? 'active' : '' }}  {{ Request::is('pending-students') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-users"></i>
               </div>
               <span>Students</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('pending-students') ? 'active' : '' }}"><a href="/pending-students">Pending Student</a></li>
               <li class="{{ Request::is('students') ? 'active' : '' }}"><a href="/students">Students</a></li>
            </ul>
         </li>
 
         <!-- begin sidebar minify button -->
         <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
         <!-- end sidebar minify button -->
      </ul>
      <!-- end sidebar nav -->
   </div>
   <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>