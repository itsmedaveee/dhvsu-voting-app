@extends ('layouts.master')
@section ('content')


<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
        @if (auth()->user()->userable->image)
            <div class="profile-header-img">
               <img src="{{ Storage::url(auth()->user()->userable->image) }}" alt="" style="height: 100%" style="width: 100%"> 
              {{--  <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%"> --}}
            </div>
            @else
            <div class="profile-header-img">
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
            @endif
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>


<div id="content" class="content">
   <!-- begin breadcrumb -->
   <ol class="breadcrumb float-xl-right">
   </ol>
   <h1>My Notifications</h1>
   <!-- end breadcrumb --> 
    <div class="row">
        <div class="col-md-12">
      <div class="panel panel-default">
          <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">
            <h4 class="panel-title">Notifications</h4>
            <div class="panel-heading-btn">
              
            </div>
          </div>
          <div class="panel-body">
            <table class="table table-bordered table-hover">
               <thead>
                  <th>Status</th>
               </thead>
               <tbody>
                  <tr> 
                        <td>
                           {{ $notif->name }}
                           {{ $campus->name }}
                        </td> 
                  </tr>
               </tbody>
            </table>
          </div>
      </div>
  </div>
</div>
</div>

 




@endsection