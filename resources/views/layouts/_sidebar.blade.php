@inject('stats','App\Stats') 
<div id="sidebar" class="sidebar">
   <!-- begin sidebar scrollbar -->
   <div data-scrollbar="true" data-height="100%">
      <!-- begin sidebar user -->
      <ul class="nav">
         <li class="nav-profile">
            <a href="javascript:;" data-toggle="nav-profile">
               <div class="cover with-shadow"></div>

              
	              <div class="image">
	                 <img src="{{ asset('img/admin.png') }}" alt="" /> 
	              </div>
	         

               <div class="info">
                  {{ auth()->user()->username }}
                  <small>{{ auth()->user()->department->code ?? null }}{{ auth()->user()->role->name }}</small>
               </div>
            </a>
         </li>
      </ul>
      <!-- end sidebar user -->
      <!-- begin sidebar nav -->
      <ul class="nav">
         <li class="nav-header">Navigation</li>
         <li class="has-sub {{ Request::is('home') ? 'active' : '' }}">
            <a href="/home">
               <div class="icon-img">
             		<i class="fa fa-home"></i>
               </div>
               <span>Dashboard</span>
            </a>
         </li>


         <li class="has-sub {{ Request::is('announcements') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-bullhorn"></i>
               </div>
               <span>Announcements</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('announcements') ? 'active' : '' }}"><a href="/announcements">Announcements</a></li>
            </ul>
         </li>

         <li class="has-sub {{ Request::is('super-admin') ? 'active' : '' }} {{ Request::is('councils') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-user-md"></i>
               </div>
               <span>Manage Users</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('super-admin') ? 'active' : '' }}"><a href="/super-admin">Super Admin</a></li>
               <li class="{{ Request::is('councils') ? 'active' : '' }}"><a href="/councils">Council Users</a></li>
            </ul>
         </li>

          <li class="has-sub {{ Request::is('lock-in-period') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-lock"></i>
               </div>
               <span>Date for Register</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('lock-in-period') ? 'active' : '' }}"><a href="/lock-in-period"> Date for Register</a></li>
            </ul>
         </li>

          <li class="has-sub {{ Request::is('campuses') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-file"></i>
               </div>
               <span>Campuses</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('campuses') ? 'active' : '' }}"><a href="/campuses">Campuses</a></li>
            </ul>
         </li>

           <li class="has-sub {{ Request::is('departments') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-building"></i>
               </div>
               <span>Colleges</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('departments') ? 'active' : '' }}"><a href="/departments">Colleges</a></li>
            </ul>
         </li>

         <li class="has-sub {{ Request::is('courses') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-book"></i>
               </div>
               <span>Department</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('courses') ? 'active' : '' }}"><a href="/courses">Department</a></li>
            </ul>
         </li>      

         <li class="has-sub {{ Request::is('program') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-book"></i>
               </div>
               <span>Programs</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('program') ? 'active' : '' }}"><a href="/program">Programs</a></li>
            </ul>
         </li>

    <li class="has-sub {{ Request::is('reports') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-list"></i>
               </div>
               <span>Reports</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('reports') ? 'active' : '' }}"><a href="/reports">Reports</a></li>
            </ul>
         </li>

    <li class="has-sub {{ Request::is('genders') ? 'active' : '' }} {{ Request::is('genders*') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
              <i class="fa fa-venus-mars" aria-hidden="true"></i>
               </div>
               <span>Sex</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('genders') ? 'active' : '' }}  {{ Request::is('genders*') ? 'active' : '' }}"><a href="/genders">Sex</a></li>
            </ul>
         </li>

         
   {{--       <li class="has-sub {{ Request::is('partylists') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               <i class="fa fa-flag"></i>
               </div>
               <span>Party Name </span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('partylists') ? 'active' : '' }}"><a href="/partylists">Party Name</a></li>
            </ul>
         </li> --}}
 
         <li class="has-sub  {{ Request::is('nominees') ? 'active' : '' }}  {{ Request::is('pending-request-file-nominee') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               <i class="fa fa-user"></i>
               </div>
               <span>Pending Candidacy <span class="label label-danger" style="padding:4px;">{{ $stats->totalNomineePending() }}</span></span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('pending-request-file-nominee') ? 'active' : '' }}"><a href="/pending-request-file-nominee">Pending Candidacies </a></li>
               <li class="{{ Request::is('nominees') ? 'active' : '' }}"><a href="/nominees">Candidates</a></li>
            </ul>
         </li>

          <li class="has-sub {{ Request::is('pending-students') ? 'active' : '' }} {{ Request::is('students') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;" class="menu-link">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-users"></i>
               </div>
               <div class="menu-text"> Students  <span class="label label-danger" style="padding:4px;">{{ $stats->totalPendingStudent() }}</span></div>

            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('pending-students') ? 'active' : '' }}"><a href="/pending-students">Pending Student</a></li>
               <li class="{{ Request::is('students') ? 'active' : '' }}"><a href="/students">Students</a></li>
            </ul>
         </li>


       <li class="has-sub {{ Request::is('pageants') ? 'active' : '' }} {{ Request::is('pageants*') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
              <i class="fa fa-file-text-o" aria-hidden="true"></i>
               </div>
               <span>Events</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('pageants') ? 'active' : '' }} {{ Request::is('pageants*') ? 'active' : '' }} "><a href="/pageants">Events</a></li>
            </ul>
         </li>
    
         <li class="has-sub {{ Request::is('elections') ? 'active' : '' }} {{ Request::is('pending-students') ? 'active' : '' }} {{ Request::is('students') ? 'active' : '' }}   {{ Request::is('position-department') ? 'active' : '' }}  {{ Request::is('courses') ? 'active' : '' }} {{ Request::is('departments') ? 'active' : '' }} {{ Request::is('campuses') ? 'active' : '' }} {{ Request::is('announcements') ? 'active' : '' }} {{ Request::is('positions') ? 'active' : '' }}  {{ Request::is('home') ? 'active' : '' }} {{ Request::is('partylists') ? 'active' : '' }} {{ Request::is('super-admin') ? 'active' : '' }} {{ Request::is('election-course') ? 'active' : '' }} {{ Request::is('position-course') ? 'active' : '' }} } {{ Request::is('election-department') ? 'active' : '' }} {{ Request::is('nominees') ? 'active' : '' }}  {{ Request::is('pending-request-file-nominee') ? 'active' : '' }}   {{ Request::is('councils') ? 'active' : '' }} "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-list-ol "></i>
               </div>
               <span>Election Campuses</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('elections') ? 'active' : '' }}"><a href="/elections">Elections</a></li>
               <li class="{{ Request::is('positions') ? 'active' : '' }}"><a href="/positions">Positions</a></li>
            </ul>
         </li>

           <li class="has-sub {{ Request::is('position-department') ? 'active' : '' }} {{ Request::is('pending-students') ? 'active' : '' }} {{ Request::is('students') ? 'active' : '' }}   {{ Request::is('elections') ? 'active' : '' }} {{ Request::is('positions') ? 'active' : '' }} {{ Request::is('courses') ? 'active' : '' }} {{ Request::is('departments') ? 'active' : '' }}{{ Request::is('campuses') ? 'active' : '' }} {{ Request::is('super-admin') ? 'active' : '' }} {{ Request::is('councils') ? 'active' : '' }}  {{ Request::is('election-department') ? 'active' : '' }}{{ Request::is('announcements') ? 'active' : '' }} {{ Request::is('election-course') ? 'active' : '' }} {{ Request::is('position-course') ? 'active' : '' }}  {{ Request::is('home') ? 'active' : '' }} {{ Request::is('nominees') ? 'active' : '' }}  {{ Request::is('pending-request-file-nominee') ? 'active' : '' }}   {{ Request::is('partylists') ? 'active' : '' }} "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-toggle-on"></i>
               </div>
               <span> Election Colleges</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('election-department') ? 'active' : '' }}"><a href="/election-department">Election </a></li>
               <li class="{{ Request::is('position-department') ? 'active' : '' }}"><a href="/position-department">Position </a></li>
            </ul>
         </li>         
         <li class="has-sub {{ Request::is('position-election') ? 'active' : '' }} {{ Request::is('announcements') ? 'active' : '' }} {{ Request::is('election-course') ? 'active' : '' }} {{ Request::is('position-course') ? 'active' : '' }} {{ Request::is('position-department') ? 'active' : '' }} {{ Request::is('elections') ? 'active' : '' }} {{ Request::is('positions') ? 'active' : '' }} {{ Request::is('courses') ? 'active' : '' }} {{ Request::is('departments') ? 'active' : '' }}{{ Request::is('campuses') ? 'active' : '' }} {{ Request::is('super-admin') ? 'active' : '' }} {{ Request::is('councils') ? 'active' : '' }}  {{ Request::is('election-department') ? 'active' : '' }} {{ Request::is('nominees') ? 'active' : '' }}  {{ Request::is('pending-request-file-nominee') ? 'active' : '' }}  {{ Request::is('partylists') ? 'active' : '' }}  {{ Request::is('pending-students') ? 'active' : '' }} {{ Request::is('students') ? 'active' : '' }}   {{ Request::is('home') ? 'active' : '' }} "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-server"></i>
               </div>
               <span> Election Department</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('election-course') ? 'active' : '' }}"><a href="/election-course">Election </a></li>
               <li class="{{ Request::is('position-course') ? 'active' : '' }}"><a href="/position-course">Position </a></li>
            </ul>
         </li>
 


{{-- 
         <li class="has-sub {{ Request::is('positions') ? 'active' : '' }}    "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
                  <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" />
               <i class="fa fa-tasks"></i>
               </div>
               <span>Positions</span>
            </a>
            <ul class="sub-menu">
                 <li class="{{ Request::is('positions') ? 'active' : '' }}"><a href="/positions">Positions</a></li>
            </ul>
         </li> --}}

        
         <!-- begin sidebar minify button -->
         <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
         <!-- end sidebar minify button -->
      </ul>
      <!-- end sidebar nav -->
   </div>
   <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>