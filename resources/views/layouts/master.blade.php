 <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>DHVSU VOTING SYSTEM</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
   {{--  <link href="{{ asset('css/apple.css') }}" rel="stylesheet" /> --}} 
    <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" />
{{--     <link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet" /> --}}
    <link href="{{ asset('css/red.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    


<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

{{--        <link href="{{ asset('default/app.css') }}" rel="stylesheet" /> --}}
{{--     <link href="{{ asset('css/switchery.css') }}" rel="stylesheet" />
    <script src="{{ asset('/js/switchery.js') }}"></script> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" type="text/javascript"></script>
{{--     <link href="{{ asset('css/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap-datepicker3.css') }}" rel="stylesheet" />
    <script src="{{ asset('css/sweetalert2/sweetalert2.min.js') }}"></script>
    <link href="{{ asset('css/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet"> --}}
   {{--  <link href="{{ asset('css/jquery.gritter.css') }}" rel="stylesheet"> --}}
   <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/trix-master/dist/trix.css') }}">
    <script type="text/javascript" src="{{ asset('assets/plugins/trix-master/dist/trix.js') }}"></script>
    <link href="{{ asset('assets/plugins/datatable/DataTables-1.10.18/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" >
     <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="{{ asset('css/sweetalert2/sweetalert2.min.js') }}"></script>
    <link href="{{ asset('css/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="https://seantheme.com/color-admin/admin/assets/plugins/@highlightjs/cdn-assets/highlight.min.js" rel="stylesheet">
  <style type="text/css">
      
      .h-30px {

        height: 30px!important;
      }
      .rounded {
        border-radius: 4px!important;
      }
  </style>
   <style>
 .swal-icon--warning {
    border-color: #f59c1a;
}
.swal-icon--warning__body, .swal-icon--warning__dot {
    position: absolute;
    left: 50%;
    background-color: #f8bb86;
}
.swal-icon--warning__dot {
    width: 7px;
    height: 7px;
    border-radius: 50%;
    margin-left: -4px;
    bottom: -11px;
}
.swal-icon--warning__body {
    width: 5px;
    height: 47px;
    top: 10px;
    border-radius: 2px;
    margin-left: -2px;
}
.swal-icon--warning:after {
    width: 7px;
    height: 7px;
    border-radius: 50%;
    margin-left: -3px;
    top: 19px;
}

info:before {
    content: "";
    position: absolute;
    left: 50%;
    background-color: #c9dae1;
}

.swal-icon {
    width: 80px;
    height: 80px;
    border-width: 4px;
    border-style: solid;
    border-radius: 50%;
    padding: 0;
    position: relative;
    box-sizing: content-box;
    margin: 20px auto;
}

    
      .chartMenu {
        width: 100vw;
        height: 40px;
        background: #1A1A1A;
        color: rgba(255, 26, 104, 1);
      }
      .chartMenu p {
        padding: 10px;
        font-size: 20px;
      }
      .chartCard {
        width: 100vw;
        height: calc(100vh - 40px);
        background: rgba(255, 26, 104, 0.2);
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .chartBox {
        width: 700px;
        padding: 20px;
        border-radius: 20px;
        border: solid 3px rgba(255, 26, 104, 1);
        background: white;
      }
    </style>
</head>
  
    <body>
 <!-- begin #page-loader -->
 <div id="app" class="app app-header-fixed app-sidebar-fixed">

    <div id="page-loader" class="fade show">
        <span class="spinner"></span>
    </div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
        <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">

        @include('layouts._header')
     @if(auth()->user()->isSuperAdmin())  
        @include('layouts._sidebar')
     @elseif (auth()->user()->isCouncil())
        @include('council-dashboard.sidebar.index')
     @elseif (auth()->user()->isStudent())
        @include('student-dashboard.sidebar._sidebar')

    @endif
    
 
        <main class="py">
            @yield('content')
        </main>
    </div>
{{--     <script src="{{ asset('/js/app.js') }}"></script> --}}
    <script src="{{ asset('/js/app.min.js') }}"></script>
    <script src="{{ asset('/js/apple.min.js') }}"></script>
{{--     <script src="{{ asset('/js/theme/default.min.js') }}"></script> --}}
    <script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('/js/toastr.min.js') }}"></script>
    <script src="{{ asset('/js/select2.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('/js/chart.min.js') }}"></script>
{{--     <script src="{{ asset('/js/switchery.min.js') }}"></script>
    <script src="{{ asset('/js/particles.js') }}"></script> --}}
{{--     <script src="{{ asset('/js/stats.js') }}"></script> --}}
{{--     <script src="{{ asset('/js/app-js.js') }}"></script> --}}
 {{--    <script src="{{ asset('/js/moment.js') }}"></script> --}}
  {{--   <script src="{{ asset('/js/jquery.gritter.js') }}"></script> --}}
 

{{--     <script src="{{ asset('/js/bootstrap-datetimepicker.min.js') }}"></script> --}}
   {{--  <script src="{{ asset('/js/slim-scroll.js') }}"></script> --}}
    {{-- <script src="{{ asset('/js/sweetalert.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('/js/ui-modal-notification.demo.js') }}" ></script> --}}
{{--     <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}

 

    <script>

        @if (Session::has('status'))
         swal("Good job!", "Success Vote!", "success");
        @endif
      @if (Session::has('statuses'))
         swal("Good job!", "Success Submit!", "success");
        @endif

       
   
        @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}");
            {{ session()->forget('success') }}
        @endif

        @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
        @endif

        @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
        @endif

        @if(Session::has('error'))
            toastr.error("{{ Session::get('error') }}");
        @endif

        @if(Session::has('votedNominees'))
            toastr.options.escapeHtml = true;
            
            let votedNominees = @json(session('votedNominees'));
            
            ul = document.createElement('ul');

            votedNominees.forEach(function (item) {
                let li = document.createElement('li');
                ul.appendChild(li);

                li.innerHTML += 'Already voted ' + item.nominee.firstname ;
            });

            toastr.error(ul);
        @endif
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
    </script>
    
<script>
     $(document).ready(function() {
        $('#slimScroll').slimScroll({
            height: '250px'
        });
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  
    @stack('scripts')
    <script src="{{ asset('assets/plugins/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatable/DataTables-1.10.18/js/dataTables.bootstrap.min.js') }}"></script>
</body>
</html>