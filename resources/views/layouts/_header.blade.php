@inject('stats','App\Stats') 
	<!-- begin #page-container -->
	<div id="page-container" >
		<!-- begin #header -->
		<div id="header" class="header navbar-inverse"  style="background: linear-gradient(to right, #5c0017  0%,  #feba29 100%)">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="#" class="navbar-brand"><span></span> <img src="{{ asset('img/dhvsu.png') }}"> &nbsp; 
					<b style="color: #feba29;">Don Honorio Ventura  State University - {{ auth()->user()->campus->location }}</b> &nbsp;  
				</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				  <li class="dropdown">
		@if (auth()->user()->isStudent())
         <a href="#" data-toggle="dropdown" class="dropdown-toggle f-s-14">
         <i class="fa fa-bell"></i>
         <span class="label">
				@if (auth()->user()->isStudent()) 
	{{-- 			@if ($student->announcements)
					@if ($stats->counts())
						{{ $stats->counts() }}
					@else 
						0
					@endif
					@endif --}}
				{{ $stats->electionStatuses() + $stats->campusStatus() }}
				@endif
	         </span>
	         </a>
	            <div class="dropdown-menu media-list dropdown-menu-right">
	               <div class="panel-body bg-light">
	                  <div class="notification" data-scrollbar="true" data-height="200px">
	                  <div class="dropdown-header">
	                     NOTIFICATIONS 
	                     @if (auth()->user()->isStudent())
							@if ($stats->electionStatuses())
							  {{ $stats->electionStatuses() + $stats->campusStatus() }}
							@else 
								0
							@endif
	                     @endif
	                  </div>
	                 


					@forelse  ($notify as $notif)
	                     @if (auth()->user()->isStudent())
	                        <a href="#" class="dropdown-item media">  
	                        	<a href="/notification/{{ $notif->id }}" class="dropdown-item media">
	                     @endif                    
	                     <div class="media-left" >
	                     </div>
	                     <div class="media-body">
	                        <h6 class="media-heading">
	                       		{{ $notif->name }}
	                        </h6>
	                        <div class="text-muted f-s-10">{{ $notif->created_at->diffForHumans() }}</div>
	                     </div>
	                    @empty
	                  	<p class="text-muted f-s-10" > &nbsp; </p>
						@endforelse
						@forelse  ($campuses as $campus)
	                     @if (auth()->user()->isStudent())
	                        <a href="#" class="dropdown-item media">  
	                        	<a href="/notification-for-campuses/{{ $campus->id }}" class="dropdown-item media">
	                     @endif                    
	                     <div class="media-left" >
	                     </div>
	                     <div class="media-body">
	                        <h6 class="media-heading">
	                       		{{ $campus->name }}
	                        </h6>
	                        <div class="text-muted f-s-10">{{ $campus->created_at->diffForHumans() }}</div>
	                     </div>
	                    @empty
	                  	<p class="text-muted f-s-10" > &nbsp; No data available!</p>
						@endforelse
	                  </a>
	               </div>
	               <div class="dropdown-footer text-center">
	               <a href="/view-all-notifications">View more</a>
	               </div>
	             </div>
         </li>
         @endif
 

				<li class="dropdown navbar-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						@if (auth()->user()->userable->image)
						 <img src="{{ Storage::url(auth()->user()->userable->image) }}"> 
						@else
						 <img src="{{ asset('img/admin.png') }}" alt="" /> 
						 @endif
							<span class="d-none d-md-inline">{{ auth()->user()->username }}</span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="/settings" class="dropdown-item">Edit Profile</a>
						<div class="dropdown-divider"></div>
						  <a href="/logout" onclick="event.preventDefault(); 
                            document.getElementById('logout-form').submit();" class="dropdown-item">
                            Logout
                        </a>
                        <form id="logout-form" action="/logout" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
					</div>
				</li>
			</ul>
			<!-- end header-nav -->
		</div>
		<!-- end #header -->
		