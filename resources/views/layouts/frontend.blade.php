<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Login</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<link href="{{ asset('default/vendor.css') }}" rel="stylesheet" />
<link href="{{ asset('default/app.css') }}" rel="stylesheet" />
<link href="{{ asset('css/toastr.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


</head>
<body class='pace-top'>
{{-- 
<div id="loader" class="app-loader">
<span class="spinner"></span>
</div> --}}


<div id="app" class="app">

@yield('content')



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{ asset('js/vendor.min.js') }}" type="6dcf7522f7dfcce7f4985d57-text/javascript"></script>
<script src="{{ asset('js/app.min.js') }}" type="6dcf7522f7dfcce7f4985d57-text/javascript"></script>
<script src="{{ asset('js/theme/default.min.js') }}" type="6dcf7522f7dfcce7f4985d57-text/javascript"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>


<script>
@if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    {{ session()->forget('success') }}
@endif

@if(Session::has('error'))
    toastr.error("{{ Session::get('error') }}");
@endif

</script>
@stack('scripts') 

</body>
</html>





