@extends('layouts.master')
@section('content')


<div id="content" class="content">
				
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Elections Department</h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Election</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/election-course">
							@csrf
						 
							<div class="form-group{{ $errors->has('election_name') ? ' has-error' : '' }}">
								<label>Election Name</label>
								<input type="text" class="form-control" name="election_name">
								@if ($errors->has('election_name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('election_name') }}</strong>
								    </span>
								@endif
							</div>		


						    <div class="form-group">
					        <label for="department">{{ __('Department') }}</label>
					             <select class="form-control{{ $errors->has('course') ? ' has-error' : '' }}" name="course" id="name">
					             <option value=""  selected="" disabled="">Select Department</option>
					             	@foreach ($courses as $course)
					             		<option value="{{ $course->id }}">{{ $course->title }}</option>
					             	@endforeach
					             </select>
					          @if ($errors->has('course'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('course') }}</strong>
								    </span>
								@endif
					        </div>
		 

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>


		<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Election lists</h4>
						<div class="panel-heading-btn">
						</div>
					</div>
					<div class="panel-body">
					  @if (auth()->user()->isSuperAdmin())
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>ID</th>
									<th>Election Name</th>
									<th>Department</th>
									<th>End Time</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($elections as $election)
								@include('events.election-course.modal.start-election')
								@include('events.election-course.modal.stop-election')
									<tr>
										<td>{{ $election->id }}</td>
										<td>{{ $election->election_name }}</td>
										<td>{{ $election->course->title ?? null }}</td>
										<td>
											@if ($election->ended_time)
												{{ \Carbon\Carbon::parse($election->ended_time)->format('h:s A')  }}</td>
											@endif
										</td>
										<td>
											@if ($election->status == 'On Going')

												<span class="label label-warning"> {{ $election->status }}</span>
											@elseif ($election->status == 'Starting now')

												<span class="label label-green"> {{ $election->status }}</span>

											@elseif ($election->status == 'End Election')

												<span class="label label-danger"> {{ $election->status }}</span>

											@endif
										</td>
										<td>
											<a href="/election-course/{{ $election->id }}/show-results" class="btn btn-info btn-xs"> View Results </a>
											{{-- <a href="/election-course/{{ $election->id }}/edit" class="btn btn-primary btn-xs"> Edit </a> --}}
											{{-- @if ($election->status == 'On Going' || $election->status == 'Starting now')
											<a href="javascript:void(0)" data-id="{{ $election->id }}" id="update-election" class="btn btn-green btn-xs">Start Election</a>	
											@endif

											<a href="javascript:void(0)" data-id="{{ $election->id }}" id="stop-election" class="btn btn-danger btn-xs">Stop Election</a>
 --}}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					  @endif	
						@if (auth()->user()->isCouncil())
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>ID</th>
										<th>Election Name</th>
										<th>Department</th>
										<th>End Time</th>
										<th>Status</th>
										<th>Manage</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

									@foreach ($elections as $election)
									@include('events.election-course.modal.start-election')
									@include('events.election-course.modal.stop-election')
										<tr>
											<td>{{ $election->id }}</td>
											<td>{{ $election->election_name }}</td>
											<td>{{ $election->course->code ?? null }}</td>
											<td>
												@if ($election->ended_time)
													{{ \Carbon\Carbon::parse($election->ended_time)->format('h:s A')  }}</td>
												@endif
											</td>
											<td>
												@if ($election->status == 'On Going')

													<span class="label label-warning"> {{ $election->status }}</span>
												@elseif ($election->status == 'Starting now')

													<span class="label label-green"> {{ $election->status }}</span>

												@elseif ($election->status == 'End Election')

													<span class="label label-danger"> {{ $election->status }}</span>

												@endif
											</td>
											<td>
												<a href="/election-course/{{ $election->id }}/show-results" class="btn btn-info btn-xs"> View Results </a>
												
												@if ($election->status == 'On Going' || $election->status == 'Starting now')
												<a href="javascript:void(0)" data-id="{{ $election->id }}" id="update-election" class="btn btn-green btn-xs">Start Election</a>	
												@endif

												<a href="javascript:void(0)" data-id="{{ $election->id }}" id="stop-election" class="btn btn-danger btn-xs">Stop Election</a>

											</td>

											<td>
												<a href="/election-course/{{ $election->id }}/edit" class="btn btn-primary btn-xs"> Edit </a>
												<form method="POST" action="/election-department/{{ $election->id }}/remove"  style="display: inline-block;">
													@csrf
													{{ method_field('DELETE') }}
													<button type="submit" class="btn btn-danger btn-xs">Remove</button>
												</form>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>


 
@endsection

@push('scripts')
<script>
   $('body').on('click', '#update-election', function () {
       var election = $(this).data('id');
       $.get('/ajax-election-course/' + election, function (election) {
           $('#electionModal').html("Modal");
           // $('#btn-update').val("edit-fee");
           $('#electionId').val(election.id);
           $('#ajax-election-modal').modal('show');
       })
   });
    $("#electionForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#btn-update').html('<i class="fa fa fa-spinner fa-spin"></i> Starting..');
        var form = $(this);
        var electionIds = $('#electionId').val();
        $.ajax({
            type: "PUT",
            url: '/ajax-start-election-course/' + electionIds,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                toastr.success("Starting Election!");
                $('#test').trigger("reset");
                $('#ajax-election-modal').modal('hide');
                location.reload();
            }
         });
    });
</script>

<script>
   $('body').on('click', '#stop-election', function () {
       var election = $(this).data('id');
       $.get('/ajax-stop-election-course/' + election, function (election) {
           $('#electionsModal').html("Modal");
           $('#btn-update').val("edit-fee");
           $('#electionId').val(election.id);
           $('#ajax-stop-election-modal').modal('show');
       })
   });
    $("#electionStopForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#btn-update').html('<i class="fa fa fa-spinner fa-spin"></i> End Election..');
        var form = $(this);
        var electionIds = $('#electionId').val();
        $.ajax({
            type: "PUT",
            url: '/ajax-stop-election-course/' + electionIds,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                toastr.error("Stop Election!");
                $('#test').trigger("reset");
                $('#ajax-stop-election-modal').modal('hide');
                location.reload();
            }
         });
    });
</script>
@endpush
