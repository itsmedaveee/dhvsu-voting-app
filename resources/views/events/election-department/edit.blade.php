@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
	
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Elections Department </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Election</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form method="post" action="/election-department/{{ $election->id }}">
							@csrf

							{{ method_field('PATCH') }}
						 
							<div class="form-group{{ $errors->has('election_name') ? ' has-error' : '' }}" >
								<label>Election name</label>
								<input type="text" class="form-control" name="election_name" value="{{ $election->election_name }}">
								@if ($errors->has('election_name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('election_name') }}</strong>
								    </span>
								@endif
							</div>
 

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 




 @endsection