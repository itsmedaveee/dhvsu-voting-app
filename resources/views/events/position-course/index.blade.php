@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Position Course</h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Position</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/position-course">
							@csrf
						 
							<div class="form-group{{ $errors->has('position_name') ? ' has-error' : '' }}">
								<label>Position name</label>
								<input type="text" class="form-control" name="position_name">
								@if ($errors->has('position_name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('position_name') }}</strong>
								    </span>
								@endif
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->

		<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Position</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="parylist-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($positionCourses as $position)
								<tr>
									<td>{{ $position->id }}</td>
									<td>{{ $position->position_name }}</td>
									<td>
										<a href="/position-course/{{ $position->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
										<form method="POST" action="/position-course/{{ $position->id }}" style="display: inline-block;">
											@csrf
											{{ method_field('DELETE') }}
											<button type="submit" class="btn btn-danger btn-xs">Delete</button>
										</form>
									</td>
								</tr>
								@endforeach
						 
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>



@endsection