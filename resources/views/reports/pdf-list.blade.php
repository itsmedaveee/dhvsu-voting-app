<!DOCTYPE html>
<html>
   <head>
      <title>PDF</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style type="text/css">
      </style>
   </head>
   <body> 
      <div class="col-md-12"> 
        <table class="tabletable-sm " width="100%" align="center">
          <tbody>
            <tr> 
            	<td><img src="{{ asset('img/dhvsu.png') }}" style="width: 100px"></td>
              <td>
                  <p class="text-center">
                    <small>
                            <strong>
                              Republic of the Philippines <br> 
                              DON HONORIO VENTURA STATE UNIVERSITY<br>
                              Villa de Bacolor, Pampanga <br>
                            </strong>
                      </small>
                  </p>
                  <p class="text-center">

                  @foreach ($nominees as $nominee)


                  	@if ($loop->last)

                  	@if (request()->has('department'))

                  	@if (optional($nominee->department)->title)
	                  	<b>{{ optional($nominee->department)->title }}</b> <br>

	                 @endif
	                @endif

                  @if (request()->has('course'))
	                 @if (optional($nominee->course)->title)
	                  <b>	{{ optional($nominee->course)->title }} </b><br>
	                 @endif
	              @endif


	              @if (request()->has('election'))
	                 @if (optional($nominee->election)->election_name)

	                 	<b>{{ optional($nominee->election)->election_name }} </b>

	                 @endif	 

		                	      

	              @endif

	                  @if (request()->has('pageant'))

		                    @if (optional($nominee->pageant)->event_name)

		                 	<b>{{ optional($nominee->pageant)->event_name }} </b>
		                 	@endif

		             @endif	

	              @if (request()->has('election_department'))

	              		@if (optional($nominee->electionDepartment)->election_name)

		                 	<b>{{ optional($nominee->electionDepartment)->election_name }} </b>

		                 @endif
	              @endif


	              @if (request()->has('election_course'))

	              		@if (optional($nominee->electionCourse)->election_name)

		                 	<b>{{ optional($nominee->electionCourse)->election_name }} </b>

		                 @endif
	              @endif




	                @endif

                  @endforeach


                  </p>
              </td>
              <td  align="right;">
              	<img src="" style="width: 100px;">
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>
      <br>

       <div class="row">
      <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">Reports </h4>
                <p style="text-align: right;">Date Printed: {{ Carbon\Carbon::now()->toDayDateTimeString() }}</p>
                <br>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr> 
                        <th>Name</th>
                        <th>College</th>
                        <th>Department</th>
                        <th>Program</th>
                        <th>Position</th>
                        <th>Total Votes</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    	@foreach ($nominees as $nominee)
                    	<tr>
							<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
							<td>{{ $nominee->department->title }}</td>
							<td>{{ $nominee->course->title }}</td>
							<td>{{ optional($nominee->program)->program_name ?? null}}</td>
							<td>
								@if ($nominee->position)
								    {{ optional($nominee->position)->position_name  }}
								@elseif ($nominee->positionDepartment)
								    {{ optional($nominee->positionDepartment)->position_name }}
								@elseif ($nominee->positionCourse)
								    {{ optional($nominee->positionCourse)->position_name }}
								@elseif ($nominee->title)
									{{ optional($nominee)->title }}
								@endif

							</td>
							<td>
								@if ($nominee->results->count())
									{{ $nominee->results->count() }}
								 @endif

								 @if ($nominee->resultsDepartment->count())
								 	{{ $nominee->resultsDepartment->count() }}
								 @endif

								 @if ($nominee->resultsCourse->count())
								 	{{ $nominee->resultsCourse->count() }}
								 @endif

								 @if ($nominee->resultsPageant->count())
								 	{{ $nominee->resultsPageant->count() }}
								 @endif
							</td>
							<td></td>
                    	</tr>
                    	@endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>







</body>
</html>