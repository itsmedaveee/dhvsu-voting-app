@extends('layouts.master')
@section('content')
 

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Reports </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Reports</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
						 <form class="form-horizontal" method="GET" action="/reports"> 

					    	<label>Select College</label> 
						       <select class="form-control  @error('department') is-invalid @enderror" name="department" id="department">
						        <option  selected="" disabled="">Select College</option>
					             {{-- 	@foreach ($departments as  $department) 
					             		<option value="{{ $department->title }}" {{ request('department') == $department->title ? 'selected' : '' }}>{{ $department->title }}</option>
					             	@endforeach --}}
								   @foreach ($departments as $department )
				                      <option value="{{ $department }}" {{ request('department') == $department ? 'selected' : '' }}>{{ $department }}</option>
				                    @endforeach
					             </select>

						        @if ($errors->has('department'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('department') }}</strong>
						            </span>
						        @endif
						    </div>
						    <div> 
						  
						    	<label>Select Department</label> 
						       <select class="form-control  @error('course') is-invalid @enderror" name="course" id="course">
						        <option  selected="" disabled="">Select Department</option>
					             	@foreach ($courses as  $course) 
					             		<option value="{{ $course->title }}" {{ request('course') == $course->title ? 'selected' : '' }}>{{ $course->title }}</option>
					             	@endforeach
					             </select>

						        @if ($errors->has('course'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('course') }}</strong>
						            </span>
						        @endif
						    </div>
						    <div> 
						    	<br>




						    	<div class="form-group">
					 
						    	<label>Select Event</label> 
						       <select class="form-control  @error('pageant') is-invalid @enderror" name="pageant" id="pageant">
						        <option  selected="" disabled="">Select Event</option>
					             	@foreach ($pageants as  $pageant) 
					             		<option value="{{ $pageant->event_name }}" {{ request('pageant') == $pageant->event_name ? 'selected' : '' }}>{{ $pageant->event_name }}</option>
					             	@endforeach
					             </select>

						        @if ($errors->has('pageant'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('pageant') }}</strong>
						            </span>
						        @endif
						    </div>
						    </div>


						  	<div class="form-group">
							<label>Select Election</label> 
						       <select class="form-control  @error('election') is-invalid @enderror" name="election" id="election">
						        <option  selected="" disabled="">Select Election Campuses</option>
					             	@foreach ($elections as  $election) 
					             		<option value="{{ $election->election_name }}" {{ request('election') == $election->election_name ? 'selected' : '' }}>{{ $election->election_name }}</option>
					             	@endforeach
					             </select>

						        @if ($errors->has('election'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('election') }}</strong>
						            </span>
						        @endif
						    </div>
						    <div> 	

						    <div class="form-group">
							<label>Select Election College</label> 
						       <select class="form-control  @error('election_department') is-invalid @enderror" name="election_department" id="election">
						        <option  selected="" disabled="">Select Election College</option>
					             	@foreach ($electionsDepartment as  $election) 
					             		<option value="{{ $election->election_name }}" {{ request('election_department') == $election->election_name ? 'selected' : '' }}>{{ $election->election_name }}</option>
					             	@endforeach
					             </select>

						        @if ($errors->has('election_department'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('election_department') }}</strong>
						            </span>
						        @endif
						    </div>    


						    <div class="form-group">
							<label>Select Election Department</label> 
						       <select class="form-control  @error('election_department') is-invalid @enderror" name="election_course" id="election">
						        <option  selected="" disabled="">Select Election Department</option>
					             	@foreach ($electionsCourse as  $election) 
					             		<option value="{{ $election->election_name }}" {{ request('election_course') == $election->election_name ? 'selected' : '' }}>{{ $election->election_name }}</option>
					             	@endforeach
					             </select>

						        @if ($errors->has('election_department'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('election_department') }}</strong>
						            </span>
						        @endif
						    </div>
						<br>

						<div class="form-group">
							<button type="submit" class="btn btn-primary">Search</button>
						</div>
					</form>
					</div>
				</div>
			</div>
			</div>

				<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Reports</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						{{--  <a href="/nominees-pdf" class="m-b-15 btn btn-inverse" target="_blank">Print Reports</a> --}}
						<a href="{{ route('nominees-pdf/report', ['course' => request('course'), 'election'	=> request('election'), 'pageant'	=> request('pageant'), 'department'	=> request('department'), 'election_department'	=> request('election_department'), 'election_course' => request('election_course')]) }}" class="m-b-15 btn btn-inverse" target="_blank">Generate  Reports</a>
					 
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>FullName</th>
									<th>Colleges</th>
									<th>Department</th>
									<th>Program</th>
									<th>Position</th>
									<th>Votes</th>
								</tr>
							</thead>
							<tbody>
								@if (request()->has('course') || request()->has('department') || request()->has('pageant') || request()->has('pageant') || request()->has('election') || request()->has('election_department') || request()->has('election_course'))
									@foreach ($nominees as $nominee)
									<tr>
										<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
										<td>{{ $nominee->department->title }}</td>
										<td>{{ $nominee->course->title }}</td>
										<td>{{ optional($nominee->program)->program_name ?? null }}</td>
										<td>

											@if ($nominee->position)
											    {{ optional($nominee->position)->position_name  }}
											@elseif ($nominee->positionDepartment)
											    {{ optional($nominee->positionDepartment)->position_name }}
											@elseif ($nominee->positionCourse)
											    {{ optional($nominee->positionCourse)->position_name }}

											@elseif ($nominee->title)
												{{ optional($nominee)->title }}
											@endif
											
										</td>
										<td>

										 @if ($nominee->results->count())
											{{ $nominee->results->count() }}
										 @endif

										 @if ($nominee->resultsDepartment->count())
										 	{{ $nominee->resultsDepartment->count() }}
										 @endif

										 @if ($nominee->resultsCourse->count())
										 	{{ $nominee->resultsCourse->count() }}
										 @endif

										 @if ($nominee->resultsPageant->count())
										 	{{ $nominee->resultsPageant->count() }}
										 @endif
										</td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>

 

@endsection