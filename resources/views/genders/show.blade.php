@extends('layouts.master')
<!-- @inject('stats','App\Stats')  -->
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">{{ $gender->gender_name }} Users</h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Total {{ $gender->gender_name }} Users - ({{ $gender->students_count }})



								</h4>
								<div class="panel-heading-btn">
									
								</div>
							</div>
							<div class="panel-body">
								<table class="table table-bordered" id="myTable">
									<thead>
										<tr>
											<th>Image</th>
											<th>Fullname</th>
											<th>Campus</th>
											<th>Department</th>
											<th>Course</th>
											<th>Contact No</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($gender->students as $student)
										<tr>
											<td> 
											@if ($student->image)
												<img src="{{ Storage::url($student->image) ? Storage::url($student->image) :  asset('img/admin.png') }}" class="rounded h-30px my-n3 mx-n1 center"/ >
											@else 
												<img src="{{ asset('img/admin.png') }}" class="rounded h-30px my-n3 mx-n1 center"/ >
											@endif

											</td>
											<td>{{ $student->firstname }} {{ $student->middlename }} {{ $student->lastname }}</td>
										<td>{{ $student->campus->name }}</td>
										<td>{{ $student->department->title }}</td>
										<td>{{ $student->course->title }}</td>
											<td>{{ $student->contact_no }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>	
					</div>
				</div>

</div>



@endsection


@push ('scripts')
<script type="text/javascript">
	$(document).ready( function () {
	    $('#myTable').DataTable();
	});
</script>

@endpush