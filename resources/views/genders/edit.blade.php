@extends('layouts.master')
@section('content')



<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
				
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Edit Sex </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Sex</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/genders/{{ $gender->id }}">
							@csrf
							{{ method_field('PATCH') }}
						 
							<div class="form-group{{ $errors->has('gender_name') ? ' has-error' : '' }}">
								<label>Sex</label>
								<input type="text" class="form-control" name="gender_name" value="{{ $gender->gender_name }}">
								@if ($errors->has('gender_name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('gender_name') }}</strong>
								    </span>
								@endif
							</div>									
						

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>






@endsection