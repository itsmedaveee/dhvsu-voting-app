@extends('layouts.master')
@section('content')



<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Student Details </h1>
				<!-- end page-header -->
				<!-- begin panel -->

				<div class="row">
				<div class="col-md-4">
					
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">QR Code details</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-md-3">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<td>{!! QrCode::size(250)->generate(route('qr-students', [$student, $election])); !!}
											{{--  <img src="{{ Storage::url($student->generate_qr_code) }}" / >
 --}}
										</td>
									</tr>
								</tbody>

							</table>
							</div>
						</div>
					
					</div>
				</div>

				<!-- end panel -->
			</div>

				<div class="col-md-8">
					
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Student details</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered">
									<tbody>
										<tr>
											<td>Student No : {{ $student->student_no }}</td>
											<td>FullName: {{ $student->firstname }} {{ $student->middlename }} {{ $student->lastname }}</td>
											<td>Address: {{ $student->address }}</td>
										</tr>
										<tr>
											<td>Birthdate: {{ $student->birth_date->toFormattedDateString() }}</td>
											<td>Email: {{ $student->email }}</td>
											<td>Year: {{ $student->year }}</td>
										</tr>
										<tr>
											<td>Section :{{ $student->section }}</td>
											<td>Sex :{{ optional($student->gender)->gender_name }}</td>
											<td>College :{{ optional($student->department)->title }}</td>
										</tr>
										<tr>
											<td>Department : {{ optional($student->course)->title }}</td>
											<td>Contact No : {{ $student->contact_no }}</td>
										</tr>
										<tr> 
											<td>{{ $student->photo }}</td>
											  <td colspan="2"> 
											  	<a href="/student/{{ $student->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i>Download</a>
											  </td> 
										</tr>
									</tbody>
		 
									</tbody>
								 
								</table>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>


@endsection