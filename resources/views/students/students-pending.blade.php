@extends('layouts.master')
@section('content')
 
<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/pending-students">Pending Students</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Pending Students </h1>
				<!-- end page-header -->
				<!-- begin panel -->

				<div class="col-md-12">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Pending Students list</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
{{-- 
						@if (auth()->user()->isCouncil())

							<table class="table table-bordered table-hover" id="admin-table">
								<thead>
									<tr>
										<th>ID</th>
										<th>Student No</th>
										<th>FullName</th>
										<th>Campus</th>
										<th>Department</th>
										<th>Course</th>
										<th>Status</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>

									@foreach ($user->campus->students as $student)
										<tr>
											<td>{{ $student->id }}</td>
											<td>{{ $student->student_no }}</td>
											<td>{{ $student->firstname }} {{ $student->middlename }} {{ $student->lastname }}</td>
											<td>{{ $student->campus->name }}</td>
											<td>{{ $student->department->code }}</td>
											<td>{{ $student->course->code }}</td>
											<td><span class="label label-warning">Pending</span></td>
											<td>
												<a href="/pending-students/{{ $student->id }}/manage" class="btn btn-primary btn-xs">Manage</a>
											</td>
										</tr>
									@endforeach
							 
								</tbody>
								
							</table>
						@endif
 --}}
						@if (auth()->user()->isSuperAdmin())
							<table class="table table-bordered table-hover" id="students-pending-table">
								<thead>
									<tr>
										<th>ID</th>
										<th>Student No</th>
										<th>FullName</th>
										<th>middlename</th>
										<th>lastname</th>
										<th>Campus</th>
										<th>College</th>
										<th>Department</th>
										<th>Status</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>

						{{-- 			@foreach ($students as $student)
										<tr>
											<td>{{ $student->id }}</td>
											<td>{{ $student->student_no }}</td>
											<td>{{ $student->firstname }} {{ $student->middlename }} {{ $student->lastname }}</td>
											<td>{{ $student->campus->name }}</td>
											<td>{{ $student->department->code }}</td>
											<td>{{ $student->course->code }}</td>
											<td><span class="label label-warning">Pending</span></td>
											<td>
												<a href="/pending-students/{{ $student->id }}/manage" class="btn btn-primary btn-xs">Manage</a>
											</td>
										</tr>
									@endforeach --}}
							 
								</tbody>
								
							</table>
							@endif
					</div>
				</div>
				</div>


@endsection


@push('scripts')
 <script type="text/javascript">
    $(document).ready(function () {
	 $('#students-pending-table').DataTable({
	    "ordering":'true',
        "order": [0, 'asc'],
        processing: true,
        serverSide: true,
        ajax: '{!! route('pending-students.index') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'student_no',},
            { data: 'firstname',},
            { data: 'middlename',},
            { data: 'lastname',},
            { data: 'campus.name',},
            { data: 'department.title',},
            { data: 'course.title',},
            { data: 'approved',},           
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ]
	  });
});
</script>
@endpush


