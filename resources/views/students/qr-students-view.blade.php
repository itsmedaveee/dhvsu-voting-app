 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">

 	<title>QR CODE</title>
	<link href="{{ asset('css/app.min.css') }}" rel="stylesheet" />

 </head>
 <body>


	<!-- begin #page-container -->
	<div id="page-container" >
		<!-- begin #header -->
		<div id="header" class="header navbar-inverse"  style="background: linear-gradient(to right, #5c0017  0%,  #feba29 100%)">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="#" class="navbar-brand"><span></span> <img src="{{ asset('img/dhvsu.png') }}"> &nbsp; 
					<b style="color: #feba29;">Don Honorio Ventura  State University </b> &nbsp;  
				</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li class="dropdown navbar-user">
					
					<div class="dropdown-menu dropdown-menu-right">
						
					</div>
				</li>
			</ul>
			<!-- end header-nav -->
		</div>
		<!-- end #header -->

<div id="container" class="container">
	<br>
		<h1 class="page-header">QR Code Info </h1>
				<!-- end page-header -->
				<!-- begin panel -->
			<div class="row">
				<div class="col-md-6">
			<div class="panel panel-default">
					<div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">
						<h4 class="panel-title">Student Info</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<div class="d-flex flex-column align-items-center text-center">
							<img src="{{ Storage::url($student->image) ? Storage::url($student->image) :  asset('img/admin.png') }}" alt="Admin" class="rounded-circle" width="150">
							<div class="mt-3">
							  <h4>{{ $student->firstname }} {{ $student->middlename }} {{ $student->lastname }}</h4>
							  <p class="text-secondary mb-1">Student No. : {{ $student->student_no }}</p>
							  <p class="text-secondary mb-1">ID No. : {{ $student->generate_id }}</p>
						</div>
					</div>
					</div>
				</div>
			</div>
 

					<div class="col-md-6">
			<div class="panel panel-default">
					<div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">
						<h4 class="panel-title">Student Info</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>

					<div class="panel-body">
						<table class="table table-bordered table-hover" style="margin-bottom: 60px ">
							<tbody>
								<tr>
									<td>Email : {{ $student->email }}</td>
								</tr>
								<tr>
									<td> Year :  {{ $student->year }}</td>
								</tr>
								<tr>
									<td> Campus : {{ optional($student->campus)->name }}</td>
								</tr>
								<tr>
									<td> College : {{ $student->department->title }}</td>
								</tr>
								<tr>
									<td> Department : {{ $student->course->title }}</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</div>

	 
 

 </body>
 </html>