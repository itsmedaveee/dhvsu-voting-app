@extends('layouts.master')
@section('content')


<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Manage Student </h1>
				<!-- end page-header -->
				<!-- begin panel -->

				<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<form method="POST" action="/pending-students/{{ $student->id }}/approved" style="display:inline-block;">
							@csrf
							{{ method_field('PATCH') }}
							<button type="submit" class="btn btn-primary btn-sm">Approve</button>
						</form>						
						<form method="POST" action="/pending-students/{{ $student->id }}/remove" style="display:inline-block;">
							@csrf
							{{ method_field('DELETE') }}
							<button type="submit" class="btn btn-danger btn-sm">Remove</button>
						</form>
					</div>
					<div class="row">
						<div class="col-md-4">
						<div class="panel panel-default">
						<div class="panel-heading">
						<h4 class="panel-title">Student ID</h4>
						<div class="panel-heading-btn">
						</div>
						</div>
						<div class="panel-body">
						  <img src="{{ Storage::url($student->photo) }}" alt="" style="height: 100%; width: 100%"> 
						</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Student Pending</h4>
								<div class="panel-heading-btn">
								</div>
							</div>
							<div class="panel-body">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<td>Student No : {{ $student->student_no }}</td>
											<td>FullName: {{ $student->firstname }} {{ $student->middlename }} {{ $student->lastname }}</td>
											<td>Address: {{ $student->address }}</td>
										</tr>
										<tr>
											<td>Birthdate: {{ $student->birth_date->toFormattedDateString() }}</td>
											<td>Email: {{ $student->email }}</td>
											<td>Year: {{ $student->year }}</td>
										</tr>
										<tr>
											<td>Section :{{ $student->section }}</td>
											<td>Sex :{{ optional($student->gender)->gender_name }}</td>
											<td>College :{{ optional($student->department)->title }}</td>
										</tr>
										<tr>
											<td>Department : {{ optional($student->course)->title }}</td>
											<td>Contact No : {{ $student->contact_no }}</td>
										</tr>
										<tr> 
											<td>{{ $student->photo }}</td>
											  <td colspan="2"> 
											  	<a href="/student/{{ $student->id }}/download" class="btn btn-success btn-xs"><i class="fa fa-download"></i>Download</a>
											  </td> 
										</tr>
									</tbody>
		 
									</tbody>
								 
								</table>
							</div>
						</div>
					</div>
				<!-- end panel -->
			</div>
 

@endsection