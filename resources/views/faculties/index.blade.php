@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/faculties">Faculty Users</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Faculty Users </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Faculty User</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/faculties">
							@csrf

						 
							<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
								<label>Firstname</label>
								<input type="text" class="form-control" name="firstname">
								@if ($errors->has('firstname'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
								    </span>
								@endif
							</div>									
							<div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
								<label>Middlename</label>
								<input type="text" class="form-control" name="middlename">
								@if ($errors->has('middlename'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
								    </span>
								@endif
							</div>							
							<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
								<label>Lastname</label>
								<input type="text" class="form-control" name="lastname">
								@if ($errors->has('lastname'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('lastname') }}</strong>
								    </span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
		                      <label for="department">Department:</label>
		                      <select class="form-control" id="department" name="department">
		                       <option value="" disabled="" selected="">Select Department</option>
		                      @foreach ($departments as $departmentId => $department)
		                        <option value="{{ $departmentId }}">{{ $department }}</option>
		                      @endforeach       
		                  
		                      </select>

		                       @if ($errors->has('department'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('department') }}</strong>
		                        </span>
		                    @endif

		                    </div>

		                 <div class="form-group{{ $errors->has('campuses') ? 'has-error' : '' }}">
		                      <label for="campuses">Satellite Campus:</label>
		                      <select class="form-control" id="campuses" name="campuses">
		                       <option value="" disabled="" selected="">Select Campus</option>
		                      @foreach ($campuses as $campusesId => $campus)
		                        <option value="{{ $campusesId }}">{{ $campus }}</option>
		                      @endforeach       
		                      </select>
		                       @if ($errors->has('campuses'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('campuses') }}</strong>
		                        </span>
		                    @endif
		                    </div>

							<div class="form-group{{ $errors->has('username') ? 'has-error' : '' }}">
								<label>Username</label>
								<input type="text" class="form-control" name="username">
								@if ($errors->has('username'))
		                        <span class="help-block">
		                            <strong style="color: red;">{{ $errors->first('username') }}</strong>
		                        </span>
		                    @endif
							</div>
							<div class="form-group{{ $errors->has('email') ? 'has-error' : '' }}">
								<label>Email</label>
								<input type="text" class="form-control" name="email">
								@if ($errors->has('email'))
			                        <span class="help-block">
			                            <strong style="color: red;">{{ $errors->first('email') }}</strong>
			                        </span>
		                   		 @endif
							</div>

							<div class="form-group{{ $errors->has('password') ? 'has-error' : '' }}">
								<label>Password</label>
								<input type="password" class="form-control" name="password">
								@if ($errors->has('password'))
			                        <span class="help-block">
			                            <strong style="color: red;">{{ $errors->first('password') }}</strong>
			                        </span>
		                   		 @endif
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->


				<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Faculty list</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="admin-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>FullName</th>
									<th>Username</th>
									<th>Department</th>
									<th>Campus</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($faculties as $faculty)
								<tr>
									<td>{{ $faculty->id }}</td>
									<td>{{ $faculty->firstname }} {{ $faculty->middlename }} {{ $faculty->lastname }}</td>
									<td>{{ $faculty->user->username }}</td>
									<td>{{ $faculty->department->code }}</td>
									<td>{{ $faculty->campus->name }}</td>
									<td><a href="/faculties/{{ $faculty->id }}/edit" class="btn btn-primary btn-xs">Edit</a></td>
								</tr>
								@endforeach
						 
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>






@endsection