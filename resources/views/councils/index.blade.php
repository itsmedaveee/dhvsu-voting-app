@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/councils">Council Users</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Council Users </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Council User</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/councils">
							@csrf

						 
							<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
								<label>Firstname</label>
								<input type="text" class="form-control" name="firstname">
								@if ($errors->has('firstname'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
								    </span>
								@endif
							</div>									
							<div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
								<label>Middlename</label>
								<input type="text" class="form-control" name="middlename">
								@if ($errors->has('middlename'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
								    </span>
								@endif
							</div>							
							<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
								<label>Lastname</label>
								<input type="text" class="form-control" name="lastname">
								@if ($errors->has('lastname'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('lastname') }}</strong>
								    </span>
								@endif
							</div>
							<div class="form-group">
					        <label for="department">{{ __('Department') }}</label>
					             <select class="form-control" name="department" id="name">
					             <option value=""  selected="" disabled="">Select Department</option>
					             	@foreach ($departments as $department)
					             		<option value="{{ $department->id }}">{{ $department->title }}</option>
					             	@endforeach
					             </select>
					            @error('department')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>

					          <div class="form-group">
					        <label for="course">{{ __('Course') }}</label>
					             <select class="form-control"  name="course" id="course">
					             <option value=""  disabled=""  selected="">Select Course</option>
					             	{{-- @foreach ($courses as $course)
					             		<option value="{{ $course->id }}">{{ $course->title }}</option>
					             	@endforeach --}}
					             </select>
					            @error('course')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>

		                 <div class="form-group{{ $errors->has('campuses') ? 'has-error' : '' }}">
		                      <label for="campuses"> Campus:</label>
		                      <select class="form-control" id="campuses" name="campuses">
		                       <option value="" disabled="" selected="">Select Campus</option>
		                      @foreach ($campuses as $campusesId => $campus)
		                        <option value="{{ $campusesId }}">{{ $campus }}</option>
		                      @endforeach       
		                      </select>
		                       @if ($errors->has('campuses'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('campuses') }}</strong>
		                        </span>
		                    @endif
		                    </div>

							<div class="form-group{{ $errors->has('username') ? 'has-error' : '' }}">
								<label>Username</label>
								<input type="text" class="form-control" name="username">
								@if ($errors->has('username'))
		                        <span class="help-block">
		                            <strong style="color: red;">{{ $errors->first('username') }}</strong>
		                        </span>
		                    @endif
							</div>
							<div class="form-group{{ $errors->has('email') ? 'has-error' : '' }}">
								<label>Email</label>
								<input type="text" class="form-control" name="email">
								@if ($errors->has('email'))
			                        <span class="help-block">
			                            <strong style="color: red;">{{ $errors->first('email') }}</strong>
			                        </span>
		                   		 @endif
							</div>

							<div class="form-group{{ $errors->has('password') ? 'has-error' : '' }}">
								<label>Password</label>
								<input type="password" class="form-control" name="password">
								@if ($errors->has('password'))
			                        <span class="help-block">
			                            <strong style="color: red;">{{ $errors->first('password') }}</strong>
			                        </span>
		                   		 @endif
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->


				<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Council list</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="admin-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>FullName</th>
									<th>Username</th>
									<th>Department</th>
									<th>Campus</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($councils as $council)
								<tr>
									<td>{{ $council->id }}</td>
									<td>{{ $council->firstname }} {{ $council->middlename }} {{ $council->lastname }}</td>
									<td>{{ $council->user->username }}</td>
									<td>{{ $council->user->department->code }}</td>
									<td>{{ $council->campus->name }}</td>
									<td><a href="/councils/{{ $council->id }}/edit" class="btn btn-primary btn-xs">Edit</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
 
@endsection


@push ('scripts')

@push('scripts')
    <script type="text/javascript">
        $("select[name='department']").change(function () {
            let department = $(this).val();

            $.ajax({
                url: "{!! url('/select-course') !!}",
                method: 'GET',
                data: { department: department },
                success: function(data) {
                    $("select[name='course']").html('');
                    console.log(data)
                    $("select[name='course']").html(data.options);
                }
            }); 
        });
    </script>
 
@endpush


@endpush