@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/councils">Council User</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Edit Council User </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Council User</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/councils/{{ $council->id }}">
							@csrf
							{{ method_field('PATCH') }}

						 
							<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
								<label>Firstname</label>
								<input type="text" class="form-control" name="firstname" value="{{ $council->firstname }}">
								@if ($errors->has('firstname'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
								    </span>
								@endif
							</div>									
							<div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
								<label>Middlename</label>
								<input type="text" class="form-control" name="middlename" value="{{ $council->middlename }}">
								@if ($errors->has('middlename'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
								    </span>
								@endif
							</div>							
							<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
								<label>Lastname</label>
								<input type="text" class="form-control" name="lastname" value="{{ $council->lastname }}">
							</div>
							<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}" >
		                      <label for="department">Department:</label>
		                      <select class="form-control" id="department" name="department">
		                       <option value="" disabled="" selected="">Select Department</option>
		                      @foreach ($departments as $departmentId => $department)
		                        <option value="{{ $departmentId }}" {{ $council->user->department_id == $departmentId ? 'selected' : '' }}>{{ $department }}</option>
		                      @endforeach       
		                  
		                      </select>

		                       @if ($errors->has('department'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('department') }}</strong>
		                        </span>
		                    @endif

		                    </div>

		                 <div class="form-group{{ $errors->has('campuses') ? 'has-error' : '' }}">
		                      <label for="campuses"> Campus:</label>
		                      <select class="form-control" id="campuses" name="campuses">
		                       <option value="" disabled="" selected="">Select Campus</option>
		                      @foreach ($campuses as $campusesId => $campus)
		                        <option value="{{ $campusesId }}" {{ $council->user->campus_id == $campusesId ? 'selected' : '' }}>{{ $campus }}</option>
		                      @endforeach       
		                      </select>
		                       @if ($errors->has('campuses'))
		                        <span class="help-block">
		                            <strong style="color:red;">{{ $errors->first('campuses') }}</strong>
		                        </span>
		                    @endif
		                    </div>
							<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
								<label>Username</label>
								<input type="text" class="form-control" name="username" value="{{ $council->user->username }}">
								@if ($errors->has('username'))
			                        <span class="help-block">
			                            <strong style="color:red;">{{ $errors->first('username') }}</strong>
			                        </span>
			                    @endif
							</div>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label>Email</label>
								<input type="text" class="form-control" name="email" value="{{ $council->user->email }}">
								@if ($errors->has('email'))
			                        <span class="help-block">
			                            <strong style="color:red;">{{ $errors->first('email') }}</strong>
			                        </span>
			                    @endif
							</div>

							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label>Password</label>
								<input type="password" class="form-control" name="password">
								@if ($errors->has('password'))
			                        <span class="help-block">
			                            <strong style="color:red;">{{ $errors->first('password') }}</strong>
			                        </span>
			                    @endif
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
 
			<!-- end #content -->

 


@endsection