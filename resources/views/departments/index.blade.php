@extends('layouts.master')
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/departments">Colleges</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Colleges </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add College</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/departments">
							@csrf
						 
							<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
								<label>Code</label>
								<input type="text" class="form-control" name="code">
								@if ($errors->has('code'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('code') }}</strong>
								    </span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
								<label>Title</label>
								<input type="text" class="form-control" name="title">
								@if ($errors->has('title'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('title') }}</strong>
								    </span>
								@endif
							</div>
					
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div> 
 
			<!-- end #content -->


				<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Colleges list</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="departments-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Code</th>
									<th>Title</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								{{-- @foreach ($departments as $department)
								<tr>
									<td>{{ $department->id }}</td>
									<td>{{ $department->code }}</td>
									<td>{{ $department->title }}</td>
									<td>
										<a href="/departments/{{ $department->id }}/edit" class="btn btn-primary btn-xs">Edit</a>	
										<a href="/departments/{{ $department->id }}/show" class="btn btn-warning btn-xs">Show</a>

										<form method="POST" action="/departments/{{ $department->id }}" style="display:inline-block;">
											@csrf
											{{ method_field('DELETE') }}
											<button type="submit" class="btn btn-danger btn-xs">Delete</button>
										</form>

									</td>
								</tr>
								@endforeach --}}
						 
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@push('scripts')
 <script type="text/javascript">
    $(document).ready(function () {
	 $('#departments-table').DataTable({
	    "ordering":'true',
        "order": [0, 'asc'],
        processing: true,
        serverSide: true,
        ajax: '{!! route('departments.index') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'code',},
            { data: 'title',},
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ]
	  });
});
</script>
@endpush