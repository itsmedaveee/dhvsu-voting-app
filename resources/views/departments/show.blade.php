@extends('layouts.master')
@section('content')




<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Department </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Department lists</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Code</th>
									<th>Title</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($department->courses as $course)
	                            <tr>
	                                <td>{{ $course->id }}</td>
	                                <td>{{ $course->code }}</td>
	                                <td>{{ $course->title }}</td>
	                               
	                            </tr>
                            @endforeach
							</tbody>
							
						</table>
						
					</div>
				</div>
			</div>
		</div>
</div>








@endsection