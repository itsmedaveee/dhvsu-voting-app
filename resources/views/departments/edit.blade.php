@extends('layouts.master')
@section('content')

	

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/departments">College</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">College </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit College</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/departments/{{ $department->id }}">
							@csrf
							{{ method_field('PATCH') }}
						 
							<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
								<label>Code</label>
								<input type="text" class="form-control" name="code" value="{{ $department->code }}">
								@if ($errors->has('code'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('code') }}</strong>
								    </span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
								<label>Title</label>
								<input type="text" class="form-control" name="title" value="{{ $department->title }}">
								@if ($errors->has('title'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('title') }}</strong>
								    </span>
								@endif
							</div>
					
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div> 
 

	






@endsection