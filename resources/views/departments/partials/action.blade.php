<td>
	<a href="/departments/{{ $department->id }}/edit" class="btn btn-primary btn-xs">Edit</a>	
	<a href="/departments/{{ $department->id }}/show" class="btn btn-warning btn-xs">Show</a>
	<form method="POST" action="/departments/{{ $department->id }}" style="display:inline-block;">
		@csrf
		{{ method_field('DELETE') }}
		<button type="submit" class="btn btn-danger btn-xs">Delete</button>
	</form>

</td>