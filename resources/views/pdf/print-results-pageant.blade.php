<!DOCTYPE html>
<html>
   <head>
      <title>PDF</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style type="text/css">
      </style>
   </head>
   <body> 
      <div class="col-md-12"> 
        <table class="tabletable-sm " width="100%" align="center">
          <tbody>
            <tr> 
            	<td><img src="{{ asset('img/dhvsu.png') }}" style="width: 100px"></td>
              <td>
                  <p class="text-center">
                    <small>
                            <strong>
                              Republic of the Philippines <br> 
                              DON HONORIO VENTURA STATE UNIVERSITY<br>
                              Villa De Bacolor, Pampanga <br>
                            </strong>
                      </small>
                  </p>
                  <p class="text-center">

                  </p>
              </td>
              <td  align="right;">
              	<img src="" style="width: 100px;">
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>

      <hr>

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>FullName</th>
            <th>Title</th>
            <th>Campus</th>
            <th>Votes</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($pageant->nominees as $nominee)
          <tr>
            <td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
            <td>{{ $nominee->title }}</td>
            <td>{{ $nominee->campus->name }}</td>
            <td>{{ $nominee->resultsPageant->count() }}</td>
            <td></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </body>
</html>


