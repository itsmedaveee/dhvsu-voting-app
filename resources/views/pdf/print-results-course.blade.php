<!DOCTYPE html>
<html>
   <head>
      <title>PDF</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style type="text/css">
      </style>
   </head>
   <body> 
      <div class="col-md-12"> 
        <table class="tabletable-sm " width="100%" align="center">
          <tbody>
            <tr> 
            	<td><img src="{{ asset('img/dhvsu.png') }}" style="width: 100px"></td>
              <td>
                  <p class="text-center">
                    <small>
                            <strong>
                              Republic of the Philippines <br> 
                              DON HONORIO VENTURA STATE UNIVERSITY<br>
                              Villa De Bacolor, Pampanga <br>
                            </strong>
                      </small>
                  </p>
                  <p class="text-center">
					<small>STUDENT COMMISSION ON ELECTION </small><br>
					<small>CERTIFICATE OF CANDIDACY FORM</small>  <br>
					<small>COLLEGE/CAMPUS STUDENT COUNCIL ELECTIONS 2021</small> <br>

                  </p>
              </td>
              <td  align="right;">
              	<img src="{{ asset('img/test.png') }}" style="width: 100px;">
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>

      <hr>

{{--       <table class="table table-bordered">
        <thead>
          <tr>
            <th>FullName</th>
            <th>Position</th>
            <th>Campus</th>
            <th>Votes</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($election->nominees as $nominee)
          <tr>
            <td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
            <td>{{ $nominee->positionCourse->position_name }}</td>
            <td>{{ $nominee->campus->name }}</td>
             <td>{{ $nominee->resultsCourse->count() }}</td>
          </tr>
          @endforeach
        </tbody>
      </table> --}}

      <div class="row">
      <div class="col-md-12">
        @if (count($grouped)) 
          @foreach($grouped as $position => $nominees)
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">{{ $position }}</h4>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr> 
                        <th>Name</th>
                        <th>PartyName</th>
                        <th>Campus</th>
                        <th>Colleges</th>
                        <th>Votes</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($nominees as $nominee)
                        <tr>
                          <td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
                          <td>{{ $nominee->partyname }}</td>
                          <td>{{ $nominee->campus->name }}</td>
                          <td>{{ $nominee->department->title }}</td>
                          <td>{{ $nominee->resultsCourse->count() }}</td>
                          <td></td>

                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          @endforeach

        @endif
      </div>
    </div> 
    </body>


    </body>
</html>


