<!DOCTYPE html>
<html>
   <head>
      <title>PDF</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style type="text/css">
      </style>
   </head>
   <body> 
      <div class="col-md-12"> 
        <table class="tabletable-sm " width="100%" align="center">
          <tbody>
            <tr> 
            	<td><img src="{{ asset('img/dhvsu.png') }}" style="width: 100px"></td>
              <td>
                  <p class="text-center">
                    <small>
                            <strong>
                              Republic of the Philippines <br> 
                              DON HONORIO VENTURA STATE UNIVERSITY<br>
                              Villa De Bacolor, Pampanga <br>
                            </strong>
                      </small>
                  </p>
                  <p class="text-center">
					<small>STUDENT COMMISSION ON ELECTION </small><br>
					<small>CERTIFICATE OF CANDIDACY FORM</small>  <br>
					<small>COLLEGE/CAMPUS STUDENT COUNCIL ELECTIONS 2021</small> <br>

                  </p>
              </td>
              <td  align="right;">
              	<img src="{{ asset('img/test.png') }}" style="width: 100px;">
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>

      <hr>


       <table class="table table-bordered">
         <thead>
           <tr>
             <th><small>PERSONAL INFORMATION:</small></th>
             <th colspan="4"><small>Date Filled Out: {{ $nominee->date_filled_out }}</small></th>
           </tr>
         </thead>
         <tbody>
          <tr>
            <td colspan="5"><small>FullName : {{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</small></td>
           
          </tr>
         
          {{--   <td></td>
             <td><small>Lastname</small></td>
             <td><small>Firstname</small></td>
             <td><small>Middlename</small></td>
             <td></td>
           </tr> --}}
           <tr>
             <td><small>Course: {{ $nominee->course->code }}</small></td>
             <td colspan="4"><small>Year and Section:</small></td>
           </tr>
           <tr>
             <td><small>Nationality: {{ $nominee->nationality }}</small></td>
             <td><small>Gender: {{ $nominee->gender }}</small></td>
             <td colspan="3"><small>Religion: {{ $nominee->religion }}</small></td>
           </tr>
           <tr>
             <td><small>Place of Birth: {{ $nominee->place_of_birth }}</small></td>
             <td colspan="4"><small>Date of Birth: {{ $nominee->birthdate }}</small></td>
           </tr>
           <tr>
             <td><small>Age:</td>
             <td colspan="4"><small>Contact No: {{ $nominee->contact_no }}</small></td>
           </tr>
           <tr>
             <td colspan="5"><small>Complete Address: {{ $nominee->address }}</small></td>
           </tr>
           <tr>
             <td colspan="5"><small>Position Aspired for: </small> <small><input type="checkbox" style="margin-top: 5;"> 
                @if ($nominee->position)
                      {{ $nominee->position->position_name  }}
                @elseif ($nominee->positionDepartment)
                      {{ $nominee->positionDepartment->position_name }}
                @elseif ($nominee->positionCourse)
                      {{ $nominee->positionCourse->position_name }}
                @endif
            </small></td>
           </tr>
           
                  
             
             <tr>
               <td colspan="5"><small>Reason for Candidacy:</small></td>
             </tr>
             <tr>
               <td colspan="5"><small>ACADEMIC RECORD</small> <br>
                              <small><i>(Separate multiple entries with commas)</i></small>
               </td>
             </tr>
             <tr>
               <td colspan="2"><small>General Average last semester</small></td>
               <td colspan="3"><small>Any previous failed subjects (specify): </small></td>
             </tr>
             <tr>
               <td colspan="5"><small>Awards/Honors received (including relevant dates):</small></td>
             </tr>
             <tr>
               <td colspan="5"><small>Other Extra- Curricular Activities (School and Community):</small></td>
             </tr>
             <tr>
               <td colspan="5"><small>Training/Seminars Attended (including relevant dates):</small></td>
             </tr>
             <tr>
               <td colspan="5"><i><small>I hereby certify that all information state herein is true and accurate to the best of my knowledge.</small></i></td>
             </tr>
             <tr>
               <td colspan="5"> </td>
             </tr>
             <tr>
               <td colspan="5" align="right"><small>Signature over Printed Name </small></td>
           </tr>
           <tr>
             <td colspan="5"><small><i>(This portion will be filled up by the S-COMELEC REPRESENTATIVE)</small></i></td>
           </tr>
           <tr>
             <td colspan="5"><small>REQUIRED DOCUMENTS: </small> <br>

                    <small><input type="checkbox" style="margin-top: 5;"> PHOTOCOPY OF CERTIFICATE OF REGISTRATION </small> <br>
                    <small><input type="checkbox" style="margin-top: 5;"> PHOTOCOPY OF VALIDATED SCHOOL I.D </small> <br>
                    <small><input type="checkbox" style="margin-top: 5;"> PHOTOCOPY OF CERTIFICATE OF ATTENDANCE/
      PARTICIPATION OF LEADERSHIP SEMINAR/ TRAINING ATTENDED </small> <br>           
       <small>
        <input type="checkbox" style="margin-top: 5;"> RECENT 2X2 PHOTO (COLORED)</small> <br>   

        <small> <input type="checkbox" style="margin-top: 5;"> COPY OF GRADES (COG)</small> <br>
         <small> <input type="checkbox" style="margin-top: 5;"> RECOMMENDATION LETTER FROM THE DEAN ON HIS/HER COLLEGE</small> <br>
         <small> <input type="checkbox" style="margin-top: 5;"> CERTIFICATE OF GOOD MORAL</small> <br>


             </td> 
           </tr>

           <tr>
             <td colspan="5"><small><i> Regular and Irregular student/s must have at least 15 units load.</i></small></td>
           </tr>
           <tr>
             <td colspan="5"></td>
           </tr>

           <tr>
             <td ><small>NOTED BY:</small></td>
             <td colspan="4" align="right"><small>CHECKED BY:</small></td>
           </tr>
           <tr>

            <td>
              <u>__________________</u><br>
              <small>Adviser, S-COMELEC 2021</small>
            </td>

            <td colspan="4" align="right">
              <u>________________________</u><br>
              <small>Chairperson, S-COMELEC 2021</small>
            </td>
           </tr>

           <tr>
             <td colspan="5"></td>
           </tr>
           <tr>
             <td>
              <u>___________________</u><br>
              <small>Co-Adviser, S-COMELEC 2021</small>

            </td>             

            <td colspan="4" align="right">
              <u>_______________________</u><br>
              <small>Co-Chairperson, S-COMELEC 2021</small>

            </td>
           </tr>

           <tr>
             <td colspan="5" align="right">

                <u>_______________________</u><br>
              <small>Secretary, S-COMELEC 2021</small></td>
           </tr>
         </tbody>
       </table>
 
      </body>
      </html>