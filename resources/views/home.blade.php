@extends('layouts.master')
@inject('stats','App\Stats') 
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content">
   <!-- begin breadcrumb -->
   {{-- <ol class="breadcrumb float-xl-right">
      <li class="breadcrumb-item"><a href="/home">Home</a></li>
      <li class="breadcrumb-item"><a href="/administrators">Administrators</a></li>
   </ol> --}}
   <!-- end breadcrumb -->
   <!-- begin page-header -->
   <h1 class="page-header">Dashboard </h1>
   <div class="row">
<div class="col-xl-6">
   <div class="card border-0 mb-3 overflow-hidden bg-inverse text-white"  style="background: linear-gradient(to right, #5c0017  0%,  #feba29 100%)">
      <div class="card-body">
         <div class="row">
            <div class="col-xl-7 col-lg-8">
               <div class="mb-3 text-grey">
                  <b>TOTAL USERS</b>
                  <span class="ml-2">
                  <i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover" data-title="Total sales" data-placement="top" data-content="Net sales (gross sales minus discounts and returns) plus taxes and shipping. Includes orders from all sales channels."></i>
                  </span>
               </div>
               <div class="d-flex mb-1">
                  <h2 class="mb-0"><span data-animation="number" data-value="{{ $stats->totalUsers() }}">0.00</span></h2>
                  <div class="ml-auto mt-n1 mb-n1">
                     <div id="total-sales-sparkline"></div>
                  </div>
               </div>
               <div class="mb-3 text-grey">
                  {{-- <i class="fa fa-caret-up"></i> <span data-animation="number" data-value="33.21">0.00</span>% compare to last week --}}
                  <div>&nbsp;</div>
               </div>
               <hr class="bg-white-transparent-2" />
               <div class="row text-truncate">
                  <div class="col-6">
                     <div class="f-s-12 text-grey">TOTAL NOMINEES</div>
                     <div class="f-s-18 m-b-5 f-w-600 p-b-1" data-animation="number" data-value="{{ $stats->totalNominees() }}">0</div>
                     <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                        <div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width" data-value="55%" style="width: 0%"></div>
                     </div>
                  </div>
                  <div class="col-6">
                     <div class="f-s-12 text-grey">TOTAL STUDENTS</div>
                     <div class="f-s-18 m-b-5 f-w-600 p-b-1"><span data-animation="number" data-value="{{ $stats->totalStudents() }}">0.00</span></div>
                     <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                        <div class="progress-bar progress-bar-striped rounded-right" data-animation="width" data-value="55%" style="width: 0%"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-5 col-lg-4 align-items-center d-flex justify-content-center">
               <img src="{{ asset('img/dhvsu.png') }}" height="150px" class="d-none d-lg-block" />
            </div>
         </div>
      </div>

   </div>
</div>

<div class="col-sm-6">

<div class="card border-0 text-truncate mb-3 bg-dark text-white"  style="background: linear-gradient(to right, #5c0017  0%,  #feba29 100%)">

<div class="card-body">

<div class="mb-3 text-grey">
<b class="mb-3">TOTAL COURSES</b>
<span class="ml-2"><i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover" data-title="Conversion Rate" data-placement="top" data-content="Percentage of sessions that resulted in orders from total number of sessions." data-original-title="" title=""></i></span>
</div>
<div class="d-flex align-items-center mb-1">
<h2 class="text-white mb-0"><span data-animation="number" data-value="{{ $stats->totalCourses() }}">0.00</span></h2>
<div class="ml-auto">
<div id="conversion-rate-sparkline"></div>
</div>
</div>
<div class="mb-4 text-grey">
</div>

<div class="d-flex mb-2">
<div class="d-flex align-items-center">
<i class="fa fa-circle text-red f-s-8 mr-2"></i>
TOTAL DEPARTMENTS
</div>
<div class="d-flex align-items-center ml-auto">
<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="{{ $stats->totalDepartments() }}">0.00</span></div>
</div>
</div>


<div class="d-flex mb-2">
<div class="d-flex align-items-center">
<i class="fa fa-circle text-warning f-s-8 mr-2"></i>

</div>
<div class="d-flex align-items-center ml-auto">

<div class="width-50 text-right pl-2 f-w-600"></div>
</div>
</div>


<div class="d-flex">
<div class="d-flex align-items-center">
<i class="fa fa-circle text-lime f-s-8 mr-2"></i>
TOTAL CAMPUSES
</div>
<div class="d-flex align-items-center ml-auto">
<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="{{ $stats->totalCampuses() }}">0.00</span></div>
</div>
</div>
<div class="d-flex">
<div class="d-flex align-items-center">
<i class="fa fa-circle text-lime f-s-8 mr-2"></i>
TOTAL ELECTIONS
</div>
<div class="d-flex align-items-center ml-auto">
<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="{{ $stats->totalElections() }}">0.00</span></div>
</div>
</div>

</div>


</div>
 
</div>



		<div class="col-md-6">
		  <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Campuses)</div>
                <div class="panel-body">
					<div id="app">
						<canvas id="myChart" width="400" height="200"></canvas>
					</div>
				</div>
			</div>
		</div>
   </div>
   </div>

       <div class="col-md-6">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Department)</div>
                <div class="panel-body">
               <div id="app">
                  <canvas id="myCharts" width="400" height="200"></canvas>
               </div>
            </div>
         </div>
   </div>


        <div class="col-md-6">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Courses)</div>
                <div class="panel-body">
               <div id="app">
                  <canvas id="myChartCourse" width="400" height="200"></canvas>
               </div>
            </div>
         </div>
        </div>


       <div class="col-md-6">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background: linear-gradient(to right,#feba29   0%, #5c0017  100%)">Data Analytics (Events)</div>
                <div class="panel-body">
               <div id="app">
                  <canvas id="myChartes" width="400" height="200"></canvas>
               </div>
            </div>
         </div>
   </div>


@endsection
 

 @push ('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

<script>

var results = {!! $result !!};
var depts = {!! $dept !!};
console.log(depts);
const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: depts,
        datasets: [{
            label: '# of Votes',
            data: results,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>
 
<script>

var resultsWithCourses = {!! $resultDepartment !!};
var courses = {!! $deptCourse !!};
console.log(resultsWithCourses);
const charting = document.getElementById('myCharts').getContext('2d');
const myCharts = new Chart(charting, {
    type: 'bar',
    data: {
        labels: courses,
        datasets: [{
            label: '# of Votes',
            data: resultsWithCourses,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>






<script>
var resultEvent = {!! $resultEvent !!};
var eventsPageant = {!! $eventsPageant !!};
console.log(resultEvent);
const test = document.getElementById('myChartes').getContext('2d');
const myChartes = new Chart(test, {
    type: 'bar',
    data: {
        labels: eventsPageant,
        datasets: [{
            label: '# of Votes',
            data: resultEvent,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>

  {{--   <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-datalabels/2.0.0/chartjs-plugin-datalabels.min.js" integrity="sha512-R/QOHLpV1Ggq22vfDAWYOaMd5RopHrJNMxi8/lJu8Oihwi4Ho4BRFeiMiCefn9rasajKjnx9/fTQ/xkWnkDACg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
    // setup 

var resultEvent = {!! $resultEvent !!};
var eventsPageant = {!! $eventsPageant !!};
    const data = {
      labels: eventsPageant,
      datasets: [{
        label: 'Events',
        data: resultEvent,
        backgroundColor: [
             'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
        ],
        borderColor: [
          'rgba(255, 26, 104, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(0, 0, 0, 1)'
        ],
        borderWidth: 1
      }]
    };
 

    const config = {
      type: 'bar',
      data, 
      options: {
         scales: {
         },
         plugins: {
            tooltip: {
               enabled: false
         },
         datalabels: {
            formatter: (value, context) => {
               const datapoints = context.chart.data.datasets[0].data;
               function totalSum(total, datapoint) {
                  return total + datapoint;
               }
               const totalPercentage = datapoints.reduce(totalSum, 0);
               const percentageValue = (value / totalPercentage * 100).toFixed(1);
               return `${percentageValue}%`;
            }
         }
      }
   },
         plugins: [ChartDataLabels]
    };
    // render init block
    const myChartes = new Chart(
      document.getElementById('myChartes'),
      config
    );
    </script> --}}




<!-- 
Course Election
 -->
<script>

var resultCourse = {!! $resultCourse !!};
var electionCourseName = {!! $electionCourseName !!};
console.log(resultsWithCourses);
const chartCourse = document.getElementById('myChartCourse').getContext('2d');
const myChartCourse = new Chart(chartCourse, {
    type: 'bar',
    data: {
        labels: electionCourseName,
        datasets: [{
            label: '# of Votes',
            data: resultCourse,
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 205, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                '#ff5b57',
                '#90ca4b',
                '#16A085',
                '#F1C40F',
                '#273746',
                'rgb(201, 203, 207)',
                '#E74C3C',
                '#F9E79F',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>


 

 

 @endpush