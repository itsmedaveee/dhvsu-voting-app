@extends('layouts.master')
@inject('election','App\Student') 
@section('content')

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Poll</a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Results </h1>
			<div class="form-group">
				<a href="/print-results/{{ $electionId }}" class="btn btn-primary"  target="_blank">Print Results</a>
			</div>
				<!-- end page-header -->
				<!-- begin panel -->
		{{-- 		<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Results</h4>
						<div class="panel-heading-btn">
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<a href="/print-results/{{ $election->id }}" class="btn btn-primary"  target="_blank">Print Results</a>
						</div>
						<table class="table table-bordered table-condensed">
							<thead>
								<tr>
									<th>FullName</th>
									<th>Position</th>
									<th></th>
								</tr>
							</thead>
						 	<tbody>
						 		@foreach ($election->nominees as $nominee)
						 		<tr>
						 			<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
						 			<td>{{ $nominee->position->position_name }}</td>
						 			<td>
						 				<div class="progress h-10px"> 
											<div class="progress-bar progress-bar-striped progress-bar-animated bg-success fs-10px fw-bold" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"  style="width:{{ count($nominee->results) / $totalVoteUsers * 100 . '%'  }}   "> 
												  {{ count($nominee->results) / $totalVoteUsers * 100 . '%'  }}  
											</div>
										</div>
						 			</td>
						 		</tr>
						 		@endforeach
						 	</tbody>
							
						</table> 


					</div>
				</div> --}}

	 
		<div class="row">
			<div class="col-md-12">
				@if (count($grouped)) 
				 	@foreach($grouped as $position => $nominees)
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">{{ $position }}</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-bordered table-hover">
										<thead>
											<tr> 
												<th>Image</th>
												<th>Name</th>
												<th>PartyName</th>
												<th>Department</th>
												<th>Campus</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($nominees as $nominee)
												<tr class="odd gradeX">
													 <td width="1%">
													{{-- <img src="{{ asset('img/admin.png') }}" class="rounded h-30px my-n1 mx-n1" /> --}}
													 	 <img src="{{ Storage::url($nominee->avatar) ? Storage::url($nominee->avatar) :  asset('img/admin.png') }}" class="rounded h-30px my-n3 mx-n1 center"/ >


													 </td>
													<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
													<td>{{ $nominee->partyname }}</td>
													<td>{{ $nominee->department->title }}</td>
													<td>{{ $nominee->campus->name }}</td>
													<td>

												@if ($election->electionEndStatus())
												  <h4>{{ count($nominee->results) / $totalVoteUsers * 100 . '%'  }}  </h4>
											<div class="progress h-10px"> 
											<div class="progress-bar fs-10px fw-bold" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"  style="width:{{ count($nominee->results) / $totalVoteUsers * 100 . '%'  }}   "> 
												
											</div>

											@endif
										</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					@endforeach

				@endif
			</div>
		</div>
 
 

</div>

 

@endsection