{{-- <div class="modal fade" id="ajax-stop-election-modal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="electionsModal"></h4>
         </div>

         <div class="modal-body">
            <form id="electionStopForm" class="form-horizontal">
               <input type="hidden" name="electionId" id="electionId">
                <h4 align="center">Do you want to Stop Election?</h4>
               <div class="form-group">
                  <button type="submit" class="btn btn-block btn-primary" id="btn-update">Stop Election</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div> --}}

<div class="modal fade" id="ajax-stop-election-modal" aria-hidden="true">
                 <div class="modal-dialog modal-dialog-centered" role="document">
                   <div class="modal-content">
                     <div class=""></div>
                     <div class="modal-body">{{-- 
                        <p class="text-center"><img src="{{ asset('img/mark.png') }}" style="width: 20%;" ></p> --}}
                        <div class="swal-icon swal-icon--warning">
                        <span class="swal-icon--warning__body">
                        <span class="swal-icon--warning__dot"></span>
                        </span>
                        </div>

                     <form id="electionStopForm" class="form-horizontal">
                     <input type="hidden" name="electionId" id="electionId">

                       <h5 style="font-size: 1.875rem;font-weight:600; " class="text-center">DO YOU WANT TO STOP THE ELECTION?</h5>

                       <div class="form-group" align="center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel </button>
                       <button type="submit" class="btn btn-primary">Stop Election</button>
                        </form>
                    </div>
                     </div>
                   </div>
                 </div>
               </div>