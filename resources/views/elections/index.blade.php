@extends('layouts.master')
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/elections">Elections</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Elections </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Election</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/elections">
							@csrf

						 
							<div class="form-group{{ $errors->has('election_name') ? ' has-error' : '' }}">
								<label>Election Name</label>
								<input type="text" class="form-control" name="election_name">
								@if ($errors->has('election_name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('election_name') }}</strong>
								    </span>
								@endif
							</div>					 
						 

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>




		<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Election lists</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered">
							<thead>
								<tr>
									<th>ID</th>
									<th>Election Name</th>
									<th>End Time</th>
									<th>Status</th>
									<th>Action</th>
									<th></th>
								</tr>
							</thead>
							<tbody>

								@foreach ($elections as $election)
								@include('elections.modal.start-election')
								@include('elections.modal.stop-election')
									<tr>
										<td>{{ $election->id }}</td>
										<td>{{ $election->election_name }}</td>
										<td>
										@if ($election->ended_time)
											{{ \Carbon\Carbon::parse($election->ended_time)->format('h:i A') }}</td>
										@endif
										</td>
										<td>
											@if ($election->status == 'On Going')

												<span class="label label-warning"> {{ $election->status }}</span>
											@elseif ($election->status == 'Starting now')

												<span class="label label-green"> {{ $election->status }}</span>

											@elseif ($election->status == 'End Election')

												<span class="label label-danger"> {{ $election->status }}</span>

											@endif
										</td>
										<td>
											<a href="/elections/{{ $election->id }}/show-results" class="btn btn-info btn-xs"> View Results </a>
											<a href="/elections/{{ $election->id }}/edit" class="btn btn-primary btn-xs"> Edit </a>
											@if ($election->status == 'On Going' || $election->status == 'Starting now')
											<a href="javascript:void(0)" data-id="{{ $election->id }}" id="update-election" class="btn btn-green btn-xs">Start Election</a>	
											@endif

											<a href="javascript:void(0)" data-id="{{ $election->id }}" id="stop-election" class="btn btn-danger btn-xs">Stop Election</a>
										</td>

										<td>
											<form method="POST" action="/elections/{{ $election->id }}/remove" style="display: inline-block;">
												@csrf 
												{{ method_field('DELETE') }}
												<button type="submit" class="btn btn-danger btn-xs">Remove</button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>




 

@endsection

@push('scripts')
<script>
   $('body').on('click', '#update-election', function () {
       var election = $(this).data('id');
       $.get('/ajax-election/' + election, function (election) {
           $('#electionModal').html("Modal");
           // $('#btn-update').val("edit-fee");
           $('#electionId').val(election.id);
           $('#ajax-election-modal').modal('show');
       })
   });
    $("#electionForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#btn-update').html('<i class="fa fa fa-spinner fa-spin"></i> Starting..');
        var form = $(this);
        var electionIds = $('#electionId').val();
        $.ajax({
            type: "PUT",
            url: '/ajax-start-election/' + electionIds,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                toastr.info(data.message);
                $('#test').trigger("reset");
                $('#ajax-election-modal').modal('hide');
                location.reload();
            }
         });
    });
</script>

<script>
   $('body').on('click', '#stop-election', function () {
       var election = $(this).data('id');
       $.get('/ajax-stop-election/' + election, function (election) {
           $('#electionsModal').html("Modal");
           $('#btn-update').val("edit-fee");
           $('#electionId').val(election.id);
           $('#ajax-stop-election-modal').modal('show');
       })
   });
    $("#electionStopForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#btn-update').html('<i class="fa fa fa-spinner fa-spin"></i> End Election..');
        var form = $(this);
        var electionIds = $('#electionId').val();
        $.ajax({
            type: "PUT",
            url: '/ajax-stop-election/' + electionIds,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                toastr.error("Stop Election!");
                $('#test').trigger("reset");
                $('#ajax-stop-election-modal').modal('hide');
                location.reload();
            }
         });
    });
</script>
@endpush
