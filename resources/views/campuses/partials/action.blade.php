<td>
	<a href="/campuses/{{ $campus->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
	<a href="/campuses/{{ $campus->id }}/show" class="btn btn-warning btn-xs">Show</a>
	<form method="POST" action="/campuses/{{ $campus->id }}" style="display:inline-block;">
		@csrf
		{{ method_field('DELETE') }}
		<button type="submit" class="btn btn-danger btn-xs">Delete</button>
	</form>
</td>