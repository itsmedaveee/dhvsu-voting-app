@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/campuses">Campus</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Campus </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Campus</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/campuses/{{ $campus->id }}">
							@csrf
						 	{{ method_field('PATCH') }}
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label>Name</label>
								<input type="text" class="form-control" name="name" value="{{ $campus->name }}">
								@if ($errors->has('name'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('name') }}</strong>
								    </span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
								<label>Location</label>
								<input type="text" class="form-control" name="location" value="{{ $campus->location }}">
								@if ($errors->has('location'))
								    <span class="help-block">
								        <strong style="color:red;">{{ $errors->first('location') }}</strong>
								    </span>
								@endif
							</div>
					
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div> 
@endsection