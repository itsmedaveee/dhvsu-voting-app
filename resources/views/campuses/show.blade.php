@extends('layouts.master')
@section('content')

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/administrators">Administrators</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Show Campus </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">



		<div class="col-md-12">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Campus</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="admin-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Location</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ $campus->id }}</td>
									<td>{{ $campus->name }}</td>
									<td>{{ $campus->location }}</td>
								</tr>
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>

			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Campus</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="admin-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Firstname</th>
									<th>Lastname</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($campus->students as $student)
								<tr> 
									<td>{{ $student->id }}</td>
									<td>{{ $student->firstname }}</td>
									<td>{{ $student->lastname }}</td>
								</tr>
							@endforeach
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>






@endsection