@extends('layouts.master')
@section('content')

<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>

<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/nominees">Nominees</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Nominee </h1>
				<!-- end page-header -->
				<!-- begin panel -->
 

@if (auth()->user()->isSuperAdmin())
	<div class="col-md-12">
				<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">Nominees list</h4>
							<div class="panel-heading-btn">
								
							</div>
						</div>
						<div class="panel-body">
				 <table class="table table-bordered table-hover" id="myTable">
					<thead>
						<tr>
							<th>ID</th>
							<th>Fullname</th>
							<th>Department</th>
							<th>Course</th>
							 <th>Position</th>
							<th>Election / Event Type</th>
							<th>Event Title</th>
							<th>Participant USC</th>
							<th>Status</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($nominees as $nominee)
						<tr>
							<td>{{ $nominee->id }}</td>
							<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
							<td>{{ $nominee->department->title ?? null}}</td>
							<td>{{ $nominee->course->title }}</td>
								<td> 
								@if ($nominee->position)
                                    {{ $nominee->position->position_name  }}
                              @elseif ($nominee->positionDepartment)
                                    {{ $nominee->positionDepartment->position_name }}
                              @elseif ($nominee->positionCourse)
                                    {{ $nominee->positionCourse->position_name }}
                                   @else 
                                   No Position
                              @endif
                           </td>
							<td>{{ $nominee->categories }}</td>
							
							<td>{{ optional($nominee->pageant)->event_name }}</td>
							<td>{{ $nominee->usc }}</td>
							<td><span class="label label-success"> {{ $nominee->status }}</span></td>
							<td>
								<a href="/nominee/{{ $nominee->id }}/view" class="btn btn-xs btn-primary">View</a>
							</td>
							@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			@endif

		@if (auth()->user()->isCouncil())

			<div class="col-md-12">
				<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">Nominees list</h4>
							<div class="panel-heading-btn">
							</div>
						</div>
						<div class="panel-body">
	<table class="table table-bordered table-hover" id="myTable">
					<thead>
						<tr>
							<th>ID</th>
							<th>Fullname</th>
							<th>Department</th>
							<th>Course</th>
							 <th>Position</th>
							<th>Election / Event Type</th>
							<th>Event Title</th>
							<th>Participant USC</th>
							<th>Status</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($nominees as $nominee)
						<tr>
							<td>{{ $nominee->id }}</td>
							<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>

							<td>{{ $nominee->department->code ?? null}}</td>
							<td>{{ $nominee->course->code }}</td>
								<td> 
								@if ($nominee->position)
                                    {{ $nominee->position->position_name  }}
                              @elseif ($nominee->positionDepartment)
                                    {{ $nominee->positionDepartment->position_name }}
                              @elseif ($nominee->positionCourse)
                                    {{ $nominee->positionCourse->position_name }}
                                   @else 
                                  
                              @endif
                           </td>
							<td>{{ $nominee->categories }}</td>
							<td>{{ $nominee->title }}</td>
							<td>{{ $nominee->usc }}</td>
							<td><span class="label label-success"> {{ $nominee->status }}</span></td>
							<td>
							<a href="/nominee/{{ $nominee->id }}/view" class="btn btn-xs btn-primary">View</a>
							</td>
							@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		@endif


	</div>

 

@endsection

@push('scripts')
{{--  <script type="text/javascript">
    $(document).ready(function () {
	 $('#nominees-table').DataTable({
	    "ordering":'true',
        "order": [0, 'asc'],
        processing: true,
        serverSide: true,
        ajax: '{!! route('nominees.index') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'firstname'},
            { data: 'middlename'},
            { data: 'lastname'},
            { data: 'campus.name'},
            { data: 'department.code'},
             { data: 'categories'},
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ]
	  });
});
</script> --}}
<script type="text/javascript">
	$(document).ready( function () {
	    $('#myTable').DataTable();
	});
</script>
@endpush	



{{-- 
@push('scripts')
  <script type="text/javascript">
      $("select[name='campus']").change(function () {
        
          $("select[name='department']").val('');
          $("select[name='course']").val('');

          $("select[name='department']").prop("disabled", false);
          $("select[name='course']").prop("disabled", false);
      });

 	 $("select[name='department']").change(function () {
          let department = $(this).val();
          let campus = $("select[name='campus']").val();

          $("select[name='course']").prop("disabled", false);

          $.ajax({
              url: "{!! url('/select-campus-department') !!}",
              method: 'GET',
              data: { 
                campus: campus, 
                department: department, 
              },
              success: function(coordinator) {
                  $("select[name='course']").html('');
                  $("select[name='course']").html(course.options);
              }
          }); 
      });
  </script>
@endpush
 --}}