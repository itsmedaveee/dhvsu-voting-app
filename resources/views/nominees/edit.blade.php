@extends('layouts.master')
@section('content')


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/nominees">Nominees</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Nominee </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Edit Nominee</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/nominees/{{ $nominee->id }}"  enctype="multipart/form-data">
							@csrf
							{{ method_field('PATCH') }}
						 <div class="row">
						 	<div class="col-md-12">
								<div class="form-group{{ $errors->has('student_no') ? ' has-error' : '' }}">
									<label>Student No</label>
									<input type="text" class="form-control" name="student_no" value="{{ $nominee->student_no }}">
									@if ($errors->has('student_no'))
									    <span class="help-block">
									        <strong style="color:red;">{{ $errors->first('student_no') }}</strong>
									    </span>
									@endif
								</div>
							</div>


							<div class="col-md-4">

								<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
									<label>First Name</label>
									<input type="text" class="form-control" name="firstname" value="{{ $nominee->firstname }}">
									@if ($errors->has('firstname'))
									    <span class="help-block">
									        <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
									    </span>
									@endif
								</div>
							</div>

							<div class="col-md-4">

								<div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
									<label>Middle Name</label>
									<input type="text" class="form-control" name="middlename" value="{{ $nominee->middlename }}">
									@if ($errors->has('middlename'))
									    <span class="help-block">
									        <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
									    </span>
									@endif
								</div>
							</div>

							<div class="col-md-4">

								<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
									<label>Last Name</label>
									<input type="text" class="form-control" name="lastname" value="{{ $nominee->lastname }}">
									@if ($errors->has('lastname'))
									    <span class="help-block">
									        <strong style="color:red;">{{ $errors->first('lastname') }}</strong>
									    </span>
									@endif
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
									<label>Contact No</label>
									<input type="text" class="form-control" name="contact_no" value="{{ $nominee->contact_no }}">
									@if ($errors->has('contact_no'))
									    <span class="help-block">
									        <strong style="color:red;">{{ $errors->first('contact_no') }}</strong>
									    </span>
									@endif
								</div>
							</div>
							<div class="col-md-4{{ $errors->has('email') ? ' has-error' : '' }}">
								<div class="form-group">
									<label>Email</label>
									<input type="text" class="form-control" name="email" value="{{ $nominee->email }}">
									@if ($errors->has('email'))
									    <span class="help-block">
									        <strong style="color:red;">{{ $errors->first('email') }}</strong>
									    </span>
									@endif
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
									<label>Birthdate</label>
									<input type="date" class="form-control" name="birthdate" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
									@if ($errors->has('birthdate'))
									    <span class="help-block">
									        <strong style="color:red;">{{ $errors->first('birthdate') }}</strong>
									    </span>
									@endif
								</div>
							</div>

						  <div class="col-md-4{{ $errors->has('gender') ? ' has-error' : '' }}">
						  	<label>Gender</label>
							<select class="form-control" id="gender" name="gender">
		                        <option value="" disabled="" selected="">Select Gender</option>
		                        <option value="Male">Male</option>
		                        <option value="Female">Female</option>
	                      	</select>
							@if ($errors->has('gender'))
							    <span class="help-block">
							        <strong style="color:red;">{{ $errors->first('gender') }}</strong>
							    </span>
							@endif
	                      </div>

							<div class="col-md-4">
								<div class="form-group">
									<label>Upload Photo</label>
									<input type="file" class="form-control" name="avatar" value="{{ $nominee->avatar }}">
								</div>
							</div>	

							<div class="col-md-4">
								<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
			                      <label for="name">Select Position:</label>
			                      <select class="form-control" id="position" name="position">
			                        <option value=""  selected="">Select Position</option>
			                        @foreach ($positions as $position )
			                        <option value="{{ $position->id }}">{{ $position->position_name }}</option>
			                        <span class="help-block">
			                            @if ($errors->has('position'))
			                                    <span class="help-block">
			                                        <strong>{{ $errors->first('position') }}</strong>
			                                    </span>
			                                @endif
			                        @endforeach
			                      </select>
			                    </div>
							</div>	
							
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('partylist') ? ' has-error' : '' }}">
			                      <label for="name">Select Partylist:</label>
			                      <select class="form-control" id="partylist" name="partylist">
			                        <option value=""  selected="">Select Partylist</option>
			                        @foreach ($partylists as $partylist )
			                        <option value="{{ $partylist->id }}">{{ $partylist->name }}</option>
			                        <span class="help-block">
			                            @if ($errors->has('partylist'))
			                                    <span class="help-block">
			                                        <strong>{{ $errors->first('partylist') }}</strong>
			                                    </span>
			                                @endif
			                        @endforeach
			                      </select>
			                    </div>
							</div>	
							
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('campus') ? ' has-error' : '' }}">
			                      <label for="name">Select Campus</label>
			                      <select class="form-control" id="campus" name="campuses" >
			                        <option value=""  selected="">Select Campus</option>
			                        @foreach ($campuses as  $campus )
			                        <option value="{{ $campus->id }}">{{ $campus->name }}</option>
			                        <span class="help-block">
			                            @if ($errors->has('campus'))
			                                    <span class="help-block">
			                                        <strong>{{ $errors->first('campus') }}</strong>
			                                    </span>
			                                @endif
			                        @endforeach
			                      </select>
			                    </div>
							</div>	
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
			                      <label for="name">Select Department</label>
			                      <select class="form-control" id="department" name="department" >
			                        <option value="" disabled="" selected="">Select Department</option>
			                        @foreach ($departments as $departmentId => $department )
			                        <option value="{{ $departmentId }}">{{ $department }}</option>
			                        <span class="help-block">
			                            @if ($errors->has('department'))
			                                    <span class="help-block">
			                                        <strong>{{ $errors->first('department') }}</strong>
			                                    </span>
			                                @endif
			                        @endforeach
			                      </select>
			                    </div>
							</div>	

							<div class="col-md-12">
								<label><strong>Educational Background :</strong></label>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label>Elementary</label>
									<input type="text" class="form-control" name="education_background_elementary" value="{{ $nominee->education_background_elementary }}">
								</div>
							</div>				

							<div class="col-md-6">
								<div class="form-group">
									<label>Year</label>
									<input type="text" class="form-control" name="education_background_elementary_year" placeholder="e.g 2014 - 2018" {{ $nominee->education_background_elementary_year }}>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label>Highschool</label>
									<input type="text" class="form-control" name="education_background_highschool" value="{{ $nominee->education_background_highschool }}">
								</div>
							</div>	

							<div class="col-md-6">
								<div class="form-group">
									<label>Year</label>
									<input type="text" class="form-control" name="education_background_highschool_year" placeholder="e.g 2014 - 2018" value="{{ $nominee->education_background_highschool_year }}">
								</div>
							</div>	

							<div class="col-md-6">
								<div class="form-group">
									<label>Senior Highschool</label>
									<input type="text" class="form-control" name="education_background_senior_highschool" value="{{ $nominee->education_background_senior_highschool }}">
								</div>
							</div>	

							<div class="col-md-6">
								<div class="form-group">
									<label>Year</label>
									<input type="text" class="form-control" name="education_background_senior_highschool_year" placeholder="e.g 2014 - 2018" value="{{ $nominee->education_background_senior_highschool_year }}">
								</div>
							</div>	
							
{{-- 
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
							      <label for="name">Select Course</label>
							      <select class="form-control" id="course" name="course" disabled="">
							        <option value="" disabled="" selected="">Select Course</option>
							        @foreach ($courses as  $course )
							        <option value="{{ $course->id }}">{{ $course->title }}</option>
							        <span class="help-block">
							            @if ($errors->has('course'))
							                    <span class="help-block">
							                        <strong>{{ $errors->first('course') }}</strong>
							                    </span>
							                @endif
							        @endforeach
							      </select>
							    </div>
							</div>	
							 --}}
							<div class="col-md-12">

								<div class="form-group">
									<label>Achievments</label>
									<textarea type="text" class="form-control" name="achievements">{{ $nominee->achievements }}</textarea>
								</div>
							</div>

							<div class="col-md-12">

								<div class="form-group">
									<label>About</label>
									<textarea type="text" class="form-control" name="about" >{{ $nominee->about }}</textarea>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			</div>
 


@endsection