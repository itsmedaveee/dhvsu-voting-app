@extends('layouts.master')
@section('content')


<div id="content" class="content content-full-width">
   <div class="profile">
      <div class="profile-header">
         <div class="profile-header-cover " id="particles-js"></div>
         <div class="profile-header-content ">
            {{-- @if (auth()->user()->avatar) --}}
            <div class="profile-header-img">
               {{-- <img src="{{ Storage::url(auth()->user()->avatar) }}" alt="" style="height: 100%" style="width: 100%"> --}}
               <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div>
           {{--  @else
            <div class="profile-header-img">
               <img src="{{ asset('img/user.png') }}" alt="" style="height: 100%" style="width: 100%">
            </div> --}}
           {{--  @endif --}}
            <div class="profile-header-info">
               <h4 class="mt-0 mb-1">{{ auth()->user()->userable->firstname }} {{ auth()->user()->userable->middlename }} {{ auth()->user()->userable->lastname }}</h4>
               <p class="mb-2">{{ ucfirst(auth()->user()->role->name) }}</p>
               <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>
               <div>
               </div>
            </div>
         </div>
         <ul class="profile-header-tab nav nav-tabs" style="background: #4e5c6869" id="hero">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;">&nbsp;<small> </small> <span class="typed"></span>
               </a>
            </li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
            <li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
         </ul>
      </div>
   </div>
</div>


<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/pending-request-file-nominee">Pending Candidacy / Events</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Pending Candidacy File / Events </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">

	<div class="col-md-12">
	<div class="panel panel-inverse">

<div class="panel-heading">
<h4 class="panel-title">
Pending 
<b class="badge d-inline-flex align-items-center p-0 ms-1">
<i class="fa fa-circle fa-fw fs-6px text-blue me-1"></i>
<i class="fa fa-circle fa-fw fs-6px text-cyan me-1"></i>
<i class="fa fa-circle fa-fw fs-6px text-teal"></i>
</b>
</h4>
<div class="panel-heading-btn">
{{-- <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a> --}}
</div>
</div>

<div class="alert alert-primary rounded-0 d-flex align-items-center mb-0 text-blue-900">
<div class="fs-24px w-80px text-center">
<i class="fa fa-lightbulb fa-4x"></i>
</div>
<div class="flex-1 ms-3">
<h4> Notes</h4>
<ul class="ps-3">
<li><strong>Double check the pending request before you approved. </strong></li>

<li><strong>Downloadable Forms </strong></li>
<li><strong>Generating Candidacy Form File PDF</strong></li>
</ul>
</div>
</div>
			<div class="panel-body">
				<table class="table table-bordered table-hover" id="myTable">
					<thead>
						<tr>
							<th>ID</th>
							<th>Fullname</th>{{-- 
							<th>Department</th>
							<th>Course</th> --}}
							 <th>Position</th>
							<th>Election / Event Type</th>
							<th>Election / Event Title</th>
							<th>Participant USC</th>
							<th>Status</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($nominees as $nominee)
						<tr>
							<td>{{ $nominee->id }}</td>
							<td>{{ $nominee->firstname }} {{ $nominee->middlename }} {{ $nominee->lastname }}</td>
						{{-- 	<td>{{ $nominee->department->title ?? null}}</td>
							<td>{{ $nominee->course->title }}</td> --}}
								<td> 
								@if ($nominee->position)
                                    {{ $nominee->position->position_name  }}
                              @elseif ($nominee->positionDepartment)
                                    {{ $nominee->positionDepartment->position_name }}
                              @elseif ($nominee->positionCourse)
                                    {{ $nominee->positionCourse->position_name }}
                                   @else 
                                   No Position
                              @endif
                           </td>
							<td>{{ $nominee->categories }}</td>
							<td>
							@if ($nominee->pageant)
								{{ optional($nominee->pageant)->event_name }}
							@elseif ($nominee->election)
								{{ optional($nominee->election)->election_name }}
							@elseif ($nominee->electionDepartment)
								{{ optional($nominee->electionDepartment)->election_name }}
							@elseif ($nominee->electionCourse)
								{{ optional($nominee->electionCourse)->election_name }}
							@endif
							</td>
							<td>{{ $nominee->usc ?? null }}</td>
							<td>
								<span class="label label-warning">{{ $nominee->status }}</span>
							</td>
							<td>
								 <a href="/request-file-nominee/{{ $nominee->id }}/manage" class="btn btn-xs btn-primary">Manage</a>
							</td>
						</tr>
						@endforeach
				 
					</tbody>
					
				</table>
			</div>
		</div>
	</div>
</div>


</div>
   
@endsection

@push ('scripts')
<script type="text/javascript">
	$(document).ready( function () {
	    $('#myTable').DataTable();
	});
</script>

@endpush
 