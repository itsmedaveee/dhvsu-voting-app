<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNomineesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nominees', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->default(0);
            $table->integer('department_id')->default(0);
            $table->integer('election_id')->nullable();
            $table->integer('election_department_id')->nullable();
            $table->integer('election_course_id')->nullable();
            $table->integer('pageant_id')->nullable();
            $table->integer('campus_id')->default(0); 
            $table->integer('position_id')->nullable();
            $table->integer('position_department_id')->nullable();
            $table->integer('position_course_id')->nullable();
            $table->integer('course_id')->default(0);
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->string('nationality')->nullable();
            $table->string('usc')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('birthdate')->nullable();
            $table->string('gender')->nullable();
            $table->string('religion')->nullable();
            $table->string('categories')->nullable();
            $table->string('title')->nullable();
            $table->string('partyname')->nullable();
            $table->string('certificate_of_registration')->nullable();
            $table->string('validate_school_id')->nullable();
            $table->string('certificate_of_attendance')->nullable();
            $table->string('recent_photo')->nullable();
            $table->string('certified')->nullable();
            $table->string('recomend_letter_from_dean')->nullable();
            $table->string('certificate_of_good_moral')->nullable();
            $table->string('class_of_grade')->nullable();
            $table->string('avatar')->nullable();
            $table->string('reason_for_candidacy')->nullable();
            $table->string('average_last_semister')->nullable();
            $table->string('previous_failed_subject')->nullable();
            $table->string('award_honors_received')->nullable();
            $table->string('extra_curricular_activities')->nullable();
            $table->string('training_seminars')->nullable();
            $table->string('date_filled_out')->nullable();
            $table->string('status_for_win')->nullable();
            $table->string('status')->nullable();
            $table->string('remark')->nullable();
            $table->string('propaganda')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nominees');
    }
}
