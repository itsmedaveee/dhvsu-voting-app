<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('course_id')->default(0);
            $table->unsignedInteger('department_id')->default(0);
            $table->unsignedInteger('campus_id')->default(0);
            $table->unsignedInteger('gender_id')->default(0);
            $table->unsignedInteger('program_id')->default(0);
            $table->string('student_no')->unique();
            $table->string('generate_id')->nullable();
            $table->string('generate_qr_code')->nullable();
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->string('address');
            $table->date('birth_date');
            $table->string('email')->unique();
            $table->string('year');
            $table->string('photo');
            $table->string('section');
            $table->string('contact_no');
            $table->string('nationality')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('religion')->nullable();
            $table->string('image')->nullable();
            $table->string('date_filled_out')->nullable();
            $table->string('approved')->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
