<?php

use Illuminate\Database\Seeder;
use App\ElectionDepartment;
class ElectionsDepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $election = ElectionDepartment::firstOrCreate([
            'user_id'       => 1,
            'department_id' => 1,
            'election_name' => 'Eleksyon 2021',
            'status'        => 'On Going'

        ]);
    }
}
