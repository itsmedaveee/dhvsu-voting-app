<?php

use Illuminate\Database\Seeder;
use App\Gender;
class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $male = Gender::create([
        	'gender_name'	=> 'Male',
        ]);        

        $female = Gender::create([
        	'gender_name'	=> 'Female',
        ]);
    }
}
