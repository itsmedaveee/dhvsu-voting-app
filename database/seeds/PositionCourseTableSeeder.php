<?php

use Illuminate\Database\Seeder;
use App\PositionCourse;
class PositionCourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $president = PositionCourse::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'President',
        ]);              
         $vicePresident = PositionCourse::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Vice President',
        ]);           

         $vicePresident = PositionCourse::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Secretary',
        ]);           
         $treasurer = PositionCourse::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Treasurer',
        ]);              

         $auditor = PositionCourse::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Auditor',
        ]);              

         $businessManager = PositionCourse::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Business Manager',
        ]);            
         $publicInformationOfficer = PositionCourse::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Public Information Officer',
        ]);                

        $GFPS = PositionCourse::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'GFPS Officer',
        ]);       
    }
}
