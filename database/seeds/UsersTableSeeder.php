<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Campus;
use App\Department;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $user = User::create([
            'email'      => 'superadmin@yahoo.com',
            'username'  => 'admin',
            'password'  => bcrypt('123456'),
            'approved'  => 'confirmed'
        ]);

        
       $campus = Campus::firstOrCreate([

            'name'      => 'Dhvsu Bacolor',
            'location'  => 'Bacolor Pampanga',
            
        ]);       

       $department = Department::firstOrCreate([

            'code' => 'CCS',
            'title' => 'College Computing Studies'
            
        ]);

        $role = Role::firstOrCreate([
            'name'  => 'Super Admin',
            'label' => 'super admin',
        ]);



        $user->role()->associate($role)->save();
        $user->campus()->associate($campus)->save();
        $user->department()->associate($department)->save();
        $user->userable()->associate($user)->save();

    }
}
