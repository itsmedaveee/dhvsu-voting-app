<?php

use Illuminate\Database\Seeder;
use App\Position;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $president = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'President',
        ]);         

        $vicePresident = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Vice President',
        ]);         

        $senatorOnRecords = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Senator on Records',
        ]);         

        $senatorOnAudit = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Senator on Audit',
        ]);         

        $senatorOnFinance = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Senator on Finance',
        ]);         

        $senatorOnBusinessAffairs = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Senator on Business Affairs ',
        ]);         

        $senatorOnStudentAffairs = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Senator on Student Affairs ',
        ]);         

        $senatorOnStudentServices = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Senator on Student Services ',
        ]);         

        $senatorOnPublicInformationOfficer = Position::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Senator on Public Information Officer',
        ]); 

    }
}
