<?php

use Illuminate\Database\Seeder;
use App\PositionDepartment;
class PositionsDepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $governor = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Governor',
        ]);              

        $viceGovernor = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Vice Governor',
        ]);            

        $boardMemberOnRecords = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Board Member on Records',
        ]);            
        $boardMemberOnFinance = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Board Member on Finance',
        ]);              
        $boardMemberOnAudit = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Board Member on Audit',
        ]);          
        $boardMemberOnStudent = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Board Member on Student',
        ]);              
        $boardMemberOnStudentAffairs = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Board Member on Student Affairs',
        ]);            
        $boardMemberOnStudentServices = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Board Member on Student Services',
        ]);             
        $boardMemberOnBusinessManager = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Board Member on Business Manager',
        ]);         
        $boardMemberOnPublicInformation = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Board Member on Public Information',
        ]);           
        $bsInformationTechnologyRepresentative = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Bs Information Technology Representative',
        ]);           
        $bsInformationTechnologyRepresentative = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Bs Information Technology Representative',
        ]);            
        $bsComputerEngineeringRepresentative = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Bs Computer Engineering Representative',
        ]);            
        $bsComputerComputerScienceRepresentative = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Bs Computer Science Representative',
        ]);          
        $bsInformationSystemsRepresentative = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Bs Information System Representative',
        ]);              
        $associateComputerTechnology = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Associate in Computer Technology Representative',
        ]);            
        $genderAndDevelopmentFocalPointSystem = PositionDepartment::firstOrCreate([
                'user_id'   => 1,
                'position_name' => 'Gender and Development Focal Point System',
        ]);         
    }
}
