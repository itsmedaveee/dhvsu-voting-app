<?php

use Illuminate\Database\Seeder;
use App\Campus;
class CampusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $campuses = Campus::firstOrCreate([

        //     'name'  => 'Dhvsu Bacolor',
        //     'location'  => 'Bacolor Pampanga'
        // ]);        

        $campuses = Campus::firstOrCreate([

            'name'  => 'Dhvsu Porac',
            'location'  => 'Porac Pampanga'
        ]);

        $campuses = Campus::firstOrCreate([

            'name'  => 'Dhvsu Mexico',
            'location'  => 'Mexico Pampanga'
        ]);       

        $campuses = Campus::firstOrCreate([

            'name'  => 'Dhvsu Sto. Tomas',
            'location'  => 'Sto. Tomas Pampanga'
        ]);        

        $campuses = Campus::firstOrCreate([

            'name'  => 'Dhvsu Lubao',
            'location'  => 'Lubao Pampanga'
        ]);
    }
}
