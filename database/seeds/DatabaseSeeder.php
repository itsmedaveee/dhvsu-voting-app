<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(GenderTableSeeder::class);
         $this->call(DepartmentsTableSeeder::class);
         $this->call(CampusesTableSeeder::class);
         $this->call(ElectionsTableSeeder::class);
         $this->call(ElectionsDepartmentTableSeeder::class);
         $this->call(ElectionsCourseTableSeeder::class);
         $this->call(ElectionsEventTableSeeder::class);
         $this->call(PartylistsTableSeeder::class);
         $this->call(PositionsTableSeeder::class);
         $this->call(PositionsDepartmentTableSeeder::class);
         $this->call(PositionCourseTableSeeder::class);
    }
}
