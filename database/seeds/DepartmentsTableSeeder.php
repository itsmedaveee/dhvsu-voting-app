<?php

use Illuminate\Database\Seeder;

use App\Department;
use App\Course;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ccs = Department::firstOrCreate([

            'code' => 'CCS',
            'title' => 'College Computing Studies'

        ]);

        $cea = Department::firstOrCreate([

            'code' => 'CEA',
            'title' => 'College of Engineering and Architecture '

        ]);

        $coe = Department::firstOrCreate([

            'code' => 'COE',
            'title' => 'College of Education'

        ]);


        $cbs = Department::firstOrCreate([

            'code' => 'CBS',
            'title' => 'College of Business Studies'

        ]);


        $chm = Department::firstOrCreate([

            'code' => 'CHM',
            'title' => 'College of Hospitality Management'

        ]);



        $bsit = Course::firstOrCreate([

            'code' => 'BSIT',
            'title' => 'Bachelor of Science in Information Technology'
        ]);

 
        $bscs = Course::firstOrCreate([

            'code' => 'BSCS',
            'title' => 'Bachelor of Science in Computer Science'

        ]);


        $bscpe = Course::firstOrCreate([

            'code' => 'BSCpE',
            'title' => 'Bachelor of Science in Computer Engineering'

        ]);

 


        $bsis = Course::firstOrCreate([

            'code' => 'BSIS',
            'title' => 'Bachelor of Science in Information System'

        ]);


 

        //CEA



        $bsce = Course::firstOrCreate([

            'code' => 'BSCE',
            'title' => 'Bachelor of in Civil Engineering'
        ]);
 

        $bsece = Course::firstOrCreate([

            'code' => 'BSECE',
            'title' => 'Bachelor of in Electronics and Communication Engineering'
        ]);


 
        $bsee = Course::firstOrCreate([

            'code' => 'BSECE',
            'title' => 'Bachelor of in Electrical Engineering'
        ]);

 

        $bsie = Course::firstOrCreate([

            'code' => 'BSIT',
            'title' => 'Bachelor of in Industrial Technology'
        ]);

 

        //COE


        $bee = Course::firstOrCreate([

            'code' => 'BSEE',
            'title' => 'Bachelor of Science in Elementary Education'

        ]);



 

        $be = Course::firstOrCreate([

            'code' => 'BSE',
            'title' => 'Bachelor of Secondary Education'

        ]);


     

        $btte = Course::firstOrCreate([

            'code' => 'BTTE',
            'title' => 'Bachelor of Technical Teacher Education (LADDERIZED)'

        ]);


    


        //CBS


        $bsba = Course::firstOrCreate([

            'code' => 'BSBA',
            'title' => 'Bachelor of Science in Business Administration'
        ]);


      


        $bse = Course::firstOrCreate([

            'code' => 'BSE',
            'title' => 'Bachelor of Science in Entrepreneurship'
        ]);


        



        //CHM


        $hrm = Course::firstOrCreate([

            'code' => 'BSHRM',
            'title' => 'Bachelor of Science in Hotel and Restaurant Management'
        ]);


        


        $bsit->department()->associate($ccs)->save();
        $bscs->department()->associate($ccs)->save();
        $bscpe->department()->associate($ccs)->save();
        $bsis->department()->associate($ccs)->save();
        $bsce->department()->associate($cea)->save();
        $bsece->department()->associate($cea)->save();
        $bsee->department()->associate($cea)->save();
        $bsie->department()->associate($cea)->save();
        $bee->department()->associate($coe)->save();
        $be->department()->associate($coe)->save();
        $btte->department()->associate($coe)->save();
        $bsba->department()->associate($cbs)->save();
        $bse->department()->associate($cbs)->save();
        $hrm->department()->associate($chm)->save();
    }
}
