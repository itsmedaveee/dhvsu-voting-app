<?php

use Illuminate\Database\Seeder;
use App\ElectionCourse;
class ElectionsCourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $election = ElectionCourse::firstOrCreate([
            'user_id'       => 1,
            'election_name' => 'Eleksyon 2021',
            'status'        => 'On Going'

        ]);
    }
}
