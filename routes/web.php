<?php

use Illuminate\Support\Facades\Route;
 
 Route::get('/test', function () {
 	return view('emails.index');
 });
 	Route::get('/reports', 'ReportsController@index');
 	Route::get('/nominees-pdf/report', 'ReportsController@nominees')->name('nominees-pdf/report');


 Route::get('/nominee/{nominee}/view', 'NomineesController@showDetails');
 Route::get('/print-results/{electionId}', 'ElectionController@printResults');
 Route::get('/print-results-pageant/{pageant}', 'PageantsController@printResults');
 Route::get('/print-results-course/{election}', 'ElectionCoursesController@printResults');
 Route::get('/print-results-department/{election}', 'ElectionDepartmentController@printResults');
Route::get('/qr-students/{student}/details/{election}', 'PendingStudentsController@view')->name('qr-students');
//Route::get('students/{student}/levels/{level}/subjects', 'StudSubjectController@show')->name('students.levels.subjects');
Route::get('/settings', 'SettingsController@index');
Route::patch('/settings', 'SettingsController@update');

Route::get('/', function () {
    return view('auth.login');
});

// Route::get('/generate-pdf/{nominee}', 'PendingRequestCocController@pdfTest');
Route::get('/generate-pdf/{nominee}', 'PDFGeneratorsController@index');
	Route::patch('/pending-students/{student}/approved', 'PendingStudentsController@approved');

	//ELECTION DEPARTMENT AJAX
	Route::put('/ajax-start-election-department/{electionIds}', 'ElectionDepartmentController@start');
	Route::get('ajax-election-department/{election}', 'ElectionDepartmentController@ajaxElection');
	Route::get('/ajax-stop-election-department/{election}', 'ElectionDepartmentController@getElection');
	Route::put('/ajax-stop-election-department/{electionIds}', 'ElectionDepartmentController@stop');

	//Registration Form Set 
	Route::put('/ajax-close-registration/{registrationId}', 'RegistersController@close');
	Route::get('lock-in-period/{registrationId}', 'RegistersController@ajaxClose');

	//Year Level
	Route::get('/year-level', 'YearLevelController@index');

	Route::get('/program', 'ProgramController@index');
	Route::post('/programs', 'ProgramController@store');
	Route::delete('/program/{program}', 'ProgramController@destroy');

	//Reports


	//ELECTION COURSE AJAX
	Route::get('/ajax-election-course/{election}', 'ElectionCoursesController@ajaxElection');
	Route::put('/ajax-start-election-course/{electionIds}', 'ElectionCoursesController@start');
	Route::get('/ajax-stop-election-course/{election}', 'ElectionCoursesController@getElection');
	Route::put('/ajax-stop-election-course/{electionIds}', 'ElectionCoursesController@stop');
	Route::get('/election-course/{election}/show-results', 'ElectionCoursesController@show');
	//ELECTION CAMPUSES AJAX
	Route::put('/ajax-stop-election/{electionIds}', 'ElectionController@stopElection');
	Route::get('ajax-election/{election}', 'ElectionController@ajaxElection');
	Route::put('/ajax-start-election/{electionIds}', 'ElectionController@startElection');
	Route::get('/ajax-stop-election/{election}', 'ElectionController@getElection');
	Route::put('/ajax-stop-election/{electionIds}', 'ElectionController@stopElection');

	//PAGEANT 
	Route::put('/ajax-stop-pageant/{pageantIds}', 'PageantsController@stopPageant');
	Route::get('ajax-pageant/{pageant}', 'PageantsController@ajaxPageant');
	Route::put('/ajax-start-pageant/{pageantIds}', 'PageantsController@startPageant');
	Route::get('ajax-stop-pageant/{pageant}', 'PageantsController@getPageant');
	Route::put('/ajax-stop-pageant/{pageantIds}', 'PageantsController@stopPageantEvent');
	Route::get('/print-results-pageant/{pageant}', 'PageantsController@showResults');
// Route::get('select-campus-department', 'NomineesController@select');


Route::get('/nominee/{nominee}/download', 'PendingRequestCocController@downloadCOR');
Route::get('/nominee/{nominee}/download', 'PendingRequestCocController@downloadValidSchoolId');
Route::get('/nominee/{nominee}/download', 'PendingRequestCocController@downloadRecentPhoto');
Route::get('/nominee/{nominee}/download', 'PendingRequestCocController@downloadCertificateAttendance');
Route::get('/nominee/{nominee}/download', 'PendingRequestCocController@downloadRecomendLetter');
Route::get('/nominee/{nominee}/download', 'PendingRequestCocController@downloadCertificateOfGoodMoral');
Route::get('/nominee/{nominee}/download', 'PendingRequestCocController@downloadTrainingSeminars');

Route::delete('/elections/{election}/remove', 'ElectionController@removed');

Route::delete('/election-department/{election}/remove', 'ElectionDepartmentController@removed');

Route::get('/student/{student}/download', 'StudentsController@download');
Route::get('/registration-from', 'RegistrationsController@index');
Route::post('/registration-from', 'RegistrationsController@store');

Route::get('select-course', 'RegistrationsController@select');
Route::get('select-program', 'RegistrationsController@selectProgram');


// Route::post('select-course', 'RegistrationsController@selectCourse');
// Route::get('select-program', 'RegistrationsController@selectProgram');



Route::group(['middleware' => 'prevent-back-history'], function() {
	Route::get('/', function () {
		if (auth()->check() && auth()->user()->role->name == 'Super Admin') 
		{
		$departments = \App\Election::with('electionCampuses')->withCount('electionCampuses')->get();
        $elections  = \App\Election::with('electionCampuses')->get();
        $result = [];
        $dept = [];
        foreach ($departments as $department) {
            //$dept[] = $department->load('nominees.resultsDepartment')->pluck('code','id');
            //$result[] = $department->nominees->count();
            $test[] = $department->load('nominees')->loadCount('electionCampuses');
            $result[] = $department->election_campuses_count;
            $dept[] = $department->election_name;
           // $result[] = $data->count();
        }

        $courses = \App\ElectionDepartment::with(['electionDepartments'])->withCount('electionDepartments')->get();

        $resultDepartment = [];
        $deptCourse = [];
        foreach ($courses as $course) {
            $test1[] = $course->load('nominees')->loadCount('electionDepartments');
            $resultDepartment[] = $course->election_departments_count;
            $deptCourse[] = $course->election_name;
        }

        $electionCourses = \App\ElectionCourse::with('electionCourses')->withCount('electionCourses')->get();
        $resultCourse = [];
        $electionCourseName = [];
        foreach ($electionCourses as $electionCourse) {
            $query[] = $electionCourse->load('nominees')->loadCount('electionCourses');
            $resultCourse[] = $electionCourse->election_courses_count;
            $electionCourseName[] = $electionCourse->election_name; 
        }

        $events = \App\Pageant::with(['nominees.resultsPageant'])->withCount('pageantEventResults')->get();
        $resultEvent = [];
        $eventsPageant = [];
        foreach ($events as $event) {
            $test2[] = $event->load('nominees')->loadCount('pageantEventResults');
            $resultEvent[] = $event->pageant_event_results_count;
            $eventsPageant[] = $event->event_name;
        }

        return view('home', compact('elections'))->with('resultEvent',json_encode($resultEvent,JSON_NUMERIC_CHECK))
                                                 ->with('resultCourse',json_encode($resultCourse,JSON_NUMERIC_CHECK))
                                                 ->with('electionCourseName',json_encode($electionCourseName,JSON_NUMERIC_CHECK))
                                                 ->with('eventsPageant',json_encode($eventsPageant,JSON_NUMERIC_CHECK))
                                                 ->with('deptCourse',json_encode($deptCourse,JSON_NUMERIC_CHECK))
                                                 ->with('resultDepartment',json_encode($resultDepartment,JSON_NUMERIC_CHECK))
                                                 ->with('dept',json_encode($dept,JSON_NUMERIC_CHECK))->with('result',json_encode($result,JSON_NUMERIC_CHECK));
			}

			if (auth()->check() && auth()->user()->role->name == 'Student')
			{
				 $student = auth()->user()->load('announcements');
				return view('student-dashboard.index', compact('student'));
			}		
			if (auth()->check() && auth()->user()->role->name == 'Council')
			{
			$departments = \App\Election::with('electionCampuses')->withCount('electionCampuses')->get();
            $elections  = \App\Election::with('electionCampuses')->get();
            $result = [];
            $dept = [];
            foreach ($departments as $department) {
                //$dept[] = $department->load('nominees.resultsDepartment')->pluck('code','id');
                //$result[] = $department->nominees->count();
                $test[] = $department->load('nominees')->loadCount('electionCampuses');
                $result[] = $department->election_campuses_count;
                $dept[] = $department->election_name;
               // $result[] = $data->count();
            }

            $courses = \App\ElectionDepartment::with(['electionDepartments'])->withCount('electionDepartments')->get();

            $resultDepartment = [];
            $deptCourse = [];
            foreach ($courses as $course) {
                $test1[] = $course->load('nominees')->loadCount('electionDepartments');
                $resultDepartment[] = $course->election_departments_count;
                $deptCourse[] = $course->election_name;
            }

            $electionCourses = \App\ElectionCourse::with('electionCourses')->withCount('electionCourses')->get();
            $resultCourse = [];
            $electionCourseName = [];
            foreach ($electionCourses as $electionCourse) {
                $query[] = $electionCourse->load('nominees')->loadCount('electionCourses');
                $resultCourse[] = $electionCourse->election_courses_count;
                $electionCourseName[] = $electionCourse->election_name; 
            }
            $events = \App\Pageant::with(['nominees.resultsPageant'])->withCount('pageantEventResults')->get();
            $resultEvent = [];
            $eventsPageant = [];
            foreach ($events as $event) {
                $test2[] = $event->load('nominees')->loadCount('pageantEventResults');
                $resultEvent[] = $event->pageant_event_results_count;
                $eventsPageant[] = $event->event_name;
            }

        return view('council-dashboard.index', compact('elections'))->with('resultEvent',json_encode($resultEvent,JSON_NUMERIC_CHECK))
                                                 ->with('resultCourse',json_encode($resultCourse,JSON_NUMERIC_CHECK))
                                                 ->with('electionCourseName',json_encode($electionCourseName,JSON_NUMERIC_CHECK))
                                                 ->with('eventsPageant',json_encode($eventsPageant,JSON_NUMERIC_CHECK))
                                                 ->with('deptCourse',json_encode($deptCourse,JSON_NUMERIC_CHECK))
                                                 ->with('resultDepartment',json_encode($resultDepartment,JSON_NUMERIC_CHECK))
                                                 ->with('dept',json_encode($dept,JSON_NUMERIC_CHECK))->with('result',json_encode($result,JSON_NUMERIC_CHECK));
			}

	    return view('auth.login');
	});	

Auth::routes();
	Route::group(['middleware' => 'auth'],function(){

	 	Route::middleware('role:Super Admin')->group(function (){
			//Super Admin
			Route::get('/home', 'HomeController@index')->name('home');
			Route::get('/super-admin', 'SuperAdminController@index');
			Route::post('/super-admin', 'SuperAdminController@store');
			Route::get('/super-admin/{admin}/edit', 'SuperAdminController@edit');
			Route::patch('/super-admin/{admin}', 'SuperAdminController@update');

			// Faculties

			Route::get('/councils' , 'CouncilController@index');
			Route::post('/councils' , 'CouncilController@store');
			Route::get('/councils/{council}/edit' , 'CouncilController@edit');
			Route::patch('/councils/{council}' , 'CouncilController@update');

			//Lock Form
			Route::get('/lock-in-period', 'RegistersController@index');
			Route::post('/add-date-regisrations', 'RegistersController@store');

			//GENDERS

			Route::resource('/genders', 'GenderController');

			//Campuses

			Route::get('/campuses', 'CampusController@index')->name('campuses.index');
			Route::post('/campuses', 'CampusController@store');
			Route::get('/campuses/{campus}/edit', 'CampusController@edit');
			Route::patch('/campuses/{campus}', 'CampusController@update');
			Route::delete('/campuses/{campus}', 'CampusController@destroy');
			Route::get('/campuses/{campus}/show', 'CampusController@show');

			 
			 //Department 

			Route::get('/departments', 'DepartmentsController@index')->name('departments.index');
			Route::post('/departments', 'DepartmentsController@store');
			Route::get('/departments/{department}/show', 'DepartmentsController@show');
			Route::get('/departments/{department}/edit', 'DepartmentsController@edit');
			Route::patch('/departments/{department}', 'DepartmentsController@update');
			Route::delete('/departments/{department}', 'DepartmentsController@destroy');

			//Courses
			Route::get('/courses', 'CoursesController@index')->name('courses.index');
			Route::post('/courses', 'CoursesController@store');
			Route::get('/courses/{course}/edit', 'CoursesController@edit');
			Route::patch('/courses/{course}', 'CoursesController@update');
			Route::delete('/courses/{course}', 'CoursesController@destroy');




		});


	 	//Faculty Dashboard

		Route::middleware('role:Council')->group(function (){

			Route::get('/council/home', 'CouncilDashboardController@index');


		});


		Route::middleware('role:Council,Super Admin')->group(function (){

			//Announcements

			Route::get('/announcements', 'AnnouncementsController@index');
			Route::post('/announcements', 'AnnouncementsController@store');

			//Pageants

			Route::get('/pageants', 'PageantsController@index');
			Route::get('/pageants/{pageant}/show-results', 'PageantsController@show');
			Route::post('/pageants', 'PageantsController@store');
			Route::get('/pageants/{pageant}/edit', 'PageantsController@edit');
			Route::patch('/pageants/{pageant}', 'PageantsController@update');


	
			//Partylist

			Route::get('/partylists', 'PartylistController@index')->name('partylists.index');
			Route::post('/partylists', 'PartylistController@store');
			Route::get('/partylists/{partylist}/edit', 'PartylistController@edit');
			Route::patch('/partylists/{partylist}', 'PartylistController@update');
			Route::delete('/partylists/{partylist}', 'PartylistController@destroy');


			//Postions
			Route::get('/positions', 'PositionsController@index')->name('positions.index');
			Route::post('/positions', 'PositionsController@store');
			Route::get('/positions/{position}/edit', 'PositionsController@edit');
			Route::patch('/positions/{position}', 'PositionsController@update');
			Route::delete('/positions/{position}', 'PositionsController@destroy');

			//Nominees

			Route::get('/nominees', 'NomineesController@index')->name('nominees.index');
			// Route::post('/nominees', 'NomineesController@store');
			Route::get('/nominees/{nominee}/show', 'NomineesController@show');
			// Route::get('/nominees/{nominee}/edit', 'NomineesController@edit');

			// Route::patch('/nominees/{nominee}', 'NomineesController@update');

			Route::delete('/nominees/{nominee}', 'NomineesController@destroy');

			//Students Pending

			Route::get('/pending-students', 'PendingStudentsController@pending')->name('pending-students.index');
			Route::get('/pending-students/{student}/manage', 'PendingStudentsController@manage');
		
			Route::delete('/pending-students/{student}/remove', 'PendingStudentsController@remove');

			//Student Approved lists

			Route::get('/students', 'StudentsController@index')->name('students.index');
			Route::get('/students/{student}/show', 'StudentsController@show');


			Route::get('/elections', 'ElectionController@index');
			Route::post('/elections', 'ElectionController@store');
			Route::get('/elections/{election}/edit', 'ElectionController@edit');
			Route::patch('/elections/{election}', 'ElectionController@update');
			Route::get('/elections/{election}/show-results', 'ElectionController@show');

			Route::patch('/start-election/{election}', 'ElectionController@startElection');
			

			Route::get('/pending-request-file-nominee' , 'PendingRequestCocController@pending')->name('pending-request-file-nominee.index');
			Route::get('/request-file-nominee/{nominee}/manage' , 'PendingRequestCocController@manage');
			Route::patch('/approved-request-file-nominee/{nominee}' , 'PendingRequestCocController@approved');
			Route::delete('/removed-request-file-nominee/{nominee}' , 'PendingRequestCocController@removed');
			Route::patch('/disqualified-request-file-nominee/{nominee}' , 'PendingRequestCocController@disqualified');

			//Events

			Route::get('/position-department', 'PositionDepartmentsController@index');
			Route::post('/position-department', 'PositionDepartmentsController@store');
			Route::get('/position-department/{position}/edit', 'PositionDepartmentsController@edit');
			Route::patch('/position-department/{position}', 'PositionDepartmentsController@update');
			Route::delete('/position-department/{position}', 'PositionDepartmentsController@destroy');

			//Election Department
			Route::get('/election-department', 'ElectionDepartmentController@index');
			Route::post('/election-department', 'ElectionDepartmentController@store');
			Route::get('/election-department/{election}/edit', 'ElectionDepartmentController@edit');
			Route::patch('/election-department/{election}', 'ElectionDepartmentController@update');
			Route::delete('/election-department/{election}', 'ElectionDepartmentController@destroy');

			Route::get('/election-department/{election}/show-results', 'ElectionDepartmentController@show');

			//Election for Courses
			Route::get('/election-course', 'ElectionCoursesController@index');
			Route::post('/election-course', 'ElectionCoursesController@store');
			Route::get('/election-course/{election}/edit', 'ElectionCoursesController@edit');
			Route::patch('/election-course/{election}', 'ElectionCoursesController@update');
			//Position Department
			Route::get('/position-course', 'PositionCourseController@index');
			Route::post('/position-course', 'PositionCourseController@store');
			Route::get('/position-course/{position}/edit', 'PositionCourseController@edit');
			Route::patch('/position-course/{position}', 'PositionCourseController@update');
			Route::delete('/position-course/{position}', 'PositionCourseController@destroy');


		});

		
		Route::get('/announcements/{announcement}', 'AnnouncementsController@show');
		Route::get('/student/home', 'StudentDashboardController@index');
		Route::get('/candidates',  'CandidatesController@index');
		Route::get('/candidates/{nominee}/view',  'CandidatesController@show');


		Route::get('/candidates-by-dept', 'CandidatesController@getByCandidatesDepartment');
		Route::get('/candidates-by-dept/{nominee}/view', 'CandidatesController@candidatesDepartmentShow');

		Route::get('/candidates-by-courses', 'CandidatesController@candidatesCourses');
		Route::get('/candidates-course/{nominee}/view', 'CandidatesController@candidatesCourseShow');

		Route::get('/candidates-pageant', 'CandidatesController@pageant');
		Route::get('/candidates-pageant/{nominee}/view', 'CandidatesController@showPageant');


		Route::post('/votes', 'VotesController@store');
		Route::post('/votes-department', 'VotesController@votesDepartment');
		Route::post('/votes-courses', 'VotesController@votesCourse');
		Route::post('/votes-for-pageant', 'VotesController@votesPageant');

		Route::get('/request-file-coc', 'RequestFileCocController@index');
		Route::post('/request-file-coc', 'RequestFileCocController@store');

		Route::get('/file-coc', 'RequestFileCocController@fileCoc');
		Route::get('/file-coc/{nominee}/show', 'RequestFileCocController@show');

		Route::get('/results', 'ResultsController@index');
		Route::get('/results-department', 'ResultDepartmentsController@index');
		Route::get('/results-courses', 'ResultCoursesController@index');

		Route::get('/results-pageant', 'ResultsPageantController@index');

		//Notifications
		Route::get('/notification/{notif}', 'ElectionNotificationsController@show');
		Route::get('/notification-for-campuses/{campus}', 'ElectionNotificationsController@show');
		Route::get('/view-all-notifications', 'ElectionNotificationsController@index');

	});
});
 